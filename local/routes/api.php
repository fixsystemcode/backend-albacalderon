<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Api\Login@login');
Route::post('register', 'Api\Login@register');
Route::post('loginAuth', 'Api\Login@handleProviderCallback');

Route::get('informacion', 'Api\InformacionController@informacion');
Route::get('slider', 'Api\InformacionController@slider');
Route::get('categoria-productos', 'Api\InformacionController@categoriaProductos');
Route::get('categoria-productos/{id}', 'Api\InformacionController@categoriaProducto');
Route::get('productos', 'Api\InformacionController@productos');
Route::get('producto/{id}', 'Api\InformacionController@producto');
Route::get('comentarios', 'Api\InformacionController@comentarios');
Route::get('noticias', 'Api\InformacionController@noticias');
Route::get('articulo/{id}', 'Api\InformacionController@articulo');
Route::get('articulo-slug/{slug}', 'Api\InformacionController@articuloSlug');
Route::get('visitas-producto/{id}', 'Api\InformacionController@visitasProducto');
Route::get('top-producto-visitas/{top?}', 'Api\InformacionController@topVisitas');
Route::post('guardar-visita-producto', 'Api\InformacionController@guardarVisitaProducto');
Route::post('enviar-mensaje', 'Api\InformacionController@enviarmensaje');

/**/
Route::get('noticiaslimit/{limit}', 'Api\InformacionController@noticiaslimit');
Route::get('ultimo-articulo-categoria/{id}', 'Api\InformacionController@ultimocategoria');
Route::get('ultimo-categoria-interna/{id}', 'Api\InformacionController@ultimocategoriainterna');


Route::get('articuloscategoria/{cat}', 'Api\InformacionController@articuloscategoria');
/**/



Route::middleware('auth:cliente-api')->get('/cliente', function (Request $request) {
    return $request->user();
});

Route::get('perfil/{id}' , 'Api\ClienteController@miPerfil')->middleware('auth:cliente-api');
Route::post('actualizar-perfil' , 'Api\ClienteController@guardarPerfil')->middleware('auth:cliente-api');
Route::get('vehiculo/{id}' , 'Api\ClienteController@mostrarVehiculo')->middleware('auth:cliente-api');
Route::post('vehiculo' , 'Api\ClienteController@guardarVehiculo')->middleware('auth:cliente-api');
Route::post('vehiculo/{id}' , 'Api\ClienteController@eliminarVehiculo')->middleware('auth:cliente-api');
//Route::get('mis-pedidos' , 'Api\ClienteController')->middleware('auth:api');

Route::get('carrito', 'Api\CartController@miCarrito')->middleware('auth:cliente-api');
Route::post('saveCarrito', 'Api\CartController@saveCard')->middleware('auth:cliente-api');
Route::get('formPago/{id_pedido}/{id_cliente}', 'Api\CartController@formPago')->middleware('auth:cliente-api');
Route::post('confirmar-pago', 'Api\CartController@formSave')->middleware('auth:cliente-api');
Route::get('pagar-pedido/{id}', 'Api\CartController@prepararPago')->middleware('auth:cliente-api');
Route::get('estado-pago/{code}/{orden}/{id_trasaccion}', 'Api\CartController@estadoPago')->middleware('auth:cliente-api');
Route::get('mis-pedidos/{id_cliente}', 'Api\ClienteController@misPedidos')->middleware('auth:cliente-api');
Route::post('cancelar-pedido', 'Api\ClienteController@cancelarPedido')->middleware('auth:cliente-api');
Route::get('saldo/{id_cliente}', 'Api\ClienteController@saldoPedido')->middleware('auth:cliente-api');