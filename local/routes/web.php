<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Rutas Frontend */ 
// Rutas de las paginas Informativas
	Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
	//Route::get('/', 'Frontend\PaginasController@index')->name('inicio');
	Route::get('/categoria/{id}', 'Frontend\PaginasController@categoria');
	Route::get('/producto/{id}', 'Frontend\PaginasController@producto');
	Route::post('/mostrardatosproducto',	'Frontend\PaginasController@mostrardatosproducto')->name('producto.mostrardatosproducto');
	Route::get('/quienes-somos', 'Frontend\PaginasController@empresa');
	Route::get('/nuestros-clientes', 'Frontend\PaginasController@nuestrosClientes');
	Route::get('/servicios', 'Frontend\PaginasController@servicios');
	Route::get('/productos', 'Frontend\PaginasController@productos');
	Route::get('/noticias', 'Frontend\PaginasController@noticias');
	Route::get('/noticias/{slug}', 'Frontend\PaginasController@articulo');
	Route::get('/certificaciones', 'Frontend\PaginasController@certificaciones');
	Route::get('/contactos', 'Frontend\PaginasController@contactos');
	Route::get('/productos/{id}', 'Frontend\PaginasController@producto');
	Route::post('/enviarmensaje',	'Frontend\PaginasController@enviarmensaje')->name('paginas.enviarmensaje');
// Fin de las rutas de las paginas informativas

// Carrito de compra
	Route::resource('cart', 'Frontend\CartsController');
	Route::post('cart/addProducto/{id}', 'Frontend\CartsController@addProducto');
	Route::post('cart/guardarPedido', 'Frontend\CartsController@guardarPedido');
	Route::post('cart/ingresar-pago', 'Frontend\CartsController@pagos');
	Route::post('save-deposito', 'Frontend\CartsController@subirDocumento');
// Fin carrito

// Lista de deseados
	Route::resource('wishlist', 'Frontend\WishlistController');
// Fin lista de deseados

// Rutas para los Cliente
	Route::prefix('cliente')->group(function () {

// Rutas de login de los clientes 
	Route::get('login', 'Auth\ClienteLoginController@login')->name('cliente.auth.login');
	Route::post('login', 'Auth\ClienteLoginController@loginCliente')->name('cliente.auth.loginCliente');
	Route::post('logout', 'Auth\ClienteLoginController@logout')->name('cliente.auth.logout');

	Route::get('redirect/{provider}', 'Auth\ClienteLoginController@redirectToProvider')->name('loginSocial');
	Route::get('callback/{provider}', 'Auth\ClienteLoginController@handleProviderCallback');
// Fin de rutas de login clientes

// Panel de adminstraccion del cliente
	Route::get('/', 'Frontend\ClienteController@index')->name('cliente.dashboard');
	Route::get('dashboard', 'Frontend\ClienteController@index')->name('cliente.dashboard');
	Route::get('mis-pedidos', 'Frontend\ClienteController@misPedidos')->name('cliente.misPedidos');
	Route::post('cancelar-pedido', 'Frontend\ClienteController@cancelarPedido');
	Route::get('ofertas', 'Frontend\ClienteController@ofertas')->name('cliente.getOfertas');
	Route::get('deseados', 'Frontend\ClienteController@deseados')->name('cliente.misDeseos');
	Route::get('miperfil', 'Frontend\ClienteController@miPerfil')->name('cliente.miPerfil');
	Route::post('actualizar-perfil', 'Frontend\ClienteController@guardarPerfil');
	Route::post('save-direccion', 'Frontend\ClienteController@guardarDireccion');
	Route::get('show-direccion/{id}', 'Frontend\ClienteController@mostrarDireccion');
	Route::get('delete-direccion/{id}', 'Frontend\ClienteController@eliminarDireccion');
	Route::get('register', 'Frontend\ClienteController@create')->name('cliente.register');
	Route::post('register', 'Frontend\ClienteController@store')->name('cliente.register.store');
// Fin del panel de adminstracion del cliente

// Rutas para el pago de compra  
	Route::get('saveCard', 'Frontend\CartsController@saveCard')->name('saveCard');
	Route::get('checkout/{id}', 'Frontend\CartsController@formPago')->name('checkout');
	Route::post('checkout-save', 'Frontend\CartsController@formSave')->name('checkoutSave');
	Route::get('pagar-pedido/{id}', 'Frontend\CartsController@pagar');
	Route::get('estado-pago/{code}/{orden}', 'Frontend\CartsController@estadoPago');
	Route::get('getcupon/{id}/{cupon}','Frontend\CartsController@agregarCupon');
// Fin del carrito de comprar

// Rutas para recuperar contraseña
	Route::get('password/reset', 'Auth\ClienteForgotPasswordController@showLinkRequestForm')->name('cliente.password.request');
	Route::post('password/email', 'Auth\ClienteForgotPasswordController@sendResetLinkEmail')->name('cliente.password.email');

});
// Fin de rutas clientes
/* Fin Rutas Frontend */



/* Inicio Backend */

// Rutas de login Administrador
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

// Password Reset Routes Administrador
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => ['auth']], function(){

	Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

	// Rutas inicialiales
	Route::get('/home', 'Backend\HomeController@index')->name('home');
	Route::get('/administracion', 'Backend\BackendController@index');
	// fin de las rutas iniciales

	// Inicio de adminsitracion de usuaruios
	Route::resource('usuarios','Backend\UsuariosController');
	Route::get('/perfil', 'Backend\UsuariosController@perfil')->name('perfil');
	Route::resource('tipos-usuarios','Backend\TiposController');
	Route::resource('menu', 'Backend\MenuController');
	Route::resource('permisos', 'Backend\PermisoController');
	// Fin de administracion de usuarios 

	// Inicio de administracion de clientes
	Route::resource('clientes', 'Backend\ClientesController');
	Route::get('saldo-cliente', 'Backend\ClientesController@saldoCliente');
	Route::resource('menu-pagina', 'Backend\MenuPaginaController');
	// fin administracion de clientes

	// Inicio  de administracion de contenido
	Route::resource('articulos', 'Backend\ArticulosController');
	Route::post('/upload', 'Backend\ArticulosController@subirfotogaleria');
	Route::post('/uploaddoc', 'Backend\ArticulosController@subirdoc');
	Route::get('/activaciones','Backend\ArticulosController@activaciones');
	Route::post('/activararticulo/{id}', 'Backend\ArticulosController@habilitArarticulo')->name('hab_articulo');
	Route::resource('categorias','Backend\CategoriasController');
	Route::resource('tags', 'Backend\TagsController');
	Route::post('/getTags/{id}', 'Backend\TagsController@getTagsByCategoria');
	// Fin de adminsitracion de contenido

	// Administracion de carrito
		Route::resource('carrito', 'Backend\CarritoController');
		Route::post('carrito/save-politica', 'Backend\CarritoController@subirDocumento');
		Route::resource('cupones','Backend\CuponController');
		Route::resource('pedidos', 'Backend\PedidosController');
		Route::resource('depositos', 'Backend\DepositosController');
	// Fin administracion de carrito

	// Administracion Productos
		Route::resource('shop','Backend\TiendaController');
		Route::post('/upfoto', 'Backend\TiendaController@subirfotogaleria');
		Route::resource('categoriaproductos','Backend\CategoriaproductosController');
		Route::post('/getCproductos/{id}', 'Backend\CategoriaproductosController@getCproductos');
	// Fin administracion de productos	

	//Inicio Modulos
		Route::resource('categoriainterna','Backend\CategoriaInternaController');
		Route::resource('animacion', 'Backend\SliderController');
		Route::resource('imagen', 'Backend\ImagenesController');
		Route::resource('video', 'Backend\VideoController');
	//Fin Modulos

	// Rutas Administracion
		Route::resource('/informacion', 'Backend\InformacionController');
		Route::resource('configuracion', 'Backend\ConfiguracionController');
	// Fin Rutas administracion

	// Inicio de campos extras
		Route::resource('camposextrasgrupos','Backend\CamposExtrasGruposController');
		Route::resource('camposextras','Backend\CamposExtrasController');
	// Fin Campos extras 

	// Inicio cursos
		Route::resource('cursos', 'Backend\CursosController');
		Route::post('cursos/filtrar', 'Backend\CursosController@filtrar');
		Route::get('modulos-articulo/{id}', 'Backend\CursosController@ModulosArticulo');

		Route::resource('modulo', 'Backend\ModuloController');
	// Fin cursos 

	// Inicio documentos
		Route::post('upload-image', 'UploadController@upload_image');
		Route::post('upload-file', 'UploadController@upload_file');
	// Fin documentos

	// Rutas que no se usan
	Route::resource('galeria', 'FotosController');
	Route::resource('especificaciones', 'EspecificacionController');
	//Route::post('/enviarconsulta',	'Frontend\PaginasController@enviarconsulta')->name('paginas.enviarconsulta');
	//Route::get('/etiqueta/{tag}',	'Frontend\PaginasController@tags')->name('Tags');
	//Route::get('/galeria/{slug}',	'Frontend\PaginasController@galeria')->name('galeria');
	//Route::get('/infoevento', 		'Frontend\PaginasController@infoeventos');
	//Route::get('/proyectos/{slug}', 'Frontend\PaginasController@articulo')->name('proyectos.slug');
	//Route::get('/email', 			'Frontend\PaginasController@email')->name('email');
	//Route::get('/apinoticia',		'ApiNoticiasController@api')->name('apinoticia');
	//Antiguo FUNCIONA Route::get('/noticia', 'ApiNoticiasController@noticia')->name('noticia');	

});