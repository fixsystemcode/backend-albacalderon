@extends('layouts.principal')

@section('contenido')
    @include('menu.header_principal')

@section('scripts')
<script>
	$(document).ready(function(){
		$('#commentcarousel').owlCarousel({
		    loop:true,
		    margin:10,
		    dots: true,
		    autoplay: true,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		});
		$(window).bind('scroll', function() {
            var distance = 100;
            if ($(window).scrollTop() > distance) {
                $('.nav_inferior').addClass('scrolled');
                $('body').css('padding-top', '56px');
             }else {
                $('.nav_inferior').removeClass('scrolled');
                $('body').css('padding-top', '0px');
             }
        });
	})
</script>
@endsection
<section class="s_colecction back_white" style="padding-top: 20px">
        <div class="container">
            <div class="s_title">
                <h2>Diseño y Desarrollo Web Manta - Ecuador</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/patronato.png') }}" alt="Card image cap">
                            <br>
                            <p>Patronato Municipal de Manta</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/centrotmo.png') }}" alt="Card image cap">
                            <br>
                            <p>Centro TMO</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/bow-sa-fixsystem-diseno-web-profesional-ecuador.jpg') }}" alt="Card image cap">
                            <br>
                            <p>BOW S.A</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/cambrdge-fixsystem-diseno-web-profesional-ecuador.jpg') }}" alt="Card image cap">
                            <br>
                            <p>Cambridge Ecuador</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/costa-mar-fr-fixsystem-com.png') }}" alt="Card image cap">
                            <br>
                            <p>Costamar FM</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/galpig-fixsystem-diseno-web-profesional-ecuador.jpg') }}" alt="Card image cap">
                            <br>
                            <p>Supermercado Galpig</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	@include('menu.footer')
@endsection