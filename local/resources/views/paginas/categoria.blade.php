@extends('layouts.principal')

@section('contenido')
    @include('menu.header_principal')
    <section class="headerpage" style="height: 100px">
        <div class="hp_content">
            <div class="hpc_">
                <div class="container">
                    <div class="s_title">
                        <h2>{{$catego->categoria}}</h2>
                        <!-- <p>A continuación los productos más buscados por nuestros clientes:</p> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="s_productos" style="padding: 20px">
        <div class="container">
            <div class="row">
                @foreach($productos as $item)
                <div class="col-lg-3">
                    <div class="sp_item">
                        <div class="spi_img">
                            <img src="{{asset('local/public/images')}}/{{$item->path}}" alt="">
                        </div>
                        <div class="spi_details">
                            <a href="{{URL::to('/producto')}}/{{$item->id}}">{{$item->nombre}}</a>
                            <p>${{$item->precio}}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('menu.footer')
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#cs_carousel').owlCarousel({
            loop:true,
            margin:15,
            nav:false,
            dots: true,
            autoplay: true,
            items: 1,
            autoplayTimeout: 8000,
            autoplaySpeed: 1000,
            smartSpeed: 1000
        });
    })
</script>
    
@endsection