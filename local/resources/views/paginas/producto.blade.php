@extends('layouts.principal')


@section('script')
<script>
	$(document).ready(function() {
		$(document).on("click", "#p_comprar", function(e){
			id = $('#id_product').val();
			$.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({
                url: '{{ url('cart/addProducto') }}/'+id,
                method: 'POST',
                data:  {},
                success:function(response){
                	showToast('Mensaje', 'Producto agregado correctamente al carrito', 'success', 10000);
                    $("#lblCartCount").text(response);
                    $("#linkCart").removeClass("disabled");

                    //location.href = "{{-- url('/') --}}";
            	},
                error: function(data){
                    console.log(data);
                }
            })
		})

		function mostrarmodal(data){
			console.log(data);
			$('#form_compra').modal('show');
		}
	});

	
	
</script>
@endsection

@section('contenido')
@include('menu.header_principal')
<div class="ibreadcumbs">
	<div class="container">
		<h1>{{$producto->nombre}}</h1>
		<ul>
			<li><a href="{{URL::to('/')}}">Inicio</a></li>
			<li><span>/</span></li>
			<li><a href="{{URL::to('/categoria')}}/{{$producto->id_categoriaproducto}}">{{$producto->nombre}}</a></li>
			<li><span>/</span></li>
			<li class="active"><a>{{$producto->nombre}}</a></li>
		</ul>
	</div>
</div>
<section class="s_productos padding_90">
    <div class="container">
        <div class="row">
        	<div class="col-lg-6">
        		<div style="width: 100%;height: auto;">
        			<img src="{{asset('local/public/images')}}/{{$producto->path}}" alt="" style="width: 100%;height: auto;object-fit: cover;">
        		</div>
        	</div>
        	<div class="col-lg-6">
        		<input type="text" value="{{$producto->id}}" style="display: none;" id="id_product">
        		<div class="sp_descrip">
        			<h4>{{$producto->nombre}}</h4>
        			<div class="sp_precio" style="display: flex;">
        				@if($producto->preciodescuento)
        				<p class="sp_price_old"><s>${{$producto->precio}}</s></p>
        				<p class="sp_desc">${{$producto->preciodescuento}}</p>
        				@else
        				<p class="sp_price">${{$producto->precio}}</p>
        				@endif
        			</div>
        			<div class="spd_details">
        				<?php echo $producto->descripcion; ?>
        			</div>
        			<div class="spd_other">
        				<p><strong>Código: </strong> {{$producto->abreviatura}}</p>
        				<p><strong>Presentación: </strong>{{$producto->presentacion}}</p>
        			</div>
        			<div  class="row">
        				<a class="ibtn ibtn_m2" id="p_comprar">Comprar</a>
        				
        				@auth('cliente')
        					<form action="{{route('wishlist.store')}}" method="post">
	  							{{csrf_field()}}
	  							<input name="id_cliente" type="hidden" value="{{ Auth::guard('cliente')->user()->id }}" />
	  							<input name="id_producto" type="hidden" value="{{ $producto->id }}" />
	  							<button class="btn btn-success" style="background: #009DE2; border-color: #009DE2" type="submit"><span class="fa fa-heart">	</span></button>
							</form>

        				@endauth
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</section>

@endsection
