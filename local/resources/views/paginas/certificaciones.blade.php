@extends('layouts.header')

@section('titulo', 'Certificaciones')

@section('menu')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/')}}">Inicio <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/quienes-somos')}}">La Empresa</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/servicios')}}">Servicios</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/productos')}}">Productos</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{URL::to('/certificaciones')}}">Certificaciones</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/noticias')}}">Noticias</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/contactos')}}">Contactos</a>
</li>
@endsection

@section('scripts')

@endsection

@section('contenido')
<section class="headerpage">
	<div class="hp_content">
		<div class="hpc_">
			<div class="container">
				<h2>Certificaciones</h2>
				<div class="hpc_url">
					<a href="{{URL::to('/')}}">Inicio</a>
					<span>/</span>
					<a>Certificaciones</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="certifica_page padding_120">
	<div class="container">
		<div class="crp_">
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					<div class="crp_content">
						<h5>{{$certificaciones->titulo}}</h5>
						<div class="crp_text">
							<?php echo $certificaciones->texto; ?>
						</div>
						<div class="crp_img">
							<img src="{{asset('local/public/images')}}/{{$certificaciones->path}}" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection