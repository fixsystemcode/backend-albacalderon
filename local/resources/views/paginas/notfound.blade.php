<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Floristería ilusión</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lightgallery.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.bxslider.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/efxvid.css')}}">
    <link rel="icon" type="image/x-icon" href="{{asset('assets/images/core/icon.png')}}" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:400,500,600,700,800&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Be+Vietnam:400,600|Montserrat:500,700,800&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Rubik:500,700,900&display=swap" rel="stylesheet">



    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:ital,wght@0,400;0,700;1,400&family=Tangerine:wght@400;700&display=swap" rel="stylesheet">



    <!--  -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Heebo:500,700,800&display=swap" rel="stylesheet">

</head>
<body>
<?php
function diaES($fecha){
    $dia = date('l', strtotime($fecha));
    $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    $nombredia = str_replace($dias_EN, $dias_ES, $dia);
    return $nombredia;
}
function mesES($fecha){
    $mes = date('M', strtotime($fecha));
    $meses_ES = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
    $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
    return $nombreMes;
}
?>

    @include('menu.header_principal')



    <section class="padding_120">
    	<div class="container">
    		<h5>Página no encontrada.</h5>
    	</div>
    </section>



    @include('menu.footer')


    <div class="efx_image_show">
        <div class="efx_overlay"></div>
        <div class="efx_image_content">
            <span><i class="fa fa-times"></i></span>
            <img src="">
            <p class="popup-desc" style="position: absolute;right: 50%;color: #fff;font-weight: 600;font-size: 18px;background-color: #0f0f0f;"></p>
        </div>
    </div>

    <div class="visho" style="">
      <div class="overlex"></div>
      <div class="vi-sho">
        <span><i class="fas fa-times"></i></span>
        <iframe width='75%' height='180' src='' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>
      </div>
    </div>
    
</body>

<script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/lightgallery.js')}}"></script>
<script src="{{asset('assets/js/jquery.bxslider.js')}}"></script>


<script src="{{asset('assets/js/scripts.js')}}"></script>
<!-- <script>
    $('#galeria').lightGallery({
        thumbnail:true
    }); 
</script> -->
<script>
    $(document).ready(function(){
        $('#cs_carousel').owlCarousel({
            loop:true,
            margin:15,
            nav:false,
            dots: true,
            autoplay: true,
            items: 1,
            autoplayTimeout: 8000,
            autoplaySpeed: 1000,
            smartSpeed: 1000
        });
    })
</script>
