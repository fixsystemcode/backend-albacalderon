@extends('layouts.principal')

@section('contenido')

	@include('menu.header_principal')
	<section class="s_productos" style="padding-top: 20px">
        <div class="container">
            <div class="s_title">
                <h2>Servicios</h2>
            </div>
            <div class="row" style="text-align: justify;">
            	<p>En FixSystem entendemos que el internet se ha posicionado como el mejor medio digital del mundo y que el Ecuador no es la excepción. En este momento millones de ecuatorianos navegan a través de internet ya sea para buscar algún servicio, comprar un producto, interactuar con sus amigos o realizar alguna consulta. </p>
                <p>Observando el potencial que tiene internet el equipo creativo de FixSystem ha desarrollado diversos servicios para que nuestros clientes exploten al máximo los recursos en internet.</p>
            </div>
            <div class="row">
			@foreach($servicios as $item)
                <div class="col-lg-3">
                    <div class="sp_item">
                        <div class="spi_img">
                            <a href="{{URL::to('/producto')}}/{{$item->id}}"><img src="{{asset('local/public/images')}}/{{$item->path}}" alt=""></a>
                        </div>
                        <div class="spi_details">
                            <a href="{{URL::to('/producto')}}/{{$item->id}}">{{$item->nombre}}</a>
                            <p>
                                ${{$item->precio}} 
                            </p>

                        </div>
                    </div>
                </div>
            @endforeach
		</div>
	</div>
</section>
@endsection