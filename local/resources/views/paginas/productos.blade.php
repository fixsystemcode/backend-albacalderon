@extends('layouts.header')

@section('titulo', 'Productos')

@section('menu')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/')}}">Inicio <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/quienes-somos')}}">La Empresa</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/servicios')}}">Servicios</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{URL::to('/productos')}}">Productos</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/certificaciones')}}">Certificaciones</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/noticias')}}">Noticias</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/contactos')}}">Contactos</a>
</li>
@endsection

@section('scripts')

@endsection

@section('contenido')
<section class="header_page">
	<div class="container">
		<div class="hp_container">
			<div class="hpc_text">
				<h1>PRODUCTOS</h1>
				<div class="hpc_url hpcinv">
					<a href="{{URL::to('/')}}">Inicio</a>
					<span>/</span>
					<a>Productos</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="productos_page padding_120">
	<div class="container">
		<div class="row">
			@foreach($productos as $producto)
			<div class="col-lg-3">
				<div class="pp_item">
					<a href="{{URL::to('/productos')}}/{{$producto->id}}">
						<div class="ppic_img">
							<img src="{{asset('local/public/images')}}/{{$producto->path}}" alt="">
						</div>
						<h5>{{$producto->nombre}}</h5>
					</a>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
@endsection