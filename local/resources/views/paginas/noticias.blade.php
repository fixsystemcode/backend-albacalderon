@extends('layouts.header')

@section('titulo', 'Noticias')

@section('menu')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/')}}">Inicio <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/quienes-somos')}}">La Empresa</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/servicios')}}">Servicios</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/productos')}}">Productos</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/certificaciones')}}">Certificaciones</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{URL::to('/noticias')}}">Noticias</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/contactos')}}">Contactos</a>
</li>
@endsection

@section('scripts')
@endsection

@section('contenido')
<section class="headerpage">
	<div class="hp_content">
		<div class="hpc_">
			<div class="container">
				<h2>Noticias</h2>
				<div class="hpc_url">
					<a href="{{URL::to('/')}}">Inicio</a>
					<span>/</span>
					<a>Noticias</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="noticias_page padding_120">
	<div class="container">
		<div style="position: relative;margin-right: -10px;margin-left: -10px;">
			<div class="row">
				@foreach($noticias as $articulo)
				<div class="col-lg-4">
					<div class="not_item">
						<a href="{{URL::to('/noticias')}}/{{$articulo->slug}}">
							<div class="ni_head">
								<img src="{{asset('local/public/images')}}/{{$articulo->path}}" alt="">
							</div>
						</a>
						<div class="not_content">
							<div class="ni_data">
								<span><i class="fas fa-user"></i> {{$articulo->nombres}}</span>
		                        <span><i class="far fa-clock"></i> <?php echo date('m.d.Y', strtotime($articulo->created_at)); ?></span>
							</div>
							<div class="ni_text">
								<a href="{{URL::to('/noticias')}}/{{$articulo->slug}}"><h5>{{$articulo->titulo}}</h5></a>
								<p><?php $text = substr($articulo->texto, 0, 120); echo strip_tags($text); ?>...<a href="{{URL::to('/noticias')}}/{{$articulo->slug}}">Leer mas</a></p>
							</div>
							<!-- <a href="{{URL::to('/noticias')}}/{{$articulo->slug}}" class="not_btn wbtn_medium colorl">Leer más</a> -->
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
@endsection