@extends('layouts.principal')

@section('contenido')
    @include('menu.header_principal')

@section('scripts')
<script>
	$(document).ready(function(){
		$('#commentcarousel').owlCarousel({
		    loop:true,
		    margin:10,
		    dots: true,
		    autoplay: true,
		    responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            nav:true
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		});
		$(window).bind('scroll', function() {
            var distance = 100;
            if ($(window).scrollTop() > distance) {
                $('.nav_inferior').addClass('scrolled');
                $('body').css('padding-top', '56px');
             }else {
                $('.nav_inferior').removeClass('scrolled');
                $('body').css('padding-top', '0px');
             }
        });
	})
</script>
@endsection
<section class="">
		<div class="s_title">
				<h2>Quienes somos</h2>
		</div>
</section>
<section class="empresa_page">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="ep_text">
					<div class="ept_" style="text-align: justify;">
						{{ $info->quienessomos }}
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="ep_img">
					<img src="{{ asset(logopagina2()) }}" alt="" width="500px">
				</div>
			</div>
		</div>
	</div>
</section>
	@include('menu.footer')
@endsection