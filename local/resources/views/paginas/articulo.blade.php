@extends('layouts.header')

@section('titulo')
{{$articulo->titulo}}
@endsection

@section('menu')
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/')}}">Inicio <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/quienes-somos')}}">La Empresa</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/servicios')}}">Servicios</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/productos')}}">Productos</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/certificaciones')}}">Certificaciones</a>
</li>
<li class="nav-item active">
    <a class="nav-link" href="{{URL::to('/noticias')}}">Noticias</a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('/contactos')}}">Contactos</a>
</li>
@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyWUJqF-JvN1rwHT0ZhXeo7sKec_E3I_w&callback=initMap"
    async defer></script>
<script>
    @if($articulo->latitud != null)
    var lati = <?php echo $articulo->latitud; ?>;
    var long = <?php echo $articulo->longitud; ?>;
    @endif
    function initMap() {
        var icon="{{asset('assets/images/icons/marker.png')}}";
        @if($articulo->latitud != null)
        var ubicacion = {lat: lati, lng: long};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: ubicacion,
            mapTypeld:"roadmap"
        });
        var makerMatriz = new google.maps.Marker({
            position: new google.maps.LatLng(lati, long),
            map: map,
            icon: icon
        });
        @endif
    }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#lightSlider').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:9,
        slideMargin:0,
        enableDrag: false,
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#lightSlider .lslide'
            });
        }   
    });  
  });
</script>
<script>
    $(document).ready(function(){
        reprod= document.getElementById("rep");
        <?php foreach ($videos as $key => $value): ?>
            var url = "<?php echo $value->path ?>";
            if (url.indexOf("youtube.com") != -1) {
                url=url.substr(url.indexOf("?v=")+3,11);
                reprod.innerHTML +="<div class='col-lg-6 col-md-8 col-sm-12 offset-lg-3 offset-md-2'><div class='apiv_'><img src='http://img.youtube.com/vi/"+url+"/0.jpg' alt=''><div class='apiv_fade'></div><div class='apiv_play'><a class='vipop-click' src='https://www.youtube.com/embed/"+url+"'><img src='{{asset('assets/images/icons/play.png')}}' alt=''></a></div></div></div>"
            }
        <?php endforeach ?>

        popup = {
            init: function(){
                $('.vipop .vipop-click').click(function(){
                    popup.open($(this));
                });
                $("span, .img-sho").click(function(){
                    popup.close();
                });
                $(".overlex").click(function(){
                    popup.close();
                });
            },
            open: function($img){
                console.log('kk');
                $image = $img.clone();
                src = $($image).attr('src');
                console.log(src);
                $(".visho").fadeIn();
                $(".vi-sho iframe").attr("src",src);
            },
            close: function(){
                $(".visho").fadeOut();
                $(".vi-sho iframe").attr("src",src);
            }
        }
        popup.init()
    })
</script>
@endsection

@section('contenido')
<section class="headerpage">
	<div class="hp_content">
		<div class="hpc_">
			<div class="container">
				<h2>{{$articulo->titulo}}</h2>
				<div class="hpc_url">
					<a href="{{URL::to('/')}}">Inicio</a>
					<span>/</span>
					<a href="{{URL::to('/noticias')}}">Noticias</a>
					<span>/</span>
					<a>{{$articulo->titulo}}</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="articulo_page padding_120">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<div class="arp_content">
					<div class="apc_head">
						<div id="lightSlider" class="apih_content">
                            <img src="{{asset('local/public/images')}}/{{$articulo->path}}" alt="" data-thumb="{{asset('local/public/images')}}/{{$articulo->path}}" data-src="{{asset('local/public/images')}}/{{$articulo->path}}">
                            @foreach($fotos as $img)
                            <img src="{{asset('local/public/galeria')}}/{{$img->path}}" alt="" data-thumb="{{asset('local/public/galeria')}}/{{$img->path}}" data-src="{{asset('local/public/galeria')}}/{{$img->path}}">
                            @endforeach
                        </div>
					</div>
					<div class="apc_text">
						<div class="apc_data">
							<span><i class="fas fa-user"></i> {{$articulo->nombres}}</span>
                        	<span><i class="far fa-clock"></i> <?php echo date('M.d.Y', strtotime($articulo->created_at)); ?></span>
						</div>
						<h2>{{$articulo->titulo}}</h2>
						<div class="apct_">
							<?php echo $articulo->texto; ?>
						</div>
					</div>
					@if($videos !='[]')
                    <div class="ac_title">
                        <h5>Videos relacionados</h5>
                        <hr>
                    </div>
					<div class="apc_video vipop" id="rep">
					</div>
					@endif
					@if($documentos !='[]')
					<div class="apc_doc">
						<div>
							<div class="ac_title">
                                <h5>Documentos adjuntos</h5>
                                <hr>                     
                            </div>
							<ul class="apcd_list">
								@foreach($documentos as $doc)
                                <li>
                                    <div class="apic_">
                                        <a href="{{asset('local/public/doc')}}/{{$doc->path}}" download><span>{{$doc->titulo}}</span></a>
                                        <a href="{{asset('local/public/doc')}}/{{$doc->path}}" target="_blank"><i class="fas fa-arrow-down"></i></a>
                                    </div>
                                </li>
                                @endforeach
							</ul>
						</div>
					</div>
					@endif
					@if($articulo->latitud != null)
                    <div class="ac_title">
                        <h5>Ubicación</h5>
                        <hr>                     
                    </div>

					<div id="map" style="width: 100%;height: 200px;position: relative;"></div>
					@endif
				</div>
			</div>
		</div>
	</div>
</section>
@endsection