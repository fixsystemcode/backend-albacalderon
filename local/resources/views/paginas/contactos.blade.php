@extends('layouts.principal')

@section('contenido')
    @include('menu.header_principal')
	@section('scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyWUJqF-JvN1rwHT0ZhXeo7sKec_E3I_w&callback=initMap"
	    async defer></script>
	<script>
	    function initMap() {
	        var icon="{{asset('assets/images/icons/marker.png')}}";
	        var ubicacion = {lat: -2.229903, lng: -80.877770};
	        var map = new google.maps.Map(document.getElementById('mapa'), {
	            zoom: 15,
	            center: ubicacion,
	            mapTypeld:"roadmap"
	        });
	        var makerMatriz = new google.maps.Marker({
	            position: new google.maps.LatLng(ubicacion),
	            map: map,
	            icon: icon
	        });
	    }
	</script>
	<script>
	    jQuery('form.contact-form').on('submit', function( e ){
	        e.preventDefault();
	        var $form = jQuery(this);
	        /jQuery($form).find('span.contact-form-respond').remove();/
	        if ($form.find('.result').hasClass('sc_infobox_style_error')) {
	            $form.find('.result').removeClass('sc_infobox_style_success');
	            return;
	        };
	        $form.find('.result').removeClass('sc_infobox_style_error');
	        $form.find('.result').css('display', "block");
	        var request = $form.serialize();
	        console.log(request);
	        var ajax = jQuery.post( "enviarmensaje", request )
	            .done(function( data ) {
	                    if (data==0){
	                        $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #555; border-radius: 2px; padding: 5px; text-align: center;''><strong>Mensaje enviado correctamente, pronto nos pondremos en contacto con usted.</strong></div>" );
	                        $('form.contact-form')[0].reset();
	                        grecaptcha.reset();
	                    }
	                    if (data==1){
	                        $( ".result" ).append( "<strong>Robot verification failed, please try again.</strong>" );
	                    }
	                    if (data==2) {
	                        $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #555; border-radius: 2px; padding: 5px; text-align: center;''><strong>El CAPTCHA es obligatorio</strong></div>" );
	                    }
	                    console.log(data);
	        })
	            .fail(function( data ) {
	                $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #555; border-radius: 2px; padding: 5px; text-align: center;''><strong>Fallo al enviar el mensaje.</strong></div>" );
	                console.log(data);
	        })
	        jQuery('.result').fadeIn().delay(5000).fadeOut();
	        $('.result').empty();
	    });
	</script>	
@endsection

<section class="headerpage" style="display: none;">
	<div class="hp_content">
		<div class="hpc_">
			<div class="container">
				<h2>Certificaciones</h2>
				<div class="hpc_url">
					<a href="{{URL::to('/')}}">Inicio</a>
					<span>/</span>
					<a>Certificaciones</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="contactos_page padding_90">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="cp_info">
					<div class="cpi_head" style="color: #fff">
						<img src="{{ asset(logopagina()) }}" alt="" style="margin-bottom: 10px">
						<h4 style="color: #fff">Datos de contacto</h4>
						<p>Ponte en contacto con nosotros, estamos para ayudarte.</p>
					</div>
					<ul class="cpi_list">
						<li><i class="fa fa-map-marker"></i> <span>{{$informacion->direccion}}</span></li>
                        <li><i class="fa fa-phone"></i> <span>{{$informacion->telefono}}</span></li>
                        <!-- <li><i class="fab fa-whatsapp"></i> <span></span></li> -->
                        <li><i class="fa fa-envelope"></i> <span>{{$informacion->email}}</span></li>
					</ul>
					<ul class="cpi_social" style="float: left;margin-top: 4px;margin-bottom: 8px;">
						<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
					<div class="isd_btns" style="display: flex;width: auto;float: left;">
                        <a href="https://www.google.com/maps?q=-2.229903,-80.877770&z=15&hl=es" target="_blank">¿Como llegar? <i class="fas fa-map-marked-alt"></i></a>
                    </div>
					<div style="height: 40px;width: 100%;"></div>
					
				</div>
			</div>
			<div class="col-lg-7">
				<div class="cp_content">
					<div class="cpc_form">
						<div class="cpc_head">
							<h4>¿Necesitas más información?</h4>
							<p>Envíanos un mensaje</p>
						</div>
						{!! Form::open(['route'=>'paginas.enviarmensaje','method'=>'POST','class'=>'form-contacto contact-form columns_p_5 inited','data-formtype'=>'contact']) !!}
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="" placeholder="Nombres" aria-required="true" id="name" name="name" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="" placeholder="Teléfono" aria-required="true" id="phone" name="phone">
									</div>
								</div>
							</div>
							<div class="form-group">
								<input type="text" class="" placeholder="E-mail" aria-required="true" id="email" name="email" required>
							</div>
							<div class="form-group">
								<input type="text" class="" placeholder="Asunto" aria-required="true" id="subject" name="subject" required>
							</div>
							<div class="form-group">
								<textarea aria-required="true" rows="3" cols="20" class="" id="message" name="message" placeholder="Mensaje" required></textarea>
							</div>
							<div class="form-group">
								<div class="" style="float: right;margin-bottom: 15px;">
									<div class="g-recaptcha" data-sitekey="6LdUBaIUAAAAAABkDtxoYDBjcVFqmjoR0xbXcZ4E"></div>
								</div>
								<button type="submit">Enviar</button>
								<div style="clear: both;"></div>
							</div>
						{!! Form::close() !!}
					</div>
					<div style="position: relative; margin-top: 15px;"><div class="result"></div></div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection