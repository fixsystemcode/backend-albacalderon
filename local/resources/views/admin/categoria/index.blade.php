@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Categorías</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('categorias.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <!-- <a class="btn btn-primary" href="{{ URL::route('categorias.create') }}"><i class="fa fa-plus"></i> Nuevo</a>
    <br> -->

    @if ($categorias->count())
        <br>
        <div class="row">
            <div class="col-md-12">
                <table id="tbbuzon" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <th>Nombre de categorías <br> <small>Descripción</small></th>
                        <th>Destacado</th>
                        <th>Acciones</th>                    
                    </thead>
                    <tbody>                        
                        @foreach ($categorias as $categoria)
                            <tr>
                                <td>
                                    <b>{{ $categoria->categoria }}</b><br>
                                    <small class="text-muted">{{ $categoria->descripcion }}</small>
                                </td>
                                <td>{{ $categoria->cat_destacado == '1' ? 'Destacado' : 'Sin destacar' }}</td>
                                <td>
                                    <a class="btn btn-success btn-sm" href="{{route('categorias.edit', $categoria->id)}}" role="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    {!! Form::open(array('method' => 'DELETE', 'route' => array('categorias.destroy', $categoria->id))) !!}
                                      <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <div class="alert alert-warning " role="alert">
            No existen categorias actualmente, empecemos creando una <a href="{{ route('categorias.create') }}">aqui</a>
        </div>
    @endif

@endsection