@extends('layouts.backend')
@section('titulo', 'Nuevo')

@section('contenido')
	<div class="row">
		<div class="col-md-8" >
			{!! Form::open(['route' => 'categorias.store', 'method' => 'POST', 'files'=>true]) !!}
				<div class="form-group">
					{!! Form::label('categoria','Nombre de Categoria') !!}
					{!! Form::text('categoria',null,['class' =>'form-control', 'placeholder' =>'Ingrese nombre de categoria','required'])!!}
				</div>
				<div class="form-group">
					{!! Form::label('cat_destacado','Destacado') !!}
					{!! Form::select('cat_destacado',['1'=>'Destacado', '0'=>'No destacado'], null,[ 'class' =>'form-control', 'required'])!!}
				</div>

				<div class="form-group">
					{!! Form::label('descripcion','Nombre de Categoria') !!}
					{!! Form::textarea('descripcion',null,['class' =>'form-control', 'placeholder' =>'Categoria destacado ingrese 1','rows'=>6])!!}
				</div>
				<?php /*<div class="form-group"  >
					{!! Form::label('path','Fondo Categoria') !!}
					{!! Form::file('path',[])!!}			
				</div>
				<div class="form-group"  >
					{!! Form::label('icono','Icono de categoria') !!}
					{!! Form::file('icono',[ ])!!}			
				</div>*/ ?>

				<div class="form-group">
					{!! Form::button('<i class="fa fa-save"></i> Registrar', ['class' =>'btn btn-primary', 'type'=>'submit']) !!}
					<a href="{{ route('categorias.index') }}" class="btn btn-warning"><i class="fa fa-chevron-left"></i> Regresar</a>
				</div>
			{!! Form::close() !!}
		</div>
		<div class="clearfix"></div>
	</div>
	
@endsection