@extends('layouts.backend')
@section('titulo', 'Editar')
@section('contenido')

	@if(count($errors) >0)
		<div class="alert alert-danger" role="alert">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{!!Form::model($categoria,['route'=>['categorias.update',$categoria],'method'=>'PUT','files' => true])!!}
		<div class="form-group">
			{!! Form::label('categoria','Nombre de Categoria') !!}
			{!! Form::text('categoria',null,['class' =>'form-control', 'placeholder' =>'Ingrese nombre de categoria','required'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('cat_destacado','Destacado') !!}
			{!! Form::select('cat_destacado',['1'=>'Destacado', '0'=>'No destacado'], null,[ 'class' =>'form-control', 'required'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('descripcion','Nombre de Categoria') !!}
			{!! Form::textarea('descripcion',null,['class' =>'form-control', 'placeholder' =>'Categoria destacado ingrese 1','rows'=>6])!!}
		</div>
		<div class="form-group">
			{!! Form::button('<i class="fa fa-save"></i> Actualizar', ['class' =>'btn btn-primary', 'type'=>'submit']) !!}
			<a href="{{ route('categorias.index') }}" class="btn btn-warning"><i class="fa fa-chevron-left"></i> Regresar</a>
		</div>
	{!! Form::close() !!}

@endsection