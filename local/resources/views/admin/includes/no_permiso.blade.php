<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Acceso Denegado</title>
	{!! Html::style('admin/plantilla1/css/style_new.css') !!}
	<link href="{{ asset('dist/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<meta http-equiv='Refresh' content='5;url={{ URL::to(URL::previous()) }}'>
</head>
<body class="app flex-row align-items-center">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
                <div class="card text-center py-4">
                    <div id="card-head">
                        <h2 style="margin-top: 10px; margin-bottom: 0px;" class="text-danger">
                            <i class="fa fa-check-square-o fa-1x"></i> {{ appEmpresa() }}
                        </h2>
                        <p style="font-style: normal;"> ACCESO DENEGADO </p>
                        {{ request()->ajax() }}
                    </div>
                    <div class="card-body">                                      
                        <h1>¡Acceso denegado!</h1>
                        <h4>
                            Usted ({{ Auth::user()->login }}) no tiene permiso para ver esta pagina.<br>
                            Será redireccionado automáticamente a la página anterior.<br>
                            Caso contrario haga clic <a class="text-danger" href='{{ route('home') }}'>aquí</a> para ir a la página principal
                        </h4>  
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>