@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Pedidos</h1>
</div>
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}</form>
	@if (count($pedidos))
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table  id="tbbuzon" class="table table-bordered  text-center table-hover display" >
                        <thead>
                            <th>Id</th>
                            {{-- <th>Detalle</th> --}}
                            <th>Estado</th>
                            <th>Subtotal</th>
                            <th>Descuento</th>
                            <th>Iva</th>
                            <th>Tarifa Envio</th>
                            <th>Total</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($pedidos as $key => $pedido)
                            <tr>
                                <td> {{ $pedido->id }} </td>                   
                                {{-- <td>
                                	<div class="row" style="font-weight: bold;">
                                			<div class="col-md-6"><small style="font-size: 0.5">Producto</small></div>
                                			<div class="col-md-3"><small style="font-size: 0.5">Cantidad</small></div>
                                			<div class="col-md-3"><small style="font-size: 0.5">Precio</small></div>
                                		</div>
                                	@foreach ($pedido->detalle as $detalle)
                                		<div class="row">
                                			<div class="col-md-6"><small style="font-size: 0.5">{{ $detalle->detalleProducto->nombre }}</small></div>
                                			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->cantidad }}</small></div>
                                			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->precio }}</small></div>
                                		</div>
                                	@endforeach
                                </td> --}}
                                <td>
									@switch($pedido->estado)
							            @case(1)
							                <span class="badge badge-warning">Pendiente de pago</span>
							                @break

							            @case(2)
							                <span class="badge badge-info">Pagado</span>
							                @break
							            @case(3)
							                <span class="badge badge-success">Pago Confirmado</span>
							                @break
							            @case(4)
							                <span class="badge badge-danger">Error en el pago</span>
							                @break
							            @case(5)
							                <span class="badge badge-primary">Enviado</span>
							                @break
							                @break
							            @default
							                <span class="badge badge-secundary">Indefinido</span>
							        @endswitch
                             	</td>
                                <td> {{ $pedido->subtotal }}</td>
                                <td> {{ $pedido->descuento }}</td>
                                <td> {{ $pedido->iva }}</td>
                                <td> {{ $pedido->tarifa_envio }}</td>
                                <td> {{ $pedido->total }}</td>
                                <td> 
								<a href="{{ url('pedidos') }}/{{ $pedido->id}}" class="btn btn-primary"><span class="fa fa-eye"></span></a>
								<a class="btn btn-danger" href="javascript:void(0)" onclick="eliminar('{{ $pedido->id }}')"><span class="fa fa-trash"></span></a>
                               	</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
        <br>
       <div class="alert alert-warning" role="alert">
            No hay pedidos</a>
        </div>
    @endif
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
	function eliminar(id){
       $("#formDelete").attr("action", '{{ URL::to('pedidos') }}/'+id).submit();
    }
	</script>
@endsection
