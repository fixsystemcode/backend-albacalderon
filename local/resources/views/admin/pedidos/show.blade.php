@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Pedidos</h1>
</div>
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
	<div class="container-fluid">
     <div class="row mb-5" style="font-size: 1.5em">
        <label for="">Id Pedido: </label>
        <font># 000{{ $pedido->id }}</font>
        @switch($pedido->estado)
            @case(1)
                <span class="badge badge-warning">Pendiente de pago</span>
                @break

            @case(2)
                <span class="badge badge-info">Pagado</span>
                @break
            @case(3)
                <span class="badge badge-success">Pago Confirmado</span>
                @break
            @case(4)
                <span class="badge badge-danger">Error en el pago</span>
                @break
            @case(5)
                <span class="badge badge-primary">Enviado</span>
                @break
                @break
            @default
                <span class="badge badge-secundary">Indefinido</span>
        @endswitch
    </div>
    <br>
    <div class="row">
           <h4>Detalle del cliente</h4>
           <hr>
           <label for="">Nombres:</label>
           <font>{{ $pedido->pedidoCliente->nombre }} {{ $pedido->pedidoCliente->apellido }}</font>
           <br>
           <label for="">Dirección:</label>
           <font>{{ $pedido->pedidoCliente->ciudad }}</font>
           <br>
           <label for="">Domicilio:</label>
           <font>{{ $pedido->pedidoCliente->direccion }}</font>
    </div>
    
    <div class="row">
        <h4>Detalle del pedido</h4>
        <hr>
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr class="info">
                        <th>Id producto</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pedido->detalle as $detalle)
                        <tr>
                            <td>{{ $detalle->detalleProducto->id }}</td>
                            <td>{{ $detalle->detalleProducto->nombre }}</td>
                            <td>{{ $detalle->cantidad }}</td>
                            <td>{{ $detalle->precio }}</td>
                            <td>{{ $detalle->cantidad * $detalle->precio }}</td>
                        </tr>
                    @endforeach
                        <tr style="border-top: 2px solid">
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td>Subtotal</td>
                            <td>{{ $pedido->subtotal }}</td>
                        </tr>
                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td>Descuento</td>
                            <td>{{ $pedido->descuento }}</td>
                        </tr>
                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td>Iva</td>
                            <td>{{ $pedido->iva }}</td>
                        </tr>
                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td>Tarifa de envio</td>
                            <td style="border-bottom: 2px solid">{{ $pedido->tarifa_envio }}</td>
                        </tr>
                        <tr>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td style="border: none;"></td>
                            <td>Total</td>
                            <td>{{ $pedido->total }}</td>
                        </tr>
                </tbody>
            </table> 
        </div>
        
    </div>
    <div class="row">
        @if ($pedido->estado != 5)
            {!! Form::open(['route'=>['pedidos.update', $pedido->id],'method'=>'PUT','id'=>'formulario','files'=>false]) !!}
            <input type="hidden" name="estado" id="estado" value="5">
            <button class=" btn btn-success" type="submit">Marcar como entregado</button>
            {!! Form::close() !!}
        @endif
        
    </div>
        
     </div>    
    </div>
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});

	</script>
@endsection
