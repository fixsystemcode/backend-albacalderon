@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Imágenes</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('imagen.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')
@include('alerts.success')
<!-- <a class="btn btn-primary" href="{{ URL::route('imagen.create') }}">Nuevo</a> -->
@if (count($imgs))
    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                <th>Imagen</th>
                <th>Título</th>
                <th>Descripción</th>
                <th>Categoria</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($imgs as $img)
                <tr>
                @if(empty($img->path))
                <td><img src="{{asset('local/public/images')}}/no-imagen.jpg"  style ="width: 100px"></td>
                @else
                <td><img src="{{asset('local/public/images')}}/{{$img->path}}"  style ="width: 100px"></td>
                @endif
                <td>{{ $img->titulo }}</td>
                <td>{{ $img->descripcion }}</td>
                <td>{{ $img->nombre }}</td>
                <td><a class="btn btn-success" href="{{route('imagen.edit', $img->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    {!! Form::open(array('method' => 'DELETE', 'route' => array('imagen.destroy', $img->id))) !!}
                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                    {!! Form::close() !!}
                </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    No hay imagenes
@endif
@stop