@extends('layouts.backend')
@section('titulo')
Editar
@stop
@section('contenido')
@include('alerts.request')
{!! Form::model($img, array('method' => 'PATCH', 'route' => array('imagen.update', $img->id) , 'files' => true)) !!}
  <div class="row">
        <div class="col-md-4">
          {!! Form::label('imagen','Imagen') !!}
          {!! Form::file('path')!!}
        </div>
        <div class="col-md-8">
                <img src="{{asset('local/public/images')}}/{{$img->path}}"  style ="width: 100px">
        </div>  
    </div>
    <div class="form-group">
            {!! Form::label('titulo', 'Titulo de la imagen:') !!}
            {!! Form::text('titulo',null,['class'=>'form-control','placeholder'=>'Ingrese Nombres']) !!}
    </div>
           <div class="form-group">
                {!! Form::label('descripcion', 'Descripcion:') !!}
                {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción de la imagen']) !!}
            </div>
    <div class="form-group">
                {!! Form::label('categoriainterna','Categoria') !!}
                {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas'])!!}
    </div>
    {!!Form::submit('Actualizar',['class'=>'btn btn-primary btn_formc'])!!}
{!! Form::close() !!}
@stop