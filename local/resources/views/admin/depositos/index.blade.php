@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Depositos</h1>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
     <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}</form>
    @if (count($depositos))
    <br>
     <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table  id="tbbuzon" class="table table-bordered  text-center table-hover display" >
                        <thead>
                            <th>Id</th>
                            <th>Numero de comprobante</th>
                            <th>Valor</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($depositos as $key => $deposito)
                            <tr>
                                <td> {{ $deposito->id }} </td>                   
                                <td> {{ $deposito->num_doc }}</td>
                                <td> {{ $deposito->valor }}</td>
                                 <td>
									{!! Form::select('estado'. $deposito->id, $estados, $deposito->estado ,['class'=>'form-control ','id' => 'estado'.$deposito->id]) !!}
                             	</td>
                                <td> 
								<a href="javascript:void(0)" onclick="guardarEstado('{{ $deposito->id }}')" class="btn btn-success"><span class="fa fa-save"></span></a>
								<a href="{{ asset('local/public') }}/{{ $deposito->adjunto->path }}" target="_blank" class="btn btn-primary"><span class="fa fa-eye"></span></a>
								<a class="btn btn-danger" href="javascript:void(0)" onclick="eliminar('{{ $deposito->id }}')"><span class="fa fa-trash"></span></a>
                               	</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
     @else
        <br>
       <div class="alert alert-warning" role="alert">
            No hay depositos</a>
        </div>
    @endif
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
	function eliminar(id){
       $("#formDelete").attr("action", '{{ URL::to('depositos') }}/'+id).submit();
    }
    function guardarEstado(id){
    	var estado = $('#estado'+id).val();
       $.ajax({
            url:'{{ url('depositos') }}',
            data: {
                'estado': estado,
                'id' : id,
                '_token': $("meta[name='csrf-token']").attr("content")
            },
            type: 'POST',
            success:function(response){
                location.reload();
            }
       });
    }
	</script>
@endsection
