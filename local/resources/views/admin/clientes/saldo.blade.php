@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Saldo clientes</h1>
</div>
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
    .dt-button {
      padding: 0;
      border: none;
      margin-bottom: 5px;
      float: right;
      margin-left: 15px;
    }
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
        <br>
    @endif
    <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}</form>
	@if (count($clientes))
    <div class="row">
        <div class="table-responsive">
            <table id="tb_saldo" class="table table-bordered  text-center table-hover display" >
                <thead>
                    <tr>
                        <th >ID</th>
                        <th >Nombres</th>
                        <th >Apellidos</th>
                        <th >Vehículos registrados</th>
                        {{-- <th >Correo</th>
                        <th >Telefono</th>
                        <th >Dirección</th> --}}
                        <th >Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->id }}</td>
                            <td>{{ $cliente->nombre }} {{ $cliente->segundo_nombre }}</td>
                            <td>{{ $cliente->apellido }}</td>
                            <td><span class="badge badge-primary">{{ $cliente->num_vehiculos }} vehículos</span></td>
                            {{-- <td>{{ $cliente->correo }}</td>
                            <td>{{ $cliente->telefono }}</td>
                            <td>{{ $cliente->direccion }}</td> --}}
                            <td>$ {{ number_format($cliente->saldo, 2) }}</td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
    @else
        <br>
       <div class="alert alert-warning" role="alert">
            No existe clientes registrados</a>
        </div>
    @endif
@endsection
@section('script')
    {!! Html::script('local/public/dist/dataTables/js/datatables.min.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/jquery.dataTables.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/dataTables.buttons.min.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/jszip.min.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/vfs_fonts.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/buttons.html5.min.js') !!}

    {!! Html::script('local/public/dist/dataTables/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/dataTables.responsive.min.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/dataTables.responsive.js') !!}
    {!! Html::script('local/public/dist/dataTables/js/responsive.bootstrap.min.js') !!}
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };
    $(document).ready(function() {
        $('#tb_saldo').dataTable({
            pageLength: 15,
            dom: 'Bfrtip',
            //responsive: true,
            "order": [[ 0, "desc" ]],
            buttons: [{
                extend: 'excel',
                //Aquí es donde generas el botón personalizado
                text: '<button class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></button>'}
                ],
            language: {
               "emptyTable":     "No hay datos disponibles en la tabla",
               "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
               "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
               "infoFiltered":   "(filtered from _MAX_ total entries)",
               "infoPostFix":    "",
               "thousands":      ",",
               "lengthMenu":     "Mostrar _MENU_ entradas",
               "loadingRecords": "Cargando...",
               "processing":     "Procesando...",
               "search":         "Buscar:",
               "zeroRecords":    "No se encontraron registros coincidentes",
               "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atrás"
                },
                "aria": {
                  "sortAscending":  ": activate to sort column ascending",
                  "sortDescending": ": activate to sort column descending"
                }
            },
            //"dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

            /*var config = {
              '.chosen-select'           : {},
              '.chosen-select-deselect'  : {allow_single_deselect:true},
              '.chosen-select-no-single' : {disable_search_threshold:10},
              '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
              '.chosen-select-width'     : {width:"100%"}
            }

            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }*/
    });
</script>
@endsection
