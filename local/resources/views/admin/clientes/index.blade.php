@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Clientes</h1>
</div>
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}</form>
	@if (count($clientes))
    <br>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th rowspan="2">Nombre</th>
                        <th rowspan="2">Apellido</th>
                        <th rowspan="2">Correo</th>
                        <th rowspan="2">Telefono</th>
                        <th rowspan="2">Dirección</th>
                        <th colspan="6" style="text-align: center;">Pedidos</th>
                    </tr>
                    <tr>
                        <th>Realizados</th>
                        <th>No pagado</th>
                        <th>Por confirmar</th>
                        <th>Con error</th>
                        <th>Por enviar</th>
                        <th>Enviados</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->nombre }}</td>
                            <td>{{ $cliente->apellido }}</td>
                            <td>{{ $cliente->correo }}</td>
                            <td>{{ $cliente->telefono }}</td>
                            <td>{{ $cliente->direccion }}</td>
                            <td style="text-align: center;"><span class="badge badge-dark"> {{ $cliente->pedidos }}</span></td>
                            <td style="text-align: center;"><span class="badge badge-warning"> {{ $cliente->pedidos_no_pagado }}</span></td>
                            <td style="text-align: center;"><span class="badge badge-info"> {{ $cliente->pedidos_pedientes }}</span></td>
                            <td style="text-align: center;"><span class="badge badge-danger"> {{ $cliente->pedidos_error }}</span></td>
                            <td style="text-align: center;"><span class="badge badge-success"> {{ $cliente->pedidos_por_enviar }}</span></td>
                            <td style="text-align: center;"><span class="badge badge-primary"> {{ $cliente->pedidos_enviados }}</span></td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
    @else
        <br>
       <div class="alert alert-warning" role="alert">
            No existe clientes registrado</a>
        </div>
    @endif
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
	function eliminar(id){
       $("#formDelete").attr("action", '{{ URL::to('pedidos') }}/'+id).submit();
    }
	</script>
@endsection
