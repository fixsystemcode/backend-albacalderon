@extends('layouts.backend')

@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Productos</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('shop.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')

    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    @if (count($productos))
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table  id="tbbuzon" class="table table-bordered  text-center table-hover display" >
                        <thead>
                            <th>Imagen</th>
                            <th style="width:35%;" class="text-left">Producto</th>
                            <th>Peso</th>
                            <th>Medidias</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                            <tr>
                                <td>
                                @if(empty($producto->path))
                                    <img src="{{asset('assets/images')}}/no-imagen.jpg" onerror=" this.src = '{{asset('assets/images')}}/articulo.png';" style ="width: 120px; height: 120px!important;object-fit: cover!important;"></td>
                                @else
                                    <div class="lightBoxGallery">
                                        <a href="{{asset('local/public/images')}}/{{$producto->path}}" title="Image from Unsplash" data-gallery="">
                                            <img  style="width: 120px;height: 120px!important;object-fit: cover;"  src="{{asset('local/public/images')}}/{{$producto->path}}" onerror="this.src = '{{asset('assets/images')}}/no-imagen.jpg';" >
                                        </a>
                                        <div id="blueimp-gallery" class="blueimp-gallery">
                                            <div class="slides"></div>
                                            <h3 class="title"></h3>
                                            <a class="prev">‹</a>
                                            <a class="next">›</a>
                                            <a class="close">×</a>
                                            <a class="play-pause"></a>
                                            <ol class="indicator"></ol>
                                        </div>
                                    </div>
                                @endif                            
                                </td>
                                <td class="text-left">
                                    <b>{{ $producto->nombre }}</b><br>
                                    <small class="text-muted">
                                        {{ strip_tags(substr($producto->descripcion,0,150)) }}{{ strlen($producto->descripcion) > 150 ? '...' : '' }}
                                    </small>
                                </td>
                                <td>{{ $producto->peso }}</td>
                                <td>{{ $producto->medidas}}</td>
                                <td>
                                    @if ($producto->estado)
                                        <span class="badge badge-primary">Activo</span>
                                    @else
                                        <span class="badge badge-warning">Inactivo</span>
                                    @endif
                                    <br>
                                    @if ($producto->destacado)
                                        <span class="badge badge-primary">Destacado</span>
                                    @else
                                        <span class="badge badge-warning">No destacado</span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-success" href="{{route('shop.edit', $producto->id)}}" role="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    {!! Form::open(array('method' => 'DELETE', 'route' => array('shop.destroy', $producto->id))) !!}
                                      <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
        <br>
       <div class="alert alert-warning" role="alert">
            No hay Artículos registrados, comencemos <a href="{{ route('shop.create') }}" class="alert-link">registrando el primero dando click aqui</a>
        </div>
    @endif

@endsection

@section('script')
    
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };
</script>
@endsection