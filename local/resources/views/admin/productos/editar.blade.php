@extends('layouts.backend')

@section('titulo', 'Editar Articulo')

@section('contenido')

<style type="text/css">
#elim{
	color:#fff;
	border:1px solid #ccc;
	background-color:#fb0000;
	margin-right:10px;
	padding: 0 2px;
	border-radius:4px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	-o-border-radius:4px;
	border-radius:4px;
	font-weight:bold;
	font-size:0.8em;
	cursor:pointer;
}
/* css galeria */
#reorder-helper{margin: 18px 10px;padding: 10px;}
.light_box {
	background: #efefef;
	padding: 20px;
	margin: 10px 0;
	text-align: center;
	font-size: 1.2em;
}
.gallery{ width:100%; float:left;padding-left:5px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li{ float:left; background:none; width:25%; height:115px; padding: 0 5px 10px 5px;}
.gallery ul li > div{
	width: 100%;
	height: 100%;
	border: 1px solid #eee;
	display: flex;
	justify-content: center;
	align-items: center;
}
div.imgprincipal img{
	width: 100%;
	height: 100%;
	object-fit: contain;
}
div.imgprincipal .imgp_{
	width: 100%;
	height: 220px;
	position: relative;
}
/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
/* style 6 */
.inputimg + label {
	color: #ec9b1b;
}
.inputimg:focus + label,
.inputimg.has-focus + label,
.inputimg + label:hover {
	color: #d47e03;
}
.inputimg + label figure {
	width: 100px;
	height: 135px;
	background-color: #ec9b1b;
	display: block;
	position: relative;
	padding: 30px;
	margin: 0 auto 10px;
}
.inputimg:focus + label figure,
.inputimg.has-focus + label figure,
.inputimg + label:hover figure {
	background-color: #d47e03;
}
.inputimg + label figure::before,
.inputimg + label figure::after {
	width: 0;
	height: 0;
	content: '';
	position: absolute;
	top: 0;
	right: 0;
}
.inputimg + label figure::before {
	border-top: 20px solid #05354e;
	border-left: 20px solid transparent;
}
.inputimg + label figure::after {
	border-bottom: 20px solid #d47e03;
	border-right: 20px solid transparent;
}
.inputimg:focus + label figure::after,
.inputimg.has-focus + label figure::after,
.inputimg + label:hover figure::after {
	border-bottom-color: #ec9b1b;
}
.inputimg + label svg {
	width: 100%;
	height: 100%;
	fill: #f1e5e6;
}

.contnex{
	background-color: #f5f5f5;
	padding: 15px;
}
.temp_box{
	display: flex;
	flex-wrap: wrap;
}
.temp_box label{
	display: block;
}
.tb_box{
	text-align: center;
	flex: 0 0 8.33%;
	width: 8.33%;
}
</style>

{!!Form::model($producto,['route'=>['shop.update',$producto->id],'method'=>'PUT','files' => true,'id'=>'my-dropzone'])!!}
	<input type="hidden" id="doc">
	<input type="hidden" id="btncancelardoc">
	<input type="hidden" name="publicar" value="0">
	<input type="hidden" name="id_usuario" value="{{ Auth::user()->id }}">

	<select name="ruta[]" id="ruta" multiple="multiple" style='display:none;' ></select>
	<select name="sobras[]" id="sobras" multiple="multiple" style='display:none;' ></select>

	@if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="contnex">
					<div class="form-group">
						{!! Form::label('abreviatura','Abreviatura') !!}
						{!! Form::text('abreviatura',null,['class' =>'form-control', 'placeholder' =>'Nombre corto del producto'])!!}
					</div>
					<div class="form-group">
						{!! Form::label('nombre','Título') !!}
						{!! Form::text('nombre',null,['class' =>'form-control', 'placeholder' =>'Nombre completo del producto','required'])!!}
					</div>
					<div class="form-group">
						{!! Form::label('categoria','Categoria') !!}
						{!! Form::select('id_categoriaproducto',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas','id' =>'categorias'])!!}
					</div>
					<div class="form-group">
						{!! Form::label('temporada','Temporada') !!}
						<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="temporada" id="temporada" type="checkbox" {{ $producto->temporada == 1 ? 'checked' : '' }}>
					</div>
					<div class="form-group">
						<div class="temp_box">
							<?php
								$meses = array("0" => "Ene", "1" => "Feb", "2" => "Mar", "3" => "Abr", "4" => "May", "5" => "Jun", "6" => "Jul", "7" => "Ago", "8" => "Sep", "9" => "Oct", "10" => "Nov", "11" => "Dic");
								$contador = 0;
								for ($i=0; $i < 12; $i++) { 
									echo "<div class='tb_box'>";
									echo "<label for=mes".$i.">".$meses[$i]."</label>";
									if ($campostempo[$i]==1) {
										echo "<input type='checkbox' name='mes".$i."' value='1' id='mes".$i."' checked>";
									}
									elseif($campostempo[$i]==0){
										echo "<input type='checkbox' name='mes".$i."' value='1' id='mes".$i."'>";
									}
									echo "</div>";
								}
							 ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="contnex">
					<div class="form-group">
						<input type="checkbox" data-on="Destacar" data-off="No Destacar" {{ $producto->destacado == 1 ? 'checked' : '' }}  data-toggle="toggle" name="destacado" id="destacado">
						<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" {{ $producto->estado == 1 ? 'checked' : '' }} name="estado" id="estado" type="checkbox">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('precio','Precio') !!}
								{!! Form::number('precio',null,['class' =>'form-control', 'placeholder' =>'$'])!!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('preciodescuento','Descuento') !!}
								{!! Form::number('preciodescuento',null,['class' =>'form-control', 'placeholder' =>'$'])!!}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('stock','Stock') !!}
								{!! Form::number('stock',null,['class' =>'form-control', 'placeholder' =>'Existencia'])!!}
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 23px;">
							<input data-toggle="toggle" data-on="Grava IVA" data-off="No grava IVA" {{ $producto->iva == 1 ? 'checked' : '' }} name="iva" id="iva" type="checkbox">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{!! Form::label('presentacion','Presentación') !!}
								{!! Form::text('presentacion',null,['class' =>'form-control', 'placeholder' =>'Presentación'])!!}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('peso','Peso') !!}
								{!! Form::text('peso',null,['class' =>'form-control', 'placeholder' =>'Peso'])!!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('medidas','Medidas') !!}
								{!! Form::text('medidas',null,['class' =>'form-control', 'placeholder' =>'Medidas'])!!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="imgprincipal">
					<label>Imagen Principal</label>
					<label for="imagenprincipal" style="display: block; border: 1px solid #1ab394;">				
						{!! Form::file('imagenprincipal',['id'=>'imagenprincipal','accept'=>'image/*','style'=>'display:none;'])!!}
						<img id="imgprincipal" src="{{ ($producto->path == NULL) ? asset('/assets/images/no-imagen.jpg') : asset('local/public/images/'.$producto->path) }}">
					</label>
				</div>
			</div>
			<div class="col-md-9">
				<label>Galeria</label>
				<div class="gallery">
					<ul class="reorder_ul reorder-photos-list" style="background: #fff" id="nuevalista">
						<!--Lista de fotos -->
						@for ($i = 1; $i <= 8 ; $i++)
							<li>
								<div>
									<a href="javascript:void(0);" style="float:none;" class="image_link">
										<input type="file" name="file-{{ $i }}" id="file-{{ $i }}" data-n="{{ $i }}" class="inputimg"  style='display:none;' accept="image/*" />
										<label for="file-{{ $i }}">
											<img id="imgfile-{{ $i }}" style="height: 100px;padding: 6px;width: 100px;object-fit: contain;" src="{{ isset($galeria[($i-1)]->path) ? asset('local/public/galeria/'.$galeria[($i-1)]->path) : asset('/assets/images/no-imagen.jpg') }}" >
											<a class="btn btn-danger btn-xs deleteImage" id="elim{{ $i }}"  onclick="eliminarElemento('imgfile-{{ $i }}','elim{{ $i }}');" style="{{ isset($galeria[($i-1)]->path) ? '' : 'display:none' }}"><i class="fa fa-trash-o"></i></a>
										</label>
									</a>
								</div>
							</li>
						@endfor
					</ul>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="form-group">
			{!! Form::label('descripcion','Contenido') !!}
			{!! Form::textarea('descripcion',null,['class' =>'form-control ckeditor', 'placeholder' =>'Contenido del artículo','required'])!!}
		</div>
	</div>


	<div class="col-md-12">
		<input type="checkbox" id="inicio" name="inicio" style="display:none;">
		<button class="btn btn-primary" type="submit" onclick="document.getElementById('inicio').checked=true;">
			<i class="fa fa-floppy-o"></i> Actualizar
		</button>
		<button class="btn btn-primary" type="submit" onclick="document.getElementById('inicio').checked=false;">
			<i class="fa fa-floppy-o"></i> Actualizar y Cerrar
		</button>
		<a class="btn btn-warning" href="{{ URL::to('shop') }}"><i class="fa fa-chevron-left"></i> Atras</a>
	</div>

	<div class="clearfix"></div>
</div>

{!! Form::close() !!}

@endsection

@section('script')
<script>

	//$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } } );

	$( document ).ready(function() {

		$('.selective').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: false,
			create: function(input) {
				return {
					value: input,
					text: input
				}
			}
		});

	});	
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>
@include('scripts.productoscript')

@endsection