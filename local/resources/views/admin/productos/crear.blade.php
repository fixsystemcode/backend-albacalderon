@extends('layouts.backend')
@section('titulo', 'Nuevo productos')

@section('contenido')
<style>
#map {
	height: 400px;
	width: 100%;
}
#elim{
	color:#fff;
	border:1px solid #ccc;
	background-color:#fb0000;
	margin-right:10px;
	padding: 0 2px;
	border-radius:4px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	-o-border-radius:4px;
	border-radius:4px;
	font-weight:bold;
	font-size:0.8em;
	cursor:pointer;
}
#map-canvas {
	height: 100%;
	margin: 0;
}
#map-canvas > .centerMarker {
	position: absolute;
	/*url of the marker*/
	background: url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;
	/*center the marker*/
	top: 50%;
	left: 50%;
	z-index: 1;
	/*fix offset when needed*/
	margin-left: -10px;
	margin-top: -34px;
	/*size of the image*/
	height: 34px;
	width: 20px;
	cursor: pointer;
}
/* css galeria */
#reorder-helper{margin: 18px 10px;padding: 10px;}
.light_box {
	background: #efefef;
	padding: 20px;
	margin: 10px 0;
	text-align: center;
	font-size: 1.2em;
}
.gallery{ width:100%; float:left;padding-left:5px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li{ float:left; background:none; width:25%; height:115px; padding: 0 5px 10px 5px;}
.gallery ul li > div{
	width: 100%;
	height: 100%;
	border: 1px solid #eee;
	display: flex;
	justify-content: center;
	align-items: center;
}
div.imgprincipal img{
	width: 100%;
	height: 100%;
	object-fit: contain;
}
div.imgprincipal .imgp_{
	width: 100%;
	height: 220px;
	position: relative;
}
/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
/* style 6 */
.inputimg + label {
	color: #ec9b1b;
}
.inputimg:focus + label,
.inputimg.has-focus + label,
.inputimg + label:hover {
	color: #d47e03;
}
.inputimg + label figure {
	width: 100px;
	height: 135px;
	background-color: #ec9b1b;
	display: block;
	position: relative;
	padding: 30px;
	margin: 0 auto 10px;
}
.inputimg:focus + label figure,
.inputimg.has-focus + label figure,
.inputimg + label:hover figure {
	background-color: #d47e03;
}
.inputimg + label figure::before,
.inputimg + label figure::after {
	width: 0;
	height: 0;
	content: '';
	position: absolute;
	top: 0;
	right: 0;
}
.inputimg + label figure::before {
	border-top: 20px solid #05354e;
	border-left: 20px solid transparent;
}
.inputimg + label figure::after {
	border-bottom: 20px solid #d47e03;
	border-right: 20px solid transparent;
}
.inputimg:focus + label figure::after,
.inputimg.has-focus + label figure::after,
.inputimg + label:hover figure::after {
	border-bottom-color: #ec9b1b;
}
.inputimg + label svg {
	width: 100%;
	height: 100%;
	fill: #f1e5e6;
}
.contnex{
	background-color: #f5f5f5;
	padding: 15px;
}
.temp_box{
	display: flex;
}
.tb_box{
	text-align: center;
}
</style>

{!! Form::open(['route'=>'shop.store','method'=>'POST','class'=>'dropzone','id'=>'my-dropzone','files'=>true]) !!}
{!! csrf_field() !!}
<select name="ruta[]" id="ruta" multiple="multiple" style='display:none;' ></select>
<select name="sobras[]" id="sobras" multiple="multiple" style='display:none;' ></select>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-6">
				<div class="contnex">
					<div class="form-group">
						{!! Form::label('abreviatura','Abreviatura') !!}
						{!! Form::text('abreviatura',null,['class' =>'form-control', 'placeholder' =>'Nombre corto del producto'])!!}
					</div>
					<div class="form-group">
						{!! Form::label('nombre','Título') !!}
						{!! Form::text('nombre',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
					</div>
					<div class="form-group">
						{!! Form::label('categoria','Categoria') !!}
						{!! Form::select('id_categoriaproducto',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas','id' =>'categorias'])!!}
					</div>
					<div class="form-group">
						{!! Form::label('temporada','Temporada') !!}
						<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="temporada" id="temporada" type="checkbox">
					</div>
					<div class="form-group">
						<div class="temp_box">
							@foreach ($meses as $idx => $mes)
								<div class="tb_box">
									<label for="mes{{$idx}}">{{ $mes }}</label>
									<input type="checkbox" value="1" name="mes{{$idx}}" id="mes{{$idx}}">
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="contnex">
					<div class="form-group">
						<input type="checkbox" data-on="Destacar" data-off="No Destacar"  data-toggle="toggle" name="destacado" id="destacado">
						<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="estado" id="estado" type="checkbox">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('precio','Precio') !!}
								{!! Form::number('precio',null,['class' =>'form-control', 'placeholder' =>'$'])!!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('preciodescuento','Descuento') !!}
								{!! Form::number('preciodescuento',null,['class' =>'form-control', 'placeholder' =>'$'])!!}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('stock','Stock') !!}
								{!! Form::number('stock',null,['class' =>'form-control', 'placeholder' =>'Existencia'])!!}
							</div>
						</div>
						<div class="col-md-6" style="padding-top: 23px;">
							<input data-toggle="toggle" data-on="Grava IVA" data-off="No grava IVA" name="iva" id="iva" type="checkbox">
						</div>

					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{!! Form::label('presentacion','Presentación') !!}
								{!! Form::text('presentacion',null,['class' =>'form-control', 'placeholder' =>'Presentación'])!!}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('peso','Peso') !!}
								{!! Form::text('peso',null,['class' =>'form-control', 'placeholder' =>'Peso'])!!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('medidas','Medidas') !!}
								{!! Form::text('medidas',null,['class' =>'form-control', 'placeholder' =>'Medidas'])!!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-3">
				<div class="imgprincipal">
					<label>Imagen Principal</label>
					<div class="imgp_">
						<label for="imagenprincipal" style="display: block; border: 1px solid #1ab394;height: 100%;">
							{!! Form::file('imagenprincipal',['id'=>'imagenprincipal','accept'=>'image/*','style'=>'display:none;'])!!}
							<img id="imgprincipal" src="{{asset('/assets/images/no-imagen.jpg')}}">
						</label>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<label>Imagen Principal</label>
				<div class="gallery">
					<ul class="reorder_ul reorder-photos-list" style="background: #fff" id="nuevalista">
						@for ($i = 1; $i <= 8 ; $i++)
							<li>
								<div>
									<a href="javascript:void(0);" style="float:none;" class="image_link">
										<input type="file" name="file-{{ $i }}" id="file-{{ $i }}" data-n="{{ $i }}" class="inputimg"  style='display:none;' accept="image/*" />
										<label for="file-{{ $i }}">
											<img id="imgfile-{{ $i }}" style="height: 100px;padding: 6px; width: 100px; object-fit: contain;" src="{{asset('/assets/images/no-imagen.jpg')}}" >
											<a class="btn btn-danger btn-xs deleteImage" id="elim{{ $i }}"  onclick="eliminarElemento('imgfile-{{ $i }}','elim{{ $i }}');" style="display:none;"><i class="fa fa-trash-o"></i></a>
										</label>
									</a>
								</div>
							</li>
						@endfor
					</ul>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="form-group">
			{!! Form::label('descripcion','Contenido') !!}
			{!! Form::textarea('descripcion',null,['class' =>'form-control summernote ckeditor', 'placeholder' =>'Contenido del artículo','required'])!!}
		</div>
		{!! Form::close() !!}

		<div class="form-group">
			{!! Form::hidden('id_usuario', Auth::user()->id, null,[]) !!}
		</div>
		<br>
		<div class="form-group">
			<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Guardar
			</button>
			<a class="btn btn-primary dim" href="{{ URL::asset('shop')}}"><i class="fa fa-chevron-left"></i> Atras
			</a>
		</div>
	</div>
</div>
{!! Form::close() !!}

@endsection

@section('script')
<script>

	$( document ).ready(function() {

		$('.selective').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: false,
			create: function(input) {
				return {
					value: input,
					text: input
				}
			}
		});

	});	
</script>

@include('scripts.productoscript');

@endsection