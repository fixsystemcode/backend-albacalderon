@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">{{ $edit == true ? 'Editar' : 'Nuevo'}} curso</h1>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('contenido')
<style type="text/css">
	.textos{
        text-align: right;
        font-size: 1.2em;        
    }
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    {!! Form::open(['url' => 'cursos', 'method' => 'POST', 'files'=>true]) !!}
        @if ($edit == true)
            <input type="hidden" name="id" id="id" value="{{$cursos->id}}">
        @endif
        <div class="row">
            <div class="control-label col-md-2 textos"><label for="titulo">Título</label></div>
            <div class="col-md-10">
                <input type="text" class="form-control" name="titulo" id="titulo" value="{{ $edit == true ? $cursos->titulo : ''}}">
            </div>
        </div>
       <div class="row">
            {!! Form::label('texto','Descripción',['class'=>'control-label col-md-2 textos']) !!}
            <div class="col-md-10">
                {!! Form::textarea('texto', $edit == true ? $cursos->texto : null,['class' =>'form-control', 'placeholder' =>'Contenido del artículo','required'])!!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-2 textos"><label for="estado" class="control-label">Activar</label></div>
            <div class="col-md-10">
                {!! Form::select('estado', $estados, $edit == true ? getEstadosCursos($cursos->estado) : null ,['class'=>'form-control ','id' => 'estado']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="portada" class="control-label">Portada</label></div>
            <div class="col-md-10">
                @if ($edit == true)
                    <img src="{{ getImagenCursos() }}/{{ $cursos->path }}" alt="" width="300px" onerror="this.onerror=null;this.src='{{ getDefaultImage() }}';">
                @endif
                <input type="file" name="path" id="path">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-2 textos"><label for="fecha_inicio" class="control-label">Fecha inicio</label></div>
            <div class="col-md-10">
                <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="{{ $edit == true ? date('Y-m-d', strtotime($cursos->fechainicio)) : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="fecha_fin" class="control-label">Fecha fin</label></div>
            <div class="col-md-10">
                <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="{{ $edit == true ? date('Y-m-d', strtotime($cursos->fechafin)) : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="usuario" class="control-label">Tutor</label></div>
            <div class="col-md-10">
                {!! Form::select('usuario', $usuarios, $edit == true ? $cursos->id_usuario : null ,['class'=>'form-control ','id' => 'usuario']) !!}
            </div>
        </div>
        <div class="row" style="text-align: right; margin-right: 2px">
            <button class="btn btn-primary" type="submit">{{ $edit == true ? 'Actualizar' : 'Guardar'}}</button>
        </div>
   {!! Form::close() !!}
    <br>
    @if ($edit == true)
        <h2>Módulos <button class="btn btn-secundary btn-sm text-success" type="button" onclick="abrirModal()"><h3>+</h3></button></h2>
        <hr>
        <div id="divModulos">
           
        </div>
    @endif
    

    <div id="modulo" class="modal fade text-dark">
        <form id="form_modulo">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modulo <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></h4>
                        
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                           <input type="hidden" id="id_modulo">
                           <div class="row">
                               <label for="nombre_modulo">Nombre</label>
                               <input type="text" class="form-control" name="nombre_modulo" id="nombre_modulo">
                           </div>
                           <div class="row">
                               <label for="descripcion_modulo">Descripción</label>
                               <textarea name="descripcion_modulo" id="descripcion_modulo" rows="10" class="form-control"></textarea>
                           </div>
                           <div class="row">
                                <label for="estado_modulo">Estado</label>
                                {!! Form::select('estado_modulo', getEstadosModulos(), null,['class'=>'form-control ','id' => 'estado_modulo']) !!}
                           </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="guardar()">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
<script>
	$( document ).ready(function() {
        @if ($edit==true)
            modulos();
        @endif
	});
    function abrirModal(){

        $('#form_modulo')[0].reset();
        $('#id_modulo').val("");
        $('#modulo').modal('show');
        
    }
    function modulos(){
         var id_articulo = $('#id').val();
         $.ajax({
            url:'{{ url('modulos-articulo') }}/'+id_articulo,
            data: {},
            type: 'GET',
            success:function(response){
                renderModulos(response);
            }, 
            error: function(j){
                console.log(j);
            }
       });
    }

    function getModulo(id){
         $.ajax({
            url:'{{ url('modulo') }}/'+id,
            data: {},
            type: 'GET',
            success:function(data){
                $('#id_modulo').val(data.id);
                $('#nombre_modulo').val(data.nombre);
                $('#descripcion_modulo').val(data.descripcion);
                $('#estado_modulo').val(data.estado);
                $('#modulo').modal('show');
            }, 
            error: function(j){
                console.log(j);
            }
       });
    }

    function renderModulos(data){
        contenedor = $('#divModulos').empty();
        if(data.length == 0){
            contenedor.append('<span class="badge badge-warning">Este curso no contiene ningun módulo</span>');
        }else{
            $.each(data, function(i,item){
                row = $('<div>').attr('class','row');
                col1 = $('<div>').attr('class','col-md-10');
                col2 = $('<div>').attr('class', 'col-md-2'); 
                col1.append(item.nombre + " - ("+ getEstadosModulos(item.estado) +")");
                col2.append($('<button>').attr('class', 'btn btn-link text-success').attr('onclick', 'getModulo('+ item.id +')').attr('type', 'button')
                    .append($('<span>').attr('class', 'fa fa-cog')));
                col2.append($('<a>').attr('class', 'btn btn-link text-warning').attr('href', '{{ url('modulo') }}/'+item.id+'/edit').attr('type', 'button')
                    .append($('<span>').attr('class', 'fa fa-edit')));
                // col2.append($('<button>').attr('class', 'btn btn-link text-danger').attr('onclick', '').attr('type', 'button')
                //     .append($('<span>').attr('class', 'fa fa-trash')));

                row.append(col1).append(col2);
                contenedor.append(row);
            });
        }
    }

    function guardar(){
        var nombre = $('#nombre_modulo').val();
        var descripcion = $('#descripcion_modulo').val();
        var id_articulo = $('#id').val();
        var id_modulo = $('#id_modulo').val();
        var estado = $('#estado_modulo').val();
       $.ajax({
            url:'{{ url('modulo') }}',
            data: { 
                'descripcion': descripcion, 
                'nombre': nombre, 
                'id_modulo': id_modulo, 
                'id_articulo': id_articulo,  
                'estado': estado
            },
            type: 'POST',
            success:function(response){
                $('#modulo').modal('hide');
                renderModulos(response);
            }, 
            error: function(j){
                console.log(j);
            }
       });
    }
	</script>
@endsection
