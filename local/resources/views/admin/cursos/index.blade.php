@extends('layouts.backend')

@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Cursos</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('cursos.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')

    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <!-- <br>
    <a class="btn btn-primary" href="{{ URL::route('cursos.create') }}"> <i class="fa fa-plus"></i> Nuevo</a>
    <br> -->
    <div class="row" id="cursos" >
        
    </div>
    <div id="paginador"></div>

@endsection

@section('script')
<script>
    $(document).ready(function(){           
        listaCursos();
    });

    function listaCursos(page, search) {
        $.ajax({
            type: 'POST',
            url: '{{ url("cursos/filtrar") }}',
            data: {}
        }).done(function(data) {
           renderTableCursos(data);
        }).fail(validacionCampos).always(function(){
        });
    }

    function renderTableCursos(data){
        contenedor = $('#cursos').empty();
        $.each(data.tabla, function(i,item){
            identifica =  'in_curso'+item.id;
            panel = $('<div>').attr('class','col-lg-4');
            card = $('<div>').attr('class','thumbnail');
            card_head = $('<div>').attr('class', ''); 
            card_body = $('<div>').attr('class', 'caption');              

            //IMAGEN
            img  = $('<img>');
            img.attr('class', 'card-img-top mt-1').attr('src', '{{ getImagenCursos() }}/'+item.path).attr('style', 'width:100%')
                .attr('onerror', "this.onerror=null;this.src='{{ getDefaultImage() }}';");
            card_head.append(img);

            card_body.append($('<h2>').attr('style', 'font-weight:bold').text(item.titulo));
            card_body.append($('<p>').append(item.texto));
            card_body.append($('<p>').append('<strong>Fecha Inicial: </strong>: ' + fechaHumana(item.fechainicio)));
            card_body.append($('<p>').append('<strong>Fecha Culminación: </strong>: ' + fechaHumana(item.fechafin)));
            card_body.append($('<p>').append('<strong>Tutor: </strong>' + item.nombres + ' ' + item.apellidos));

            card_links = $('<div>').attr('style', 'text-aling:center');
            card_links.append('<a href="{{ url('cursos') }}/'+item.id+'" class="card-link"> <i class="fa fa-eye"></i> Mostrar</a>')
             card_links.append('<a href="{{ url('cursos') }}/'+item.id+'/edit" class="card-link text-warning"> <i class="fa fa-edit"></i> Editar</a>')
            card_body.append(card_links);
            card.append(card_head).append(card_body);
            panel.append(card);
            contenedor.append(panel);
        });
    }
</script>
@endsection