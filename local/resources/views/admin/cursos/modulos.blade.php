@extends('layouts.backend')

@section('titulo', $modulo->curso->titulo. ' / '. $modulo->nombre)

@section('contenido')

<style type="text/css">
#map {
    height: 400px;
    width: 100%;
}
#elim{
    color:#fff;
    border:1px solid #ccc;
    background-color:#fb0000;
    margin-right:10px;
    padding: 0 2px;
    border-radius:4px;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    -o-border-radius:4px;
    border-radius:4px;
    font-weight:bold;
    font-size:0.8em;
    cursor:pointer;
}
/* css galeria */
#reorder-helper{margin: 18px 10px;padding: 10px;}
.light_box {
    background: #efefef;
    padding: 20px;
    margin: 10px 0;
    text-align: center;
    font-size: 1.2em;
}
.gallery{ width:100%; float:left; margin-top:10px;padding-left:5px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
/* style 6 */
.inputimg + label {
    color: #ec9b1b;
}
.inputimg:focus + label,
.inputimg.has-focus + label,
.inputimg + label:hover {
    color: #d47e03;
}
.inputimg + label figure {
    width: 100px;
    height: 135px;
    background-color: #ec9b1b;
    display: block;
    position: relative;
    padding: 30px;
    margin: 0 auto 10px;
}
.inputimg:focus + label figure,
.inputimg.has-focus + label figure,
.inputimg + label:hover figure {
    background-color: #d47e03;
}
.inputimg + label figure::before,
.inputimg + label figure::after {
    width: 0;
    height: 0;
    content: '';
    position: absolute;
    top: 0;
    right: 0;
}
.inputimg + label figure::before {
    border-top: 20px solid #05354e;
    border-left: 20px solid transparent;
}
.inputimg + label figure::after {
    border-bottom: 20px solid #d47e03;
    border-right: 20px solid transparent;
}
.inputimg:focus + label figure::after,
.inputimg.has-focus + label figure::after,
.inputimg + label:hover figure::after {
    border-bottom-color: #ec9b1b;
}
.inputimg + label svg {
    width: 100%;
    height: 100%;
    fill: #f1e5e6;
}
#imgprincipal{
    max-width: 100%;
    height: 300px;
    display: block;
    object-fit: cover!important;
    margin: auto;
}
</style>

{!!Form::model($modulo,['route'=>['modulo.update',$modulo->id],'method'=>'PUT','files' => true,'id'=>'my-dropzone'])!!}
    <input type="hidden" id="doc">
    <input type="hidden" id="btncancelardoc">
    <input type="hidden" name="publicar" value="0">

    <select name="rutadocs[]" id="rutadocs" multiple="multiple" style='display:none;'></select>
    <select name="titulodocs[]" id="titulodocs" multiple="multiple" style='display:none;'></select>
    <select name="sobrasdocs[]" id="sobrasdocs" multiple="multiple" style='display:none;'></select>

    <select name="videos[]" id="videos" multiple="multiple" style='display:none;'></select>
    <select name="titulovideo[]" id="titulovideo" multiple="multiple" style='display:none;'></select>
    <select name="sobrasvideos[]" id="sobrasvideos" multiple="multiple" style='display:none;' ></select> 

    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            <div class="form-group" style="background: #ddeff4;border: 1px solid #1ab394;padding: 6px;padding-top: 15px;text-align: center;">
                <input type="file" id="nuevo_documento" class="inputfile"  style='display:none;'  />
                <label for="nuevo_documento" class="btn btn-primary" style="color: #fff;">
                    <img style="height: 20px;" src="{{asset('/assets/images/newdoc.png')}}" >
                    Agregar Documento
                </label>

                <div id="upload-progress" class="row" style="width: 100%; display: none" >
                    <div class="col-xs-10">
                        <div class="progress">
                            <div id="porcentaje" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                        </div>
                    </div>
                    <div class="col-xs-2">                  
                        <button class="btn btn-danger btn-block btn-xs" type="button" id="cancelar_documento"> <i class="fa fa-times"></i> Cancelar</button>
                    </div>
                </div>

                <ul class="reorder-documentos-list reorder_ul list-group" id="listadocs" style="background: #fff">
                    @foreach ($docs as $doc)
                        <li id="{{$doc->path}}" class="list-group-item">
                            <a onclick="eliminardoc(this)" title="Eliminar Documento">
                                <img style='height: 20px;' src='{{URL::asset('/assets/images/elidoc.png')}}'>
                            </a>
                            <a href='{{URL::asset('/local/public/doc/'.$doc->path)}}' target='_blank' title='Visualizar documento' class="btn btn-link">
                                {{$doc->titulo}}
                            </a>
                        </li>
                    @endforeach
                </ul>

            </div>

            <div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;text-align: center;">
                <p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Lista de Videos</p>
                <ul class="reorder-videos-list reorder_ul list-group" id="listaDesordenada" name="videos">
                    @foreach ($mis_videos as $vide)
                    <li id="{{$vide->path}}" class="list-group-item active">
                        <span onclick="eliminar(this)" title="Eliminar Video de la Lista">
                            <img  style="height: 25px;" src="{{asset('/assets/images/delvideo.png')}}" >
                        </span>
                        &nbsp;
                        <a href="javascript:void(0);" style="float:none; color: white;" class="image_link">
                            {{$vide->titulo}}
                        </a>
                        <span onclick="modificamodal('{{$vide->path}}')" title="Reproducir Video">
                            <a href="" data-target="#modal-vervideo" data-toggle="modal">
                                &nbsp;
                                <img  style="height: 25px;" src="{{asset('/assets/images/play.png')}}">
                            </a>
                        </span>
                    </li>
                    @endforeach
                </ul>
                
                <div class="input-group">
                    {!! Form::text('video',null,['class' =>'form-control', 'id'=>'nuevo_video' ,'placeholder' =>'URL YouTube/Facebook/Vimeo de video'])!!}
                    <span class="input-group-btn">
                        <button class="btn btn-primary"  type="button" id="btnagregavideo" align="center" onclick="return add_li()" >
                            <img style="height: 20px;" src="{{asset('/assets/images/addvideo.png')}}" > Agregar Video
                        </button>
                    </span>
                </div>
                
                <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-vervideo"  >
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button>
                                <h4 class="modal-title">Visualizar Video</h4>
                            </div>
                            <div class="modal-body">
                                <div id="reproductor" >
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  

        </div>

        <div class="col-md-12">
            <input type="checkbox" id="inicio" name="inicio" style="display:none;">
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-floppy-o"></i> Actualizar
            </button>
            <a class="btn btn-warning" href="{{ URL::to('cursos') }}"><i class="fa fa-chevron-left"></i> Atras</a>
        </div>

        <div class="clearfix"></div>
    </div>

{{-- {!! Form::close() !!} --}}

@endsection

@section('script')
<script>

    //$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } } );

    $( document ).ready(function() {

        $('.selective').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            }
        });

    }); 

    var updateProgressBar = function (progress, value) {
        var $div = $(progress);//.find('div');
        //var $span = $div.find('span');
        $div.attr('aria-valuenow', value);
        $div.css('width', value + '%');
        $div.text(value + '%');
    }

    /* CARGA DE ARCHIVOS */
    $( document ).ready(function() {

        $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } } );

        var nuevo_documento = document.getElementById('nuevo_documento');
        var div_progress = document.getElementById('upload-progress');
        var progress = document.getElementById('porcentaje');
        var cancelar_documento = document.getElementById('cancelar_documento');

        var imagenPrincipalInput = document.getElementById('imagenprincipal');
        var imagenPrincipal = document.getElementById('imgprincipal');

        var subirDocumentoAjax;

        nuevo_documento.onchange = function (e) {
            var documento = this.files[0];
            var nombre = documento.name.replace(/ /g,"-");
            nombre =  Math.floor(Math.random() * 10000001) + nombre;

            var formData = new FormData();
            formData.append('path', nombre);
            formData.append('archivo', documento);

            subirDocumentoAjax = $.ajax({
                url: "{{URL::asset('uploaddoc')}}",
                type: "post",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(div_progress).css('display', 'block');
                    progress.className = 'progress-bar progress-bar-striped progress-bar-success active';
                    updateProgressBar(progress, 0);
                },
                xhr: function(){
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = Math.round((evt.loaded / evt.total)*100);
                            updateProgressBar(progress, percentComplete);
                        }
                    }, false);
                    return xhr;
                },
                success: function(data){
                    progress.className = 'progress-bar progress-bar-striped progress-bar-success';                          
                    updateProgressBar(progress, 100);
                    div_progress.style.display = 'none';
                    var list=document.createElement('li');
                    list.id=nombre;
                    list.className = 'list-group-item';
                    list.innerHTML = "<a onclick='eliminardoc(this)' title='Eliminar Documento'><img style='height: 20px;' src='{{URL::asset('/assets/images/elidoc.png')}}'></a> ";
                    list.innerHTML+= "<a href='{{URL::asset('/local/public/doc')}}/"+nombre+"' target='_blank' title='Visualizar documento' class='btn btn-link'>"+documento.name+"</a>";
                    document.getElementById("listadocs").appendChild(list);
                    
                    var option=document.createElement("option");
                    option.value=nombre;
                    option.text=nombre;
                    option.selected=true;
                    document.getElementById("rutadocs").appendChild(option);

                    option.value=documento.name;
                    option.text=documento.name;
                    document.getElementById("titulodocs").appendChild(option);
                    nuevo_documento.value = "";

                },
                error: function(data){
                    nuevo_documento.value = "";
                    progress.className = 'progress-bar progress-bar-striped progress-bar-danger';
                    updateProgressBar(progress, 100);
                    progress.innerHTML="Documento no Subido";
                    window.setTimeout(function(){div_progress.style.display = 'none'}, 3000);
                }

            });
        }

        cancelar_documento.onclick = function(e){
            if (subirDocumentoAjax != null) {
                subirDocumentoAjax.abort();
                subirDocumentoAjax = null;
            }
        };
        /* FIN CARGA DE ARCHIVOS */

        //galeria dinamica
        $("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
        $("ul.reorder-documentos-list").sortable({ tolerance: 'pointer' });
        $("ul.reorder-videos-list").sortable({ tolerance: 'pointer' });
        $('#reorder-helper').slideDown('slow');
        $('.image_link').attr("href","javascript:void(0);");
        $('.image_link').css("cursor","move");


        $("#my-dropzone").on('submit', function(evt){
            $("#ruta").empty();
            $("ul.reorder-photos-list li a label img").each(function() {
                if ($(this).attr('name')!=undefined) {
                    if ($(this).attr('name')!="nada")   {
                        var option=document.createElement("option");
                        option.value=$(this).attr('name');
                        option.text=$(this).attr('name');
                        option.selected=true;
                        document.getElementById("ruta").appendChild(option)
                    }
                }
            });
            $("#videos").empty();
            $("ul.reorder-videos-list li").each(function() {
                var option=document.createElement("option");
                option.value=$(this).attr('id');
                option.text=$(this).attr('id');
                option.selected=true;
                document.getElementById("videos").appendChild(option);
            });
            $("#titulovideo").empty();
            $("ul.reorder-videos-list li a").each(function() {
                if ($(this)["0"].innerText!="") {
                    var option=document.createElement("option");
                    option.value=$(this)["0"].innerText;
                    option.text=$(this)["0"].innerText;
                    option.selected=true;
                    document.getElementById("titulovideo").appendChild(option);
                }
            });
            $("#rutadocs").empty();
            $("#titulodocs").empty();
            $("ul.reorder-documentos-list li").each(function() {
                var option=document.createElement("option");
                option.value=$(this).attr('id');
                option.text=$(this).attr('id');
                option.selected=true;
                document.getElementById("rutadocs").appendChild(option);
                var option=document.createElement("option");
                option.value=$(this)["0"].innerText;
                option.text=$(this)["0"].innerText;
                option.selected=true;
                document.getElementById("titulodocs").appendChild(option);
            });
        });

    });
/**
* Funcion que añade un <li> dentro del <ul>
*/
function add_li()
{
    var nuevoLi=document.getElementById("nuevo_video").value;
    var expreg = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    if(expreg.test(nuevoLi))
    {
        if(find_li(nuevoLi))
        {
            if (nuevoLi.indexOf("youtube.com") != -1) {
                var videourl = nuevoLi;
                var titulo="";
                videourl =videourl.substr(nuevoLi.indexOf("?v=")+3,11);
                url = 'https://www.googleapis.com/youtube/v3/videos?id='+videourl+'&key=AIzaSyBttQXTDr6P1tJy90K6LMXj7KcVFfr-Gb0&fields=items(id,snippet(title))&part=snippet';
                datos = {};
                    //obtener titulo dle vidoe Youtube
                    $.getJSON(url, function(datos){
                        $.each(datos.items,function(indice){
                            titulo=datos.items[indice].snippet.title;
                            var lis=document.createElement('li');
                            lis.id=nuevoLi;
                            lis.innerHTML="<span onclick='eliminar(this)' title='Eliminar Video de la Lista'> <img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}' ></span> &nbsp <a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")' title='Reproducir Video'> &nbsp<a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('/assets/images/play.png')}}'></span></a>";
                            document.getElementById("listaDesordenada").appendChild(lis);
                            var options=document.createElement("option");
                            options.value=titulo;
                            options.text=titulo;
                            options.selected=true;
                            document.getElementById("titulovideo").appendChild(options)
                        });
                        document.getElementById(nuevoLi).className = "list-group-item active";
                    });
                }
                if (nuevoLi.indexOf("youtu.be/") != -1) {
                    var videourl = nuevoLi;
                    var titulo="";
                    videourl =videourl.substr(nuevoLi.indexOf("be")+3,11);
                    url = 'https://www.googleapis.com/youtube/v3/videos?id='+videourl+'&key=AIzaSyBttQXTDr6P1tJy90K6LMXj7KcVFfr-Gb0&fields=items(id,snippet(title))&part=snippet';
                    datos = {};
                    //obtener titulo del video Youtube
                    $.getJSON(url, function(datos){
                        $.each(datos.items,function(indice){
                            titulo=datos.items[indice].snippet.title;
                            var lis=document.createElement('li');
                            lis.id=nuevoLi;
                            lis.innerHTML="<span onclick='eliminar(this)'> <img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}'></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('assets/images/play.png')}}'></span></a>";
                            document.getElementById("listaDesordenada").appendChild(lis);
                            var options=document.createElement("option");
                            options.value=titulo;
                            options.text=titulo;
                            options.selected=true;
                            document.getElementById("titulovideo").appendChild(options)
                        });
                        document.getElementById(nuevoLi).className = "list-group-item active";
                    });
                }
                if (nuevoLi.indexOf("vimeo.com") != -1) {
                    var videourl = nuevoLi;
                    var titulo="";
                    videourl =videourl.substr(nuevoLi.indexOf("om")+3,10);
                    url = 'https://vimeo.com/api/v2/video/'+videourl+'.json';
                    datos = {};
                    //obtener titulo del viideo de vimeo
                    $.getJSON( url, function( datos ){
                        $.each(datos,function(indice){
                            titulo=datos[indice].title;
                            var lis=document.createElement('li');
                            lis.id=nuevoLi;
                            lis.innerHTML="<span onclick='eliminar(this)'><img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}' ></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+"</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('assets/images/play.png')}}'></span></a>";
                            document.getElementById("listaDesordenada").appendChild(lis);
                            var options=document.createElement("option");
                            options.value=titulo;
                            options.text=titulo;
                            options.selected=true;
                            document.getElementById("titulovideo").appendChild(options)
                        });
                        document.getElementById(nuevoLi).className = "list-group-item active";
                    });
                }
                if (nuevoLi.indexOf("facebook.com") != -1) {
                    var videourl = nuevoLi;
                    var titulo="";
                    var inicio,fin=0;
                    inicio=nuevoLi.indexOf("pcb.");
                    if (inicio != -1) {
                        fin=nuevoLi.indexOf("/",inicio+4);
                        videourl= videourl.slice(inicio+4,fin);
                    }else{
                        inicio=nuevoLi.indexOf("os/");
                        fin=nuevoLi.indexOf("/",inicio+3);
                        videourl =videourl.slice(inicio+3,fin);
                    }
                    url = 'https://graph.facebook.com/v2.9/'+videourl+'?access_token=EAAJKkknnwgMBAKCeXZBr9eIZCJ0xwzQadWdCaev9YekGIZCSOLZBbYe7lXKuSgEdvChFS3whVEKliMSMxgE2pCcydDCvAdZBYZBLmvI1yfQlUy3OisqS55blaT2qtYqfr4jXJtzHyJxSscEd5e7BHPozi9nQdVJFxwJLh46sn4NgZDZD';
                    datos = {};
                    //obtener titulo del video
                    $.getJSON(url, function(datos){
                        $.each(datos,function(indice){
                            titulo=datos.description;
                            var lis=document.createElement('li');
                            lis.id=nuevoLi;
                            lis.innerHTML="<span onclick='eliminar(this)'><img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}'></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('assets/images/play.png')}}' ></span></a>";
                            document.getElementById("listaDesordenada").appendChild(lis);
                            var options=document.createElement("option");
                            options.value=titulo;
                            options.text=titulo;
                            options.selected=true;
                            document.getElementById("titulovideo").appendChild(options)
                            return false;
                        });
                        document.getElementById(nuevoLi).className = "list-group-item active";
                    });
                }
                //Ahora para crear el objeto option que le vas a añadir seria
                var option=document.createElement("option");
                option.value=nuevoLi;
                option.text=nuevoLi;
                option.selected=true;
                document.getElementById("videos").appendChild(option)
            }
        }else{
            alert("Ingrese URL válida");
            document.getElementById("nuevo_video").value="";
            return false;
        }
        document.getElementById("nuevo_video").value="";
    }

    function modificamodal(url){
        reproductor= document.getElementById("reproductor");
        if (url.indexOf("youtube.com") != -1) {
            url=url.substr(url.indexOf("?v=")+3,11);
            reproductor.innerHTML="<iframe width='500' height='400' src='https://www.youtube.com/embed/"+url+"?rel=0' frameborder='0'  encrypted-media' allowfullscreen></iframe>";
        }
        if (url.indexOf("youtu.be/") != -1) {
            url=url.substr(url.indexOf("be")+3,11);
            reproductor.innerHTML="<iframe width='500' height='400' src='https://www.youtube.com/embed/"+url+"?rel=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>";
        }
        if (url.indexOf("vimeo.com") != -1) {
            url=url.substr(url.indexOf("om")+3,10);
            reproductor.innerHTML="<iframe src='https://player.vimeo.com/video/"+url+"' width='500' height='400' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
        }
        if (url.indexOf("facebook.com") != -1 ) {
            var inicio,fin=0;
            inicio=url.indexOf("pcb.");
            if (inicio != -1) {
                fin=url.indexOf("/",inicio+4);
                url= url.slice(inicio+4,fin);
            }else{
                inicio=url.indexOf("os/");
                fin=url.indexOf("/",inicio+3);
                url =url.slice(inicio+3,fin);
            }
            reproductor.innerHTML="<iframe src='https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook%2Fvideos%2F"+url+"%2F&width=500&show_text=false&appId=571198976560471&height=280' width='500' height='280' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true' allowFullScreen='true'></iframe>";
        }
    }
/**
* Funcion que busca si existe ya el <li> dentrol del <ul>
* Devuelve true si no existe.
*/
    function find_li(contenido)
    {
        if (contenido.indexOf("youtube.com") != -1 ||contenido.indexOf("youtu.be") != -1 || contenido.indexOf("facebook.com") != -1 || contenido.indexOf("vimeo.com") != -1) {
            $("#listaDesordenada li").each(function(){
                if(this.id == contenido){
                    return false;
                }
            });
        }else{
            alert("Solo se aceptan videos de Youtube, Facebook y Vimeo");
            return false;
        }
        return true;
    }
/**
* Funcion para eliminar los elementos
* Tiene que recibir el elemento pulsado
*/
    function eliminar(elemento)
    {
        var id=elemento.parentNode.getAttribute("id");
        var option=document.createElement("option");
        option.value=id;
        option.text=id;
        option.selected=true;
        document.getElementById("sobrasvideos").appendChild(option);
        node=document.getElementById(id);
        node.parentNode.removeChild(node);
        $("#videos option[value='"+id+"']").remove();
    }
    function eliminardoc(elemento)
    {
        var id=elemento.parentNode.getAttribute("id");
        node=document.getElementById(id);
        node.parentNode.removeChild(node);
        $("#rutadocs option[value='"+id+"']").remove();
        var option=document.createElement("option");
        option.value=id;
        option.text=id;
        option.selected=true;
        document.getElementById("sobrasdocs").appendChild(option);
    }
    function eliminarElemento(id,elim){
        imagen = document.getElementById(id);
        elimin=document.getElementById(elim);
        if (!imagen.id){
            alert("El elemento selecionado no existe");
        } else {
            var option=document.createElement("option");
            option.value=imagen.name;
            option.text=imagen.name;
            option.selected=true;
            document.getElementById("sobras").appendChild(option);
            imagen.src="{{URL::asset('assets/images/no-imagen.jpg')}}";
            elimin.style="display:none;";
            imagen.name="nada";
        }
    }
</script>

@endsection