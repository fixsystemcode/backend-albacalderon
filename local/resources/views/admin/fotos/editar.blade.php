@extends('layouts.backend')


@section('contenido')

@include('alerts.request')

<h1>Editar Tipo de Usuario</h1>


{!! Form::model($img, array('method' => 'PATCH', 'route' => array('imagen.update', $img->id) , 'files' => true)) !!}
    
    
  <div class="row">
        <div class="col-md-6">
          {!! Form::label('imagen','Imagen') !!}
          {!! Form::file('path')!!}
        </div>

        <div class="col-md-6">

                <img src="{{asset('images')}}/{{$img->path}}"  style ="width: 100px">

        </div>  

    </div>


   

    {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}



{!! Form::close() !!}



@stop