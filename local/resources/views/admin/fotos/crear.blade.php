@extends('layouts.backend')


@section('contenido')

@include('alerts.request')

<h1>Ingresar Imagen</h1>


 {!! Form::open(['route' => 'imagen.store', 'method' => 'POST','files'=>true]) !!}

    <div class="form-group">
          {!! Form::label('imagen','Imagen') !!}
          {!! Form::file('path')!!}
    </div>

    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
 
{!! Form::close() !!}


@stop