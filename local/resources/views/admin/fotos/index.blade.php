@extends('layouts.backend')

@section('ruta')
Listado de Usuarios
@stop

@section('titulo')
Usuarios
@stop

@section('contenido')



@stop

@section('contentPanel')
@include('alerts.success')
    <div class="row">
 
        @foreach ($imgs2 as $img)

               <div class="col-md-2" style=" padding: 15px;">
                   @if(empty($img->path))

                   <td><img src="{{asset('assets/images')}}/no-imagen.jpg"  style="width: 100%;height: 100px!important;object-fit: cover!important;"></td>
                   @else
                   <td><img src="{{asset('local/public/galeria')}}/{{$img->path}}"  style="width: 100%;height: 100px!important;object-fit: cover!important;"></td>
                   @endif
                   <td>
                      {!! Form::open(array('method' => 'DELETE', 'route' => array('galeria.destroy', $img->id))) !!}
                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                      {!! Form::close() !!}
                   </td>
               </div>
        @endforeach
    </div>
@endsection