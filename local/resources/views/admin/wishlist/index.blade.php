@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Favoritos</h1>
</div>
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}</form>
	@if (count($wishlist))
    <br>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Favorito</th>
                        <th>Ver</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($wishlist as $producto)
                        <tr>
                            <td>{{ $producto->wishlistProducto->nombre }}</td>
                            <td>{!! $producto->wishlistProducto->descripcion !!}</td>
                            <td>$ {{ $producto->wishlistProducto->precio }}</td>
                            <td style="text-align: center;"><span class="badge badge-primary">A {{ $producto->cantidad }} clientes le gusta</span></td>
                            <td><a href="{{ URL::to('producto') }}/{{ $producto->id_producto }}" class="btn btn-info"> <span class="fa fa-eye"></span> </a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
    @else
        <br>
       <div class="alert alert-warning" role="alert">
            No ha marcado ningun producto como favorito</a>
        </div>
    @endif
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
	function eliminar(id){
       $("#formDelete").attr("action", '{{ URL::to('pedidos') }}/'+id).submit();
    }
	</script>
@endsection
