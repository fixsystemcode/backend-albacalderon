<?php
//ESTAS FUNCIONES TIENE QUE SALIR DE AQUI
function crearLinea($menu, $mostrar){
	$tiene_hijos = isset($menu->children);
	$padding = 'padding-left:'.(($menu->nivel - 1) * 30 ).'px';
	$existe_permiso = ($menu->permiso_id != null);
	$text = '
	<tr class="text-left nivel nivel-'.$menu->nivel.'" padre="per'.$menu->padre.'">
		<td style="'.$padding.'">
			<input type="hidden" name="permisos['.$menu->id.'][permiso_id]" value="'.$menu->permiso_id.'">';
	if($mostrar){
		$text.= getInfoPermiso($existe_permiso);
	}else {
		$text.='<input name="permisos['.$menu->id.'][estado]" id="per'.$menu->id.'" class="seleccionar_permiso" '.($existe_permiso ? "checked" : "" ).' type="checkbox" accion="no_editar" hijos="'.$tiene_hijos.'">';
	}
	$text.='
		</td>
		<td style="'.$padding.'"><label for="per'.$menu->id.'">'.$menu->nombre.'</label></td>
		<td>'.$menu->ruta.'</td>
		<td class="info iguales">'.getSelectPermiso('crear', $menu->crear, $menu->permiso_id, $menu->id, $mostrar).'</td>		
		<td class="info iguales">'.getSelectPermiso('editar', $menu->editar, $menu->permiso_id, $menu->id, $mostrar).'</td>
		<td class="info iguales">'.getSelectPermiso('eliminar', $menu->eliminar, $menu->permiso_id, $menu->id, $mostrar).'</td>
	</tr>';
	if($tiene_hijos){
		foreach ($menu->children as $sub_menu){
			$text.= crearLinea($sub_menu, $mostrar);
		}
	}
	return $text;
}
function getOculto($permiso_id){
	return ($permiso_id == null ? "oculto" : "" );
}
function getInfoPermiso($estado){
	return '<span class="text-'.($estado == 1 ? "success" : "danger").'">
				<i class="fa fa-'.($estado == 1 ? "check" : "times").'"></i></span>';
}
function getSelectPermiso($permiso, $estado, $permiso_id, $menu_id, $mostrar){
	if($mostrar) return getInfoPermiso($estado);
	return '<select name="permisos['.$menu_id.']['.$permiso.']" accion="no_editar" class="form-control permiso '.getOculto($permiso_id).'">
		<option value="1">Si</option>
		<option '. ($estado== 0 ? "selected" : "").' value="0">No</option>
	</select>';
}
?>
@extends('layouts.backend')
@section('titulo', 'Permisos por usuario')
@section('contenido')
	
	@include('alerts.success')
	@include('alerts.errors')

	<style type="text/css">		
		.nivel:hover td{
		    background: #d9edf7 !important;
		}
		.nivel-1 td:nth-child(1),.nivel-1 td:nth-child(2){
			padding-left: 5px !important;
		}
		.nivel-1:hover td{
		    background: #000 !important;
		}
		.nivel-1 td {
		    background: #74b7d9 !important; /*#8e9193*/
		    font-weight: 800 !important;
		    color: white;
		}
		.permiso {
		    text-align: center;
		    padding: 0px 0px;
    		border-left: 1px solid;
    		height: 24px;
		}
		.oculto{
			display: none;
		}
		.iguales{
			width: 90px;
			text-align: center;
		}
	</style>
	<div class="row">
		<div class="card col-md-10 col-md-offset-1">
			<div class="card-body">
				<form action="{{ route('permisos.store') }}"  method="POST">
					{{ csrf_field() }}
					@if($puede_editar)
						@if(!isset($solo_ver))
							<button type="submit" class="btn btn-success">
								<i class="fa fa-save"></i> Guardar permisos
							</button>
						@else
							<a href="{{ route('permisos.edit', $perfil->id) }}" class="btn btn-info">
								<i class="fa fa-pencil"></i> Editar permisos
							</a>
						@endif
					@endif

					<h2 style="border-bottom: 1px solid gray; ">
						Permisos del perfil "{{ $perfil->nombre }}"
					</h2>
					<input type="hidden" name="perfil" value="{{ $perfil->id }}">
					<table class="table table-sm table-bordered">
						<thead>
							<tr>
								<th>VER</th>
								<th>Permiso</th>
								<th>Ruta</th>
								<th class="info iguales">CREAR</th>
								<th class="info iguales">EDITAR</th>
								<th class="info iguales">ELIMINAR</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($permisos as $menu)
								{!! crearLinea($menu, isset($solo_ver)) !!}					
							@endforeach
						</tbody>
					</table>
					@if(!isset($solo_ver) && $puede_editar)
						<button type="submit" class="btn btn-success">
							<i class="fa fa-save"></i> Guardar permisos
						</button>
					@endif
				</form>
			</div>
		</div>
	</div>				
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.seleccionar_permiso').on('change', function(){
				var btn = $(this);
				var padre = btn.attr('id');
				if(btn.attr('hijos') == '1'){
					$('[padre='+padre+']').find('.seleccionar_permiso').prop('checked',btn.is(':checked') );
					if(btn.is(':checked') ){
						$('[padre='+padre+']').find('select.permiso').removeClass('oculto');
					}else{
						$('[padre='+padre+']').find('select.permiso').addClass('oculto');
					}
				}
				var parent_id = btn.parent().parent().attr('padre');
				if(btn.is(':checked')){
					$('#'+parent_id).prop('checked',true);
				}

				if(btn.is(':checked')){
					btn.parent().parent().find('select.permiso').removeClass('oculto');
				}else{
					btn.parent().parent().find('select.permiso').addClass('oculto');
				}
			});
		})
	</script>
@endsection