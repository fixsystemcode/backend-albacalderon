@extends('layouts.backend')

@section('ruta', 'Gertor CMS FIXSYSTEM')

@section('titulo','Administración Portal Web | Gestor CMS FIXSYSTEM')

@section('script')
@endsection

@section('contenido')
<div class="row">
	<div class="col-lg-3 col-md-6">
		<a href="{{ route('usuarios.index') }}">
			<div class="wiget_fs">
				<div class="wfs_head">
					<img src="{{asset('admin/images/user.png')}}" alt="">
				</div>
				<div class="wfs_text">
					<div class="wfst_">
						<h1>{{$cusuarios}}</h1>
						<h5>Usuarios</h5>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-6">
		<a href="{{ URL::asset('articulos') }}">
			<div class="wiget_fs">
				<div class="wfs_head">
					<img src="{{asset('admin/images/articulo.png')}}" alt="">
				</div>
				<div class="wfs_text">
					<div class="wfst_">
						<h1>{{$cusuarios}}</h1>
						<h5>Artículos</h5>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-6">
		<a href="{{ URL::asset('animacion') }}">
			<div class="wiget_fs">
				<div class="wfs_head">
					<img src="{{asset('admin/images/image.png')}}" alt="">
				</div>
				<div class="wfs_text">
					<div class="wfst_">
						<h1>{{$cslider}}</h1>
						<h5>Slider</h5>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-lg-3 col-md-6">
		<a href="{{ URL::asset('video') }}">
			<div class="wiget_fs">
				<div class="wfs_head">
					<img src="{{asset('admin/images/play.png')}}" alt="">
				</div>
				<div class="wfs_text">
					<div class="wfst_">
						<h1>{{$cvideos}}</h1>
						<h5>Videos</h5>
					</div>
				</div>
			</div>
		</a>
	</div>

	<!--  -->
	<div class="col-lg-3" style="display: none;">
		<div class="widget-head-color-box navy-bg p-lg text-center">
			<a href="{{ route('usuarios.index') }}" style="color: white;">
				<div class="m-b-md">
					<i class="fa fa-user fa-4x"></i>
					<h1 class="m-xs">{{$cusuarios}}</h1>
					<h3 class="font-bold no-margins">
						Usuarios
					</h3>					
				</div>
			</a>
		</div>
		
	</div>
	<div class="col-lg-3" style="display: none;">
		<div class="widget yellow-bg p-lg text-center">
			<a href="{{ URL::asset('articulos') }}" style="color: white;">
				<div class="m-b-md">
					<i class="fa fa-newspaper-o fa-4x"></i>
					<h1 class="m-xs">{{$carticulo}}</h1>
					<h3 class="font-bold no-margins">
						Artículos
					</h3>
					
				</div>
			</a>
		</div>
		
	</div>
	<div class="col-lg-3" style="display: none;">
		<div class="widget red-bg p-lg text-center">
			<a href="{{ URL::asset('animacion') }}" style="color: white;">
				<div class="m-b-md">
					<i class="fa fa-picture-o fa-4x"></i>
					<h1 class="m-xs">{{$cslider}}</h1>
					<h3 class="font-bold no-margins">
					Slider
					</h3>
					
				</div>
			</a>
		</div>
		
	</div>
	<div class="col-lg-3" style="display: none;">
		<div class="widget lazur-bg p-lg text-center">
			<a href="{{ URL::asset('video') }}" style="color: white;">
				<div class="m-b-md">
					<i class="fa fa-youtube-play fa-4x"></i>
					<h1 class="m-xs">{{$cvideos}}</h1>
					<h3 class="font-bold no-margins">
					Videos
					</h3>					
				</div>
			</a>
		</div>		
	</div>
</div>
<div style="margin-bottom: 15px;"></div>
<div class="row" style="display: none;">
	<div style="position: relative; height:280px; width:400px">
		<canvas id="myChart"></canvas>
	</div>
</div>
<div class="row">
<div class="col-md-8">
	<br>
	@if (count($articulos) > 0)		
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<h5>Ultimos artículos | <a href="{{URL::to('/articulos')}}">Ver todos</a></h5>
						<table class="table table-bordered text-center table-hover" >
	                        <thead>
	                            <th class="text-left">Título <br> <small class="text-muted">Contenido</small></th>
	                            <th>Categoria</th>
	                            <th>Acciones</th>
	                        </thead>
	                        <tbody>
	                            @foreach ($articulos as $articulo)
	                            <tr>
	                                <td class="text-left">
	                                    <b>{{ $articulo->titulo }}</b><br>
	                                    <small class="text-muted">
	                                    	<b>Activo :</b> 
	                                    	@if ($articulo->estado == 1)
		                                        <span class="text-navy"><i class="fa fa-check"></i></span>
		                                    @else
		                                        <span class="text-danger"><i class="fa fa-times"></i></span>
		                                    @endif
		                                     | <b>Destacado :</b> 
	                                    	@if ($articulo->destacado)
		                                        <span class="text-navy"><i class="fa fa-check"></i></span>
		                                    @else
		                                        <span class="text-danger"><i class="fa fa-times"></i></span>
		                                    @endif
		                                     | <b>Autor :</b> {{ $articulo->nombres }}
	                                    	<br>
	                                    
	                                        {{ strip_tags(substr($articulo->texto,0,150)) }}{{ strlen($articulo->texto) > 150 ? '...' : '' }}
	                                    </small>
	                                </td>
	                                <td>{{ $articulo->categoria }}</td>                                
	                                <td>
	                                    <a class="btn btn-success btn-sm" href="{{route('articulos.edit', $articulo->id)}}" role="button">
	                                        <i class="fa fa-pencil-square-o"></i>
	                                    </a>
	                                    <a class="btn btn-danger btn-sm" href="{{route('articulos.destroy', $articulo->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button">
	                                        <i class="fa fa-trash-o"></i>
	                                    </a>
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
						</table>											
					</div>				
				</div>
			</div>		
		</div>
	@else
		<div class="alert alert-warning" role="alert">
	        No hay Artículos registrados, comencemos <a href="{{ route('articulos.create') }}" class="alert-link">registrando el primero dando click aqui</a>
	    </div>
	@endif
</div>
<div class="col-md-4">
	<br>
	<div class="box">
		<div class="box-body">
			<h5>Categorías | <a href="{{URL::to('/categorias')}}">Ver todos</a></h5>
			<ul class="list-group">
				@foreach ($categorias as $categoria)
					<li class="list-group-item">
						<a href="{{ route('categorias.edit', $categoria->id) }}">{{ $categoria->categoria }}</a>
						<span class="badge badge-primary">{{ $categoria->total }}</span>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>
</div>

<div class="clearfix"></div>


@endsection