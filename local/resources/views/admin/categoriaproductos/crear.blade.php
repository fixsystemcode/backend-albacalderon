@extends('layouts.backend')
@section('titulo', 'Nuevo')

@section('contenido')
	<div class="row">
		<div class="col-md-8" >
			{!! Form::open(['route' => 'categoriaproductos.store', 'method' => 'POST', 'files'=>true]) !!}
				<div class="form-group">
					{!! Form::label('categoria','Nombre de Categoria') !!}
					{!! Form::text('categoria',null,['class' =>'form-control', 'placeholder' =>'Ingrese nombre de categoria','required'])!!}
				</div>
				<div class="form-group">
					{!! Form::label('destacado','Destacado') !!}
					{!! Form::select('destacado',['1'=>'Destacado', '0'=>'No destacado'], null,[ 'class' =>'form-control', 'required'])!!}
				</div>

				<div class="form-group">
					{!! Form::label('descripcion','Descripción de la Categoria') !!}
					{!! Form::textarea('descripcion',null,['class' =>'form-control', 'placeholder' =>'Categoria destacado ingrese 1','rows'=>6])!!}
				</div>
				<div class="form-group"  >
					{!! Form::label('path','Imagen') !!}
					{!! Form::file('path',[])!!}			
				</div>

				<div class="form-group">
					{!! Form::button('<i class="fa fa-save"></i> Registrar', ['class' =>'btn btn-primary', 'type'=>'submit']) !!}
					<a href="{{ route('categoriaproductos.index') }}" class="btn btn-warning"><i class="fa fa-chevron-left"></i> Regresar</a>
				</div>
			{!! Form::close() !!}
		</div>
		<div class="clearfix"></div>
	</div>
	
@endsection