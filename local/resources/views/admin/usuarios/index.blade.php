@extends('layouts.backend')

@section('ruta', 'Listado de Usuarios')

@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Usuario</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('usuarios.create') }}">Nuevo</a>
</div>
@endsection

@section('contenido')

    @include('alerts.success')

    @include('alerts.errors')

@if (count($usuarios))

    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display">
        <thead>
            <tr>
                <th>#</th>
                <th>Imagen</th>
                <th>Nombres</th>
		        <th>Email</th>
		        <th>Teléfono</th>
                <th>Usuario</th>
                <th>Fecha de Nacimiento</th>
                <th>Tipo de Usuario</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @php 
                $s = 0;
            @endphp

            @foreach ($usuarios as $usuario)
            <tr>
                <td> {{ $s = $s+1 }}</td>
                <td>
                    <img src="{{ obtenerFotoUsuario($usuario->path) }}" alt="{{ obtenerFotoUsuario($usuario->path) }}" style ="width: 50px">
                </td>
                <td>{{ $usuario->nombres }}</td>
                <td>{{ $usuario->email }}</td>
                <td>{{ $usuario->telefono }}</td>
                <td>{{ $usuario->login }}</td>
                <td>
                    @if($usuario->fecha_nacimiento != "")
                    {{\Carbon\Carbon::parse ($usuario->fecha_nacimiento)->format('d/m/Y')}}
                    @endif
                </td>
                <td>{{ $usuario->tipo }}</td>
                <td>{!! link_to_route('usuarios.edit', 'Editar', array($usuario->id), array('class' => 'btn btn-info')) !!}</td>
                <td>
                    {!! Form::open(array('method' => 'DELETE', 'route' => array('usuarios.destroy', $usuario->id))) !!}                       
                    {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                    {!! Form::close() !!}
                </td>
            </tr>

            @endforeach             

        </tbody>     

    </table>

@else
    No hay usuarios
@endif

@endsection