@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">{{ $edit == true ? 'Editar' : 'Nuevo'}} cupón</h1>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('contenido')
<style type="text/css">
	.textos{
        text-align: right;
        font-size: 1.2em;        
    }
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    {!! Form::open(['url' => 'cupones', 'method' => 'POST' ]) !!}
        @if ($edit == true)
            <input type="hidden" name="id" value="{{$cupon->id}}">
        @endif
        <div class="row">
            <div class="col-md-2 textos"><label for="descripcion" class="control-label">Descripción</label></div>
            <div class="col-md-10">
            <textarea name="descripcion" id="descripcion" cols="30" rows="10" class="form-control">{{ $edit == true ? $cupon->descripcion : ''}}</textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="codigo" class="control-label">Código</label></div>
            <div class="col-md-10">
                <input type="text" class="form-control" name="codigo" id="codigo" value="{{ $edit == true ? $cupon->codigo : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="descuento" class="control-label">Descuento %</label></div>
            <div class="col-md-10">
                <input type="text" class="form-control" name="descuento" id="descuento" value="{{ $edit == true ? $cupon->descuento : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="limite" class="control-label">Límite de uso</label></div>
            <div class="col-md-10">
                <input type="text" class="form-control" name="limite" id="limite" value="{{ $edit == true ? $cupon->limite : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="fecha_inicio" class="control-label">Fecha inicio</label></div>
            <div class="col-md-10">
                <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="{{ $edit == true ? date('Y-m-d', strtotime($cupon->fecha_inicio)) : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="fecha_fin" class="control-label">Fecha fin</label></div>
            <div class="col-md-10">
                <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="{{ $edit == true ? date('Y-m-d', strtotime($cupon->fecha_fin)) : ''}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 textos"><label for="estado" class="control-label">Estados</label></div>
            <div class="col-md-10">
                {!! Form::select('estado', $estados, $edit == true ? getEstadosCupon($cupon->estado) : null ,['class'=>'form-control ','id' => 'estado']) !!}
            </div>
        </div>
        <div class="row" style="text-align: right; margin-right: 2px">
            <button class="btn btn-primary" type="submit">{{ $edit == true ? 'Actualizar' : 'Guardar'}}</button>
        </div>
   {!! Form::close() !!}
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
    function guardar(){
        var descripcion = $('#descripcion').val();
        var codigo = $('#codigo').val();
        var descuento = $('#descuento').val();
        var limite = $('#limite').val();
        var fecha_inicio = $('#fecha_inicio').val();
        var fecha_fin = $('#fecha_fin').val();
        var estado = $('#estado').val();
       $.ajax({
            url:'{{ url('depositos') }}',
            data: { 'descripcion': descripcion, 'codigo': codigo, 'descuento': descuento, 'limite': limite, 'fecha_inicio': fecha_inicio, 'fecha_fin': fecha_fin, 'estado': estado
            },
            type: 'POST',
            success:function(response){
                
            }
       });
    }
    function actualizar(){
        var id = $('#id').val();
        var descripcion = $('#descripcion').val();
        var codigo = $('#codigo').val();
        var descuento = $('#descuento').val();
        var limite = $('#limite').val();
        var fecha_inicio = $('#fecha_inicio').val();
        var fecha_fin = $('#fecha_fin').val();
        var estado = $('#estado').val();
       $.ajax({
            url:'{{ url('depositos') }}',
            data: { formData
            },
            type: 'POST',
            success:function(response){
                location.reload();
            }
       });
    }
	</script>
@endsection
