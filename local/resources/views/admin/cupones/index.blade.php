@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Cupones</h1>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    <a class="btn btn-primary" href="{{ route('cupones.create') }}">Nuevo cupón</a>
    <br>
     <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}</form>
    @if (count($cupones))
    <br>
     <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table  id="tbbuzon" class="table table-bordered  text-center table-hover display" >
                        <thead>
                            <th>Id</th>
                            <th>Descripción</th>
                            <th>Cupón</th>
                            <th>Descuento %</th>
                            <th>Usos</th>
                            <th>Limite de uso</th>
                            <th>Estado</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Acción</th>
                        </thead>
                        <tbody>
                            @foreach ($cupones as $key => $cupon)
                            <tr>
                               <td>{{ $cupon->id }}</td>
                               <td>{{ $cupon->descripcion }}</td>
                               <td>{{ $cupon->codigo }}</td>
                               <td>{{ $cupon->descuento }}</td>
                               <td>{{ $cupon->usos }}</td>
                               <td>{{ $cupon->limite }}</td>
                               <td>{{ getEstadosCupon($cupon->estado) }}</td>
                               <td>{{ date('Y-m-d', strtotime($cupon->fecha_inicio)) }}</td>
                               <td>{{ date('Y-m-d', strtotime($cupon->fecha_fin)) }}</td>
                               <td>
                                    <a href="{{ route('cupones.edit', $cupon->id) }}" class="btn btn-primary">
                                        <span class="fa fa-edit"></span>
                                        
                                    </a>
                                     <a  href="javascript:void(0)" onclick="eliminar('{{ $cupon->id }}')" class="btn btn-danger">
                                        <span class="fa fa-trash"></span>
                                        
                                    </a>
                               </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
     @else
        <br>
       <div class="alert alert-warning" role="alert">
            No existen cupones de descuentos, cree uno <a href="{{ route('cupones.create') }}">aquí </a>
        </div>
    @endif
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
	function eliminar(id){
       $("#formDelete").attr("action", '{{ URL::to('cupones') }}/'+id).submit();
    }
    function guardarEstado(id){
    	var estado = $('#estado'+id).val();
       $.ajax({
            url:'{{ url('depositos') }}',
            data: {
                'estado': estado,
                'id' : id,
                '_token': $("meta[name='csrf-token']").attr("content")
            },
            type: 'POST',
            success:function(response){
                location.reload();
            }
       });
    }
	</script>
@endsection
