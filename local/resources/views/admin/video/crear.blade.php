@extends('layouts.backend')
@section('titulo', 'Nuevo video')
@section('contenido')
    
    @include('alerts.request')

    <div class="row">
        
        <div class="col-md-6">  
            {!! Form::open(['route' => 'video.store', 'method' => 'POST']) !!}
                <div class="form-group">
                    {!! Form::label('titulo','Título') !!}
                    {!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('descripcion', 'Descripcion:') !!}
                    {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción del Slider','required']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('categoria','Categoria') !!}
                    {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control','required' , 'list' =>'listas'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('path','URL del Video de Youtube') !!}
                    {!! Form::text('path',null,['class' =>'form-control', 'placeholder' =>'Pegue aquí la URL del video','required'])!!}
                </div>
               <div class="form-group">
                    {!! Form::button('<i class="fa fa-save"></i> Guardar', ['class' =>'btn btn-primary', 'type'=>'submit']) !!}
                    <a href="{{ route('video.index') }}" class="btn btn-warning"><i class="fa fa-chevron-left"></i> Regresar</a>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="clearfix"></div>
    </div>
    
@endsection