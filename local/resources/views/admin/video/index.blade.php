@extends('layouts.backend')

@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Videos</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('video.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')
    @include('alerts.success')
    <!-- <a class="btn btn-primary" href="{{ URL::route('video.create') }}">Nuevo</a> -->
    @if (count($videos))
        <table id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
                <tr>
                    
                    <th>Título</th>
                    <th>Descrición</th>
                    <th>Categoria</th>
                    <th>URL</th>            
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($videos as $video)
                <tr>
                    <td>{{$video->titulo}}</td>
                    <td>{{ $video->descripcion }}</td>
                    <td>{{ $video->nombre }}</td>
                    <td><a href="#" onclick="openVideoModal('{{ $video->path }}')">{{ $video->path }}</a></td>
                    <td><a class="btn btn-success" href="{{route('video.edit', $video->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    {!! Form::open(array('method' => 'DELETE', 'route' => array('video.destroy', $video->id))) !!}
                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
            
        </tbody>

        </table>

        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" width="100%" height="350" src=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="alert alert-warning " role="alert">
            No existen videos actualmente, empecemos creando uno <a href="{{ route('video.create') }}">aqui</a>
        </div>
    @endif
    <style type="text/css">
        .modal-dialog {
            max-width: 800px;
            margin: 30px auto;
        }
        .modal-body {
            position:relative;
            padding:0px;
        }
        .close {
            position:absolute;
            right:-30px;
            top:0;
            z-index:999;
            font-size:2rem;
            font-weight: normal;
            color:#fff;
            opacity:1;
        }
    </style>
    <script type="text/javascript">

        function openVideoModal(url){
            link = ''
            if (url.indexOf("youtube.com") != -1) {
                link = url.substr(url.indexOf("?v=")+3,11);
            }else if(url.indexOf("youtu.be/") != -1) {
                link = url.substr(url.indexOf("be")+3,11);
            }
            link = (link != '' ? '//www.youtube.com/embed/'+link : url );
            $("#videoModal iframe").attr("src", link + '?modestbranding=1&showinfo=0');
            $('#videoModal').modal('show');
        }

        $("#videoModal").on('hidden.bs.modal', function (e) {
            $("#videoModal iframe").attr("src", '');
        });
    </script>
@endsection
