@extends('layouts.backend')
@section('titulo', 'Editar')
@section('contenido')
    @include('alerts.request')
    <div class="row">
        
        <div class="col-md-6">  
            {!! Form::model($video, array('method' => 'PATCH', 'route' => array('video.update', $video->id) , 'files' => true)) !!}
                <div class="form-group">
                    {!! Form::label('titulo','Título') !!}
                    {!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('descripcion', 'Descripcion:') !!}
                    {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción de la imagen']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('categoriainterna','Categoria') !!}
                    {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('path','URL del Video de YouTuBe') !!}
                    {!! Form::text('path',null,['class' =>'form-control', 'placeholder' =>'Pegue aquí la URL del video','required'])!!}
                </div>
               <div class="form-group">
                    {!! Form::button('<i class="fa fa-save"></i> Actualizar', ['class' =>'btn btn-primary', 'type'=>'submit']) !!}
                    <a href="{{ route('video.index') }}" class="btn btn-warning"><i class="fa fa-chevron-left"></i> Regresar</a>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="clearfix"></div>
    </div>
    
@stop