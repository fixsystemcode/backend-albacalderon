@extends('layouts.backend')


@section('titulo')
    <i class="fa fa-save"></i> Editar
@endsection

@section('contenido')



@include('alerts.request')

{!! Form::model($dato, array('method' => 'PATCH', 'route' => array('camposextrasgrupos.update', $dato->id))) !!}

    
    <div class="form-group">
            {!! Form::label('descripcion', 'Ingrese descripcion:') !!}
            {!! Form::text('descripcion',$dato->descripcion,['class'=>'form-control','placeholder'=>'Ingrese Area Responsable']) !!}
    </div>

    <div class="form-group">
			{!! Form::label('categorias[]', 'Asigne a una categoria ') !!}
		    {!! Form::select('categorias[]', $categorias, $mis_Categorias,['class'=>'form-control selective','multiple'=>'multiple']) !!}
	</div>

   <div class="form-group">
         {!! Form::label('vistacampos_id', 'Asignadar Vista de campos',['class'=>'control-label']) !!}

         {!! Form::select('vistacampos_id', $vistacampos, null,['placeholder'=>'Escoga','required' => 'required','class'=>'form-control select2']) !!}

    </div>

	 <div class="form-group">
            {!! Form::label('icono', 'Ingrese codigo ícono:') !!}
            {!! Form::text('icono',null,['class'=>'form-control','placeholder'=>'Ingrese codigo ícono:']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('orden', 'Orden:') !!}
            {!! Form::text('orden',null,['class'=>'form-control','placeholder'=>'Orden:']) !!}
    </div>
		

            {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


{!! Form::close() !!}

<script>

$('.selective').selectize({
    plugins: ['remove_button'],
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});



 </script>

@stop