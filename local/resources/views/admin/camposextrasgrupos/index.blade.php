@extends('layouts.backend')

@section('titulo')

    Lista grupo de campos extras

@endsection



@section('contenido')





<a class="btn btn-primary btn-app" href="{{ URL::route('camposextrasgrupos.create') }}"> <i class="fa fa-calendar-check-o"></i>Nuevo</a>

<br>





@if (count($datos))

 



<br>

    <div class="row">

      <div class="col-md-12">



         <div class="box">

           

            <!-- /.box-header -->

            <div class="box-body">

               <table id="example1" class="table table-striped table-bordered">

                    <thead>

                        <tr>

                            <th>#</th>

                            <th>Nombre</th>

                            <th>Orden</th>

                             

                            <th style="width:20%">Acciones</th>

                           

                        </tr>

                    </thead>



                    <tbody>

                       <?php $s = 0 ; ?>

                        @foreach ($datos as $dato)

                            <tr>

                                <td> <?php $s = $s + 1; echo $s; ?></td>

                                <td>{{ $dato->descripcion }}</td>

                                <td>{{ $dato->orden }}</td>

                                 



                                 <td >
                                  <a class="btn btn-success btn-xs" href="{{route('camposextrasgrupos.edit', $dato->id)}}" role="button"><i class="fa fa-pencil-square-o"></i> Editar</a>
                                  
                                  {!! Form::open(array('method' => 'DELETE', 'route' => array('camposextrasgrupos.destroy', $dato->id))) !!}
                                      <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span> Eliminar</button>
                                  {!! Form::close() !!}

                                </td>





                            </tr>

                        @endforeach

                          

                    </tbody>

                  

                </table>

            </div>

            <!-- /.box-body -->

          </div>

       



      </div>

      <div class="col-md-6">



      </div>

    </div>





    

@else

      <p style="

    text-align: center;

    color: #a3a3a3;

    font-size: 24px;

    padding: 35px;

    text-transform: uppercase;"> No hay datos</p>

@endif



@stop