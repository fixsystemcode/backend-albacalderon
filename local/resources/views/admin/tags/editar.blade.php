@extends('layouts.backend')
@section('titulo', 'Editar')

@section('title','Editar tag: '. $tag->tag)
@section('contenido')
	{!!Form::model($tag,['route'=>['tags.update',$tag],'method'=>'PUT','files' => true])!!}

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('tag','Título') !!}
					{!! Form::text('tag',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
				</div>
				<div class="form-group">
					{!! Form::label('descripcion','Descripcion') !!}
		            {!! Form::text('descripcion',null,['class' =>'form-control', 'placeholder' =>'Descripcion del Tag...','required'])!!}
				</div>

				<div class="form-group">
					{!! Form::label('categoria_id','Categoria') !!}
		            {!! Form::select('categoria_id',$categorias->pluck('categoria', 'id'),null, ['class' =>'form-control', 'required'])!!}
				</div>
			</div>
		</div>

		{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}

@endsection