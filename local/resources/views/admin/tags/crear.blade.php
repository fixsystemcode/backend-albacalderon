@extends('layouts.backend')
@section('titulo')
Nuevo
@stop
@section('contenido')
{!! Form::open(['route' => 'tags.store', 'method' => 'POST','files'=>true]) !!}


<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{!! Form::label('tag','Título') !!}
			{!! Form::text('tag',null,['class' =>'form-control', 'placeholder' =>'Título del Tag','required'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('imagen','Imagen') !!}
			{!! Form::file('path')!!}
		</div>
	</div>
</div>


<div class="form-group">
	{!! Form::submit('Registrar', ['class' =>'btn btn-primary']) !!}
</div>

{!! Form::close() !!}
@endsection