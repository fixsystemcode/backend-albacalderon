@extends('layouts.backend')

@section('titulo', 'Etiquetas')

@section('contenido')
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
<div class="row">
    <div class="col-md-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                Listado
            </div>
            <div class="panel-body">
                @if (count($tags))                
                    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
                        <thead>
                            <th>Categoria</th>
                            <th>Tags</th>
                            <th>Imagen</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>                    
                            @foreach ($tags as $tag)
                            <tr>
                                <td>{{ $tag->categoria->categoria }}</td>
                                <td>{{ $tag->tag }}</td>
                                <td>{{ $tag->descripcion }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{route('tags.edit', $tag->id)}}" role="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    {!! Form::open(array('method' => 'DELETE', 'route' => array('tags.destroy', $tag->id))) !!}
                                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                                      {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>    
                @else
                    <div class="alert alert-warning" role="alert">
                        No hay Etiquetas registrados, registra el primero ;)</a>
                    </div>
                @endif

            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                Nuevo
            </div>
            <div class="panel-body">
                {!! Form::open(['route' => 'tags.store', 'method' => 'POST','files'=>true]) !!}
                    <div class="form-group">
                        {!! Form::label('tag','Título') !!}
                        {!! Form::text('tag',null,['class' =>'form-control', 'placeholder' =>'Título del Tag','required'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('descripcion','Descripcion') !!}
                        {!! Form::text('descripcion',null,['class' =>'form-control', 'placeholder' =>'Descripcion del Tag...','required'])!!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('categoria_id','Categoria') !!}
                        {!! Form::select('categoria_id',$categorias->pluck('categoria', 'id'),null, ['class' =>'form-control', 'required'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Registrar', ['class' =>'btn btn-primary btn_formc']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        
    </div>
</div>

@endsection