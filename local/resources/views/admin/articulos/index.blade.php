@extends('layouts.backend')

@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Artículos</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('articulos.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')

    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <!-- <br>
    <a class="btn btn-primary" href="{{ URL::route('articulos.create') }}"> <i class="fa fa-plus"></i> Nuevo</a>
    <br> -->

    @if (count($articulos))
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table  id="tbbuzon" class="table table-bordered  text-center table-hover display" >
                        <thead>
                            <th>ID</th>
                            <th style="width:50%;" class="text-left">Título <br> <small class="text-muted">Contenido</small></th>
                            <th>Autor</th>
                            <th>Activado</th>
                            <th>destacado</th>
                            <th>Categoria</th>
                            <th>Imagen</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach ($articulos as $articulo)
                            <tr>
                                <td>{{ $articulo->id }}</td>
                                <td class="text-left">
                                    <b>{{ $articulo->titulo }}</b><br>
                                    <small class="text-muted">
                                        {{ strip_tags(substr($articulo->texto,0,150)) }}{{ strlen($articulo->texto) > 150 ? '...' : '' }}
                                    </small>
                                </td>
                                <td>{{ $articulo->nombres }}</td>
                                <td>
                                    @if ($articulo->estado)
                                        <span class="badge badge-primary">SI</span>
                                    @else
                                        <span class="badge badge-warning">NO</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($articulo->destacado)
                                        <span class="badge badge-primary">SI</span>
                                    @else
                                        <span class="badge badge-warning">NO</span>
                                    @endif
                                </td>
                                <td>{{ $articulo->categoria }}</td>
                                <td>
                                @if(empty($articulo->path))
                                    <img src="{{asset('assets/images')}}/no-imagen.jpg" onerror=" this.src = '{{asset('assets/images')}}/articulo.png';" style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                                @else
                                    <div class="lightBoxGallery">
                                        <a href="{{asset('local/public/images')}}/{{$articulo->path}}" title="Image from Unsplash" data-gallery="">
                                            <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/images')}}/{{$articulo->path}}" onerror="this.src = '{{asset('assets/images')}}/no-imagen.jpg';" >
                                        </a>
                                        <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                                        <div id="blueimp-gallery" class="blueimp-gallery">
                                            <div class="slides"></div>
                                            <h3 class="title"></h3>
                                            <a class="prev">‹</a>
                                            <a class="next">›</a>
                                            <a class="close">×</a>
                                            <a class="play-pause"></a>
                                            <ol class="indicator"></ol>
                                        </div>
                                    </div>
                                @endif                            
                                </td>
                                <td>
                                    <a class="btn btn-success" href="{{route('articulos.edit', $articulo->id)}}" role="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    {!! Form::open(array('method' => 'DELETE', 'route' => array('articulos.destroy', $articulo->id))) !!}
                                      <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
        <br>
       <div class="alert alert-warning" role="alert">
            No hay Artículos registrados, comencemos <a href="{{ route('articulos.create') }}" class="alert-link">registrando el primero dando click aqui</a>
        </div>
    @endif

@endsection

@section('script')
    
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };
</script>
@endsection