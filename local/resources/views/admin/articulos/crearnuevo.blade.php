@extends('layouts.backend')

@section('titulo', 'Nuevo artículo')

@section('contenido')

<style type="text/css">
#map {
	height: 400px;
	width: 100%;
}
#elim{
	color:#fff;
	border:1px solid #ccc;
	background-color:#fb0000;
	margin-right:10px;
	padding: 0 2px;
	border-radius:4px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	-o-border-radius:4px;
	border-radius:4px;
	font-weight:bold;
	font-size:0.8em;
	cursor:pointer;
}
/* css galeria */
#reorder-helper{margin: 18px 10px;padding: 10px;}
.light_box {
	background: #efefef;
	padding: 20px;
	margin: 10px 0;
	text-align: center;
	font-size: 1.2em;
}
.gallery{ width:100%; float:left; margin-top:10px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li{ padding:4px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
/* style 6 */
.inputimg + label {
	color: #ec9b1b;
}
.inputimg:focus + label,
.inputimg.has-focus + label,
.inputimg + label:hover {
	color: #d47e03;
}
.inputimg + label figure {
	width: 100px;
	height: 135px;
	background-color: #ec9b1b;
	display: block;
	position: relative;
	padding: 30px;
	margin: 0 auto 10px;
}
.inputimg:focus + label figure,
.inputimg.has-focus + label figure,
.inputimg + label:hover figure {
	background-color: #d47e03;
}
.inputimg + label figure::before,
.inputimg + label figure::after {
	width: 0;
	height: 0;
	content: '';
	position: absolute;
	top: 0;
	right: 0;
}
.inputimg + label figure::before {
	border-top: 20px solid #05354e;
	border-left: 20px solid transparent;
}
.inputimg + label figure::after {
	border-bottom: 20px solid #d47e03;
	border-right: 20px solid transparent;
}
.inputimg:focus + label figure::after,
.inputimg.has-focus + label figure::after,
.inputimg + label:hover figure::after {
	border-bottom-color: #ec9b1b;
}
.inputimg + label svg {
	width: 100%;
	height: 100%;
	fill: #f1e5e6;
}
#imgprincipal{
	max-width: 100%;
    height: 300px;
    display: block;
    object-fit: cover!important;
    margin: auto;
}
</style>

{!! Form::open(['route'=>'articulos.store','method'=>'POST','class'=>'dropzone','id'=>'my-dropzone','files'=>true]) !!}
	<input type="hidden" id="doc">
	<input type="hidden" id="btncancelardoc">
	<input type="hidden" name="publicar" value="0">
	<input type="hidden" name="id_usuario" value="{{ Auth::user()->id }}">

	<select name="rutadocs[]" id="rutadocs" multiple="multiple" style='display:none;'></select>
	<select name="titulodocs[]" id="titulodocs" multiple="multiple" style='display:none;'></select>
	<select name="sobrasdocs[]" id="sobrasdocs" multiple="multiple" style='display:none;'></select>

	<select name="ruta[]" id="ruta" multiple="multiple" style='display:none;' ></select>
	<select name="sobras[]" id="sobras" multiple="multiple" style='display:none;' ></select>

	<select name="videos[]" id="videos" multiple="multiple" style='display:none;'></select>
	<select name="titulovideo[]" id="titulovideo" multiple="multiple" style='display:none;'></select>
	<select name="sobrasvideos[]" id="sobrasvideos" multiple="multiple" style='display:none;' ></select>
	
<div class="row">
	<div class="col-md-8">
		<div class="form-group">
			{!! Form::label('titulo','Título') !!}
			{!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
		</div>
		<div class="form-group">
			<label>Imagen Principal</label>			
			<label for="imagenprincipal" style="display: block; border: 1px solid #1ab394;">				
				{!! Form::file('imagenprincipal',['id'=>'imagenprincipal','accept'=>'image/*','style'=>'display:none;'])!!}
				<img id="imgprincipal" src="{{asset('/assets/images/no-imagen.jpg')}}">
			</label>
		</div>

		<div class="form-group">
			{!! Form::label('texto','Contenido') !!}
			{!! Form::textarea('texto',null,['class' =>'form-control ckeditor', 'placeholder' =>'Contenido del artículo','required'])!!}
		</div>

		<div class="form-group">
			<div class="gallery">
				<ul class="reorder_ul reorder-photos-list" style="background: #fff" id="nuevalista">
					<!--Lista de fotos -->
					@for ($i = 1; $i <= 10 ; $i++)
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-{{ $i }}" id="file-{{ $i }}" data-n="{{ $i }}" class="inputimg"  style='display:none;' accept="image/*" />
								<label for="file-{{ $i }}">
									<img id="imgfile-{{ $i }}" style="height: 100px;padding: 6px;" src="{{asset('/assets/images/no-imagen.jpg')}}" >
									<a class="btn btn-danger btn-xs deleteImage" id="elim{{ $i }}"  onclick="eliminarElemento('imgfile-{{ $i }}','elim{{ $i }}');" style="display:none;"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
					@endfor
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="form-group" style="background: #ddeff4;border: 1px solid #1ab394;padding: 6px;padding-top: 15px;text-align: center;">
			<input type="file" id="nuevo_documento" class="inputfile"  style='display:none;'  />
			<label for="nuevo_documento" class="btn btn-primary" style="color: #fff;">
				<img style="height: 20px;" src="{{asset('/assets/images/newdoc.png')}}" >
				Agregar Documento
			</label>

			<div id="upload-progress" class="row" style="width: 100%; display: none" >
				<div class="col-xs-10">
					<div class="progress">
						<div id="porcentaje" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
					</div>
				</div>
				<div class="col-xs-2">					
					<button class="btn btn-danger btn-block btn-xs" type="button" id="cancelar_documento"> <i class="fa fa-times"></i> Cancelar</button>
				</div>
			</div>

			<ul class="reorder-documentos-list reorder_ul list-group" id="listadocs" style="background: #fff"></ul>

		</div>

		<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;text-align: center;">
			<p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Lista de Videos</p>
			<ul class="reorder-videos-list reorder_ul list-group" id="listaDesordenada" name="videos"></ul>
			
			<div class="input-group">
				{!! Form::text('video',null,['class' =>'form-control', 'id'=>'nuevo_video' ,'placeholder' =>'URL YouTube/Facebook/Vimeo de video'])!!}
				<span class="input-group-btn">
					<button class="btn btn-primary"  type="button" id="btnagregavideo" align="center" onclick="return add_li()" >
						<img style="height: 20px;" src="{{asset('/assets/images/addvideo.png')}}" > Agregar Video
					</button>
				</span>
			</div>
			
			<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-vervideo"  >
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true"></span>
							</button>
							<h4 class="modal-title">Visualizar Video</h4>
						</div>
						<div class="modal-body">
							<div id="reproductor" >
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
		</div>	

	</div>

	<div class="col-md-4" style="background: #ddeff5 /*#f4f4f4*/;padding: 18px;">
		<div class="form-group" style="padding: 6px; " >
			<div style="margin-bottom: 8px">
				<input type="checkbox" data-on="Destacar" data-off="No Destacar"  data-toggle="toggle" name="destacado" id="destacado">
			</div>
			<div style="margin-bottom: 8px;">
				<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="estado" id="estado" type="checkbox">
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('categoria','Categoria') !!}
			{!! Form::select('id_categoria',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas','id' =>'categorias'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('tags[]', 'Tags o Etiquetas') !!}
			{!! Form::select('tags[]', $tags, null,['class'=>'form-control selective','multiple'=>'multiple', 'id' => 'etiquetas']) !!}
		</div>
		<div class="form-group" id="lugar" style="display: none;">
			{!! Form::label('lugar','Lugar ( 50 Caracteres máximo)') !!}
			{!! Form::text('lugar',null,['class' =>'form-control', 'placeholder' =>'Ubicación del local','maxlength'=>'50'])!!}
		</div>

		<hr> 
		<div class="form-group">
			{!! Form::label('prioridad','Prioridad') !!}
			{!! Form::select('id_prioridad',$prioridad, null,[ 'class' =>'form-control' , 'listprop' =>'listasprio','id' =>'prioridad'])!!}
		</div>

		<hr>

		<div class="form-group">
			{!! Form::label('seo_keyword','SEO KEYWORD') !!}
			{!! Form::text('seo_keyword',null,['class' =>'form-control', 'placeholder' =>'Seo Keyword'])!!}
		</div>

		<div class="form-group">
			{!! Form::label('seo_descripcion','SEO Descripción') !!}
			{!! Form::textarea('seo_descripcion',null,['class' =>'form-control', 'placeholder' =>'SEO Descripción','style'=>'resize: none','rows'=>'5'])!!}
		</div>

		<div class="form-group">
			{!! Form::label('descripcionredes','Comentario en Redes Sociales ( 200 caracteres máximo)') !!}
			{!! Form::text('descripcionredes',null,['class' =>'form-control', 'placeholder' =>'Comentario en Redes Sociales','maxlength'=>'200'])!!}
		</div>

		<hr>
		<!-- MAPAS -->
		<div class="form-group">
			{!! Form::label('latitud','Latitud') !!}
			{!! Form::text('latitud',null,['class' =>'form-control', 'placeholder' =>'Latitud'])!!}
		</div>

		<div class="form-group">
			{!! Form::label('longitud','Longitud') !!}
			{!! Form::text('longitud',null,['class' =>'form-control', 'placeholder' =>'Longitud'])!!}
		</div>

		<div class="form-group"><div id="map"></div></div>
		<!-- FIN MAPAS -->

		<div class="panel-group" id="accordion">
			@foreach ($gruposCampos as $grupo)
				<div class="panel panel-default">
					<a data-toggle="collapse" data-parent="#accordion" href="#{{ $grupo['id'] }}">
						<div class="panel-heading">
							<h5 class="panel-title">{{ $grupo['descripcion'] }}</h5>
						</div>
					</a>
					<div class="panel-collapse collapse" id="{{ $grupo['id'] }}">
						<div class="panel-body">
							<div class="row">
								
								@foreach ($grupo['campos'] as $campo)
									<div class="form-group clearfix">
									{!! Form::label($campo['nombre'], $campo['descripcion'].':', array("class"=>"col-md-5 control-label") ) !!}
									<div class="col-md-7">
										@if( $campo['tipo'] == 'date' )
											{!! Form::input('date',$campo['nombre'], date('Y-m-d'), ['class' => 'form-control', 'placeholder' => 'Date']) !!}
										@elseif( $campo['tipo'] == 'select')
											{!! Form::select($campo['nombre'],json_encode($campo['valor']),Input::old($campo['nombre']), array('class' => 'form-control')) !!}
										@elseif( $campo['tipo'] == 'multipleselect' )
											{!! Form::select($campo['nombre'].'[]',json_encode($campo['valor']),Input::old($campo['nombre']), array('class' => 'form-control', 'multiple')) !!}
										@elseif( $campo['tipo'] == 'textarea')
											{!! Form::textarea($campo['nombre'], Input::old($campo['nombre']), array('maxlength'=>'500','class' => 'form-control','placeholder'=>'Ingrese '.$campo['nombre'])) !!}
										@else
											{!! Form::text($campo['nombre'], Input::old($campo['nombre']), array('class' => 'form-control','placeholder'=>'Ingrese valor ')) !!}
										@endif
									</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>

	<div class="col-md-12">
		<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
		<a class="btn btn-warning" href="{{ URL::to('articulos') }}"><i class="fa fa-chevron-left"></i> Atras</a>
	</div>

	<div class="clearfix"></div>
</div>

{!! Form::close() !!}

@endsection

@section('script')
<script>

	//$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } } );

	function initMap() {
		var myLatlng = {lat: -0.9683171526333203, lng: -80.70951460000003};

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: myLatlng
		});

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			draggable:true,
			title:"Ubicacion"
		});

		marker.setMap(map);
		marker.bindTo('position', map, 'center');
		google.maps.event.addListener(map, "center_changed", function() {
			var markerLatLng = marker.getPosition();
			$("#latitud").val(markerLatLng.lat());
			$("#longitud").val(markerLatLng.lng());
		});
		cargado = 1;
	}

	$( document ).ready(function() {

		$('.selective').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: false,
			create: function(input) {
				return {
					value: input,
					text: input
				}
			}
		});

		$('#categorias').on('change', function(e){
			var etiquetas = $('#etiquetas');
			var id_categoria = $(this).val();
			$.ajax({
				url: '{{ URL::to('/getTags') }}/'+id_categoria,
				data: '',
				method: 'POST',
				dataType: 'json',
				success: function(data){
					etiquetas.selectize()[0].selectize.destroy();
					etiquetas.empty();
					$.each(data, function (i, item) {
					    etiquetas.append($('<option>', { 
					        value: item.id,
					        text : item.tag 
					    }));
					});

					etiquetas.selectize({
						plugins: ['remove_button'],
						delimiter: ',',
						persist: false,
						create: function(input) {
							return {
								value: input,
								text: input
							}
						}
					});
				}, error: function(j){
					console.log(j);
					toastr.error('Error al obtener datos');
				}

			});
		});

	});	
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>
@include('scripts.articuloscript')

@endsection