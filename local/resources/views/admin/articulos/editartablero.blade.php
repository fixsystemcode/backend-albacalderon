@extends('layouts.paginas')

@section('ruta')
Tablero del sistema
@stop

@section('titulo')
Tablero del sistema
@stop

@section('script')
 
<script src='https://www.google.com/recaptcha/api.js'></script>

@stop

@section('contenido')

 <section class="mainContentSection">
      <div class="container">
         <div class="row">
<br>

{!!Form::model($articulo,['route'=>['articulos.update',$articulo],'method'=>'PUT','files' => true])!!}

	<div class="col-sm-12 col-xs-12">
            <div role="tabpanel" class="tabArea">
              
              <!-- Nav Tabs -->
              <ul class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" class="active">
                  <a href="#recent" aria-controls="recent" role="tab" data-toggle="tab">Contenido</a>
                </li>
                <li role="presentation">
                  <a href="#popular" aria-controls="popular" role="tab" data-toggle="tab">Multimedia</a>
                </li>
                <li role="presentation">
                  <a href="#new" aria-controls="new" role="tab" data-toggle="tab">Campos Adicionales</a>
                </li>
              </ul>
            
              <!-- Tab Panels -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="recent">
                  	<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{!! Form::label('titulo','Título') !!}
								{!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
							</div>
						</div>

						<div class="col-md-6">

							<div class="form-group">
								{!! Form::label('categoria','Categoria') !!}
								{!! Form::select('id_categoria',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas'])!!}
							</div>
						</div>

						<div class="col-md-6">
						
							<div class="form-group">
					                  {!! Form::label('tags[]', 'Tags o Etiquetas') !!}
					                  {!! Form::select('tags[]', $tags, $mis_Tags,['class'=>'form-control selective','multiple'=>'multiple']) !!}
					        </div>
								
						</div>

					</div>

					<div class="form-group">
						{!! Form::label('texto','Contenido') !!}
						{!! Form::textarea('texto',null,['class' =>'form-control summernote ckeditor', 'placeholder' =>'Contenido del artículo','required'])!!}
					</div>
                   
                   
                </div>

                <div role="tabpanel" class="tab-pane" id="popular">
                   	
                   	<div class="row">
                   		<div class="col-md-4">
                   			<div class="hr-line-dashed"></div>
				  		   <div class="btn-group" style="width: 100%;">
			                <label title="Upload image file" for="inputImage" class="btn btn-primary">
						                {!! Form::label('imagen','Elegir imagen principal',['class'=>'control-label']) !!}
										{!! Form::file('path')!!}
			                </label>
			               
			               </div>

			                @if($articulo->path == NULL)

		                          
		                           		 <img src="{{asset('/assets/images/no-imagen.jpg')}}"  onerror=" this.src = '../local/public/images/articulo.png';" 
			                             style ="width: 100%; height: 120px!important;object-fit: cover!important; border: 1px solid #1ab394;">
		                           

		                   @else
		                     
		                            <img src="{{asset('local/public/images')}}/{{$articulo->path}}" onerror=" this.src = '../local/public/images/articulo.png';" 			                             style ="width: 100%; height: 120px!important;object-fit: cover!important; border: 1px solid #1ab394;">
			                         
		                              
		                  @endif

                   		</div>
                   		<div class="col-md-4">
                   			<div class="hr-line-dashed"></div>
					  		   <div class="btn-group" style="width: 100%;">
				               <label title="Upload image file" for="inputImage" class="btn btn-primary">
							                {!! Form::label('adjunto','Subir doc adjunto',['class'=>'control-label']) !!} 
											{!! Form::file('rutadoc')!!}
				               </label>
				               
				               </div>

				                <p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Subir doc adjunto</p>
 
                   		</div>
                   		<div class="col-md-4">
                   				{!! Form::label('texto','Insertar Video') !!}

								{!! Form::text('video',null,['class' =>'form-control summernote ckeditor', 'placeholder' =>'URL YouTube de video'])!!}

								<p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Insertar Video </p>

								<p style=" margin-bottom: 86px;"></p>
                   		</div>

                   		<div class="col-md-12">
                   		    <br>
	                   		<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;">

								{!! Form::label('imagen','Galeria de Imagen') !!}
								{!! Form::file('ruta[]',['multiple'=>'multiple'])!!}
							   
						    </div>
	                   	</div>

                   	</div>

                    		
                   
                   
                </div>

                <div role="tabpanel" class="tab-pane" id="new">
                   	 
                   	  
 
					   <div class="col-md-6">
					  	<div class="form-group">
							{!! Form::label('precio','Precio oferta') !!}
							{!! Form::text('precio',null,['class' =>'form-control', 'placeholder' =>'Precio oferta'])!!}
						</div>
					  </div>
					  <div class="col-md-6">
					  	<div class="form-group">
							{!! Form::label('mapa','# Huesped') !!}
							{!! Form::text('mapa',null,['class' =>'form-control', 'placeholder' =>'# Huesped'])!!}
						</div>
					  </div>
					  <div class="col-md-6">
					  	<div class="form-group">
							{!! Form::label('seo_descripcion','SEO Descripción') !!}
							{!! Form::text('seo_descripcion',null,['class' =>'form-control', 'placeholder' =>'SEO Descripción'])!!}
						</div>
					  </div>
					  <div class="col-md-6">
					  	<div class="form-group">
							{!! Form::label('seo_keyword','SEO KEYWORD') !!}
							{!! Form::text('seo_keyword',null,['class' =>'form-control', 'placeholder' =>'Seo Keyword'])!!}
						</div>
					  </div>
				 
 					
 					<p>Campos Extras</p>

 					<div class="row">
			 
			 <div class="col-lg-12">
                    <div class="ibox-content">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">
                                	<?php

										foreach ($grupos as $grupo) {

                                    echo '<div class="panel panel-default">';
                                    	

										echo '<a data-toggle="collapse" data-parent="#accordion" href="#'.$grupo->id.'">
										<div class="panel-heading">
                                            <h5 class="panel-title">'
                                                .$grupo->descripcion. 
                                            '</h5> 
                                        </div> </a>';

                                        echo '<div id="'.$grupo->id.'" class="panel-collapse collapse">
                                            <div class="panel-body">';
										 
											        	$totalextra = count($camposextras);
										                     echo '<div class="row" style="margin-bottom: 6px;">';
										                      for($i=0;$i<$totalextra;$i++)
										                        {
										                       
										                         if ($grupo->id == $camposgruposid[$i]) {
										                         	echo '<div class="col-md-12">'.
										                            Form::label($camposextras[$i],$camposextrasdesc[$i].':',array("class"=>"col-md-7 control-label"));
										                         echo '<div class="col-md-5">';
										                          if($camposextrastipo[$i]=='date'){
										                              echo Form::input('date',$camposextras[$i], date('Y-m-d'), ['class' => 'form-control', 'placeholder' => 'Date']); 
										                          }elseif($camposextrastipo[$i]=='select'){  
										                               $camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$i] . ';';
										                               eval($camposeleccion);  
										                               echo Form::select($camposextras[$i],[null => "Escoja..."]+$camposeleccion,Input::old($camposextras[$i]), array('class' => 'form-control col-md-12',$camposextras[$i]));                
										                          }elseif($camposextrastipo[$i]=='textarea'){   
										                              echo Form::textarea($camposextras[$i], Input::old($camposextras[$i]), array('maxlength'=>'500','class' => 'form-control','placeholder'=>'Ingrese '.$camposextras[$i]));
										                          }elseif($camposextrastipo[$i]=='multipleselect'){  
										                              $camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$i] . ';';
										                              eval($camposeleccion);   
										                              echo Form::select($camposextras[$i].'[]',$camposeleccion,Input::old($camposextras[$i]), array('multiple class' => 'select2','style' => 'width:100%','placeholder'=>'Elija opción')); 
										                          }else{
										                              echo Form::text($camposextras[$i], Input::old($camposextras[$i]), array('class' => 'form-control','placeholder'=>'Ingrese valor '));
										                          }
										                           
										                          echo '</div>';
										                        echo '</div>';
										                         }
										                      
										                        

										                  }
										                   echo '</div>';

										                   echo "<br>";
				
                                        		echo '</div> </div>
                                        		</div>';
											     }           
											        
											 ?>		
                                        
                                         
                                               
                                           
                                    
                                     
                                </div>
                            </div>
                        </div>
                </div>
                   
                   
                </div>
              </div>

            </div>
          </div>

			 



			</div>	
	 			
		 
 	<br>
	 

	<div class="form-group">
		<button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Actualizar
		</button>
						
	</div>    


{!!Form::close()!!}
<br>

</div>
      </div>
    </section>
	
<script>

$('.selective').selectize({
    plugins: ['remove_button'],
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});

</script>

@endsection

