<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-vervideo"  >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true"></span>
				</button>
				<h4 class="modal-title">Visualizar Video</h4>
			</div>
			<div class="modal-body">
				<p>Eliminar Impresora: </p>
				<div id="reproductor">
					<iframe width='600' height='600' src='https://www.youtube.com/embed/"+videourl+"?rel=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>