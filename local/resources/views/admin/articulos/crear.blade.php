@extends('layouts.backend')
@section('titulo', 'Nuevo artículo')

@section('contenido')
<style>
#map {
	height: 400px;
	width: 100%;
}
#elim{
	color:#fff;
	border:1px solid #ccc;
	background-color:#fb0000;
	margin-right:10px;
	padding: 0 2px;
	border-radius:4px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	-o-border-radius:4px;
	border-radius:4px;
	font-weight:bold;
	font-size:0.8em;
	cursor:pointer;
}
#map-canvas {
	height: 100%;
	margin: 0;
}
#map-canvas > .centerMarker {
	position: absolute;
	/*url of the marker*/
	background: url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;
	/*center the marker*/
	top: 50%;
	left: 50%;
	z-index: 1;
	/*fix offset when needed*/
	margin-left: -10px;
	margin-top: -34px;
	/*size of the image*/
	height: 34px;
	width: 20px;
	cursor: pointer;
}
/* css galeria */
#reorder-helper{margin: 18px 10px;padding: 10px;}
.light_box {
	background: #efefef;
	padding: 20px;
	margin: 10px 0;
	text-align: center;
	font-size: 1.2em;
}
.gallery{ width:100%; float:left; margin-top:10px;padding-left:5px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
/* style 6 */
.inputimg + label {
	color: #ec9b1b;
}
.inputimg:focus + label,
.inputimg.has-focus + label,
.inputimg + label:hover {
	color: #d47e03;
}
.inputimg + label figure {
	width: 100px;
	height: 135px;
	background-color: #ec9b1b;
	display: block;
	position: relative;
	padding: 30px;
	margin: 0 auto 10px;
}
.inputimg:focus + label figure,
.inputimg.has-focus + label figure,
.inputimg + label:hover figure {
	background-color: #d47e03;
}
.inputimg + label figure::before,
.inputimg + label figure::after {
	width: 0;
	height: 0;
	content: '';
	position: absolute;
	top: 0;
	right: 0;
}
.inputimg + label figure::before {
	border-top: 20px solid #05354e;
	border-left: 20px solid transparent;
}
.inputimg + label figure::after {
	border-bottom: 20px solid #d47e03;
	border-right: 20px solid transparent;
}
.inputimg:focus + label figure::after,
.inputimg.has-focus + label figure::after,
.inputimg + label:hover figure::after {
	border-bottom-color: #ec9b1b;
}
.inputimg + label svg {
	width: 100%;
	height: 100%;
	fill: #f1e5e6;
}
</style>

{!! Form::open(['route'=>'articulos.store','method'=>'POST','class'=>'dropzone','id'=>'my-dropzone','files'=>true]) !!}
{!! csrf_field() !!}
<div class="row" style="background: #f4f4f4;padding: 18px;border-radius: 8px;border: 1px solid #e7eaec;">
	<div class="col-md-12">
		<div class="row">

			<div class="col-md-7">
				<div class="form-group">
					{!! Form::label('titulo','Título') !!}
					{!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
				</div>
			</div>
			
			<div class="col-md-5" style="padding-top: 16px;">
				<div class="form-group" style="padding: 6px; " >
					<input type="checkbox" data-on="Destacar" data-off="No Destacar"  data-toggle="toggle" name="destacado" id="destacado">
					<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="activado" id="activado" type="checkbox">
					<input data-toggle="toggle" data-on="Redes Sociales" data-off="Redes Sociales" name="publicar" id="publicar" type="checkbox">
				</div>
			</div>
			
		</div>
		<div = class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('categoria','Categoria') !!}
					{!! Form::select('id_categoria',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas','id' =>'categorias'])!!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('tags[]', 'Tags o Etiquetas') !!}
					{!! Form::select('tags[]', $tags, null,['class'=>'form-control selective','multiple'=>'multiple', 'id' => 'etiquetas']) !!}
				</div>
			</div>
		</div>
		<div id ="calendario" style="display: none;">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('fechainicio','Fecha y hora de Inicio') !!}
						<div class='input-group date' id='datetimeinicio'>
							<input type='text' name="fechainicio" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('fechafin','Fecha y hora de Finalización') !!}
						<div class='input-group date' id='datetimefin'>
							<input type='text' name="fechafin" class="form-control" />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('prioridad','Prioridad') !!}
						{!! Form::select('id_prioridad',$prioridad, null,[ 'class' =>'form-control' , 'listprop' =>'listasprio','id' =>'prioridad'])!!}
					</div>
				</div>
			</div>
		</div>
		<div id="lugar" style="display: none;">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{!! Form::label('lugar','Lugar del Evento ( 50 Caracteres máximo)') !!}
						{!! Form::text('lugar',null,['class' =>'form-control', 'placeholder' =>'Lugar del Evento','maxlength'=>'50'])!!}
					</div>
				</div>
			</div>
		</div>
	<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;">
			<center>
				{!! Form::label('imagen','Galeria de Imagen') !!}
			</center>
			{!! Form::open(array('url'=>'upload','method'=>'POST', 'id'=>'frmA')) !!}
			<meta name="_token" content="{!! csrf_token() !!}"/>
			<div class="row">
				<div class="col-md-3">
					<div class="btn-group" style="width: 100%;">
						<center>
							<label title="Upload image file" for="inputImage" class="btn btn-primary">
								{!! Form::label('imagen','Imagen principal',['class'=>'control-label']) !!}
								{!! Form::file('imagenprincipal',['id'=>'imagenprincipal','accept'=>'image/*','style'=>'display:none;'])!!}
							</label>
						</center>
					</div>
					<label for="imagenprincipal">
						<img id="imgprincipal" src="{{asset('/assets/images/no-imagen.jpg')}}"
						style ="width: 100%; object-fit: cover!important; border: 1px solid #1ab394;">
					</label>
				</div>
				<div class="col-md-9">
					<div class="gallery">
						<ul class="reorder_ul reorder-photos-list" id="nuevalista">
							<!--Lista de fotos -->
							@for ($i = 1; $i <= 10 ; $i++)
								<li>
									<a href="javascript:void(0);" style="float:none;" class="image_link">
										<input type="file" name="file-{{ $i }}" id="file-{{ $i }}" data-n="{{ $i }}" class="inputfile inputimg"  style='display:none;' accept="image/*" />
										<label for="file-{{ $i }}">
											<img id="imgfile-{{ $i }}" style="height: 100px;padding: 6px;" src="{{asset('/assets/images/no-imagen.jpg')}}" >
											<a class="btn btn-danger btn-xs deleteImage" id="elim{{ $i }}"  onclick="eliminarElemento('imgfile-{{ $i }}','elim{{ $i }}');" style="display:none;"><i class="fa fa-trash-o"></i></a>
										</label>
									</a>
								</li>
							@endfor
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('texto','Contenido') !!}
			{!! Form::textarea('texto',null,['class' =>'form-control summernote ckeditor', 'placeholder' =>'Contenido del artículo','required'])!!}
		</div>
		<div class="form-group">
			{!! Form::label('descripcionredes','Comentario en Redes Sociales ( 200 caracteres máximo)') !!}
			{!! Form::text('descripcionredes',null,['class' =>'form-control', 'placeholder' =>'Comentario en Redes Sociales','maxlength'=>'200'])!!}
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;">
					<div class="btn-group" style="width: 100%;">
						<center>
							<p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Lista de Documentos</p>
							<ul class="reorder-documentos-list reorder_ul list-group" id="listadocs" name="documentos">
							</ul>
							<div id="subiendodoc" style="display:none;">Subiendo Documento Espere...</div>
							<div id="barra"  class="progress progress-striped active" style="width: 90%;display:none;">
								<div id="porcentaje" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="progress-bar progress-bar-success" >
								</div>
							</div>
								<button class="btn btn-danger" type="button" id="btncancelardoc" style="display: none;">Cancelar</button>
							<label id="botonsubirdoc" title="Upload image file" for="inputImage" class="btn btn-primary">
								<a href="javascript:void(0);" style="float:none;" class="image_link">
									<input type="file" name="doc" id="doc" class="inputfile inputimg"  style='display:none;'  />
									<label for="doc"  style="color: white;">
										<img id="imgfile-1" style="height: 30px;" src="{{asset('/assets/images/newdoc.png')}}" >
										Agregar Documento
									</label>
								</a>
							</label>
						</center>
					</div>
					<select name="rutadocs[]" id="rutadocs" multiple="multiple" style='display:none;' >
					</select>
					<select name="titulodocs[]" id="titulodocs" multiple="multiple" style='display:none;' >
					</select>
					<select name="sobrasdocs[]" id="sobrasdocs" multiple="multiple" style='display:none;' >
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;">
					<center>
						<p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Lista de Videos</p>
						<ul class="reorder-videos-list reorder_ul list-group" id="listaDesordenada" name="videos">
						</ul>
					</center>
					<div class="input-group">
						{!! Form::text('video',null,['class' =>'form-control summernote ckeditor', 'id'=>'nuevo_video' ,'placeholder' =>'URL YouTube/Facebook/Vimeo de video'])!!}<span class="input-group-btn"><button  class="btn btn-primary"  type="button" id="btnagregavideo" align="center" onclick="return add_li()" ><img  style="height: 20px;" src="{{asset('/assets/images/addvideo.png')}}" > Agregar Video</button></span>
					</div>
					<select name="videos[]" id="videos" multiple="multiple" style='display:none;'>
					</select>
					<select name="titulovideo[]" id="titulovideo" multiple="multiple" style='display:none;'>
					</select>
					<select name="sobrasvideos[]" id="sobrasvideos" multiple="multiple" style='display:none;' ></select>
					<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-vervideo"  >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true"></span>
									</button>
									<h4 class="modal-title">Visualizar Video</h4>
								</div>
								<div class="modal-body">
									<div id="reproductor" >
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
		<select name="ruta[]" id="ruta" multiple="multiple" style='display:none;' >
		</select>
		<select name="sobras[]" id="sobras" multiple="multiple" style='display:none;' >
		</select>
		<div class="form-group">
			{!! Form::hidden('id_usuario', Auth::user()->id, null,[]) !!}
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('latitud','Latitud') !!}
							{!! Form::text('latitud',null,['class' =>'form-control', 'placeholder' =>'Latitud'])!!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('longitud','Longitud') !!}
							{!! Form::text('longitud',null,['class' =>'form-control', 'placeholder' =>'Longitud'])!!}
						</div>
					</div>
					<div class="col-md-12">
						<h3>Mapa</h3>
						<div id="map"></div>
						<br>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12">
						<div class="form-group">
							{!! Form::label('seo_descripcion','SEO Descripción') !!}
							{!! Form::textarea('seo_descripcion',null,['class' =>'form-control', 'placeholder' =>'SEO Descripción','style'=>'resize: none','rows'=>'5'])!!}
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							{!! Form::label('seo_keyword','SEO KEYWORD') !!}
							{!! Form::text('seo_keyword',null,['class' =>'form-control', 'placeholder' =>'Seo Keyword'])!!}
						</div>
					</div>
				</div>
			</div>
		</div>
		<p>Campos Extras</p>
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox-content">
					<div class="panel-body">
						<div class="panel-group" id="accordion">
							@php
								$totalExtras = count($camposextras);
							@endphp
							@foreach ($grupos as $grupo)
								<div class="panel panel-default">
									<a data-toggle="collapse" data-parent="#accordion" href="#{{ $grupo->id }}">
										<div class="panel-heading">
											<h5 class="panel-title">{{ $grupo->descripcion }}</h5>
										</div>
									</a>
									<div class="panel-collapse collapse" id="{{ $grupo->id }}">
										<div class="panel-body">
											<div class="row" style="margin-bottom: 6px;">
												<div class="col-md-12">
													@for ($i = 0; $i < $totalExtras ; $i++)
													@if ( $grupo->id == $camposgruposid[$i] )
														

														{!! Form::label($camposextras[$i],$camposextrasdesc[$i].':',array("class"=>"col-md-5 control-label")) !!}
														<div class="col-md-7">
														@if ( $camposextrastipo[$i]=='date' )
															{!! Form::input('date',$camposextras[$i], date('Y-m-d'), ['class' => 'form-control', 'placeholder' => 'Date']) !!}
														@elseif ($camposextrastipo[$i]=='select')
															@php
															$camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$i] . ';';
															eval($camposeleccion);
															echo Form::select($camposextras[$i],[null => "Escoja..."]+$camposeleccion,Input::old($camposextras[$i]), array('class' => 'form-control col-md-12',$camposextras[$i]));
															@endphp
														@elseif($camposextrastipo[$i]=='textarea')
															{!! Form::textarea($camposextras[$i], Input::old($camposextras[$i]), array('maxlength'=>'500','class' => 'form-control','placeholder'=>'Ingrese '.$camposextras[$i])) !!}
														@elseif($camposextrastipo[$i]=='multipleselect')
															@php
															$camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$i] . ';';
															eval($camposeleccion);
															echo Form::select($camposextras[$i].'[]',$camposeleccion,Input::old($camposextras[$i]), array('multiple class' => 'select2','style' => 'width:100%','placeholder'=>'Elija opción'));
															@endphp	

														@else
															{!! Form::text($camposextras[$i], Input::old($camposextras[$i]), array('class' => 'form-control','placeholder'=>'Ingrese valor ')) !!}
														@endif
														</div>
													@endif
													@endfor												
												</div>
											</div>
										</div>										
									</div>
								</div>
							@endforeach
							
							<?php
							/*
							foreach ($grupos as $grupo) {
								echo '<div class="panel panel-default">';
								echo '<a data-toggle="collapse" data-parent="#accordion" href="#'.$grupo->id.'">
								<div class="panel-heading">
								<h5 class="panel-title">'
								.$grupo->descripcion.
								'</h5>
								</div> </a>';
								echo '<div id="'.$grupo->id.'" class="panel-collapse collapse">
								<div class="panel-body">';
								$totalextra = count($camposextras);
								echo '<div class="row" style="margin-bottom: 6px;">';
								for($i=0;$i<$totalextra;$i++)
								{
									if ($grupo->id == $camposgruposid[$i]) {
										echo '<div class="col-md-12">'.
										Form::label($camposextras[$i],$camposextrasdesc[$i].':',array("class"=>"col-md-7 control-label"));
										echo '<div class="col-md-5">';
										if($camposextrastipo[$i]=='date'){
											echo Form::input('date',$camposextras[$i], date('Y-m-d'), ['class' => 'form-control', 'placeholder' => 'Date']);
										}elseif($camposextrastipo[$i]=='select'){
											$camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$i] . ';';
											eval($camposeleccion);
											echo Form::select($camposextras[$i],[null => "Escoja..."]+$camposeleccion,Input::old($camposextras[$i]), array('class' => 'form-control col-md-12',$camposextras[$i]));
										}elseif($camposextrastipo[$i]=='textarea'){
											echo Form::textarea($camposextras[$i], Input::old($camposextras[$i]), array('maxlength'=>'500','class' => 'form-control','placeholder'=>'Ingrese '.$camposextras[$i]));
										}elseif($camposextrastipo[$i]=='multipleselect'){
											$camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$i] . ';';
											eval($camposeleccion);
											echo Form::select($camposextras[$i].'[]',$camposeleccion,Input::old($camposextras[$i]), array('multiple class' => 'select2','style' => 'width:100%','placeholder'=>'Elija opción'));
										}else{
											echo Form::text($camposextras[$i], Input::old($camposextras[$i]), array('class' => 'form-control','placeholder'=>'Ingrese valor '));
										}
										echo '</div>';
										echo '</div>';
									}
								}
								echo '</div>';
								echo "<br>";
								echo '</div> </div>
								</div>';
							}
							*/
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="form-group">
			<button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Guardar
			</button>
			<a class="btn btn-primary dim" href="{{ URL::asset('articulos')}}"><i class="fa fa-chevron-left"></i> Atras
			</a>
		</div>
	</div>
</div>
{!! Form::close() !!}

@endsection

@section('script')
<script>

	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

	function initMap() {
		var myLatlng = {lat: -0.9683171526333203, lng: -80.70951460000003};
			// var mapOptions = {
			//    zoom: 18,
			// mapTypeId: google.maps.MapTypeId.HYBRID,
			//           center: myLatlng,
			//           label: 1
			//         }
			//         var map = new google.maps.Map(document.getElementById("map"), mapOptions);
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: myLatlng
		});
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			draggable:true,
			title:"Ubicacion"
		});
		// To add the marker to the map, call setMap();
		marker.setMap(map);
		marker.bindTo('position', map, 'center');
		google.maps.event.addListener(map, "center_changed", function() {
			var markerLatLng = marker.getPosition();
			$("#latitud").val(markerLatLng.lat());
			$("#longitud").val(markerLatLng.lng());
		});
		cargado = 1;
	}

	

	$( document ).ready(function() {

		$('.selective').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: false,
			create: function(input) {
				return {
					value: input,
					text: input
				}
			}
		});
		
		var ahora=moment();

		$('#datetimeinicio').datetimepicker({
			locale: 'es',
			minDate: ahora,
			format:'YYYY/MM/DD HH:mm',
		});
		$('#datetimefin').datetimepicker({
			useCurrent: false, //Important! See issue #1075
			locale: 'es',
			format:'YYYY/MM/DD HH:mm',
		});
		$("#datetimeinicio").on("dp.change", function (e) {
			$('#datetimefin').data("DateTimePicker").minDate(e.date);
		});
		$("#datetimefin").on("dp.change", function (e) {
			$('#datetimeinicio').data("DateTimePicker").maxDate(e.date);
		});

		$('#categorias').on('change', function(e){
			var etiquetas = $('#etiquetas');
			var id_categoria = $(this).val();
			$.ajax({
				url: '{{ URL::to('/getTags') }}/'+id_categoria,
				data: '',
				method: 'POST',
				dataType: 'json',
				success: function(data){
					etiquetas.selectize()[0].selectize.destroy();

					etiquetas.empty();
					$.each(data, function (i, item) {
					    etiquetas.append($('<option>', { 
					        value: item.id,
					        text : item.tag 
					    }));
					});

					etiquetas.selectize({
						plugins: ['remove_button'],
						delimiter: ',',
						persist: false,
						create: function(input) {
							return {
								value: input,
								text: input
							}
						}
					});
				}, error: function(j){
					console.log(j);
					toastr.error('Error al obtener datos');
				}

			});
		});

	});	
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>
@include('scripts.articuloscript');

@endsection