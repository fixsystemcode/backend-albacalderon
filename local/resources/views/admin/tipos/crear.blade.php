@extends('layouts.backend')

@section('titulo', 'Nuevo')

@section('contenido')
	@include('alerts.request')
	{!! Form::open(['route' => 'tipos-usuarios.store', 'method' => 'POST','files'=>true]) !!}
		<div class="form-group">
			{!! Form::label('tipo', 'Tipo:') !!}
			{!! Form::text('tipo',null,['class'=>'form-control','placeholder'=>'Ingrese tipo de usuario']) !!}
		</div>
		{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	{!! Form::close() !!}
@endsection