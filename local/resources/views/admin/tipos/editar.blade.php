@extends('layouts.backend')

@section('titulo', 'Editar')

@section('contenido')

@include('alerts.request')
	{!! Form::model($tipo, array('method' => 'PATCH', 'route' => array('tipos-usuarios.update', $tipo->id))) !!}
		<div class="form-group">
			{!! Form::label('tipo', 'Tipo:') !!}
			{!! Form::text('tipo',null,['class'=>'form-control','placeholder'=>'Ingrese tipo de usuario']) !!}
		</div>
		{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	{!! Form::close() !!}
@stop