<?php
	$es_paginado = (method_exists($tabla, 'render'));
?>
@extends('layouts.backend')
@section ('titulo') {{ $configuracion['titulo'] }} @stop
@section ('contenido')

	@include('alerts.success')
	@include('alerts.errors')

	<style type="text/css">
		form.form-inline {
		    display: inline-block;
		}
		#separador{
			margin-bottom: 5px;
		}
	</style>
	
	<div class="card">
		<div class="card-body">
		    <div class="formulario">
		        @if (isset($filtros_vista) && count($filtros_vista) > 0)
		            <form method="GET">
		                @foreach ($filtros_vista as $nombre_filtro => $filtro)
		                    <?php
		                        $filtro_tipo = isset($filtro['type']) ? $filtro['type'] : '';
		                    ?>
		                    <div class="col-md-3">
		                        <label for="{{ $nombre_filtro }}">{{ $filtro['text'] }}</label>
		                        @if ($filtro_tipo == 'date')
		                            <div class="input-group date cualquier_fecha">
		                                <input type="text" id="{{ $nombre_filtro }}" name="{{ $nombre_filtro }}" value="{{ request()->has($nombre_filtro) ? request()->{$nombre_filtro} : date('Y-m-d') }}" class="form-control calendario {{ $filtro['class'] }}" readonly>
		                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
		                            </div>
		                        @elseif($filtro_tipo == 'select')
		                            {!! Form::select($nombre_filtro,$filtro['options'], (request()->has($nombre_filtro) ? request()->{$nombre_filtro} : '') ,['class' =>'form-control selective '.$filtro['class'], 'placeholder'=>'Seleccione'])!!}
		                        @else
		                            <input type="text" name="{{ $nombre_filtro }}" id="{{ $nombre_filtro }}" value="{{ (request()->has($nombre_filtro) ? request()->{$nombre_filtro} : '') }}" class="form-control {{ $filtro['class'] }}">
		                        @endif                        
		                    </div>
		                @endforeach
		                <div class="col-md-3">
		                    <label for="">&nbsp;</label>
		                    <button type="submit" class="btn btn-info btn-block">Filtrar</button>
		                </div>  
		            </form>
		            <div class="clearfix"></div>
		            <br>
		        @endif      
		    </div>

		    <div>
		        @if($configuracion['todos'])
		        	<a  href="{{ url($configuracion['url']) }}" class="btn btn-primary"> <i class="fa fa-list"></i> Todos</a>
		        @endif
		        @if($configuracion['crear'] && $puede_crear)
		            <a href="{{ url($configuracion['url'].'/create') }}" class="btn btn-info"> <i class="fa fa-plus"></i> Nuevo</a> 
		        @endif
		        @if($es_paginado)
		        	<a onclick="exportarexcel()" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Exportar Excel</a>
		        @endif      
		    </div>
		    <hr>
		    @if($es_paginado)
			    <div class="row" style="background: #e5e6e7; margin-bottom: 5px;padding: 8px;border: 1px solid white;">
			        <label for="txtfiltro" class="col-lg-1 col-md-1" style="font-size: 1.5em;">Buscar</label>
			        <div class="col-lg-11 col-md-11" style="margin-bottom: 0px;">
			            <div class="form-group">
			                <input type="text" placeholder="Búsqueda" class="form-control" onkeyup="filtrardato($(this).val())" onchange="filtrardato($(this).val())" id="txtfiltro">
			            </div>
			        </div>
			    </div>
		    @endif
		    <table id="table-render" class="table table-striped table-bordered table-hover display buscar">
		    	<thead>
		    		<tr>
		    			@foreach($campos as $value)
		    				<th>{{ $value->Descripcion }}</th>
		    			@endforeach
		    			<th>ACCIÓN</th>
		    		</tr>
		    	</thead>
		    	<tfoot>
		    		<tr>
		    			@foreach($campos as $value)
		    				<th>{{ $value->Descripcion }}</th>
		    			@endforeach
		    			<th>ACCIÓN</th>
		    		</tr>
		    	</tfoot>
		        <tbody>
		            @foreach($tabla as $key => $value)        
		            <tr>                
		                @foreach($campos as $id => $campo)
			                <td>
			                    @if ($campo->tipo == 'date')
			                    	{!! date('Y-m-d', strtotime($value->{$campo->Nombre})) !!}
			                    @elseif($campo->tipo == 'datetime')
			                    	{!! date('Y-m-d H:i:s', strtotime($value->{$campo->Nombre})) !!}
			                    @elseif($campo->tipo == 'list')
			                    	{{--<span class="badge {{ $campo->Clase }}" >{!! $campo->Valor->{$value->{$campo->Nombre}} !!}</span>--}}
			                    	@foreach($campo->Valor as $i => $item)
			                    		@if($i == $value->{$campo->Nombre} )
			                    			<span class="badge {{ $item->Clase }}" >{!! $item->Valor !!}</span>
			                    		@endif
			                    	@endforeach
			                    @elseif($campo->tipo == 'html')
			                    	{!! $value->{$campo->Nombre} !!}
			                    @else
			                    	{{ $value->{$campo->Nombre} }}
			                    @endif
			                </td>
		                @endforeach
		                <td>
		                	@if($configuracion['ver'])                   
			                    <a class= "btn btn-success btn-sm" title="Ver registro" href="{{ url($configuracion['url'].'/'.$value->id) }}">
			                        <i class="fa fa-eye"></i>
			                    </a>
		                    @endif
		                    @if($configuracion['editar'] && $puede_editar)
		                        <a class="btn btn-warning btn-sm" title="Editar este registro" href="{{ url($configuracion['url'].'/'.$value->id.'/edit') }}">
		                            <i class="fa fa-pencil-square-o"></i>
		                        </a>
		                    @endif
		                    @if($configuracion['eliminar'] && $puede_eliminar)
		                    	{!! Form::open(['route' => [$configuracion['url'].'.destroy', $value->id], 'method'=>'delete', 'id'=>'frmElimina'.$value->id,'class'=>'form-inline']) !!}
						            <a href="#" class= "btn btn-danger btn-sm" title="Eliminar este registro" onclick="eliminar({{ $value->id }}, $(this).closest('tr'))">
		                                <i class="fa fa-trash"></i>
		                            </a>
						        {!! Form::close() !!}
		                    @endif
		                </td>
		            </tr>
		            @endforeach
		        </tbody>    
		    </table>
		   	@if($es_paginado)
		    	{!! $tabla->appends($_GET)->render() !!}
		    @endif 
		</div>   
	</div>
@endsection
@section ('scripts')
	<script>
		@if(!$es_paginado)
		$(document).ready(function() {
			createDatatable('#table-render');
		});
		@else
		//SI ES PAGINADO entonces muestre el boton de exportar excel
		function  exportarexcel(){
			$("#table-render").table2excel({
				name: "Datos",
				filename: "{{ $configuracion['titulo'] }}", //do not include extension
				exclude_img: false,
			});
		}

		function filtrardato(cadena) {
		    var rex = new RegExp(cadena, 'i');
		    $('.buscar tbody tr').hide();
		    $('.buscar tbody tr').filter(function () {
		        return rex.test($(this).text());
		    }).show();
		    $('.ocultar_detalles').html('');
		    $('.aparecertr').show();
		}
		@endif
		function eliminar(id, fila){
			confirmModal('¿Desea eliminar?', 'Seguro de eliminar este registro?', 'warning', function{				
				$('#frmElimina'+id).on('submit', function (e) {
					e.preventDefault();
					url = $(this).attr('action');
					formu = $(this).serialize();

					$.ajax({
						type: 'POST',
						url: url,
						data: formu
					}).done(function(data){
						showtoast('Success',data,'success');
						fila.remove();
					}).fail(function($e){
						console.log($e)							
						showtoast('Error','No se pudo completar el proceso', 'error');
					});

				});
				$('#frmElimina'+id).submit();
			});
		}
	</script>
@endsection