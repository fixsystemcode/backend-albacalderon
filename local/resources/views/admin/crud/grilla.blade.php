@extends ('layouts.backend')
@section('titulo', $configuracion['titulo'])
@section ('contenido')
	<div class="card" id="bloquear">
		<div class="card-body">
			<div class="formulario">
				@if (isset($filtros_vista) && count($filtros_vista) > 0)
					<form method="GET">
						@foreach ($filtros_vista as $index => $filtro)
							<?php $filtro_tipo = isset($filtro['type']) ? $filtro['type'] : ''; ?>
							<div class="col-md-3">
								<label for="{{ $index }}">{{ $filtro['text'] }}</label>
								@if ($filtro_tipo == 'date')
									<div class="input-group date cualquier_fecha">
										<input type="text" id="{{ $index }}" name="{{ $index }}" 
											value="{{ request()->has($index) ? request()->{$index} : date('Y-m-d') }}" 
											class="form-control calendario {{ $filtro['class'] }}" readonly>
										<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
									</div>
								@elseif($filtro_tipo == 'select')
									<select id="{{ $index }}" name="{{ $index }}" class="form-control {{ $filtro['class'] }}">
										@foreach ($filtro['options'] as $key => $value)
											<option value="{{ $key }}" {{ request($index, null) == $key ? 'selected' : '' }}>{{ $value }}</option>
										@endforeach
									</select>
								@else
									<input type="text" name="{{ $index }}" id="{{ $index }}" class="form-control {{ $filtro['class'] }}"
										value="{{ (request()->has($index) ? request()->{$index} : '') }}">
								@endif
							</div>
						@endforeach
						<div class="col-md-3">
							<label for="">&nbsp;</label>
							<button type="submit" class="btn btn-info btn-block">Filtrar</button>
						</div>
					</form>
					<div class="clearfix"></div>
					<br>
				@endif
			</div>

			<div>
				@if($configuracion['todos'])
		        	<a href="{{ url($configuracion['url']) }}" class="btn btn-primary"> <i class="fa fa-list"></i> Todos</a>
		        @endif
		        @if($configuracion['crear'] && $puede_crear)
		            <a id="nuevo" class="btn btn-info" accion="crear"> <i class="fa fa-plus"></i> Nuevo</a> 
		        @endif
			</div>
			<br>

			<div class="table-responsive">
				<table id="mainTable" class="table table-striped" >
					<thead>
						<tr>
							<th>ID</th>
							@foreach($campos as $campo)
								<th>{{ $campo->texto }}</th>
							@endforeach
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tabla as $key => $value)
							<tr id="tr{{ $value->id }}">
								<td class="edit-disabled" data-name="id">{{ $value->id }}</td>
								@foreach ($campos as $campo)
									@if ($campo->tipo == 'disabled')
										<td class="edit-disabled" data-name="{{ $campo->nombre }}">{{ $value->{$campo->nombre} }}</td>
									@elseif($campo->tipo == 'html')
										<td data-name="{{ $campo->nombre }}">{!! $value->{$campo->nombre} !!}</td>
									@elseif($campo->tipo == 'select')
										<td>
											{!! Html::select($campo->nombre, $campo->options,$value->{$campo->nombre},['class'=>'form-control']) !!}
										</td>
									@elseif($campo->tipo == 'enum')
										<?php $array = (array) $campo->lista; ?>
										<td class="edit-disabled">{{ $array[$value->{$campo->nombre}] }}</td>
									@else
										<td data-name="{{ $campo->nombre }}">{{ $value->{$campo->nombre} }}</td>
									@endif
								@endforeach
								<td class="edit-disabled">
									@if($puede_editar)
										<a class="btn btn-link btn-sm text-success botonActualizar" data-id="{{ $value->id }}">
											<i class="fa fa-save"></i> Guardar
										</a>
									@endif
									@if ($configuracion['eliminar'] && $puede_eliminar)
										<a class="btn btn-link text-danger btn-sm botonEliminar" data-id="{{ $value->id }}">
											<i class="fa fa-trash"></i> Eliminar
										</a>
									@endif									
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@section ('script')
	{!! Html::script('admin/plantilla1/js/plugins/otros/js/jquery.blockUI.js') !!}
	{!! Html::script('admin/plantilla1/js/plugins/otros/js/mindmup-editabletable.js') !!}
	<script type="text/javascript">
		$(document).ready(function(){
			editable();
		});
		
		function editable(){
			$('#mainTable').editableTableWidget().find('td:first').focus();
		}	

		$(document).on('click','.botonActualizar', function(){
			actualizardatos($(this).attr('data-id'));
		});

		$(document).on('click','.botonEliminar', function(){
			var  n_id = $(this).attr('data-id');
			if(n_id == '0') removeTr(n_id);
			else eliminarDatos(n_id);
		});

		function actualizardatos(id){
			bloquearPantalla(true, '#bloquear');
			var formData = new FormData();
			$('#tr'+id).find('td').each(function(i, campo){
				if($(campo).attr('data-name') != undefined){
					formData.append($(campo).attr('data-name'), $(campo).text());
				}else{
					var campo2 = $(campo).find('select');
					if(campo2.attr('name') != undefined){
						formData.append($(campo2).attr('name'), $(campo2).val());
					}
				}
		    });

			$.ajax({
				headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data: formData,
				type: 'POST',
				url: '{{ url('/').'/'.$configuracion['url'] }}',
				processData: false,
				contentType: false	            
		    }).done(function(data) {
	        	if($.isNumeric(data) == true){
            		$("#tr0 td[data-name=id]").html(data);
            		$("#tr0").attr('id', 'tr'+data);
            		$("#nuevoguardar").attr('data-id',data).attr('id', '');            		
            		$("#nuevoEliminar").attr('data-id',data).attr('id', '');

	        		$('#tr'+data).css('background','#79ffd1');
	        		showToast('Correcto','Registro actualizado correctamente','success',10000);
	        	}else{
	        		showToast('Informacion','El registro fue correcto, pero termino de manera inesperada','warning',10000);
	        	}
	        	bloquearPantalla(false, '#bloquear');
	        }).fail(function(jqXHR, textStatus, errorThrown) {
	        	validacionCampos(jqXHR, textStatus, errorThrown)
	        	bloquearPantalla(false, '#bloquear');
	        });
		}

		function removeTr(id){
			$('#tr'+id).remove();
		}

		function eliminarDatos(id){
		    $.ajax({
		    	headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'DELETE',
				url: '{{ url('/').'/'.$configuracion['url'] }}/'+id,           
		    }).done(function(data) {
		    	if($.isNumeric(data) == true) removeTr(id);
		    	else showToast('Informacion', data ,'warning',10000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
	        	validacionCampos(jqXHR, textStatus, errorThrown)
	        	bloquearPantalla(false, '#bloquear');
	        });
		}
		$(document).on('click','#nuevo', function(){
			agregarrow();
		});

		function agregarrow()
		{	
			$('#mainTable tr:first').after(
				'<tr id="tr0">'+
				'<td class="edit-disabled" data-name="id">0</td>'+
				@foreach ($campos as $campo)
					@if($campo->tipo == 'select')
					'<td>{!! Html::select($campo->nombre, $campo->options,null,['class'=>'form-control']) !!}</td>'+
					@else
					'<td data-name="{{ $campo->nombre }}"></td>'+
					@endif
				@endforeach
				'<td class="edit-disabled">'+
					@if ($puede_editar)
						'<a id="nuevoguardar" data-id="0" class="btn btn-link text-success btn-sm botonActualizar">'+
						'<i class="fa fa-save"></i> Guardar</a>'+
					@endif
					@if($configuracion['eliminar'] && $puede_eliminar)
						'<a id="nuevoEliminar" data-id="0" class="btn btn-link text-danger btn-sm botonEliminar">'+
						'<i class="fa fa-trash"></i> Eliminar</a>'+
					@endif
				'</td></tr>'
			);
			editable();
		}
	</script>
@endsection