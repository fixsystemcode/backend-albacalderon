@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <h1 style="margin-right: 30px; margin-bottom: 0px;">Carrito de compras</h1>
</div>
@endsection
@section('contenido')
<style type="text/css">
	h2{
		color: #009DE2;
	}
	#upload-progress{
		display: none;
	    width: 100%;
	    background: #2f353aa6;
	}
</style>
    @if(Session::has('message'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif
    {!! Form::open(['route'=>'carrito.store','method'=>'POST','id'=>'formulario','files'=>true]) !!}
	{!! csrf_field() !!}
        {{-- <h2>Activar carrito de compra</h2>
            <label class="switchBtn">
                <input type="checkbox" data-toggle="toggle" data-on="Activado" data-off="Desactivado" class="carrito" onchange="carritoIO()" {{ $carrito->estado == 1? 'checked': '' }}>
                <input type="hidden" name ="carrito" id="carrito" value="{{$carrito->estado}}">
            </label> --}}
        <h2>Metodos de pago</h2>
            <h4>PayPal</h4> 
            <label class="switchBtn">
                <input type="checkbox" data-toggle="toggle" data-on="Activado" data-off="Desactivado" class="paypal" onchange="paypalIO()" {{ $carrito->estado_paypal == 1? 'checked': '' }}>
                <input type="hidden" name ="estado_paypal" id="estado_paypal" value="{{$carrito->estado_paypal}}">
            </label>
            <hr>      
            <div id="div_paypal" class="row">
                <div class="col-md-3">
                    <label for="paypal" class="label-control">E-mail:</label>
                </div>
                <div class="col-md-9">
                    <input type="text" name="paypal" id="paypal" class="form-control" value="{{ $carrito->paypal }}">
                </div>
            </div>
    		<h4>Tarjetas</h4> 
            <label class="switchBtn">
                <input type="checkbox" data-toggle="toggle" data-on="Activado" data-off="Desactivado" class="tarjeta" onchange="tarjetaIO()" {{ $carrito->estado_tarjeta == 1? 'checked': '' }}>
                <input type="hidden" name ="estado_tarjeta" id="estado_tarjeta" value="{{$carrito->estado_tarjeta}}">
            </label>
    		<hr>
            <div id="div_tarjeta">
                <div class="row">  
                    <div class="col-md-3">
                        <label for="paypal" class="label-control">App Code:</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" name="app_code" id="app_code" class="form-control" value="{{ $carrito->app_code }}">
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-md-3">
                        <label for="paypal" class="label-control">App Key:</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" name="app_key" id="app_key" class="form-control" value="{{ $carrito->app_key }}">
                    </div>
                </div> --}}
    	    </div>
    		<h4>Deposito Bancario</h4> 
                <label class="switchBtn">
                    <input type="checkbox" data-toggle="toggle" data-on="Activado" data-off="Desactivado" class="deposito" onchange="depositoIO()" {{ $carrito->estado_deposito == 1? 'checked': '' }}>
                    <input type="hidden" name ="estado_deposito" id="estado_deposito" value="{{$carrito->estado_deposito}}">
                </label>
    		<hr>
            <div id="div_deposito">
                <div class="row">
        			<div class="col-md-3">
        				<label for="paypal" class="label-control">Numero de Cuenta:</label>
        			</div>
            		<div class="col-md-9">
            			<input type="text" name="num_cuenta" id="num_cuenta" class="form-control" value="{{ $carrito->num_cuenta }}">
            		</div>
                </div>
                <div class="row">
            		<div class="col-md-3">
        				<label for="paypal" class="label-control">Propietario:</label>
        			</div>
            		<div class="col-md-9">
            			<input type="text" name="nombre_cuenta" id="nombre_cuenta" class="form-control" value="{{ $carrito->nombre_cuenta }}">
            		</div>
                </div>
                <div class="row">
            		<div class="col-md-3">
        				<label for="paypal" class="label-control">Nombre del Banco:</label>
        			</div>
            		<div class="col-md-9">
            			<input type="text" name="banco" id="banco" class="form-control" value="{{ $carrito->banco }}">
            		</div>
                </div>
                <div class="row">
            		<div class="col-md-3">
        				<label for="paypal" class="label-control">Tipo de Cuenta:</label>
        			</div>
            		<div class="col-md-9">
            			{!! Form::select('tipo_cuenta', $tipo_cuenta, $carrito->tipo_cuenta ,['class'=>'form-control ','id' => 'tipo_cuenta']) !!}

            		</div>
                </div>
                <div class="row">
            		<div class="col-md-3">
        				<label for="paypal" class="label-control">Correo:</label>
        			</div>
            		<div class="col-md-9">
            			<input type="text" name="correo" id="correo" class="form-control" value="{{ $carrito->correo }}">
            		</div>
                </div>
            </div>
    <h2>% Descuento</h2>
    	<div class="row">
        	<div class="col-md-12">
        		 <input type="text" class="form-control" id="descuento" name="descuento" value="{{ $carrito->descuento }}">
        	</div>       	
		</div>
	<h2>% Iva</h2>
    	<div class="row">
        	<div class="col-md-12">
        		 <input type="text" class="form-control" id="iva" name="iva" value="{{ $carrito->iva }}">
        	</div>       	
		</div>
    <h2>Tiempo estimado de entrega</h2>
    	<div class="row">
        	<div class="col-md-6">
        		 <input type="text" class="form-control" id="tiempo" name="tiempo" value="{{ $carrito->tiempo_estimado }}">
        	</div>
        	<div class="col-md-6">
        		{!! Form::select('time', $time, $carrito->time ,['class'=>'form-control ','id' => 'time']) !!}
        	</div>
        	
		</div>
    <h2>Métodos de entrega</h2>
        <h4>Local</h4> 
            <label class="switchBtn">
                <input type="checkbox" data-toggle="toggle" data-on="Activado" data-off="Desactivado" class="local" onchange="localIO()" {{ $carrito->estado_local == 1? 'checked': '' }}>
                <input type="hidden" name ="estado_local" id="estado_local" value="{{$carrito->estado_local}}">
            </label>
            <hr>
            <div id="div_local">
                <div class="row">
                    <div class="col-md-3">
                        <label for="tarifa_local" class="label-control">Recargo:</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="tarifa_local" name="tarifa_local" value="{{ $carrito->tarifa_local }}">
                    </div>          
                </div>
            </div>
        <h4>A Domicilio</h4> 
            <label class="switchBtn">
                <input type="checkbox" data-toggle="toggle" data-on="Activado" data-off="Desactivado" class="domicilio" onchange="domicilioIO()" {{ $carrito->estado_domicilio == 1? 'checked': '' }}>
                <input type="hidden" name ="estado_domicilio" id="estado_domicilio" value="{{$carrito->estado_domicilio}}">
            </label>
        <hr>
        <div id="div_domicilio">
            <div class="row">
                <div class="col-md-1">
                    <input type="radio" name="tipo_domicilio" value="fija" {{ $carrito->tipo_domicilio == "fija"? "checked" : "" }}>
                </div>
                <div class="col-md-3">
                    <label for="tarifa_envio" class="label-control">Tarifa de Fija en dolares:</label>
                </div>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="tarifa_envio" name="tarifa_envio" value="{{ $carrito->tarifa_envio }}">
                </div>      	
    		</div>
            <div class="row">
                <div class="col-md-1">
                   <input type="radio" name="tipo_domicilio" value="kilometro" {{ $carrito->tipo_domicilio == "kilometro"? "checked" : "" }}>
                </div>
                <div class="col-md-3">
                    <label for="tarifa_kilometro" class="label-control">Tarifa por Kilometros:</label>
                </div>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="tarifa_kilometro" name="tarifa_kilometro" value="{{ $carrito->tarifa_kilometro }}">
                </div>          
            </div>
        </div>
	<h2>Politicas de la empresa</h2>
	<div class="form-group">
		{!! Form::textarea('texto',$carrito->politicas,['class' =>'form-control ckeditor', 'placeholder' =>'Politicas de la empresa','required', 'id', 'text'])!!}
	</div>
	<div class="row">
    	<div class="col-md-12">
    		@if ($carrito->id_adjunto != null)
	    		<a href="{{ asset('local/public') }}/{{ $carrito->politica->path }}" target="_blank" class="">{{ $carrito->politica->nombre }}</a>
	    		<br><br>
	    	@endif
    		<input type="file" class="form-control input-upload" name="file" id="file"  onchange="saveAdjunto()">
            <div id="upload-progress" class="form-row m-0 mt-2">      
              	<div class="progress">
                	<div id="porcentaje" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
             	</div>
        	</div>
			<input type="hidden" name="id_adjunto" id="id_adjunto" value="{{ $carrito->id_adjunto }}">            
		</div>
    </div>
    <button class="btn btn-primary" type="submit"> Guardar</button>
	{!! Form::close() !!}
@endsection
@section('script')
<script>
	$( document ).ready(function() {
            metodosPagoActivos();
            metodosEntrega();
	});
    function metodosPagoActivos(){
        if($('#estado_paypal').val() == 1){
            $('#div_paypal').show();
        }else{
            $('#div_paypal').hide();
        }
        if($('#estado_tarjeta').val() == 1){
            $('#div_tarjeta').show();
        }else{
            $('#div_tarjeta').hide();
        }
        if($('#estado_deposito').val() == 1){
            $('#div_deposito').show();
        }else{
            $('#div_deposito').hide();
        }
    }
    function metodosEntrega(){
        if($('#estado_local').val() == 1){
            $('#div_local').show();
        }else{
            $('#div_local').hide();
        }
        if($('#estado_domicilio').val() == 1){
            $('#div_domicilio').show();
        }else{
            $('#div_domicilio').hide();
        }
    }
	function carritoIO(){
		if($('.carrito').is(':checked')){
			$('#carrito').val("1");
		}else{
			$('#carrito').val("0");
		}		
	}
    function paypalIO(){
        if($('.paypal').is(':checked')){
            $('#estado_paypal').val("1");
            $('#div_paypal').show();
        }else{
            $('#estado_paypal').val("0");
            $('#div_paypal').hide();
        }       
    }
    function tarjetaIO(){
        if($('.tarjeta').is(':checked')){
            $('#estado_tarjeta').val("1");
            $('#div_tarjeta').show();
        }else{
            $('#estado_tarjeta').val("0");
            $('#div_tarjeta').hide();
        }       
    }
    function depositoIO(){
        if($('.deposito').is(':checked')){
            $('#estado_deposito').val("1");
            $('#div_deposito').show();
        }else{
            $('#estado_deposito').val("0");
            $('#div_deposito').hide();
        }       
    }
    function localIO(){
        if($('.local').is(':checked')){
            $('#estado_local').val("1");
            $('#div_local').show();
        }else{
            $('#estado_local').val("0");
            $('#div_local').hide();
        }       
    }
    function domicilioIO(){
        if($('.domicilio').is(':checked')){
            $('#estado_domicilio').val("1");
            $('#div_domicilio').show();
        }else{
            $('#estado_domicilio').val("0");
            $('#div_domicilio').hide();
        }       
    }
	function guardar(){
		var datos = new FormData($("#formulario")[0]);
		$.ajax({
			data: datos,	 
			method: 'POST',
			url: '{{ url('carrito') }}',
			processData: false, 
			contentType: false,
			success: function(data){
				console.log('se guardaron los datos correctamente');
			}, 
			error: function(j){
				console.log(j);
				toastr.error('Error al obtener datos');
			}
		});
	}
	var updateProgressBar = function (progress, value) {
          var $div = $(progress);
          $div.attr('aria-valuenow', value);
          $div.css('width', value + '%');
          $div.text(value + '%');
      }
    function saveAdjunto(){
      var div_progress = document.getElementById('upload-progress');
      var progress = document.getElementById('porcentaje');
      var datos = new FormData($("#formulario")[0]);
      $.ajax({
            data: datos,
            type: 'POST',
            url: '{{ url('carrito/save-politica') }}' ,
            processData: false, 
            contentType: false,
            beforeSend: function() {
          div_progress.style.display = 'block';
          progress.className = 'progress-bar progress-bar-striped progress-bar-success progress-bar-animated';
          updateProgressBar(progress, 0);
        },
        xhr: function(){
          var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt){
              if (evt.lengthComputable) {
                var percentComplete = Math.round((evt.loaded / evt.total)*100);
                updateProgressBar(progress, percentComplete);
                }
              }, false);
            return xhr;
          },
        error: function(data){
          progress.className = 'progress-bar progress-bar-striped bg-danger';
          updateProgressBar(progress, 100);
                  progress.innerHTML="Documento no Subido";
                  window.setTimeout(function(){div_progress.style.display = 'none'}, 1000);
              }
      }).done(function(retorno){
        progress.className = 'progress-bar progress-bar-striped bg-success';                  
        updateProgressBar(progress, 100);
        progress.innerHTML='Completo 100%';
        setTimeout(function(){div_progress.style.display = 'none'}, 1000)
        console.log(retorno);
          $('#id_adjunto').val(retorno);
      }).fail(console.log('error')).always(function(){
          
        });
    }
	</script>
@endsection
