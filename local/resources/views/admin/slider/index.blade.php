@extends('layouts.backend')
@section('ruta')
Listado de Usuarios
@stop
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Animación principal</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('animacion.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')
@include('alerts.success')
<!-- <a class="btn btn-primary" href="{{ URL::route('animacion.create') }}">Nuevo</a> -->
@if (count($imgs))
    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                <th>#</th>
                <th>Imagen</th>
                <th>Titulo</th>
                <th>Enlace</th>
                <th>Categoria</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($imgs as $img)
                <tr>
                <td>{{ $img->id }}</td>
                @if(empty($img->path))
                  <td><img src="{{asset('assets/images')}}/no-imagen.jpg"  style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                @else
                  <td>
                     <div class="lightBoxGallery">
                            <a href="{{asset('local/public/images')}}/{{$img->path}}" title="Image from Unsplash" data-gallery="">
                              <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/images')}}/{{$img->path}}">
                            </a>
                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                        </div>
                  </td>
                @endif
                <td>{{ $img->titulo }}</td>
                <td>{{$img->enlace}}</td>
                 <td>{{ $img->nombre }}</td>
                <td><a class="btn btn-success" href="{{route('animacion.edit', $img->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    {!! Form::open(array('method' => 'DELETE', 'route' => array('animacion.destroy', $img->id))) !!}
                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                    {!! Form::close() !!}
                </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    No hay imagenes
@endif
@stop