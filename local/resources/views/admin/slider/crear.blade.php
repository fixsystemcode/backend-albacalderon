@extends('layouts.backend')

@section('titulo')
Nuevo  
@stop
@section('contenido')
@include('alerts.request')
    {!! Form::open(['route' => 'animacion.store', 'method' => 'POST','files'=>true]) !!}
        <div class="form-group">
            {!! Form::label('imagen','Imagen') !!}
            {!! Form::file('path')!!}
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-5">
                    {!! Form::label('titulo', 'Titulo del Slider:') !!}
                    {!! Form::text('titulo',null,['class'=>'form-control','placeholder'=>'Ingrese Título','required']) !!}
                </div>
                <div class="col-md-7">
                    {!! Form::label('descripcion', 'Descripción:') !!}
                    {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción del Slider','required']) !!}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    {!! Form::label('categoria','Categoria') !!} 
                    {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control','required' , 'list' =>'listas'])!!}
                </div>
                <div class="col-md-4 col-sm-4">
                    <label for="mostrar">Mostrar texto:</label><br>
                    <input type="checkbox" data-on="Activo" data-off="Desactivado"  data-toggle="toggle" name="mostrar" id="mostrar">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    {!! Form::label('enlace', 'Enlace:') !!}
                    {!! Form::text('enlace',null,['class'=>'form-control','placeholder'=>'enlace del slider']) !!}
                </div>
                <div class="col-md-4 col-sm-4">
                    <label for="tipo">Tipo de enlace:</label><br>
                    <input type="checkbox" data-on="Externo" data-off="Interno"  data-toggle="toggle" name="tipo" id="tipo">
                </div>
            </div>
        </div>
        <div class="form-group" >
            <div style="clear: both;"></div>
        </div>
        {!!Form::submit('Registrar',['class'=>'btn btn-primary btn_formc'])!!}
    {!! Form::close() !!}
@stop