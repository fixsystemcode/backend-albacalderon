@extends('layouts.backend')
@section('titulo', 'Información del sitio')
@section('title','Información del sitio')
@section('contenido')
	
	</style>
	@if(Session::has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    {{ Session::get('message') }}
		</div>
	@endif
	 <form method="POST" action="{{ route('informacion.update', $informacion->id) }}" enctype="multipart/form-data">
		        {{ csrf_field() }}
		        <input type="hidden" name="_method" value="PUT">
		        <input type="hidden" name="id" value="{{ $informacion->id }}">
                <h2>Información general</h2>
                <hr>
            	<div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="empresa">Nombre:</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nombre"  placeholder="Ingrese el nombre de la empresa" style="text-align: center;" value="{{ $informacion->nombre }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="direccion">Dirección:</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="direccion"  placeholder="Ingrese su contraseña" style="text-align: center;" value="{{ $informacion->direccion }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="telefono">Telefono:</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="telefono"  placeholder="Ingrese su contraseña" style="text-align: center;" value="{{ $informacion->telefono }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="email">Email:</label>
                            </div>
                            <div class="col-md-9">
                                 <input type="text" class="form-control" name="email"  placeholder="Ingrese su contraseña" style="text-align: center;" value="{{ $informacion->email }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="biografia">Eslogan:</label>
                            </div>
                            <div class="col-md-9">
                                <textarea class="form-control" name="biografia" id="biografia" rows="3">{{ $informacion->biografia }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="informacion">Logo:</label>
                            </div>
                            <div class="col-md-9">
                                <img src="{{ asset(logoPagina()) }}" alt="" width="50%" class="mb-2">
                                <input type="file" name="logo" id="logo" class="form-control" id="logo" accept="image/x-png,image/gif,image/jpeg">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3 label-form">
                                <label for="informacion">Icono:</label>
                            </div>
                            <div class="col-md-9">
                                <img src="{{ asset(iconPagina()) }}" alt="" width="20%" class="mb-2">
                                <input type="file" name="icono" id="icono" class="form-control" id="icono" accept="image/x-png,image/gif,image/jpeg">
                            </div>
                        </div>
                    </div>
                </div>
                
                <h2>Información adicional</h2>
                <hr>
                <div class="form-group">
                    <label for="quienessomos">Quienes somos:</label>
                    <textarea name="quienessomos" id="quienessomos" class="form-control" cols="30" rows="10">{{ $informacion->quienessomos }}</textarea>
                </div>
                <div class="form-group">
                    <label for="mision">Misión:</label>
                    <textarea name="mision" id="mision" class="form-control" cols="30" rows="10">{{ $informacion->mision }}</textarea>
                </div>
                <div class="form-group">
                    <label for="vision">Visión:</label>
                    <textarea name="vision" id="vision" class="form-control" cols="30" rows="10">{{ $informacion->vision }}</textarea>
                </div>

                <h2>Información general</h2>
                <hr>
                <div class="form-group">
                    <label for="cliente">Activar clientes:</label>
                    <input type="checkbox" name="cliente" id="cliente" {{ $informacion->cliente == 1 ? 'checked' : ''}}>
                </div>
                <div class="form-group">
                    <label for="carrito">Activar Carrito de compra:</label>
                    <input type="checkbox" name="carrito" id="carrito" {{ $informacion->carrito == 1 ? 'checked' : ''}}>
                </div>

                 <div class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                </div>
		    </form>
@endsection

@section('script')
	<script type="text/javascript">
        $(document).ready(function(){
            $('#logo').on('change', function(e) {
                var file = e.target.files[0];
                if (file) {
                    if (/^image\//i.test(file.type)) {
                        readFile(file);
                    } else {
                        alert('EL archivo seleccionado no es una imagen');
                    }
                }
            });
            $('#icono').on('change', function(e) {
                var file = e.target.files[0];
                if (file) {
                    if (/^image\//i.test(file.type)) {
                        readFile(file);
                    } else {
                        alert('EL archivo seleccionado no es una imagen');
                    }
                }
            });
        });

        function readFile(file) {
            var reader = new FileReader();
            reader.onloadend = function () {
                processFile(reader.result, file.type);
            }
            reader.onerror = function () {
                alert('Ah ocurrido un error procesando la imagen!');
            }
            reader.readAsDataURL(file);
        }
        //Cambiar el callback
        function processFile(dataURL, fileType) {
            var maxWidth = 500;
            var maxHeight = 500;

            var image = new Image();
            image.src = dataURL;

            image.onload = function () {
                var width = image.width;
                var height = image.height;
                var shouldResize = (width > maxWidth) || (height > maxHeight);

                if (!shouldResize) {
                    muestraImagenSeleccionada(dataURL);
                    return;
                }

                var newWidth;
                var newHeight;

                if (width > height) {
                    newHeight = height * (maxWidth / width);
                    newWidth = maxWidth;
                } else {
                    newWidth = width * (maxHeight / height);
                    newHeight = maxHeight;
                }

                var canvas = document.createElement('canvas');
                canvas.width = newWidth;
                canvas.height = newHeight;

                var context = canvas.getContext('2d');
                context.drawImage(this, 0, 0, newWidth, newHeight);
                dataURL = canvas.toDataURL(fileType);
            };

            image.onerror = function () {
                alert('Ah ocurrido un error procesando la imagen!');
            };
        }
    </script>
	
@endsection