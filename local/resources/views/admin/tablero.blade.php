@extends('layouts.paginas')
@section('ruta', 'Tablero del sistema')
@section('titulo', 'Tablero del sistema')

@section('script')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('contenido')

<section class="mainContentSection">
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('message')}}
                    </div>
                @endif
                
                <!-- PRICING TABLE 3 -->
                <section class="whiteSection">
                    <div class="container">
                        <div class="row priceTable">
                            <div class="col-xs-12">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                        Ingresar nuevo Item
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <a class="btn buttonTransparent" href="{{ URL::route('articulos.create') }}">Nuevo</a>
                                    </div>
                                </div>
                                <div class="sectionTitle">
                                    <h2><span>Administración de Items</span></h2>
                                </div>
                            </div>
                            @if (count($articulos))
                            @foreach ($articulos as $articulo)
                            <div class="col-sm-3 col-xs-12">
                                <div class="panel panel-default">
                                    
                                    <div class="panel-body bodyImageBg">
                                        <img src="{{asset('local/public/images')}}/{{$articulo->path}}" alt="image">
                                        <div class="priceCircle">
                                            <h3 style="font-size: 14px;line-height: 15px;">{{$articulo->titulo}} </h3>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <?php if ($articulo->activado == true) {
                                        echo  '<li><i class="fa fa-check-circle" aria-hidden="true"></i> Activado por el admin</li>';
                                        }else{
                                        echo '<li><i class="fa fa-minus-square" aria-hidden="true"></i> Desactivado por el admin</li>';
                                        }
                                        ?>
                                        
                                        
                                    </ul>
                                    <div class="panel-footer"><a href="{{route('articulos.edit', $articulo->id)}}" class="btn buttonTransparent">Editar</a> <a href="{{route('articulos.destroy', $articulo->id)}}" class="btn buttonTransparent"  onclick="return confirm('Quiere borrar el registro?')" >Eliminar</a></div>
                                    
                                </div>
                            </div>
                            @endforeach
                            @else
                            No tienes propiedades todavía
                            @endif
                            
                            
                            
                        </div>
                    </div>
                </section>
                <!-- FOOTER -->
                
            </div>
        </div>
    </div>
</section>
@endsection