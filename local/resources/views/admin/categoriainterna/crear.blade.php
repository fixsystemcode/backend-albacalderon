@extends('layouts.backend')
@section('titulo')
Nueva Categoria
@stop
@section('contenido')
           {!! Form::open(['route' => 'categoriainterna.store', 'method' => 'POST',
           					'files'=>true]) !!}
				<div class="form-group">
					{!! Form::label('nombre','Nombre de Categoria') !!}
					{!! Form::text('nombre',null,['class' =>'form-control', 'placeholder' =>'Ingrese nombre de categoria','required'])!!}
				</div>
				<div class="form-group">
					{!! Form::label('descripcion','Descrición') !!}
					{!! Form::text('descripcion',null,['class' =>'form-control', 'placeholder' =>'Ingrese Descripción','required'])!!}
			</div>
			<div class="form-group"  >
						{!! Form::label('path','Imagen Categoria') !!}
						{!! Form::file('path',[])!!}
				</div>
				<div class="form-group">
					{!! Form::submit('Registrar', ['class' =>'btn btn-primary btn_formc']) !!}
				</div>
			{!! Form::close() !!}
@endsection			
