@extends('layouts.backend')
@section('titulo')
<div style="display: flex;justify-content: center;align-items: center;">
    <p style="margin-right: 30px; margin-bottom: 0px;">Categorías</p>
    <a class="btn btn-primary btn_crear" href="{{ URL::route('categoriainterna.create') }}">Nuevo</a>
</div>
@endsection
@section('contenido')
@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>
@endif
<!-- <a class="btn btn-primary" href="{{ URL::route('categoriainterna.create') }}">Nuevo</a>
<br> -->
@if ($categorias->count())
<br>
    <div class="row">
      <div class="col-md-12">  
<table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
  <thead>
    <th>Nombre de categorías</th>
    <th>Descripción</th>
    <th>Imagen</th>
    <th>Acciones</th>
  </thead>
  <tbody>
    <tr>
      @foreach ($categorias as $categoria)
      <td>{{$categoria->nombre}}</td>
      <td>{{$categoria->descripcion}}</td>
      @if(empty($categoria->path))
                  <td><img src="{{asset('assets/images')}}/no-imagen.jpg"  style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                @else
                  <td>
                     <div class="lightBoxGallery">
                            <a href="{{asset('local/public/imagescat')}}/{{$categoria->path}}" title="Image from Unsplash" data-gallery="">
                              <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/imagescat')}}/{{$categoria->path}}">
                            </a>
                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                        </div>
                  </td>
                @endif
      <td><a class="btn btn-success" href="{{route('categoriainterna.edit', $categoria->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
        {!! Form::open(array('method' => 'DELETE', 'route' => array('categoriainterna.destroy', $categoria->id))) !!}
          <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash"></span></button>
        {!! Form::close() !!}
          
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
   </div>
      <div class="col-md-6">
      </div>
    </div>
@else
    No hay Categorias
@endif
{!! $categorias->render() !!}
@endsection