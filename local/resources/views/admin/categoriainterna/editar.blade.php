@extends('layouts.backend')
@section('titulo')
Editar
@stop
@section('contenido')
@if(count($errors) >0)
<div class="alert alert-danger" role="alert">
  <ul>
  	@foreach($errors->all() as $error)
  	<li>{{$error}}</li>
  	@endforeach
  </ul>
</div>
@endif
{!!Form::model($categoria,['route'=>['categoriainterna.update',$categoria],'method'=>'PUT','files' => true])!!}
			<div class="form-group"  >
				<div class="col-md-2">
					<img src="{{asset('local/public/images')}}/{{$categoria->path}}" onerror="if (this.src != '../../local/public/images/avatar.png') this.src = '../../local/public/images/avatar.png';"  style ="width: 100px">
				</div>
		       	<div class="col-md-10">
						{!! Form::label('path','Imagen Categoria') !!}
						{!! Form::file('path',[])!!}
				</div>
				<div style="clear: both;"></div>
			</div>
          	<div class="form-group">
          			{!! Form::label('nombre', 'Nombre de categoria:') !!}
                    {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Ingrese nombre','required']) !!}
	       </div>
	       <div class="form-group">
					{!! Form::label('descripcion','Descripción de Categoria') !!}
					{!! Form::text('descripcion',null,['class' =>'form-control', 'placeholder' =>'Descripción de categoria','required'])!!}
			</div>
			<div class="form-group">
				{!! Form::submit('Actualizar', ['class' =>'btn btn-primary btn_formc']) !!}
			</div>
			{!! Form::close() !!}
@endsection			
