@extends('layouts.principal')

@section('contenido')
<link rel="stylesheet" href="{{asset('admin/plantilla1/css/plugins/multi-form/multi-form.css?v2')}}">
<style>
    #map {
    height: 200px;
    width: 100%;
}
</style>
<div class="card m-5">
    <div class="card-body">
        <form id="myForm" action="{{ route('checkoutSave') }}" method="POST">
            {!! csrf_field() !!}
            <!-- One "tab" for each step in the form: -->
            <div class="tab">
                <h1>Confirmar información del cliente</h1>
                <hr>
                <div class="row">   
                    <div class="col-xs-12 col-lg-6">  
                        <div class="row mb-2">
                            <input type="hidden" name="id_cliente" id="id_cliente" value="{{ $cliente->id }}">
                            <div class="col-md-4">C.I o RUC:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="ci_ruc" id="ci_ruc" class="form-control" value="{{ $cliente != null? $cliente->ci_ruc : '' }}" required>
                            </div>
                        </div>
                         <div class="row mb-2">
                            <div class="col-md-4">Nombres:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $cliente != null? $cliente->nombre : '' }}" required>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">Apellidos:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="apellido" id="apellido" class="form-control" value="{{ $cliente != null? $cliente->apellido : '' }}" required>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">Telefono:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="telefono" id="telefono" class="form-control" value="{{ $cliente != null? $cliente->telefono : '' }}" required>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">E-mail:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="correo" id="correo" class="form-control" value="{{ $cliente != null? $cliente->correo : '' }}" required="">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">Mensaje:</div>
                            <div class="col-md-8">
                                <textarea name="mensaje" id="mensaje" class="form-control" rows="3">{{ $cliente != null? $cliente->mensaje : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="row mb-2">
                            <div class="col-md-4">Ciudad:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="ciudad" id="ciudad" class="form-control" value="{{ $cliente != null? $cliente->ciudad : '' }}" required>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">Dirección:<span class="text-danger">*</span></div>
                            <div class="col-md-8">
                                <input type="text" name="direccion" id="direccion" class="form-control" value="{{ $cliente != null? $cliente->direccion : '' }}" required>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">Latitud:</div>
                            <div class="col-md-8">
                                <input type="text" name="latitud" id="latitud" class="form-control" value="{{ $cliente != null? $cliente->latitud : '' }}">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-4">Longitud:</div>
                            <div class="col-md-8">
                                <input type="text" name="longitud" id="longitud" class="form-control" value="{{ $cliente != null? $cliente->longitud : '' }}">
                            </div>
                        </div>
                        <div class="form-group"><div id="map"></div></div>
                    </div>
                </div>
            </div>
            <div class="tab">
                <h1>Seleccione el método de pago y entrega</h1>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="mb-2">Metodos de pago</h3>
                            @if ($carrito->estado_tarjeta == 1)
                                <div class="form-check">
                                    <input type="radio" name="metodo_pago" value="tarjeta" class="form-check-input">
                                    <label for="metodo_pago" class="form-check-label">Tarjeta</label>
                                </div>
                            @endif
                            @if ($carrito->estado_paypal == 1)
                                <div class="form-check">
                                    <input type="radio" name="metodo_pago" value="paypal" class="form-check-input">
                                    <label for="metodo_pago" class="form-check-label">Paypal</label>
                                </div>
                            @endif
                            @if ($carrito->estado_deposito == 1)
                                <div class="form-check">
                                    <input type="radio" name="metodo_pago" value="deposito" class="form-check-input">
                                    <label for="metodo_pago" class="form-check-label">Deposito</label>
                                </div>
                            @endif
                    </div>
                    <div class="col-md-6">
                        <h3 class="mb-2">Metodos de entrega</h3>
                            @if ($carrito->estado_local == 1)
                                <div class="form-check">
                                    <input type="radio" name="metodo_entrega" value="local" class="form-check-input tipoEntrega">
                                    <label for="metodo_entrega" class="form-check-label">Local</label>
                                </div>
                            @endif
                            @if ($carrito->estado_domicilio == 1)
                                <div class="form-check">
                                    <input type="radio" name="metodo_entrega" value="domicilio" class="form-check-input tipoEntrega">
                                    <label for="metodo_entrega" class="form-check-label">Domicilio</label>
                                </div>
                            @endif
                    </div>
                </div>
                
            </div>
            <div class="tab">
                <h1>Confirmar Pedido</h1>
                <hr>
                <div class="row">
                    <div class="col-lg-7 col-xs-12">
                        <h4>Detalle del pedido</h4>
                        <hr>
                            @if(count($productos))
                            @foreach($productos as $producto)
                            <div class="card mb-2">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img class="card-img-left m-2 p-2" src="{{asset('local/public/images')}}/{{$producto->options->img}}" alt="{{$producto->id}}" width="150px">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <h5 class="card-text"> {{ $producto->name }}</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p>Precio:</p>
                                                    <p>Cantidad:</p>
                                                    <p>Total:</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="card-text">$ {{ number_format($producto->price, 2) }}</p>
                                                    <p class="card-text"> {{ $producto->qty }} Und.</p>
                                                    <p class="card-text">$ {{ number_format($producto->price * $producto->qty, 2) }}</p>
                                                </div>
                                            </div>                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                                <span class="badge badge-pill badge-warning p-2" class="form-control">No ha ingresado ningun producto al carrito</span>
                            @endif
                    </div>
                    <div class="col-lg-5 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <h4>Resumen del Pedido</h4>
                                <hr>
                                <div class="row">
                                    <div class="col-8 col-md-7">
                                        <label for="">Subtotal:</label>
                                    </div>
                                    <div class="col-4 col-md-5" style="text-align: right;">
                                        <label for="" id="subtotal">$ {{ Cart::subtotal() }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 col-md-7">
                                        <label for="">Descuento {{ $carrito->descuento }}%:</label>
                                    </div>
                                    <div class="col-4 col-md-5" style="text-align: right;">
                                        <label for="" id="descuento">$ {{ number_format($descuento, 2) }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 col-md-7">
                                        <label for="">Iva {{ $carrito->iva }}%:</label>
                                    </div>
                                    <div class="col-4 col-md-5" style="text-align: right;">
                                        <label for="" id="iva">$ {{ number_format($iva, 2) }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 col-md-7">
                                        <label for="">Tarifa envio:</label>
                                    </div>
                                    <div class="col-4 col-md-5" style="text-align: right;">
                                        <input type="hidden" id="tarifa_local" value="{{ $carrito->tarifa_local }}">
                                        <input type="hidden" id="tarifa_domicilio" data-tipo="{{ $carrito->tipo_domicilio }}" value="{{ $carrito->tipo_domicilio == 'kilometro'? $carrito->tarifa_kilometro: $carrito->tarifa_envio }}">$ 
                                        <label for="" id="tarifa"></label>
                                        <input type="hidden" name="tarifa_envio" id="tarifa_envio">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-8 col-md-7">
                                        <label for="">Total:</label>
                                    </div>
                                    <div class="col-4 col-md-5" style="text-align: right;">$ 
                                        <label for="" id="total"> {{ number_format(Cart::subtotal() - $descuento + $iva , 2)}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2 mb-2" style="font-size: 0.8em">
                            <input type="checkbox" class="col-md-2" name="acepta" id="acepta" {{ count(Cart::content()) == 0 ? 'disabled' : ''  }}>
                            <label class="col-md-10" for="acepta">Acepto los <a href="{{ asset('local/public') }}/{{ $carrito->politica->path }}" target="_blank">terminos y condiciones</a> establecidos por la empresa</label>
                        </div>
                    </div>
                </div>
            </div>
            
            <div style="overflow:auto;">
                <div style="float:right; margin-top: 5px;">
                    <button type="button" class="previous">Atras</button>
                    <button type="button" class="next">Siguiente</button>
                    <button type="button" class="submit">Pagar</button>
                </div>
            </div>
            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center;margin-top:40px;">
                <span class="step">1</span>
                <span class="step">2</span>
                <span class="step">3</span>
            </div>
        </form>
    </div>
    
</div>
@endsection

@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>

<script src="{{asset('admin/plantilla1/js/plugins/validate/jquery.validate.min.js')}}"></script>
<script src="{{asset('admin/plantilla1/js/plugins/multi-form/multi-form.js')}}"></script>
<script>
    function initMap() {
        @guest('cliente')
            var latitud = -0.9660858726870881;
            var longitud = -80.70762632485355;
            var zoommap = 14;
        @else
            if ("{{$cliente->latitud}}" == "" || "{{$cliente->longitud}}" == "")
            {
                var latitud = -0.9660858726870881;
                var longitud = -80.70762632485355;
                var zoommap = 14;
            }
            else{
                var latitud = '{{$cliente->latitud}}';
                var latitud = parseFloat(latitud);
                var longitud = '{{$cliente->longitud}}';
                var longitud = parseFloat(longitud);
                var zoommap = 20;
                $("#latitud").val('{{$cliente->latitud}}');
                $("#longitud").val('{{$cliente->longitud}}');
            }
        @endguest

        var myLatlng = {lat: latitud, lng: longitud};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myLatlng
        });

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Ubicacion"
        });

        marker.setMap(map);
        marker.bindTo('position', map, 'center');
        google.maps.event.addListener(map, "center_changed", function() {
            var markerLatLng = marker.getPosition();
            $("#latitud").val(markerLatLng.lat());
            $("#longitud").val(markerLatLng.lng());
        });
        cargado = 1;
    }
    $(document).ready(function(){
        // $.validator.addMethod('date', function(value, element, param) {
        //     return (value != 0) && (value <= 31) && (value == parseInt(value, 10));
        // }, 'Please enter a valid date!');
        // $.validator.addMethod('month', function(value, element, param) {
        //     return (value != 0) && (value <= 12) && (value == parseInt(value, 10));
        // }, 'Please enter a valid month!');
        // $.validator.addMethod('year', function(value, element, param) {
        //     return (value != 0) && (value >= 1900) && (value == parseInt(value, 10));
        // }, 'Please enter a valid year not less than 1900!');
        // $.validator.addMethod('username', function(value, element, param) {
        //     var nameRegex = /^[a-zA-Z0-9]+$/;
        //     return value.match(nameRegex);
        // }, 'Only a-z, A-Z, 0-9 characters are allowed');

        var val =   {
            // Specify validation rules
            rules: {
                ci_ruc: "required",
                nombre: "required",
                apellido: "required",
                ciudad: "required",
                direccion: "required",
                latitud: "required",
                longitud: "required",
                correo: {
                        required: true,
                        email: true
                    },
                telefono: {
                    required:true,
                    minlength:10,
                    maxlength:10,
                    digits:true
                },
                metodo_pago: "required",
                metodo_entrega: "required",
                acepta: "required",
            },
            // Specify validation error messages
            messages: {
                ci_ruc: "Su numero de identificación es obligatorio",
                nombre: "Su nombre es obligatorio",
                apellido: "Su apellido es obligatorio",
                ciudad: "El nombre de la ciudad es obligatorio",
                direccion: "La dirección es obligatoria",
                latitud: "La latiud es obligatoria, para colocar el datos mueva el marcador en el mapa inferior",
                longitud: "La longitud es obligatoria, para colocar el datos mueva el marcador en el mapa inferior",
                correo: {
                    required:   "E-mail es requerido",
                    email:      "Por favor ingrese un email valido",
                },
                telefono:{
                    required:   "Numero de telefono es requerido",
                    minlength:  "Minimos 10 digitos",
                    maxlength:  "Maximo 10 digitos",
                    digits:     "Solo acepta numeros"
                },
                latitud: "Seleccione un metodo de pago",
                longitud: "Seleccione un metodo de entrega",
                metodo_pago: "Seleccione un metodo de pago",
                metodo_entrega: "Seleccione un metodo de entrega",
                acepta: "Para poder confirmar su pedido tiene que aceptar los terminos y condiciones",
            }
        }
        $("#myForm").multiStepForm(
        {
            // defaultStep:0,
            beforeSubmit : function(form, submit){
                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations:val,
        }
        ).navigateTo(0);
    });

    $('.tipoEntrega').on('change', function(){
        if($('input:radio[name=metodo_entrega]:checked').val() == "local"){
            $('#tarifa').text(parseFloat($('#tarifa_local').val()).toFixed(2));
        }else{
            if($('#tarifa_domicilio').data('tipo') == "fija"){
                $('#tarifa').text(parseFloat($('#tarifa_domicilio').val()).toFixed(2));
                $('#tarifa_envio').val(parseFloat($('#tarifa_domicilio').val()).toFixed(2));
                subtotal = {{ Cart::subtotal() }};
                iva = {{ $iva }};
                descuento = {{ $descuento }};
                tarifa_envio = parseFloat($('#tarifa').text()).toFixed(2);
                total = parseFloat(subtotal) - parseFloat(descuento) + parseFloat(iva) + parseFloat(tarifa_envio);
                console.log(subtotal + ' ' + descuento + ' ' + iva + tarifa_envio);
                $('#total').text(parseFloat(total).toFixed(2));
            }else{
                calcularKm($('#tarifa_domicilio').val());
            }
        }
    });
    function calcularKm(tarifa){
        var distancia = 1;
        var directionsServiceTmp = new google.maps.DirectionsService;
        var directionsDisplayTmp = new google.maps.DirectionsRenderer;

        location_ini = 14.595188 + ',' + -90.5166266;
        location_fin = 14.641828 + ',' + -90.5152771;

        directionsServiceTmp.route({
            origin: location_ini,
            destination: location_fin,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                // Aqui con el response podemos acceder a la distancia como texto 
                console.log(response.routes[0].legs[0].distance.text);
                // Obtenemos la distancia como valor numerico en metros 
                console.log(response.routes[0].legs[0].distance.value);
                distancia = (response.routes[0].legs[0].distance.value / 1000);
                total_tarifa = (distancia * tarifa).toFixed(2);
                $('#tarifa').text(total_tarifa);
                $('#tarifa_envio').val(total_tarifa); 
                subtotal = {{ Cart::subtotal() }};
                iva = {{ $iva }};
                descuento = {{ $descuento }};
                total = parseFloat(subtotal) - parseFloat(descuento) + parseFloat(iva) + parseFloat(total_tarifa);
                console.log(subtotal + ' ' + descuento + ' ' + iva + total_tarifa);
                $('#total').text(parseFloat(total).toFixed(2));    
            }
        });   
    }
</script>
@endsection
