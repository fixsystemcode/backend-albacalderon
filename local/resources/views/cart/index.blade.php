
@extends('layouts.principal')

@section('contenido')
<style>
    #map {
    height: 200px;
    width: 100%;
}
</style>
<div class="card m-2">
    <div class="card-body row">
        
        <div class="col-lg-12 col-xs-12">
            <form id="formDelete" action="#" method="POST" style="display: none;"><input name="_method" type="hidden" value="DELETE">{{ csrf_field() }}
            </form>
            <div class="row">
                <div class="col-6">
                    <h4>
                        <font class="mr-5">Detalle del pedido</font> 
                        <span class="badge badge-pill badge-success">Subtotal: $ {{ Cart::subtotal() }}</span> 
                    </h4>                
                </div>
                <div class="col-6" style="text-align: right;">
                    <a href="{{ route('inicio') }}" class="btn btn-info">Seguir Comprando</a>
                    @if (Cart::count() && Auth::guard('cliente')->user())
                        <a href="{{ route('saveCard') }}" class="btn btn-success">Checkout <span class="fa fa-check"></span></a>
                        @else
                        <a href="{{ route('cliente.auth.loginCliente') }}" class="btn btn-success">Checkout <span class="fa fa-check"></span></a>
                    @endif                

                </div>
                
            </div>
            <hr>
                @if(count($productos))
                @foreach($productos as $producto)
                <div class="card">
                    <div class="row">
                        <div class="col-md-4" style="text-align: center;">
                            <img class="card-img-left m-2 p-2" src="{{asset('local/public/images')}}/{{$producto->options->img}}" alt="{{$producto->id}}" width="150px">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-text"> {{ $producto->name }}</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Precio:</p>
                                        <p>Cantidad:</p>
                                        <p>Total:</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="card-text">$ {{ number_format($producto->price, 2) }}</p>
                                        <input type="number" name="qty_{{$producto->id}}" id="qty_{{$producto->id}}" class="form-control" value="{{ $producto->qty }}" onchange="upQty('{{ $producto->rowId}}', '{{ $producto->id }}')">
                                        <p class="card-text">$ {{ number_format($producto->price * $producto->qty, 2) }}</p>
                                    </div>
                                </div>
                                
                                <div style="text-align: right;">
                                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="eliminar('{{ $producto->rowId }}')"><span class="fa fa-trash"></span> Eliminar</a>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-12">
                    <span class="badge">Nota: El precio de los productos no incluye iva.</span>
                </div>
                @else
                    <span class="badge badge-pill badge-warning p-2" class="form-control">No ha ingresado ningun producto al carrito</span>
                @endif
        </div>
    </div>
    
</div>
@endsection


@section('script')

<script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/lightgallery.js')}}"></script>
<script src="{{asset('assets/js/jquery.bxslider.js')}}"></script>


<script src="{{asset('assets/js/scripts.js')}}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>
<script>
    $(document).ready(function(){
        $('#acepta').click(function(){
            if($(this).is(':checked')){
                $('#btn_confirmacion').removeAttr('disabled');
                console.log('disabled not');
            }else{
                $('#btn_confirmacion').attr('disabled', 'disabled');
                console.log('disabled');
            }
        });    
    })
    function eliminar(id){
       $("#formDelete").attr("action", '{{ URL::to('cart') }}/'+id).submit();
    }
    function upQty(rowId, id){
        var qty = $('#qty_'+id).val();
        var token = $('#token').val();
       $.ajax({
            url:'{{ url('cart') }}',
            data: {
                'qty': qty,
                'id' : rowId,
                '_token': $("meta[name='csrf-token']").attr("content")
            },
            type: 'POST',
            success:function(response){
                location.reload();
            }
       });
    }
    $('#submit').on('click', function(){
        guardarPedido();
    });
    function guardarPedido(){
        $.ajax({
            url : '{{ url('cart/guardarPedido') }}',
            data : {},
            type : 'GET',
        success:function(response){
            console.log(response);
        },
        error: function(j){
            console.log(j);
            showToast('Error', j, 'error', 1000);
        }
        });
        
    }
    function initMap() {
            @guest('cliente')
                var latitud = -0.9660858726870881;
                var longitud = -80.70762632485355;
                var zoommap = 14;
            @else
                if ("{{$cliente->latitud}}" == "" || "{{$cliente->longitud}}" == "")
                {
                    var latitud = -0.9660858726870881;
                    var longitud = -80.70762632485355;
                    var zoommap = 14;
                }
                else{
                    var latitud = '{{$cliente->latitud}}';
                    var latitud = parseFloat(latitud);
                    var longitud = '{{$cliente->longitud}}';
                    var longitud = parseFloat(longitud);
                    var zoommap = 20;
                    $("#latitud").val('{{$cliente->latitud}}');
                    $("#longitud").val('{{$cliente->longitud}}');
                }
            @endguest

            var myLatlng = {lat: latitud, lng: longitud};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: myLatlng
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable:true,
                title:"Ubicacion"
            });

            marker.setMap(map);
            marker.bindTo('position', map, 'center');
            google.maps.event.addListener(map, "center_changed", function() {
                var markerLatLng = marker.getPosition();
                $("#latitud").val(markerLatLng.lat());
                $("#longitud").val(markerLatLng.lng());
            });
            cargado = 1;
        }
</script>
@endsection
