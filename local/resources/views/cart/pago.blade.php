
@extends('layouts.principal')

@section('contenido')
    <style>
        .contendor{
            width: 800px;
            margin: 0 auto;
        }
        ul.tabs{
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        ul.tabs li{
            background: none;
            color: #222;
            display: inline-block;
            padding: 10px 15px;
            cursor: pointer;
        }

        ul.tabs li.current{
            background: #4fcbfd;
            color: #222;
        }

        .tab-content{
            display: none;
            border: 1px solid #929292; 
            padding: 15px;
        }

        .tab-content.current{
            display: inherit;
        }
    </style>
<div class="card m-2">
    <div class="card-body row">
        <div class="col-md-8 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        @if ($pedido->metodo_pago == "tarjeta")
                            <div id="tarjeta">
                                <div class="row mb-2">
                                    @if (isset($responseData['error_code']))
                                        <p>Se produjo un error en la carga del botón. la descripción del error es: {{ $responseData['error_description'] }}</p>
                                    @else
                                        <iframe src="https://portal.botonpagos.com/api/datafast/boton/<?=$responseData['code']?>" width="100%" style="height: 766px; border: none"></iframe>    
                                    @endif          
                                </div>
                            </div>
                        @endif
                        @if ($pedido->metodo_pago == "paypal")
                            <div id="paypal">
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="text-align: center;">
                                    <input type="hidden" name="cmd" value="_xclick">
                                    <input type="hidden" name="business" value="5TDDPA72TSMKC">
                                    <input type="hidden" name="lc" value="US">
                                    <input type="hidden" name="item_name" value="Floristería ilusión">
                                    <input type="hidden" name="amount" value="{{ $pedido->total }}">
                                    <input type="hidden" name="currency_code" value="USD">
                                    <input type="hidden" name="button_subtype" value="services">
                                    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
                                    <input type="image" src="https://www.paypalobjects.com/es_XC/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
                                    <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
                                </form>
                            </div>
                        @endif
                        @if ($pedido->metodo_pago == "deposito")
                            <div id="div_deposito">
                                {!! Form::open(['url'=>'cart/ingresar-pago','method'=>'POST','id'=>'deposito','files'=>true]) !!}
                                {!! csrf_field() !!}
                                <input type="hidden" name="id_pedido" id="id_pedido" value="{{ $pedido->id }}">
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <label for="">Numero del Documento</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="num_doc" id="num_doc" required>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <label for="">Valor Pagado</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="valor" id="valor" required>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <label for="">Documento</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="file" class="form-control input-upload" name="file" id="file"  onchange="saveAdjunto()">
                                        <div id="upload-progress" class="form-row m-0 mt-2">      
                                          <div class="progress">
                                            <div id="porcentaje" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          </div>
                                        </div>
                                        <input type="hidden" name="id_adjunto" id="id_adjunto" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="text-align: right;">
                                        <button class="btn btn-primary">Guardar pago</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-md-4 col-12">
        <div class="row justify-content-center mb-2"> 
            <span class="badge badge-pill badge-success py-2 px-5">
                <label style="font-size: 1.5em; font-weight: bold;">Total a pagar:</label> 
                <label style="font-size: 1.5em;">$ {{ $pedido->total }}</label>
            </span>  
        </div>           
            <h4>Detalle del pedido</h4>
            <hr>
                @if(count($pedido->detalle))
                @foreach($pedido->detalle as $producto)
                <div class="card">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="card-img-left m-2 p-2" src="{{asset('local/public/images')}}/{{$producto->detalleProducto->path}}" alt="{{$producto->id_producto}}" width="150px">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-text"> {{ $producto->detalleProducto->nombre }}</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Precio:</p>
                                        <p>Cantidad:</p>
                                        <p>Total:</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="card-text">$ {{ number_format($producto->precio, 2) }}</p>
                                        <p class="card-text">{{ $producto->cantidad }} Und.</p>
                                        <p class="card-text">$ {{ number_format($producto->precio * $producto->cantidad, 2) }}</p>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                    <span class="badge badge-pill badge-warning p-2" class="form-control">No ha ingresado ningun producto al carrito</span>
                @endif
        </div>
    </div>
    
</div>

@endsection

@section('script')

<script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/lightgallery.js')}}"></script>
<script src="{{asset('assets/js/jquery.bxslider.js')}}"></script>


<script src="{{asset('assets/js/scripts.js')}}"></script>

<script>
    $(document).ready(function(){
    
    });
    var updateProgressBar = function (progress, value) {
          var $div = $(progress);
          $div.attr('aria-valuenow', value);
          $div.css('width', value + '%');
          $div.text(value + '%');
      }
    function saveAdjunto(){
      var div_progress = document.getElementById('upload-progress');
      var progress = document.getElementById('porcentaje');
      var datos = new FormData($("#deposito")[0]);
      $.ajax({
            data: datos,
            type: 'POST',
            url: '{{ url('save-deposito') }}' ,
            processData: false, 
            contentType: false,
            beforeSend: function() {
          div_progress.style.display = 'block';
          progress.className = 'progress-bar progress-bar-striped progress-bar-success progress-bar-animated';
          updateProgressBar(progress, 0);
        },
        xhr: function(){
          var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt){
              if (evt.lengthComputable) {
                var percentComplete = Math.round((evt.loaded / evt.total)*100);
                updateProgressBar(progress, percentComplete);
                }
              }, false);
            return xhr;
          },
        error: function(data){
          progress.className = 'progress-bar progress-bar-striped bg-danger';
          updateProgressBar(progress, 100);
                  progress.innerHTML="Documento no Subido";
                  window.setTimeout(function(){div_progress.style.display = 'none'}, 1000);
              }
      }).done(function(retorno){
        progress.className = 'progress-bar progress-bar-striped bg-success';                  
        updateProgressBar(progress, 100);
        progress.innerHTML='Completo 100%';
        setTimeout(function(){div_progress.style.display = 'none'}, 1000)
        console.log(retorno);
          $('#id_adjunto').val(retorno);
      }).fail(console.log('error')).always(function(){
          
        });
    }
</script>
@endsection
