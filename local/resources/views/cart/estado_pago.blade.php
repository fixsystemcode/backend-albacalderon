
@extends('layouts.principal')

@section('contenido')
@include('menu.header_principal')
<div class="row justify-content-md-center mt-5">
    <div class="card col-md-4" style="text-align: center;">
        @if ($responseData['error_code'] != 0)
            <span class="fa fa-times" style="color: #ff0000; font-size: 4em"></span>
            <p>Se produjo un error: {{ $responseData['error_description'] }}, para volver a intentarlo presione <a href="{{ url('cliente/pagar-pedido') }}/{{ $orden_id }}">aqui</a></p>
        @else
            <span class="fa fa-check" style="color: #008F39; font-size: 4em"></span>
            <p>Se ha recibido su pago correctamente, gracias por su compra <a href="{{ url('/') }}">volver a inicio</a></p>   
        @endif          
    </div>
    
</div>

@endsection

@section('script')

<script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/lightgallery.js')}}"></script>
<script src="{{asset('assets/js/jquery.bxslider.js')}}"></script>


<script src="{{asset('assets/js/scripts.js')}}"></script>

<script>
    $(document).ready(function(){
    
    });
    var updateProgressBar = function (progress, value) {
          var $div = $(progress);
          $div.attr('aria-valuenow', value);
          $div.css('width', value + '%');
          $div.text(value + '%');
      }
    function saveAdjunto(){
      var div_progress = document.getElementById('upload-progress');
      var progress = document.getElementById('porcentaje');
      var datos = new FormData($("#deposito")[0]);
      $.ajax({
            data: datos,
            type: 'POST',
            url: '{{ url('save-deposito') }}' ,
            processData: false, 
            contentType: false,
            beforeSend: function() {
          div_progress.style.display = 'block';
          progress.className = 'progress-bar progress-bar-striped progress-bar-success progress-bar-animated';
          updateProgressBar(progress, 0);
        },
        xhr: function(){
          var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt){
              if (evt.lengthComputable) {
                var percentComplete = Math.round((evt.loaded / evt.total)*100);
                updateProgressBar(progress, percentComplete);
                }
              }, false);
            return xhr;
          },
        error: function(data){
          progress.className = 'progress-bar progress-bar-striped bg-danger';
          updateProgressBar(progress, 100);
                  progress.innerHTML="Documento no Subido";
                  window.setTimeout(function(){div_progress.style.display = 'none'}, 1000);
              }
      }).done(function(retorno){
        progress.className = 'progress-bar progress-bar-striped bg-success';                  
        updateProgressBar(progress, 100);
        progress.innerHTML='Completo 100%';
        setTimeout(function(){div_progress.style.display = 'none'}, 1000)
        console.log(retorno);
          $('#id_adjunto').val(retorno);
      }).fail(console.log('error')).always(function(){
          
        });
    }
</script>
@endsection
