@extends('layouts.principal')

@section('contenido')
<style>
    #map {
    height: 200px;
    width: 100%;
    }
    .card-header{
        font-weight: bold;
        background: #009DE2;
        color: #ffffff;
    }
    .btn-floristeria{
        background-color: #009DE2; 
        color: #ffffff; 
        border: none; 
        padding: 10px 20px; 
        font-size: 17px; 
        font-family: Raleway; 
        cursor: pointer; 
    }
    .fullscreen-modal .modal-dialog {
      margin: 0;
      margin-right: auto;
      margin-left: auto;
      width: 100%;
    }
    @media (min-width: 768px) {
      .fullscreen-modal .modal-dialog {
        width: 750px;
      }
    }
    @media (min-width: 992px) {
      .fullscreen-modal .modal-dialog {
        width: 970px;
      }
    }
    @media (min-width: 1200px) {
      .fullscreen-modal .modal-dialog {
         width: 1170px;
      }
    }
</style>
    <div class="container mt-5">
        <form id="myForm" action="{{ route('checkoutSave') }}" method="POST">
            <input type="hidden" name="idPedido" id="idPedido" value="{{ $pedido->id }}">
            <div class="row">
                <div class="col-lg-8 p-2">
                    {!! csrf_field() !!}
                    <div class="card">
                        <div class="card-header">
                            Información de envio
                        </div>
                        <div class="card-body">
                            {{ $cliente->nombre }} {{ $cliente->apellido }}<br>
                            @if (\Sistema::informacionCompleta(Auth::guard('cliente')->user()->id) == false)
                                <span class="badge badge-warning">No puede continuar con el pago porque falta llenar datos personales, llenelos <a href="{{ url('cliente/miperfil') }}">aquí</a></span>
                            @endif
                            @if (count($direcciones))
                                <div class="row">
                                    <div class="col-md-3">Seleccione una dirección:</div>
                                    <div class="col-md-9">
                                        {!! Form::select('id_direccion',$direcciones, is_null($pedido->id_direccion)? null: $pedido->id_direccion,[ 'class' =>'form-control','required'])!!}
                                    </div>
                                </div>
                                @else
                                    <span class="badge badge-warning">Por favor agregue una ubicación para poder continuar</span>
                            @endif
                            <div style="overflow:auto;">
                                <div style="float:right; margin-top: 5px;">
                                    <button type="button" class="btn btn-floristeria" data-toggle="modal" data-target="#exampleModal">Agregar dirección</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            Metodo de Pago
                        </div>
                        <div class="card-body">
                            <div class="container row">
                                @if ($carrito->estado_tarjeta == 1)
                                    <div class="form-check col">
                                        <input type="radio" name="metodo_pago" value="tarjeta" class="form-check-input" required {{ $pedido->metodo_pago =="tarjeta"? "checked" :"" }}>
                                        <label for="metodo_pago" class="form-check-label">Tarjeta</label>
                                    </div>
                                @endif
                                @if ($carrito->estado_paypal == 1)
                                    <div class="form-check col">
                                        <input type="radio" name="metodo_pago" value="paypal" class="form-check-input" required {{ $pedido->metodo_pago =="paypal"? "checked" :"" }}>
                                        <label for="metodo_pago" class="form-check-label">Paypal</label>
                                    </div>
                                @endif
                                @if ($carrito->estado_deposito == 1)
                                    <div class="form-check col">
                                        <input type="radio" name="metodo_pago" value="deposito" class="form-check-input" required {{ $pedido->metodo_pago =="deposito"? "checked" :"" }}>
                                        <label for="metodo_pago" class="form-check-label">Deposito</label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="card-header">
                            Metodo de Entrega
                        </div>
                        <div class="card-body">
                            <div class="container row mb-2">
                                @if ($carrito->estado_local == 1)
                                    <div class="form-check col">
                                        <input type="radio" name="metodo_entrega" value="local" class="form-check-input tipoEntrega" required {{ $pedido->metodo_entrega =="local"? "checked" :"" }}>
                                        <label for="metodo_entrega" class="form-check-label">Local</label>
                                    </div>
                                @endif
                                @if ($carrito->estado_domicilio == 1)
                                    <div class="form-check col">
                                        <input type="radio" name="metodo_entrega" value="domicilio" class="form-check-input tipoEntrega" required {{ $pedido->metodo_entrega =="domicilio"? "checked" :"" }}>
                                        <label for="metodo_entrega" class="form-check-label">Domicilio</label>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="card-header">
                            Detalle del carrito
                        </div>
                        <div class="card-body">
                            @if(count($productos))
                                @foreach($productos as $producto)
                                <input type="hidden" name="id_pedido" value="{{ $pedido->id }}">
                                <div class="row mb-2">
                                    <div class="col-md-4" style="text-align: center;">
                                        <img class="card-img-left" src="{{asset('local/public/images')}}/{{$producto->detalleProducto->path}}" alt="{{$producto->id}}" width="100px">
                                    </div>
                                    <div class="col-md-8">
                                        <h5> {{ $producto->detalleProducto->nombre }}</h5>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div>Precio:</div>
                                                <div>Cantidad:</div>
                                                <div>Total:</div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card-text">$ {{ number_format($producto->detalleProducto->precio, 2) }}</div>
                                                <div class="card-text"> {{ $producto->cantidad }} Und.</div>
                                                <div class="card-text">$ {{ number_format($producto->precio * $producto->cantidad, 2) }}</div>
                                            </div>
                                        </div>                                           
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                            @else
                                <span class="badge badge-pill badge-warning p-2" class="form-control">No ha ingresado ningun producto al carrito</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 p-2">
                    <div class="card">
                        <div class="card-header">
                            Resumen del pago
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-8 col-md-7">
                                    <label for="">Subtotal:</label>
                                </div>
                                <div class="col-4 col-md-5" style="text-align: right;">
                                    <label for="" id="subtotal">$ {{ number_format($pedido->subtotal + $pedido->subtotal_12 + $pedido->descuento + $pedido->descuento_cupon, 2) }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8 col-md-7">
                                    <label for="">Descuento {{ $carrito->descuento }}%:</label>
                                </div>
                                <div class="col-4 col-md-5" style="text-align: right;">
                                    <label for="" id="descuento">$ {{ number_format($pedido->descuento, 2) }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8 col-md-7">
                                    <label for="">Iva {{ $carrito->iva }}%:</label>
                                </div>
                                <div class="col-4 col-md-5" style="text-align: right;">
                                    <label for="" id="iva">$ {{ number_format($pedido->iva, 2) }}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-8 col-md-7">
                                    <label for="">Tarifa envio:</label>
                                </div>
                                <div class="col-4 col-md-5" style="text-align: right;">
                                    <input type="hidden" id="tarifa_local" value="{{ $carrito->tarifa_local }}">
                                    <input type="hidden" id="tarifa_domicilio" data-tipo="{{ $carrito->tipo_domicilio }}" value="{{ $carrito->tipo_domicilio == 'kilometro'? $carrito->tarifa_kilometro: $carrito->tarifa_envio }}">$ 
                                    <label for="" id="tarifa">{{ $pedido->tarifa_envio != null? number_format($pedido->tarifa_envio, 2): "0.00" }}</label>
                                    <input type="hidden" name="tarifa_envio" id="tarifa_envio">
                                </div>
                            </div>
                            @if (!is_null($pedido->descuento_cupon))
                                <div class="row">
                                    <div class="col-8 col-md-7">
                                        <label for="">Cupon Descuento:</label>
                                    </div>
                                    <div class="col-4 col-md-5" style="text-align: right;">
                                        $ {{ number_format($pedido->descuento_cupon, 2) }}
                                    </div>
                                </div>
                            @endif
                            <hr>
                            <div class="row">
                                <div class="col-8 col-md-7">
                                    <label for="">Total:</label>
                                </div>
                                <div class="col-4 col-md-5" style="text-align: right;">$ 
                                    <label for="" id="total"> {{ number_format($pedido->total, 2)}}</label>
                                </div>
                            </div>
                            @if (count($direcciones) && \Sistema::informacionCompleta(Auth::guard('cliente')->user()->id) == true ) 
                                <div style="overflow:auto;">
                                    <div style="float:right; margin-top: 5px;">
                                        <button type="submit" class="btn btn-floristeria">Pagar</button>
                                    </div>
                                </div>
                            @endif
                            <small style="font-size: 0.8em">Tiempo estimado de entrega: {{ $carrito->tiempo_estimado }} {{ \Sistema::getTime($carrito->time) }}</small><br>
                            <small style="font-size: 0.8em;">Al presionar pagar confirma que esta de acuerdo con los <a href="{{ asset('local/public') }}/{{ $carrito->politica->path }}" target="_blank">terminos y condiciones</a> establecidos por la empresa. </small><br>
                            <small style="font-size: 0.7em">{!! $carrito->politicas !!}</small>
                        </div>
                    </div>
                    @if (is_null($pedido->cupon))
                        <div class="card mt-2">
                            <div class="card-header">
                                Cupón de descuento
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="codigo" class="control-label">Codigo:</label>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="codigo" id="codigo" class="form-control">
                                    </div>
                                </div>
                                <div style="text-align: right;">
                                    <button class="btn btn-floristeria mt-1" type="button" onclick="cajearCupon()">Agregar</button>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <form id="form-user">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Nueva dirección</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <form id="form_direccion">
                                 <input type="hidden" name="id_direccion" id="id_direccion">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="ciudad" class="control-label">Ciudad</label>
                                        <input type="text" class="form-control" name="ciudad" id="ciudad">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="latitud" class="control-label">Latitud</label>
                                        <input type="text" class="form-control" name="latitud" id="latitud">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="direccion" class="control-label">Dirección</label>
                                        <input type="text" class="form-control" name="direccion" id="direccion"> 
                                    </div>
                                    <div class="col-md-6">
                                        <label for="longitud" class="control-label">Longitud</label>
                                        <input type="text" class="form-control" name="longitud" id="longitud">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="referencia" class="control-label">Referencia</label>
                                        <textarea name="referencia" id="referencia" cols="5" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div id="map" class="col-md-6 mt-5"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer p-1">
                        <button type="button" class="btn btn-success" onclick="guardarDireccion()">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>
<script>
    var subtotal = {{ $pedido->subtotal +  $pedido->subtotal_12 }};
    var iva = {{ $pedido->iva }};
    var descuento = {{ $pedido->descuento }};
    var cupon = {{ $pedido->descuento_cupon != null? $pedido->descuento_cupon : 0 }};
    var total = {{ $pedido->total }};
    
    function initMap() {
        @guest('cliente')
            var latitud = -0.9660858726870881;
            var longitud = -80.70762632485355;
            var zoommap = 14;
        @else
            if ("{{$cliente->latitud}}" == "" || "{{$cliente->longitud}}" == "")
            {
                var latitud = -0.9660858726870881;
                var longitud = -80.70762632485355;
                var zoommap = 14;
            }
            else{
                var latitud = '{{$cliente->latitud}}';
                var latitud = parseFloat(latitud);
                var longitud = '{{$cliente->longitud}}';
                var longitud = parseFloat(longitud);
                var zoommap = 20;
                $("#latitud").val('{{$cliente->latitud}}');
                $("#longitud").val('{{$cliente->longitud}}');
            }
        @endguest

        var myLatlng = {lat: latitud, lng: longitud};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myLatlng
        });

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Ubicacion"
        });

        marker.setMap(map);
        marker.bindTo('position', map, 'center');
        google.maps.event.addListener(map, "center_changed", function() {
            var markerLatLng = marker.getPosition();
            $("#latitud").val(markerLatLng.lat());
            $("#longitud").val(markerLatLng.lng());
        });
        cargado = 1;
    }

    $('.tipoEntrega').on('change', function(){
        if($('input:radio[name=metodo_entrega]:checked').val() == "local"){
            $('#tarifa').text(parseFloat($('#tarifa_local').val()).toFixed(2));
            tarifa_envio = parseFloat($('#tarifa').text()).toFixed(2);
            total_pagar = parseFloat(total) + parseFloat(tarifa_envio);
            $('#total').text(parseFloat(total_pagar).toFixed(2));
            console.log(tarifa_envio);
        }else{
            if($('#tarifa_domicilio').data('tipo') == "fija"){
                $('#tarifa').text(parseFloat($('#tarifa_domicilio').val()).toFixed(2));
                $('#tarifa_envio').val(parseFloat($('#tarifa_domicilio').val()).toFixed(2));
                tarifa_envio = parseFloat($('#tarifa').text()).toFixed(2);
                total_pagar = parseFloat(total) + parseFloat(tarifa_envio);
                $('#total').text(parseFloat(total_pagar).toFixed(2));
            }else{
                calcularKm($('#tarifa_domicilio').val());
            }
        }
    });
    function calcularKm(tarifa){
        var distancia = 1;
        var directionsServiceTmp = new google.maps.DirectionsService;
        var directionsDisplayTmp = new google.maps.DirectionsRenderer;

        location_ini = 14.595188 + ',' + -90.5166266;
        location_fin = 14.641828 + ',' + -90.5152771;

        directionsServiceTmp.route({
            origin: location_ini,
            destination: location_fin,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                // Aqui con el response podemos acceder a la distancia como texto 
                console.log(response.routes[0].legs[0].distance.text);
                // Obtenemos la distancia como valor numerico en metros 
                console.log(response.routes[0].legs[0].distance.value);
                distancia = (response.routes[0].legs[0].distance.value / 1000);
                total_tarifa = (distancia * tarifa).toFixed(2);
                $('#tarifa').text(total_tarifa);
                $('#tarifa_envio').val(total_tarifa); 
                total_pagar = parseFloat(total) + parseFloat(total_tarifa);
                $('#total').text(parseFloat(total_pagar).toFixed(2));    
            }
        });   
    }

    function guardarDireccion(){
        id_direccion = $('#id_direccion').val();
        ciudad = $('#ciudad').val();
        direccion = $('#direccion').val();
        longitud = $('#longitud').val();
        latitud = $('#latitud').val();
        referencia = $('#referencia').val();
        open_ajax();
        $.ajax({
            url:'{{ url('cliente/save-direccion') }}',
            type:'POST',
            data: {'id_direccion': id_direccion,'ciudad': ciudad, 'direccion': direccion, 'referencia': referencia, 'latitud': latitud, 'longitud': longitud, "_token": $("meta[name='csrf-token']").attr("content")}
        }).done(function(datos){                                  
            location.reload();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            validacionCampos(jqXHR, textStatus, errorThrown);
        }).always(function() {
            close_ajax();
        })
    }

    function cajearCupon(){
        var id = $('#idPedido').val();
        var cupon = $('#codigo').val();
        if(cupon != ''){
            $.ajax({
                data: {},
                type: 'GET',
                url: '{{ url('cliente/getcupon') }}/'+ id +'/' + cupon,
            success:function(response){
                console.log(response);
                if(response.mensaje == 4){
                    location.reload();
                }else{
                    showToast('Alerta', response.mensaje, 'warning', 10000);
                }           
            },
            error: function(j){
                console.log(j);
                showToast('Error', j, 'error', 1000);
            }
            });
        }else{
            showToast('Alerta', 'Ingresa el codigo de un cupón', 'warning', 10000);
        }
        
    }
</script>
@endsection
