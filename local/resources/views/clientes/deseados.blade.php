@extends('layouts.principal')
@section('titulo')
	<h1>Productos deseados</h1>
@endsection 
@section('contenido')
	<div class="">
		@include('menu.header_principal')
	    <div class="row mt-2 p-3">
	    	<div class="col-md-2">
	    		{!! \Sistema::renderiza_menu_cliente() !!}
	    	</div>
	        <div class="col-md-10">
				@if(count($wishlist))
				<div class="card-group">
					@foreach ($wishlist as $deseo)
						<div class="card col-md-3">
							<div style="text-align: center;">
								<img class="card-img-top" src="{{asset('local/public/images')}}/{{$deseo->wishlistProducto->path}}" alt="Card image cap" style="width: 200px; height: 200px;">
							</div>
						    <div class="card-body">
						      <h5 class="card-title">{{ $deseo->wishlistProducto->nombre }}</h5>
						      <p class="card-text">{!! $deseo->wishlistProducto->presentacion !!}</p>
						      	<div style="text-align: center;">
			        				<input type="hidden" value="{{ $deseo->wishlistProducto->id }}" id="id_product">		   
			        				@auth('cliente')
			        					<form action="{{route('wishlist.store')}}" method="post">
				  							{{csrf_field()}}
				  							<a class="btn btn-success" id="p_comprar" style="background: #ff48aa; color: #fff; border-color: #ff48aa; margin-top: 8px;">Comprar</a>
				  							<input name="id_cliente" type="hidden" value="{{ Auth::guard('cliente')->user()->id }}" />
				  							<input name="id_producto" type="hidden" value="{{ $deseo->wishlistProducto->id }}" />
				  							<button class="btn btn-success mt-2" style="background: #ff48aa; border-color: #ff48aa" type="submit"><span class="fa fa-heart">	</span></button>
										</form>

			        				@endauth
								</div>
						    </div>
						</div>
						{{-- <div class="row" style="margin-bottom: 15px">
							<div class="col-md-4" style="text-align: center;">
								<img src="{{asset('local/public/images')}}/{{$deseo->wishlistProducto->path}}" alt="{{ $deseo->wishlistProducto->slug }}" width="120px">
							</div>
							<div class="col-md-4">
								<label for="" class="Control-label" style="font-size: 1.2em">{{ $deseo->wishlistProducto->nombre }}</label>
								<div class="spd_details">
				        			{!! $deseo->wishlistProducto->descripcion !!}
				        		</div>
			        			<div class="spd_other">
			        				<p><strong>Código:</strong> {{ $deseo->wishlistProducto->abreviatura }}</p>
			        				<p><strong>Presentación:</strong>{{ $deseo->wishlistProducto->presentacion }}</p>
			        			</div>
			        		</div>
			        		<div  class="col-md-4" style="text-align: center;">
		        				<input type="hidden" value="{{ $deseo->wishlistProducto->id }}" id="id_product">
		        				<a class="btn btn-success" id="p_comprar" style="background: #ff48aa; border-color: #ff48aa">Comprar</a>
		        				
		        				@auth('cliente')
		        					<form action="{{route('wishlist.store')}}" method="post">
			  							{{csrf_field()}}
			  							<input name="id_cliente" type="hidden" value="{{ Auth::guard('cliente')->user()->id }}" />
			  							<input name="id_producto" type="hidden" value="{{ $deseo->wishlistProducto->id }}" />
			  							<button class="btn btn-success mt-2" style="background: #ff48aa; border-color: #ff48aa" type="submit"><span class="fa fa-heart">	</span></button>
									</form>

		        				@endauth
							</div>
						</div> --}}
					@endforeach
				</div>
					{!! $wishlist->render('pagination::bootstrap-4') !!}
				@else
					<span class="badge badge-warning" style="font-size: 1em"> No ha agregado ningun producto como deseado</span>
				@endif
			</div>
		</div>
	</div>
@include('menu.footer')	   
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$(document).on("click", "#p_comprar", function(e){
			id = $('#id_product').val();
			$.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({
                url: '{{ url('cart/addProducto') }}/'+id,
                method: 'POST',
                data:  {},
                success:function(response){
                	location.href = "{{ url('/') }}";
            	},
                error: function(data){
                    console.log(data);
                }
            })
		})

		function mostrarmodal(data){
			console.log(data);
			$('#form_compra').modal('show');
		}
	});

	
	
</script>
@endsection