@extends('layouts.principal')
@section('titulo')
	<h1>Mi perfil</h1>
@endsection 
@section('contenido')
    <style>
        #map {
        height: 200px;
        width: 100%;
    }
    </style>
    @include('menu.header_principal')
    <div class="row mb-2 p-3">
        <div class="col-md-3">
            {!! \Sistema::renderiza_menu_cliente() !!}
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('cliente/actualizar-perfil') }}" method="POST">
                            {!! csrf_field() !!}
                        <h3>Información Personal</h3>
                        <hr>    
                        <div class="row">   
                            <div class="col-md-6">  
                                <div class="row mb-2">
                                    <input type="hidden" name="id_cliente" id="id_cliente" value="{{ $cliente->id }}">
                                    <div class="col-md-4">C.I o RUC:<span class="text-danger">*</span></div>
                                    <div class="col-md-8">
                                        <input type="text" name="ci_ruc" id="ci_ruc" class="form-control" value="{{ $cliente != null? $cliente->ci_ruc : '' }}" required>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">Primer Nombre:<span class="text-danger">*</span></div>
                                    <div class="col-md-8">
                                        <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $cliente != null? $cliente->nombre : '' }}" required>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">Segundo Nombre:<span class="text-danger">*</span></div>
                                    <div class="col-md-8">
                                        <input type="text" name="segundo_nombre" id="segundo_nombre" class="form-control" value="{{ $cliente != null? $cliente->segundo_nombre : '' }}" required>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6"> 
                                <div class="row mb-2">
                                    <div class="col-md-4">Apellidos:<span class="text-danger">*</span></div>
                                    <div class="col-md-8">
                                        <input type="text" name="apellido" id="apellido" class="form-control" value="{{ $cliente != null? $cliente->apellido : '' }}" required>
                                    </div>
                                </div> 
                                <div class="row mb-2">
                                    <div class="col-md-4">Telefono:<span class="text-danger">*</span></div>
                                    <div class="col-md-8">
                                        <input type="text" name="telefono" id="telefono" class="form-control" value="{{ $cliente != null? $cliente->telefono : '' }}" required>
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4">E-mail:<span class="text-danger">*</span></div>
                                    <div class="col-md-8">
                                        <input type="text" name="correo" id="correo" class="form-control" value="{{ $cliente != null? $cliente->correo : '' }}" required="">
                                    </div>
                                </div>
                                <div class="row mb-2 mr-1" style="float: right;">
                                   <button class="btn btn-primary">Actualizar datos</button>
                                </div>
                            </div>
                        </div>
                        <br>    
                        <h3>Direcciones Registradas</h3>
                        <hr>    
                        <div class="row">
                            <button class="btn btn-success ml-2" type="button" onclick="abrirModalDireccion()"><span class="fa fa-plus"></span> Agregar nueva dirección</button>  
                            <div class="table-responsive p-2">
                                <table class="table table-bordered table-sm"> 
                                    <thead>
                                        <th>Ciudad</th>
                                        <th>Dirección</th>
                                        <th>Referencia</th>
                                        <th>Acción</th>
                                        {{-- <th>Latitud</th>
                                        <th>Longitud</th>
                                        <th>Mapa</th> --}}
                                    </thead>
                                    <tbody id="direccion_body">
                                        @if (count($cliente->direcciones))
                                            @foreach ($cliente->direcciones as $direccion)
                                                <tr>
                                                    <td>{{ $direccion->ciudad }}</td>
                                                    <td>{{ $direccion->direccion }}</td>
                                                    <td>{{ $direccion->referencia }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-outline-primary btn_direccion" onclick="editarModalDireccion({{ $direccion->id }})"><span class="fa fa-edit"></span></button>
                                                        <button type="button" class="btn btn-outline-danger btn_direccion" onclick="eliminarDireccion({{ $direccion->id }})"><span class="fa fa-trash"></span></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="4"><span class="badge badge-pill badge-warning">No ha registrado ninguna dirección</span></td>
                                            </tr>
                                         @endif 
                                    </tbody>
                                </table>
                            </div>
                        </div>                      
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_direccion" class="modal fade text-dark">
        <form id="produccion">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Nueva dirección</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <form id="form_direccion">
                                 <input type="hidden" name="id_direccion" id="id_direccion">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="ciudad" class="control-label">Ciudad</label>
                                        <input type="text" class="form-control" name="ciudad" id="ciudad">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="latitud" class="control-label">Latitud</label>
                                        <input type="text" class="form-control" name="latitud" id="latitud">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="direccion" class="control-label">Dirección</label>
                                        <input type="text" class="form-control" name="direccion" id="direccion"> 
                                    </div>
                                    <div class="col-md-6">
                                        <label for="longitud" class="control-label">Longitud</label>
                                        <input type="text" class="form-control" name="longitud" id="longitud">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="referencia" class="control-label">Referencia</label>
                                        <textarea name="referencia" id="referencia" cols="5" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div id="map" class="col-md-6 mt-5"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer p-1">
                        <button type="button" class="btn btn-success" onclick="guardarDireccion()">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @include('menu.footer')
@endsection
@section('script')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap"></script>
    <script>     
        function initMap() {
            var getLat = $('#latitud').val();
            var getLon = $('#longitud').val()
            if (getLat == "" || getLon == "")
            {
                var latitud = -0.9660858726870881;
                var longitud = -80.70762632485355;
                var zoommap = 14;
            }
            else{
                var latitud = getLat;
                var latitud = parseFloat(latitud);
                var longitud = getLon;
                var longitud = parseFloat(longitud);
                var zoommap = 20;
                $("#latitud").val(latitud);
                $("#longitud").val(longitud);
            }
            var myLatlng = {lat: latitud, lng: longitud};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: myLatlng
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable:true,
                title:"Ubicacion"
            });

            marker.setMap(map);
            marker.bindTo('position', map, 'center');
            google.maps.event.addListener(map, "center_changed", function() {
                var markerLatLng = marker.getPosition();
                $("#latitud").val(markerLatLng.lat());
                $("#longitud").val(markerLatLng.lng());
            });
            cargado = 1;
        }
    function abrirModalDireccion(){
            $('#produccion')[0].reset();  
             $('#id_direccion').val(null);        
            $('#modal_direccion').modal('show'); 
            initMap();
    }
    function editarModalDireccion(id){
         open_ajax();
        $.ajax({
            url:'{{ url('cliente/show-direccion') }}/'+id,
            type:'GET',
            data: {}
        }).done(function(datos){
            $('#id_direccion').val(datos.direccion.id);
            $('#ciudad').val(datos.direccion.ciudad);
            $('#direccion').val(datos.direccion.direccion);  
            $('#referencia').val(datos.direccion.referencia);
            $('#latitud').val(datos.direccion.latitud);
            $('#longitud').val(datos.direccion.longitud);      
            $('#modal_direccion').modal('show');
            initMap();
        }).fail(function(jqXHR, textStatus, errorThrown) {
            validacionCampos(jqXHR, textStatus, errorThrown);
        }).always(function() {
            close_ajax();
        });
    }
    function eliminarDireccion(id){
         open_ajax();
        $.ajax({
            url:'{{ url('cliente/delete-direccion') }}/'+id,
            type:'GET',
            data: {}
        }).done(function(datos){
            showToast('Correcto','Dirección eliminada correctamente','success',10000);
            crearTablaDirecciones(datos.direcciones);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            validacionCampos(jqXHR, textStatus, errorThrown);
        }).always(function() {
            close_ajax();
        });
    }
    function cerrarModalDireccion(){
        $('#produccion')[0].reset();
        $('#id_direccion').val(null);            
        $('#modal_direccion').modal('hide'); 
    }
    function guardarDireccion(){
        id_direccion = $('#id_direccion').val();
        ciudad = $('#ciudad').val();
        direccion = $('#direccion').val();
        longitud = $('#longitud').val();
        latitud = $('#latitud').val();
        referencia = $('#referencia').val();
        open_ajax();
        $.ajax({
            url:'{{ url('cliente/save-direccion') }}',
            type:'POST',
            data: {'id_direccion': id_direccion,'ciudad': ciudad, 'direccion': direccion, 'referencia': referencia, 'latitud': latitud, 'longitud': longitud, "_token": $("meta[name='csrf-token']").attr("content")}
        }).done(function(datos){                                  
            crearTablaDirecciones(datos.direcciones);
            cerrarModalDireccion();
            showToast('Correcto','Dirección agregada correctamente','success',10000);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            validacionCampos(jqXHR, textStatus, errorThrown);
        }).always(function() {
            close_ajax();
        })
    }
    function crearTablaDirecciones(datos){
        contenedor = $('#direccion_body').empty();
        $.each(datos , function(i,item){
            console.log(item);
            tr = $('<tr>');
            tr.append($('<td>').text(item.ciudad));
            tr.append($('<td>').text(item.direccion));
            tr.append($('<td>').text(item.referencia));
            tr.append($('<td>').append($('<button type="button" class="btn btn-outline-primary" onclick="editarModalDireccion('+ item.id +')"><span class="fa fa-edit"></span></button>'+ ' '+ '<button type="button" class="btn btn-outline-danger" onclick="eliminarDireccion('+ item.id +')"><span class="fa fa-trash"></span></button>')));
            contenedor.append(tr);
        });
        
    }
    </script>
@endsection