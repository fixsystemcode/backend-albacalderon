@extends('layouts.principal')

@section('contenido')
@include('menu.header_principal')
<div class="container">
    <div class="row mt-4 mb-4">
        <div class="col-xs-12 col-lg-10 offset-lg-1">
            <div class="card" style="border: 1px solid #009DE2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 mb-2" style="text-align: center;">
                            <h2>Inicie sesión con las redes sociales o manualmente:</h2>
                        </div>
                        <div class="col-md-6 mt-5">
                            <a href="{{ route('loginSocial', 'facebook') }}" class="btn btn-block" style="background-color: #3B5998; color: #fff">
                              <i class="fa fa-facebook fa-fw"></i> Login with Facebook
                            </a>
                            {{-- <a href="#" class="btn btn-info btn-block">
                              <i class="fa fa-twitter fa-fw"></i> Login with Twitter
                            </a> --}}
                            <a href="{{ route('loginSocial', 'google') }}" class="btn btn-block" style="background-color: #dd4b39; color: #fff">
                              <i class="fa fa-google fa-fw"></i> Login with Google+
                            </a>
                        </div>
                        <div class="col-md-6">
                            <form class="form-horizontal" method="POST" action="{{ route('cliente.auth.loginCliente') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
                                    <label for="correo" class="control-label">E-Mail</label>

                                    <div>
                                        <input id="correo" type="email" class="form-control" name="correo" value="{{ old('correo') }}" required autofocus>

                                        @if ($errors->has('correo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('correo') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Contraseña</label>

                                    <div class="">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                         <div class="form-group">
                                            <div class="col-md-offset-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   {{--  <div class="col" style="text-align: right;">
                                        <a class="btn btn-link" href="{{ route('cliente.password.request') }}">

                                            Olvidó su contraseña?

                                        </a>
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Ingresar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
