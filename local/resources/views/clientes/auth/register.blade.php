@extends('layouts.principal')

@section('contenido')
@include('menu.header_principal')
<div class="container">
    <div class="row mt-4 mb-4">
        <div class="col-xs-12 col-lg-10 offset-lg-1">
            <div class="card" style="border: 1px solid #009DE2">
                <div class="card-body">
                    <form class="" method="POST" action="{{ route('cliente.register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <div class="form-group{{ $errors->has('ci') ? ' has-error' : '' }}">
                                    <label for="ci" class="control-label">CI-RUC</label>

                                    <div class="">
                                        <input id="ci" type="text" class="form-control" name="ci" value="{{ old('ci') }}" required>

                                        @if ($errors->has('ci'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ci') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                                    <label for="nombre" class="control-label">Primer nombre</label>

                                    <div class="">
                                        <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                        @if ($errors->has('nombre'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('nombre') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                                    <label for="segundo_nombre" class="control-label">Segundo nombre</label>

                                    <div class="">
                                        <input id="segundo_nombre" type="text" class="form-control" name="segundo_nombre" value="{{ old('nombre') }}" required autofocus>

                                        @if ($errors->has('segundo_nombre'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('segundo_nombre') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                                    <label for="apellido" class="control-label">Apellido</label>

                                    <div class="">
                                        <input id="apellido" type="text" class="form-control" name="apellido" value="{{ old('apellido') }}" required autofocus>

                                        @if ($errors->has('apellido'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('apellido') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
                                    <label for="correo" class="control-label">Correo</label>

                                    <div class="">
                                        <input id="correo" type="email" class="form-control" name="correo" value="{{ old('correo') }}" required>

                                        @if ($errors->has('correo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('correo') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
                                    <label for="celular" class="control-label">Celular</label>

                                    <div class="">
                                        <input id="celular" type="number" class="form-control" name="celular" value="{{ old('celular') }}" required>

                                        @if ($errors->has('celular'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('celular') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Contraseña</label>

                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="control-label">Confirmar Contraseña</label>

                                    <div class="">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-xs-12 col-lg-12">
                                <div class="form-group">
                                    <div class="">
                                        <button type="submit" class="btn btn-primary">
                                            Registrarse
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
