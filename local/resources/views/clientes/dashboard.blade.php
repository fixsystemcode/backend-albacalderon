@extends('layouts.principal')
@section('titulo')
	<h1>Dashboard</h1>
@endsection 
@section('contenido')
@include('menu.header_principal')
    <div class="row mb-2 p-3">
    	<div class="col-md-2">
    		{!! \Sistema::renderiza_menu_cliente() !!}
    	</div>
        <div class="col-md-10">
		    <div class="row">
		    	<div class="col-md-6">
		        	<h3>Pedidos Rechazados</h3>
		        	@if (count($pedidosError))
			        	<div class="table-responsive">
			        		<table class="table table-bordered table-hover">
				        		<thead>
				        			<tr>
				        				<th>Detalle</th>
				        				<th class="row-sm">Estado</th>
				        			</tr>
				        		</thead>
				        		<tbody>
			        				@foreach ($pedidosError as $pedidoerror)
				        				<tr>
				        					<td>
				                            	<div class="row" style="font-weight: bold;">
				                            			<div class="col-md-6"><small style="font-size: 0.5">Producto</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">Cantidad</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">Precio</small></div>
				                            		</div>
				                            	@foreach ($pedidoerror->detalle as $detalle)
				                            		<div class="row">
				                            			<div class="col-md-6"><small style="font-size: 0.5">{{ $detalle->detalleProducto->nombre }}</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->cantidad }}</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->precio }}</small></div>
				                            		</div>
				                            	@endforeach
				                            </td>
				        					<td> {!! estadoPedido($pedidoerror->estado) !!} </td>
				        				</tr>
					           		@endforeach
				        		</tbody>		
				        	</table>
			        	</div>
			        	
		        	@else
						<span class="badge badge-pill badge-primary" style="font-size: 1em"> No tiene pedidos rechazado ¡Felicitaciones!</span> 	
		           @endif
		    	</div>
		    	<div class="col-md-6">
		        	<h3>Pedidos Pendiente de Entrega</h3>
		        	@if (count($pedidosPendientes))
			        	<div class="table-responsive">
			        		<table class="table table-bordered table-hover">
				        		<thead>
				        			<tr>
				        				<th>Detalle</th>
				        				<th class="row-sm">Estado</th>
				        			</tr>
				        		</thead>
				        		<tbody>
			        				@foreach ($pedidosPendientes as $pedidopendiente)
				        				<tr>
				        					<td>
				                            	<div class="row" style="font-weight: bold;">
				                            			<div class="col-md-6"><small style="font-size: 0.5">Producto</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">Cantidad</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">Precio</small></div>
				                            		</div>
				                            	@foreach ($pedidopendiente->detalle as $detalle)
				                            		<div class="row">
				                            			<div class="col-md-6"><small style="font-size: 0.5">{{ $detalle->detalleProducto->nombre }}</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->cantidad }}</small></div>
				                            			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->precio }}</small></div>
				                            		</div>
				                            	@endforeach
				                            </td>
				        					<td> {!! estadoPedido($pedidopendiente->estado) !!} </td>
				        				</tr>
					           		@endforeach
				        		</tbody>		
				        	</table>
			        	</div>
			        	
		        	@else
						<span class="badge badge-pill badge-warning" style="font-size: 1em"> No tiene ningun pedido aprobado para su entrega</span> 	
		           @endif
				        
		    	</div>
		    </div>
		</div>
	</div>
@include('menu.footer')
@endsection