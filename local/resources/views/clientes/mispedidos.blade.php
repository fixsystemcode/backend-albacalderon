@extends('layouts.principal')
@section('titulo')
	<h1>Mis Pedidos</h1>
@endsection 
@section('contenido')
	<style>
		.row-sm{
			width: 15%;
		}
	</style>
	<div class="">
		@include('menu.header_principal')
	    <div class="row mb-2 p-3">
	    	<div class="col-md-2">
	    		{!! \Sistema::renderiza_menu_cliente() !!}
	    	</div>
	        <div class="col-md-10">
	        	@if (count($pedidos))
	        	<div class="table-responsive">
	        		<table class="table table-bordered table-hover">
		        		<thead>
		        			<tr>
		        				<th class="row-sm">Fecha</th>
		        				<th class="row-sm">Estado</th>
		        				<th>Detalle</th>
		        				<th class="row-sm">Total</th>
		        				<th class="row-sm">Acción</th>
		        			</tr>
		        		</thead>
		        		<tbody>
	        				@foreach ($pedidos as $pedido)
		        				<tr>
		        					<td>{{ fechaHumana($pedido->created_at) }}</td>
		        					<td> {!! estadoPedido($pedido->estado) !!} </td>
		        					<td>
		                            	<div class="row" style="font-weight: bold;">
		                            			<div class="col-md-6"><small style="font-size: 0.5">Producto</small></div>
		                            			<div class="col-md-3"><small style="font-size: 0.5">Cantidad</small></div>
		                            			<div class="col-md-3"><small style="font-size: 0.5">Precio</small></div>
		                            		</div>
		                            	@foreach ($pedido->detalle as $detalle)
		                            		<div class="row">
		                            			<div class="col-md-6"><small style="font-size: 0.5">{{ $detalle->detalleProducto->nombre }}</small></div>
		                            			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->cantidad }}</small></div>
		                            			<div class="col-md-3"><small style="font-size: 0.5">{{ $detalle->precio }}</small></div>
		                            		</div>
		                            	@endforeach
		                            </td>
		        					<td>{{ $pedido->total }}</td>
		        					<td>
		        						@if ($pedido->estado == 1)
		        							<a class="btn btn-warning" href="{{ url('cliente/checkout') }}/{{ $pedido->id }}"><span class="fa fa-edit"></span></a>
		        							<a class="btn btn-info" href="{{ url('cliente/pagar-pedido') }}/{{ $pedido->id }}"><span class="fa fa-money"></span></a>
		        							<a class="btn btn-danger" href="javascript:void(0)" onclick="eliminar('{{ $pedido->id }}')"><span class="fa fa-trash"></span></a>	
		        						@endif

		        					</td>
		        				</tr>
			           		@endforeach
		        		</tbody>		
		        	</table>
		        	{!! $pedidos->render('pagination::bootstrap-4') !!}
	        	</div>
	        	
	        	@else
					<span class="badge badge-warning" style="font-size: 1em"> No ha realizado ningun pedido, realice su pedido <a href="{{ route('inicio') }}">  aquí</a></span> 	
	           @endif
	        </div>
	    </div>
	</div>
	@include('menu.footer')
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	});
	function eliminar(id){
		$.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
		$.ajax({
            url: '{{ url('cliente/cancelar-pedido') }}',
            method: 'POST',
            data:  {'id': id},
            success:function(response){
            	location.reload();
        	},
            error: function(data){
                console.log(data);
            }
        })
    }
</script>
@endsection