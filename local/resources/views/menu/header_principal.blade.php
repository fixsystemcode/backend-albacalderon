
<header>
    <style type="text/css">
        #lblCartCount {
            font-size: 10px;
            background: #ff0000;
            color: #fff;
            padding: 0 5px;
            vertical-align: top;
            margin-left: -10px;
        }
    </style>
    {{-- <nav class="nbr_info" style="display: block;padding: 30px 0;">
        <div class="container">
            <div style="text-align: center;">
                <img src="{{asset(logoPagina())}}" alt="" style="height: 54px;">
            </div>
        </div>
    </div> --}}
</nav>

<nav class="navbar navbar-expand-lg navbar-light nbr_principal">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            @include('menu.menu')
        </div>
    </div>
</nav>