<div class="col-lg-3">
    <div class="institucion_menu">
        <div class="institucion_titulo"><h2>La Institución</h2></div>
        <div class="institucion_list">
            <ul>
                <li><a href="{{URL::to('/institucion/mision-y-vision')}}">Misión y visión</a></li>
                <li><a href="{{URL::to('/institucion/historia')}}">Historia</a></li>
                <li><a href="{{URL::to('/institucion/instalaciones')}}">Instalaciones</a></li>
                <li><a href="{{URL::to('/institucion/autoridades')}}">Autoridades</a></li>
                <li><a href="{{URL::to('/institucion/docentes')}}">Docentes</a></li>
            </ul>
        </div>
    </div>
</div>