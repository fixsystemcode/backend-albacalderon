<footer>
    <div class="m_footer">
        <div class="mf_bar"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div>
                        <img src="{{asset(logopagina2())}}" alt="" style="width: 100%;height: auto; margin-top: 50px">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="contact_data">
                        <div class="cd_title">
                            <h5>Visita nuestra tienda.</h5>
                            <p>Lorem ipsum dolor sit amet.</p>
                        </div>
                        <div class="cd_datos">
                            <div class="cdd_item">
                                <h5>Dirección</h5>
                                <p>{{$info->direccion}}</p>
                            </div>
                            <div class="cdd_item">
                                <h5>Teléfonos</h5>
                                <p>{{$info->telefono}}</p>
                            </div>
                            <div class="cdd_item">
                                <h5>Email</h5>
                                <p>{{$info->email}}</p>
                            </div>
                        </div>
                        <div class="cd_link">
                            <a href="https://www.google.com/maps/place/Floristeria+Ilusion/@-0.9516238,-80.7026223,15z/data=!4m5!3m4!1s0x0:0xdcb5363e6cc79cd2!8m2!3d-0.9516238!4d-80.7026223" target="_blank" class="ibtn ibtn_m1">Ver ubicación</a>
                            <a href="{{URL::to('/contactos')}}" class="ibtn ibtn_m2">Contactanos</a>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="mf_3">
                        <h3>Contactos</h3>
                        <ul>
                            <li><i class="fa fa-map-marker"></i> {{ \Sistema::getInfo()->direccion }}</li>
                            <li><i class="fa fa-phone"></i> {{ \Sistema::getInfo()->telefono }}</li>
                            <li><i class="fa fa-envelope"></i> {{ \Sistema::getInfo()->email }}</li>
                        </ul>
                        <a href="{{URL::to('/contactos')}}">Enviar mensaje</a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="mfb_fix">
                <div class="mbff_1">
                    <p style="font-size: 12px;">Derechos reservados a <span>{{ \Sistema::getInfo()->nombre }}</span>.</a></p>
                </div>
                <div class="mbff_2" style="margin-left: auto;">
                    <ul>
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-facebook-f"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>