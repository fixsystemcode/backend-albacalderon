<div class="col-lg-3">
    <div class="institucion_menu">
        <div class="institucion_titulo"><h2>Funciones Sustantivas</h2></div>
        <div class="institucion_list">
            <ul>
                <li><a href="{{URL::to('/funciones-sustantivas/docencia')}}">Docencia</a></li>
                <li><a href="{{URL::to('/funciones-sustantivas/investigacion')}}">Investigación</a></li>
                <li><a href="{{URL::to('/funciones-sustantivas/vinculacion')}}">Vinculación</a></li>
                <li><a href="{{URL::to('/funciones-sustantivas/gestion')}}">Gestión</a></li>
            </ul>
        </div>
    </div>
</div>