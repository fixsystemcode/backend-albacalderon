<ul class="navbar-nav m-auto">
    <li class="nav-item">
        <a class="nav-link" href="{{URL::to('/')}}">Inicio</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{URL::to('/quienes-somos')}}">Quienes somos</a>
    </li>
    <li class="nav-item mydrop" style="position: relative;">
        <a class="nav-link" href="{{URL::to('/servicios')}}">Servicios</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{URL::to('/nuestros-clientes')}}">Nuestros Clientes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{URL::to('/contactos')}}">Contactenos</a>
    </li>
</ul>
<!-- nav_btn -->