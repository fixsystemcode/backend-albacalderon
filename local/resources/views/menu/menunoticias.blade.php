<div class="col-lg-3">
    <div class="institucion_menu">
        <div class="institucion_titulo" style="margin-bottom: 20px"><h2>Noticias Populares</h2></div>
        <div class="noticia_list">
            @foreach($populares as $item)
            <div class="noticiasl_box">
                <div class="nl_img">
                    <img src="{{asset('local/public/images')}}/{{$item->path}}" alt="">
                    <div class="nl_fade">
                        <a href="{{URL::to('/noticias')}}/{{$item->slug}}">Mas información</a>
                    </div>
                </div>
                <div class="nl_title">
                    <a href="{{URL::to('/noticias')}}/{{$item->slug}}"><h5>{{$item->titulo}}</h5></a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

