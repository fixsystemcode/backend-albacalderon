<div class="col-lg-3">
    <div class="institucion_menu">
        <div class="institucion_titulo"><h2>Oferta Académica</h2></div>
        <div class="institucion_list">
            <ul>
                @foreach($carreras as $item)
                <li><a href="{{URL::to('/oferta-academica/')}}/{{$item->slug}}">{{$item->titulo}}</a></li>
                @endforeach
                <li><a href="{{URL::to('/oferta-academica/')}}">Carreras en construcción</a></li>
            </ul>
        </div>
    </div>
</div>