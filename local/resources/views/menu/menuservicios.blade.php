<div class="col-lg-3 col-md-4 order-md-first order-last col-sm-12">
    <div class="institucion_menu">
        <div class="institucion_titulo"><h2>Servicios</h2></div>
        <div class="institucion_list">
            <ul>
                <li><a href="{{URL::to('/servicios/biblioteca')}}"><i class="fas fa-chevron-right"></i> Biblioteca dentro del campus</a></li>
                <li>
                    <a><i class="fas fa-chevron-right"></i> Bienestar estudiantil</a>
                    <div style="padding-left: 20px;">
                        <ul>
                            <li><a href="{{URL::to('/servicios/becas')}}">Becas</a></li>
                            <li><a href="{{URL::to('/servicios/seguimiento-a-graduados')}}">Seguimiento a graduados</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="{{URL::to('/no-page')}}"><i class="fas fa-chevron-right"></i> Repositorio</a></li>
                <li><a href="{{URL::to('/bolsa-de-empleo')}}"><i class="fas fa-chevron-right"></i> Bolsa de empleo</a></li>
                <li>
                    <a><i class="fas fa-chevron-right"></i> Servicios en linea</a>
                    <div style="padding-left: 20px;">
                        <ul>
                            <li><a href="{{URL::to('/no-page')}}"> Aula virtual</a></li>
                            <li><a href="https://istspe.runacode.com/public/login" target="_blank">Plataforma Educativa</a></li>
                            <li><a href="https://biblioteca.utm.edu.ec/opac_css/" target="_blank">Biblioteca virtual</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>