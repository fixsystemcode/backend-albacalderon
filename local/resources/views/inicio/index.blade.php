@extends('layouts.principal')

@section('contenido')
    <style>
        p{
            text-align: center;
            color: #009DE2;
        }
    </style>
	@include('menu.header_principal')
    <section id="ss_secti">
        <div class="slider_section">
            <div class="sc_slider">
                <div class="bx-slidah">
                    @foreach($slider as $sl)
                    <div class="bxs_item" >
                        <img src="{{asset('local/public/images')}}/{{$sl->path}}" alt="" style="width: 100%;height: 100%;object-fit: cover;position: absolute;">
                        <div style="top: 50%;transform: translateY(-50%);position: relative;z-index: 99">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="sls_context">
                                            @if($sl->mostrar == 1)
                                            <h5>{{$sl->titulo}}</h5>
                                            <!-- <h2>FLORISTERIA ILUSIÓN</h2> -->
                                            <p>{{$sl->descripcion}}</p>
                                            @endif
                                            {{-- @if($sl->enlace != null)
                                            <a href="{{$sl->enlace}}" class="btn555" @if($sl->tipo != 0) target='_blank' @endif>Más Información</a>
                                            @endif --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


    <section class="s_colecction back_white" style="padding-top: 20px">
        <div class="container">
            <div class="s_title">
                <h2>Diseño y Desarrollo Web Manta - Ecuador</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/patronato.png') }}" alt="Card image cap">
                            <br>
                            <p>Patronato Municipal de Manta</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/centrotmo.png') }}" alt="Card image cap">
                            <br>
                            <p>Centro TMO</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/bow-sa-fixsystem-diseno-web-profesional-ecuador.jpg') }}" alt="Card image cap">
                            <br>
                            <p>BOW S.A</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/cambrdge-fixsystem-diseno-web-profesional-ecuador.jpg') }}" alt="Card image cap">
                            <br>
                            <p>Cambridge Ecuador</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/costa-mar-fr-fixsystem-com.png') }}" alt="Card image cap">
                            <br>
                            <p>Costamar FM</p>
                        </div>
                        <div class="col-md-4">
                            <img class="card-img-top" src="{{ asset('assets/images/clientes/galpig-fixsystem-diseno-web-profesional-ecuador.jpg') }}" alt="Card image cap">
                            <br>
                            <p>Supermercado Galpig</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>  


    <div class="efx_image_show">
        <div class="efx_overlay"></div>
        <div class="efx_image_content">
            <span><i class="fa fa-times"></i></span>
            <img src="">
            <p class="popup-desc" style="position: absolute;right: 50%;color: #fff;font-weight: 600;font-size: 18px;background-color: #0f0f0f;"></p>
        </div>
    </div>

    <div class="visho" style="">
      <div class="overlex"></div>
      <div class="vi-sho">
        <span><i class="fas fa-times"></i></span>
        <iframe width='75%' height='180' src='' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>
      </div>
    </div>

	@include('menu.footer')
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
        $('#cs_carousel').owlCarousel({
            loop:true,
            margin:15,
            nav:false,
            dots: true,
            autoplay: true,
            items: 1,
            autoplayTimeout: 8000,
            autoplaySpeed: 1000,
            smartSpeed: 1000
        });
    })
</script>
	
@endsection