<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Fixsystem</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    {!! Html::style('admin/plantilla1/font-awesome/css/font-awesome.css') !!}
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/lightgallery.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.bxslider.css')}}">

    <link rel="stylesheet" href="{{asset('assets/css/efxvid.css')}}">
    <link rel="icon" type="image/x-icon" href="{{asset(iconPagina())}}" />



    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:400,500,600,700,800&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Be+Vietnam:400,600|Montserrat:500,700,800&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Rubik:500,700,900&display=swap" rel="stylesheet">



    <link href="https://fonts.googleapis.com/css2?family=PT+Serif:ital,wght@0,400;0,700;1,400&family=Tangerine:wght@400;700&display=swap" rel="stylesheet">

    <!--  -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Heebo:500,700,800&display=swap" rel="stylesheet">
    @yield('style')
</head>
<body>
    <style type="text/css">
        #lblCartCount {
            font-size: 10px;
            background: #ff0000;
            color: #fff;
            padding: 0 5px;
            vertical-align: top;
            margin-left: -10px;
        }
    </style>
    <header>
        @include('layouts.appCliente')
    </header>
    @if (Session::has('message'))
       <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
        @yield('contenido')
    
</body>

<script src="{{asset('assets/js/jquery-3.3.1.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/lightgallery.js')}}"></script>
<script src="{{asset('assets/js/jquery.bxslider.js')}}"></script>

{!! Html::style('admin/plantilla1/css/plugins/toast/css/jquery.toast.css') !!}    
{!! Html::script('admin/plantilla1/js/plugins/toast/js/jquery.toast.js') !!}
<script src="{{asset('assets/js/scripts.js')}}"></script>
<script src="{{ asset('local/public/js/miapp.js?v1') }}"></script>
    @yield('script')

</html>