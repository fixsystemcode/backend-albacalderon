<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->	<html> <!--<![endif]-->

	<head>

		<meta charset="utf-8" />

		<title>La Ciudad - @yield('title')</title> 

    <meta name="keywords" content="La ciudad, Manta, Proyectos, Actividades, Lugares Y Emprendimient." />

    <meta name="description" content="<?php if (isset($texto)) { echo "La ciudad ".$texto; }  ?> " />
    <meta name="title" content="<?php if (isset($titulo)) { echo  $titulo; }  ?> " />

    
    <meta property="og:image" content="<?php if (isset($imagen)) { echo $imagen; }  ?>" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@laciudad">
    <meta name="twitter:creator" content="@laciudad">
    <meta name="twitter:title" content="<?php if (isset($articulo->titulo)) { echo $articulo->titulo; }  ?>  ">
    <meta name="twitter:image" content="<?php if (isset($imagen)) { echo $imagen; }  ?> ">
    <meta name="twitter:description" content="<?php if (isset($texto)) { echo $texto; }  ?>  ">
	 
	 


		<!-- mobile settings -->

		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->



		<!-- WEB FONTS : use %7C instead of | (pipe) -->

		{!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700') !!}


		<!-- CORE CSS -->
		{!! Html::style('assets/plugins/bootstrap/css/bootstrap.min.css') !!}
		


		<!-- REVOLUTION SLIDER -->

		{!! Html::style('assets/plugins/slider.revolution/css/extralayers.css') !!}
		{!! Html::style('assets/plugins/slider.revolution/css/settings.css') !!}

	
		<!-- THEME CSS -->

		{!! Html::style('assets/css/essentials.css') !!}

		{!! Html::style('assets/css/layout.css') !!}



		<!-- PAGE LEVEL SCRIPTS -->

		{!! Html::style('assets/css/header-1.css') !!}

		{!! Html::style('assets/css/color_scheme/green.css') !!}



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8&appId=644942119027203";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		

	</head> 



	<!--

		AVAILABLE BODY CLASSES:

		

		smoothscroll 			= create a browser smooth scroll

		enable-animation		= enable WOW animations



		bg-grey					= grey background

		grain-grey				= grey grain background

		grain-blue				= blue grain background

		grain-green				= green grain background

		grain-blue				= blue grain background

		grain-orange			= orange grain background

		grain-yellow			= yellow grain background

		

		boxed 					= boxed layout

		pattern1 ... patern11	= pattern background

		menu-vertical-hide		= hidden, open on click

		

		BACKGROUND IMAGE [together with .boxed class]

		data-background="assets/images/boxed_background/1.jpg"

	-->

	<body class="smoothscroll enable-animation">



		<!-- wrapper -->

		<div id="wrapper">



			<!-- 

				AVAILABLE HEADER CLASSES



				Default nav height: 96px

				.header-md 		= 70px nav height

				.header-sm 		= 60px nav height



				.noborder 		= remove bottom border (only with transparent use)

				.transparent	= transparent header

				.translucent	= translucent header

				.sticky			= sticky header

				.static			= static header

				.dark			= dark header

				.bottom			= header on bottom

				

				shadow-before-1 = shadow 1 header top

				shadow-after-1 	= shadow 1 header bottom

				shadow-before-2 = shadow 2 header top

				shadow-after-2 	= shadow 2 header bottom

				shadow-before-3 = shadow 3 header top

				shadow-after-3 	= shadow 3 header bottom



				.clearfix		= required for mobile menu, do not remove!



				Example Usage:  class="clearfix sticky header-sm transparent noborder"

			-->

			<!-- Top Bar -->

		
			<div class="border-top block clearfix" style="border-top: 4px solid #2d1815;">

					<div class="container">



						<a class="img-responsive  logo has-banner pull-left v" href="{{ URL::asset('/') }}" >
 
							{!! Html::image('assets/images/logo.png', '', ['class' => 'img-responsive', 'style' => 'float: left']) !!}


						</a>


<!--
						<a class="banner pull-right hidden-sm hidden-xs" href="#">

							<img src="assets/images/la-salud-junto-a-ti-patronato.png" alt="banner" />

						</a>
						<a class="banner pull-right hidden-sm hidden-xs" href="#">

							<img src="assets/images/la-salud-junto-a-ti-patronato.png" alt="banner" />

						</a>-->

					</div>

				</div>

		
			<div id="header" class="sticky header-sm clearfix">



			



				<!-- TOP NAV -->

				<header id="topNav">

					<div class="container">



						<!-- Mobile Menu Button -->

						<button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">

							<i class="fa fa-bars"></i>

						</button>



						<!-- BUTTONS -->

						<ul class="pull-right nav nav-pills nav-second-main">







						



						</ul>

						<!-- /BUTTONS -->



						<!-- 

							Top Nav 

							

							AVAILABLE CLASSES:

							submenu-dark = dark sub menu

						-->

						@include('layouts.menu')



					</div>

				</header>

				<!-- /Top Nav -->



			</div>

 

			<section style="padding: 10px;">
				<div class="container">
					
					<div class="row">
					
					
							@yield('contenido')

							
					
				</div>
			</section>

			

			<!-- FOOTER -->


<footer id="footer">
				<div class="container">

					<div class="row margin-top-10 margin-bottom-40 size-13">

						<!-- col #1 -->
						<div class="col-md-8 col-sm-8">

							<!-- Footer Logo -->
							<img class="footer-logo hidden-xs" src="/assets/images/logo.png" alt="">

							@if (count($informacion))
								<p>{{$informacion->hora}}</p>

								<!-- Contact Address -->
								<address>
									<ul class="list-unstyled">
										<li class="footer-sprite address">
											Dirección<br>
											{{$informacion->direccion}}<br>
										</li>
										
										<li class="footer-sprite email">
											
											<a href="favira2010@hotmail.com">{{$informacion->email}}</a>
										</li>
									</ul>
								</address>
								<!-- /Contact Address -->
							@else

								<p>----</p>

								<!-- Contact Address -->
								<address>
									<ul class="list-unstyled">
										<li class="footer-sprite address">
											Dirección<br>
											---<br>
										</li>
										
										<li class="footer-sprite email">
											
											<a href="#">--</a>
										</li>
									</ul>
								</address>
								<!-- /Contact Address -->

							@endif

				</div>
			<!-- /col #1 -->

			<!-- col #2 -->
			
			<!-- /col #2 -->

			<!-- col #3 -->
					 
							 
			<!-- /col #3 -->

		</div>

	</div>

	<div class="copyright">
		<div class="container">
			<ul class="pull-right nomargin list-inline mobile-block">
			
			</ul>

			© All Rights Reserved, Unidad Educativa Montecristi | Diseñado por <a target="_blank"  href="http://www.fixsystem.com.ec/">FixSystem</a>
		</div>
	</div>

</footer>

			<!-- /FOOTER -->



		</div>

		<!-- /wrapper -->





		<!-- SCROLL TO TOP -->

		<a href="#" id="toTop"></a>




	<!-- PRELOADER -->

		<div id="preloader">

			<div class="inner">

				<img src="/assets/images/carga.gif" alt="" style="margin-left: -6px;margin-top: 6px;" />

				<span class="loader">cargando</span>



			</div>

		</div><!-- /PRELOADER -->





		<!-- JAVASCRIPT FILES -->


			<!-- JAVASCRIPT FILES -->
		{!! Html::script('/assets/js/blazy.min.js') !!}
		<script type="text/javascript">

		var plugin_path = '/assets/plugins/';

		var bLazy = new Blazy({
		    selector: 'img'
		});

		</script>


        {!! Html::script('/assets/plugins/jquery/jquery-2.1.4.min.js') !!}
        {!! Html::script('/assets/js/scripts.js') !!}

			

		 
		{!! Html::script('http://www.uegraleloyalfaro.edu.ec/assets/plugins/smoothscroll.js') !!}
		

		<!-- REVOLUTION SLIDER -->
        {!! Html::script('/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js') !!}
        {!! Html::script('/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js') !!}
        {!! Html::script('/assets/js/view/demo.revolution_slider.js') !!}

        {!! Html::script('/assets/plugins/bootstrap/js/bootstrap.min.js') !!}


		
	</body>

</html>



