<div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
              
              <li class="active dropdown singleDrop">
                <a href="{{ URL::asset('/') }}"><i class="fa fa-home" aria-hidden="true"></i> Inicio</a>
              </li>
              <li class="active dropdown singleDrop">
                <a href="{{ URL::asset('huecas') }}">Huecas</a>
              </li>
              
              <li class="active dropdown singleDrop">
                <a href="{{ URL::asset('deportes') }}">Deportes</a>
              </li>
              <li class="active dropdown singleDrop">
                <a href="{{ URL::asset('artistas') }}">Artistas</a>
              </li>
               <li class="active dropdown singleDrop">
                <a href="{{ URL::asset('proyectos') }}">Proyectos</a>
              </li>
                <?php 
                $acep = Auth::user(); 

              ?>
              
              @if($acep == null)
                  <li class="active dropdown singleDrop">
                    <a href="{{ URL::asset('registro') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i>Regístrate</a>
                  </li>
              @else
                  
                  <li class="active dropdown singleDrop">
                    <a href="{{ URL::asset('logout') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Cerrar Sesión</a>
                  </li>

              @endif
             
            </ul>
</div>
 