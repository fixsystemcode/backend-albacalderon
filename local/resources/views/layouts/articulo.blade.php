<!DOCTYPE html>
<html lang="es" id="html-hidden">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <title>La Ciudad | {{$articulo->categoria}} en Manta | @yield('titulo') | Proyectos, Actividades, Lugares Y Emprendimientos en Manta, Manabí, Ecuador </title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="laciudad, la ciudad" name="keywords">
        <meta content="FixSystem" name="author">
        <link href={{asset('assets/fin/css/icons.css')}} rel="stylesheet">
        <link href={{asset('assets/fin/css/animate.min.css')}} rel="stylesheet">
        <link href={{asset('assets/fin/css/style.css')}} rel="stylesheet" type="text/css">
        <link href={{asset('assets/fin/css/responsive.css')}} rel="stylesheet" type="text/css">
        <link href={{asset('assets/fin/css/bootstrap.css')}} rel="stylesheet" type="text/css">
        <link href={{asset('assets/fin/css/owl.carousel.css')}} rel="stylesheet">
        <link rel="stylesheet" href={{asset('assets/fin/css/owl.theme.default.css')}}>
        <link rel="stylesheet" href="{{asset('assets/fin/css/principal.css')}}">
        <link href="{{asset('assets/fin/css/font-awesome.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/fin/css/listing.min.css')}}">
    </head>
    <body style="background-color: #fff;">
        <div class="theme-layout" id="scrollup">
            <div class="responsive-header">
                <div class="responsive-menubar">
                    <div class="res-logo">
                        <a href="/" title="">
                            <img alt="" src="{{asset('assets/fin/images/logo.png')}}"/>
                        </a>
                    </div>
                    <div class="menu-resaction">
                        <div class="res-openmenu">
                            <img alt="" src="{{asset('assets/fin/images/icon.png')}}"/>
                            Menu
                        </div>
                        <div class="res-closemenu">
                            <img alt="" src="{{asset('assets/fin/images/icon2.png')}}"/>
                            Cerrar
                        </div>
                    </div>
                </div>
                <div class="responsive-opensec" style="overflow: hidden;">
                    <div class="btn-extars">
                        <ul class="account-btns">
                            <li class="signup-popup">
                                <a class="post-job-btn" title="" id="myBtn">
                                    <i class="la la-key">
                                    </i>
                                    Registrarse
                                </a>
                            </li>
                            <li class="signin-popup">
                                <a class="post-job-btn" title="" id="myBtn2">
                                    <i class="fa fa-sign-in">
                                    </i>
                                    Login
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- Btn Extras -->
                    <form class="res-search">
                        <input placeholder="Buscar..." type="text"/>
                        <button type="submit">
                            <i class="la la-search">
                            </i>
                        </button>
                    </form>
                    <div class="responsivemenu">
                        <ul>
                            <li class="menu-item">
                                <a href="/" title="">
                                    Inicio
                                </a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#" title="">
                                    Donde comer
                                </a>
                                <ul>
                                    <li>
                                        <a href="/restaurantes" title="">
                                            Restaurantes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/huecas" title="">
                                            Huecas
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#" title="">
                                    Que Hacer
                                </a>
                                <ul>
                                    <li>
                                        <a href="/deportes" title="">
                                            Deportes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/lugares" title="">
                                            Lugares
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/diversion-y-entretenimiento" title="">
                                            Divertirse
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/eventos" title="">
                                            Eventos
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#" title="">
                                    Donde Hospedarse
                                </a>
                                <ul>
                                    <li>
                                        <a href="/hoteles">
                                            Hotel
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#" title="">
                                    Invertir En Manta
                                </a>
                                <ul>
                                    <li>
                                        <a href="/proyectos">
                                            Proyectos Inmobiliarios
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/negocios">
                                            Negocios
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/emprendimientos">
                                            Emprendimiento
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#" title="">
                                    Nuestra Gente
                                </a>
                                <ul>
                                    <li>
                                        <a href="/personajes" title="">
                                            Personajes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/artistas" title="">
                                            Artistas
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <header class="gradient2 navbar-me">
                <div class="menu-sec">
                    <div class="container">
                        <div class="logo">
                            <a href="/" title="">
                                <img alt="" src="{{asset('assets/fin/images/logo-color-big.png')}}" width="176px"/>
                            </a>
                        </div>
                        <!-- Logo -->
                        <div class="btn-extars">
                            <ul class="account-btns">
                                <li class="">
                                    <a class="post-job-btn" title="" id="myBtn3">
                                        <i class="la la-key">
                                        </i>
                                        Registrarse
                                    </a>
                                </li>
                                <li class="">
                                    <a class="post-job-btn" title="" id="myBtn4">
                                        <i class="fa fa-sign-in">
                                        </i>
                                        Login
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- Btn Extras -->
                        <nav>
                            <ul>
                                <li class="menu-item">
                                    <a href="/" title="">
                                        Inicio
                                    </a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" title="">
                                        Donde comer
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="/restaurantes" title="">
                                                Restaurantes
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/huecas" title="">
                                                Huecas
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" title="">
                                        Que Hacer
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="/deportes" title="">
                                                Deportes
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/lugares" title="">
                                                Lugares
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/diversion-y-entretenimiento" title="">
                                                Divertirse
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/eventos" title="">
                                                Eventos
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" title="">
                                        Donde Hospedarse
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="/hoteles">
                                                Hotel
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" title="">
                                        Invertir En Manta
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="/proyectos">
                                                Proyectos Inmobiliarios
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/negocios">
                                                Negocios
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/emprendimientos">
                                                Emprendimiento
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" title="">
                                        Nuestra Gente
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="/personajes" title="">
                                                Personajes
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/artistas" title="">
                                                Artistas
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!-- Menus -->
                    </div>
                </div>
            </header>
            <a class="btn_subir" href="#">
        		<i class="fa fa-arrow-up"></i>
        	</a>
            <section>
                <div class="div-head">
                    <img src="/local/public/images/{{$articulo->path}}" alt="">
                </div>
            </section>
            <section style="background-color: #fff;">
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12" style="padding: 20px 3%;">
                                <div class="col-md-8">
                                    <!-- <div class="ruta-post">
                                        <a href="{{URL::asset('/')}}">inicio</a><span>/</span><span>{{$articulo->categoria}}</span><span>/</span><span>{{$articulo->titulo}}</span>
                                    </div> -->
                                    <div class="post-header">
                                        <div class="post-title">
                                            <h1>{{$articulo->titulo}}</h1>
                                            <hr style="width: 60%">
                                            <span>{{$articulo->seo_descripcion}}</span>
<!--                                             <p><i class="la la-unlink"></i> www.jobhunt.com</p>
                                            <p><i class="la la-phone"></i> +90 538 963 54 87</p>
                                            <p><i class="la la-envelope-o"></i> ali.tufan@jobhunt.com</p> -->
                                        </div>
                                        <div class="post-gallery">
                                            <div class="owl-carousel owl-gallery" data-slider-id="1">
                                                @foreach($fotos as $foto)
                                                <div class="item"><img src="{{asset('local/public/galeria')}}/{{$foto->path}}" alt=""></div>
                                                @endforeach
                                            </div>
                                            <div class="owl-thumbs owl-thumbs-gallery" data-slider-id="1">
                                                @foreach($fotos as $foto)
                                                <img src="{{asset('local/public/galeria')}}/{{$foto->path}}" alt="">
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="social-post">
                                       <span style="font-size: 15px;color: #000000;">Dale un "Me gusta"</span>
                                        <div class="social-post-btns">
                                            <ul>
                                                    <div class="fb-like" data-href="http://laciudad.ec/{{$articulo->categoria}}/{{$articulo->slug}}" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="content_post">
                                        @if ($articulo->fechainicio !="" )
                                        <br>
                                        <p><strong> Lugar: </strong> {{$articulo->lugar}}</p>
                                        <p><strong> Empieza: </strong> {{$articulo->fechainicio}}</p>
                                        <p><strong>Finaliza: </strong> {{$articulo->fechafin}}</p>
                                        <br>
                                        @endif
                                        <?php echo $articulo->texto; ?>
                                    </div>
                                    <div class="tags-post">
                                        @if(count($tags))
                                           <div class="tag-post" style="margin-top: 20px;">
                                                <div class="tag-post-item">
                                                     <a  style="background: #FF713C;color: white;" href="#">#Etiquetas</a>
                                                     @foreach($tags as $etiquetas)
                                                            <a href="{{ URL::asset('/etiqueta/'.$etiquetas->tag) }}">
                                                                <span class="eltd-tax-title">#{{$etiquetas->tag}}</span>
                                                            </a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="share-bar">
                                        <span>Compartir</span><a href="#" title="" class="share-fb"><i class="fa fa-facebook"></i></a><a href="#" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>
                                    </div>
                                    <!-- <div class="fb" style="min-height: 400px;">
                                        <h3>Comentarios</h3>
                                        <div class="fb-comments" data-href="http://laciudad.ec/eventos/{{$articulo->slug}}" data-width="100%" data-numposts="5"></div>
                                    </div> -->

                                </div>
                                <div class="col-md-4">
                                    <div class="job-location">
                                        <h3>Ubicación</h3>
                                        <div class="job-lctn-map">
                                            <!-- AIzaSyCwgFwm-Xgnqy3qTW_h0L5g2DtYa26LEOs -->
                                            <iframe
                                              width="600"
                                              height="450"
                                              frameborder="0" style="border:0"
                                              src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCwgFwm-Xgnqy3qTW_h0L5g2DtYa26LEOs&q={{$articulo->latitud}},{{$articulo->longitud}}" allowfullscreen>
                                            </iframe>

                                        </div>
                                    </div>
                                    <?php 
                                    if ($articulo->longitud==null) {
                                        echo '<a class="apply-thisjob" href="#" title="" target="_blank"><i class="la la-paper-plane"></i>Ir al mapa</a>';
                                    }else{
                                        echo '<a class="apply-thisjob" href="//maps.google.com/maps?daddr='.$articulo->latitud.','.$articulo->longitud.'" title="" target="_blank"><i class="la la-paper-plane"></i>Ir al mapa</a>';
                                    }
                                     ?>
                                    <!-- <a class="apply-thisjob" href="https://www.google.com/maps/@ {{$articulo->latitud}},{{$articulo->longitud}},22z" title="" target="_blank"><i class="la la-paper-plane"></i>Ir al mapa</a> -->
                                    <div class="job-overview">
                                        <ul>
                                        <?php
                                        // dd($camposextrasid);
                                        foreach ($grupos as $grupo) {
                                            if($grupo->vistacampos_id == 4 )
                                            {
                                                $contadortitulo = 0;
                                                //$totalextra = count($camposextras);
                                                foreach($camposextrasid as $key  => $value)
                                                {
                                                  if ($camposextrascategoria[$key] == $articulo->id_categoria) {
                                                      if(isset($camposextrasguardados[$value])){
                                                        $valorcampos = $camposextrasguardados[$value];
                                                      }else {
                                                          $valorcampos = null;
                                                      }
                                                      if ($grupo->id == $camposgruposid[$key]) {
                                                          if ($valorcampos != null) {
                                                                /// mostrar titulo de categoria
                                                              if ($contadortitulo == 0) {
                                                                 // echo '<div class="col-md-12"><h3 style="    border-top: 2px solid #e0e0e0;padding: 6px;font-size: 16px;background: rgba(226, 226, 226, 0.3);color: #4a4a4a;">'.$grupo->descripcion.'</h3 ></div>' ;
                                                                 $contadortitulo = $contadortitulo + 1;
                                                              }
                                                              ///////////////
                                                               echo '<li><i class="'.$camposextrasiconos[$key].' fa-4"></i><h3>'.$camposextrasdesc[$key].'</h3><span>'.$valorcampos.'</span></li>';
                                                           }
                                                      }
                                                  }
                                                }
                                                if($contadortitulo==0){
                                                echo "<style>.job-overview ul{border:none}</style>";
                                                break;}
                                            }
                                        }
                                        ?>
                                       </ul>
                                    </div>
                                    <!-- <div class="sec-video">
                                        <div class="col-md-12 col-xs-12">
                                            <a href="">
                                                <div class="col-md-4 col-xs-4 video-thumb">
                                                    <img src="assets/fin/images/thumb.jpg" alt="">
                                                </div>
                                                <div class="col-md-8 col-xs-8 video-desc">
                                                    <h3>Titulo del video</h3>
                                                    <span>Descripcion del video</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-md-12 col-xs-12">
                                            <a href="">
                                                <div class="col-md-4 col-xs-4 video-thumb">
                                                    <img src="assets/fin/images/thumb.jpg" alt="">
                                                </div>
                                                <div class="col-md-8 col-xs-8 video-desc">
                                                    <h3>Titulo del video</h3>
                                                    <span>Descripcion del video</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div> -->
                                        <!--                                         <div class="apply-alternative">
                                            <a href="#" title=""><i class="la la-linkedin"></i> Apply with Linkedin</a>
                                            <span><i class="la la-heart-o"></i> Shortlist</span>
                                        </div> -->
                                </div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="col-md-12 ultimo">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="fb" style="min-height: 400px;">
                                            <h3>Comentarios</h3>
                                            <div class="fb-comments" data-href="http://laciudad.ec/eventos/{{$articulo->slug}}" data-width="100%" data-numposts="5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 30px; z-index: 1;"><h2>CONOCE MÁS</h2></div>
                                @foreach($ultimos as $ultimo)
                                <div class="col-md-4 col-sm-4">
                                    <div class="ultimo-content">
                                        <a href="{{ URL::asset($ultimo->categoria)}}/{{$ultimo->slug}}">
                                            <div class="ultimo-head">
                                                <div class="ultimo-icon">
                                                    <img src="{{asset('local/public/images')}}/{{$ultimo->icono}}" alt="{{$ultimo->titulo}}">
                                                </div>
                                                <div class="ultimo-img">
                                                    <img src="{{asset('local/public/images')}}/{{$ultimo->path}}" alt="{{$ultimo->titulo}}">
                                                </div>
                                            </div>
                                        </a>
                                        
                                        <div class="ultimo-desc">
                                            <div class="ultimo-titulo">
                                                <h4><a href="{{ URL::asset($ultimo->categoria)}}/{{$ultimo->slug}}">{{$ultimo->titulo}}</a></h4>
                                            </div>
                                            <div class="ultimo-info">
                                                <span>{{$ultimo->seo_descripcion}}</span>
                                            </div>
                                            <div class="rating-item">
                                                <div class="rating-number">
                                                    <span>3</span>
                                                </div>
                                                <div class="rating-star">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="footer-p">
                <div class="middle-footer">
                    <div class="content">
                        <div class="col-lg-3 column">
                            <div class="widget">
                                <div class="about_widget">
                                    <div class="logo">
                                        <a href="#" title=""><img src="{{URL::asset('assets/fin/images/logo.png')}}" width="174px" alt="" /></a>
                                    </div>
                                    <span>
                                        <i class="fa fa-map-marker"></i>
                                        Manta, Ecuador.
                                    </span>
                                    <span>
                                        <i class="fa fa-phone"></i>
                                        <a href="tel:00593987797635"> 0987797635 </a> - <a href="tel:00593982103827"> 0982103827 </a> - <a href="tel:00593985246717"> 0985246717 </a></span>
                                    <span>
                                        <i class="fa fa-envelope-o"></i>
                                        <a href="mailto:info@laciudad.com"> info@laciudad.com</a></span>
                                    <div class="social">
                                        <a target="_blank" href="https://www.facebook.com/laciudad.ec/" title="laciudad.ec"><i class="fa fa-facebook"></i></a>
                                        <a target="_blank" href="https://twitter.com/LaciudadEcuador" title="LaciudadEcuador"><i class="fa fa-twitter"></i></a>
                                        <a target="_blank" href="https://www.youtube.com/channel/UCnffswy3iEnLqgf6c21B_CA" title="La Ciudad Manta"><i class="fa fa-youtube-play"></i></a>
                                        <a target="_blank" href="https://www.instagram.com/la_ciudad_ec/" title="la_ciudad_ec"><i class="fa fa-instagram"></i></a>

                                    </div>
                                </div><!-- About Widget -->
                            </div>
                        </div>
                        <div class="col-lg-4 column">
                            <div class="">
                                <h3 class="footer-titulo">Categorias</h3>
                                <div class="link_widgets">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <a href="{{URL::asset('/restaurantes')}}" title="">Restaurantes </a>
                                            <a href="{{URL::asset('/huecas')}}" title="">Huecas</a>
                                            <a href="{{URL::asset('/deportes')}}" title="">Deportes </a>
                                            <a href="{{URL::asset('/lugares')}}" title="">Lugares </a>
                                            <a href="{{URL::asset('/diversion-y-entretenimiento')}}" title="">Divertirse </a>
                                            <a href="{{URL::asset('/eventos')}}" title="">Eventos </a>
                                        </div>
                                        <div class="col-lg-4">
                                            <a href="{{URL::asset('/hoteles')}}" title="">Hotel </a>
                                            <a href="{{URL::asset('/proyectos')}}" title="">Proyecto Inmoviliarios </a>
                                            <a href="{{URL::asset('/negocios')}}" title="">Negocios </a>
                                            <a href="{{URL::asset('/emprendimientos')}}" title="">Emprendimiento </a>
                                            <a href="{{URL::asset('/personajes')}}" title="">Personajes</a>
                                            <a href="{{URL::asset('/artistas')}}" title="">Artistas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 column">
                            <div class="">
                                <h3 class="footer-titulo">Sobre Nosotros</h3>
                                <div class="link_widgets">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <a href="#" title="">Contáctanos</a>
                                            <a href="#" title="">Sobre Nosotros</a>
                                            <a href="#" title="">FixSystem</a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-footer">
                    <span>Copyright © La Ciudad 2017 <a target="_blank" href="http://www.fixsystem.com.ec/">Diseñado por FixSystem</a></span>
                </div>
            </footer>
        </div>
        <div id="modal-registro" class="modal-registro">
            <!-- Modal content -->
            <div class="modal-registro-content">
                <span class="close">&times;</span>
                <h3>Registro</h3>
                <form>
                    <div class="campo-modal">
                        <input type="text" placeholder="usuario" />
                        <i class="la la-user"></i>
                    </div>
                    <div class="campo-modal">
                        <input type="password" placeholder="********" />
                        <i class="la la-key"></i>
                    </div>
                    <div class="campo-modal">
                        <input type="text" placeholder="Email" />
                        <i class="la la-envelope-o"></i>
                    </div>
                    <div class="dropdown-field">
                        <select data-placeholder="Please Select Specialism" class="chosen">
                            <option>Web Development</option>
                            <option>Web Designing</option>
                            <option>Art & Culture</option>
                            <option>Reading & Writing</option>
                        </select>
                    </div>
                    <div class="campo-modal">
                        <input type="text" placeholder="Número de Teléfono" />
                        <i class="la la-phone"></i>
                    </div>
                    <button type="submit">Registrarse</button>
                </form>
                <div class="extra-login">
                    <span>Iniciar Sesión Con red Social</span>
                    <div class="login-social">
                        <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                        <a class="go-login" href="#" title=""><i class="fa fa-google"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-login" class="modal-login">
            <div class="modal-login-content">
                <span class="closelogin">&times;</span>
                <h3> Login</h3>
                <form>
                    <div class="campo-modal">
                        <input type="text" placeholder="Usuario" />
                        <i class="la la-user"></i>
                    </div>
                    <div class="campo-modal">
                        <input type="password" placeholder="********" />
                        <i class="la la-key"></i>
                    </div>
                    <p class="remember-label">
                        <input type="checkbox" name="cb" id="cb1"><label for="cb1">Recordarme</label>
                    </p>
                    <a href="#" title="">Olvidaste la Contraseña?</a>
                    <button type="submit">Login</button>
                </form>
                <div class="extra-login">
                    <span>Iniciar Sesión Con red Social</span>
                    <div class="login-social">
                        <a class="fb-login" href="#" title=""><i class="fa fa-facebook"></i></a>
                        <a class="go-login" href="#" title=""><i class="fa fa-google"></i></a>

                    </div>
                </div>
            </div>
        </div>
        <!-- SIGNUP POPUP -->
        <script src="{{asset('assets/fin/js/jquery.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/fin/js/jquery.scrollbar.min.js')}}" type="text/javascript">
        </script>
        <script src="{{asset('assets/fin/js/script.js')}}" type="text/javascript"></script>
        <script type='text/javascript' src='//maps.googleapis.com/maps/api/js?key=AIzaSyBBIBj1S_-OreVLNdy1wi1C_bBOVEobIao&#038;libraries=geometry%2Cplaces&#038;ver=4.8'></script>
        <script src="{{asset('assets/fin/js/maps.js')}}" type="text/javascript">
        </script>
        <!-- Nice Select -->
        <script src="{{asset('assets/fin/js/bootstrap.min.js')}}">
        </script>
        <script src="{{asset('assets/fin/js/jquery.bcSwipe.min.js')}}">
        </script>
        <script src="{{asset('assets/fin/js/owl.carousel.js')}}">
        </script>
        <script src="{{asset('assets/fin/js/owl.carousel2.thumbs.js')}}"></script>
        <!-- <script src="assets/fin/js/materialize.js"></script> -->
        <script>
            $(document).ready(function() {
              $('.owl-gallery').owlCarousel({
              	dots: false,
                loop: false,
                mouseDrag: true,
                items: 1,
                thumbs: true,
                thumbsPrerendered: true
              })
            })
        </script>
        <script>
        	$(document).ready(function(){
				$('.btn_subir').click(function(){
					$('body, html').animate({
						scrollTop: '0px'
					}, 300);
				});
				$(window).scroll(function(){
					if( $(this).scrollTop() > 200 ){
						$('.btn_subir').fadeIn(300);
					} else {
						$('.btn_subir').fadeOut(300);
					}
				});
			 
			});
        </script>
        <script>
            $(window).scroll(function() {
                if($(this).scrollTop()>10) {
                    $( ".navbar-me" ).addClass("fixed-me");
                    $(".menu-sec nav > ul > li > a").addClass("colorr");
                    $(".menu-sec").addClass("sec-scroll");
                } else {
                    $( ".navbar-me" ).removeClass("fixed-me");
                    $(".menu-sec nav > ul > li > a").removeClass("colorr");
                    $(".menu-sec").removeClass("sec-scroll");
                }
            });
        </script>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10&appId=644942119027203';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        <script>
            var html = document.getElementById('html-hidden');
            var modalregistro = document.getElementById('modal-registro');
            var modallogin = document.getElementById('modal-login');

            var btn = document.getElementById("myBtn");
            var btn2 = document.getElementById("myBtn2");
            var btn3 = document.getElementById("myBtn3");
            var btn4 = document.getElementById("myBtn4");

            var span = document.getElementsByClassName("close")[0];
            var spanlogin = document.getElementsByClassName("closelogin")[0];

            btn.onclick = function() {
                modalregistro.style.display = "block";
                html.style.overflow = "hidden";
            }
            btn2.onclick = function() {
                modallogin.style.display = "block";
                html.style.overflow = "hidden";
            }
            btn3.onclick = function() {
                modalregistro.style.display = "block";
                html.style.overflow = "hidden";
            }
            btn4.onclick = function() {
                modallogin.style.display = "block";
                html.style.overflow = "hidden";
            }
            span.onclick = function() {
                modalregistro.style.display = "none";
                html.style.overflow = "";
            }
            spanlogin.onclick = function() {
                modallogin.style.display = "none"
                html.style.overflow = "";
            }

            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        </script>
    </body>
</html>