<nav class="navbar justify-content-between navbar-expand-lg" style="background: #009DE2;">
    <a class="navbar-brand" href="{{ route('inicio') }}">
        <img src="{{ asset(logopagina()) }}" width="100" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <form class="form-inline" id="logout-form" action="{{ route('cliente.auth.logout') }}" method="POST">
        {{ csrf_field() }}
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                @guest('cliente')
                    {{-- <li class="nav-item">
                        <a href="{{ route('cart.index') }}" class="nav-link">
                            <span class="fa fa-shopping-cart"></span> 
                        </a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link {{ Cart::count() == 0? 'disabled': '' }}" href="{{ route('cart.index') }}" id="linkCart" style="color: #fff">
                            <i class="fa fa-shopping-cart"></i>
                            <span class='badge badge-success' id='lblCartCount'> {{ Cart::count() }} </span>
                        </a>
                        
                    </li>
                    <li class="nav-item"><a href="{{ route('cliente.auth.login') }}" class="nav-link" style="color: #fff">Ingresar</a></li>
                    <li class="nav-item"><a href="{{ route('cliente.register') }}" class="nav-link" style="color: #fff">Registrarse</a></li>
                @else
                    <li class="nav-item">
                        <a href="{{ route('cliente.dashboard') }}" class="nav-link" style="color: #fff">
                            {{ Auth::guard('cliente')->user()->nombre . ' ' . Auth::guard('cliente')->user()->apellido }}
                            <span class="fa fa-user"></span> 
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('cliente/deseados') }}" class="nav-link" style="color: #fff">
                            <span class="fa fa-heart"></span> 
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a href="{{ route('cart.index') }}" class="nav-link">
                            <span class="fa fa-shopping-cart"></span> 
                        </a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link {{ Cart::count() == 0? 'disabled': '' }}" href="{{ route('cart.index') }}" style="color: #fff" id="linkCart">
                            <i class="fa fa-shopping-cart"></i>
                            <span class='badge badge-success' id='lblCartCount'> {{ Cart::count() }} </span>
                        </a>
                        
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('cliente.auth.logout') }}" onclick="event.preventDefault();                                     document.getElementById('logout-form').submit();" class="nav-link" style="color: #fff">
                            <i class="fa fa-sign-out"></i> Cerrar Sesión
                        </a>
                    </li>
                @endguest
            </ul>
        </div>
    </form>
</nav>
