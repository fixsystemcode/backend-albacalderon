@if( !Auth::user()->isAdmin )

    <li {{ (Request::is("administracion") ? 'class=active' : '') }}>
        <a href="{{ URL::asset('administracion') }}"><i class="fa fa-tachometer"></i>
            <span class="nav-label">Inicio </span>
        </a>
    </li>

    <li {{ (Request::is("articulos","categorias","tags" ) ? 'class=active' : '') }}>
        <a href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span class="nav-label">Contenido</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li {{ (Request::is("articulos") ? 'class=active' : '') }}><a href="{{ URL::asset('articulos')}}" ><i class="fa fa-file-text-o-"></i>Artículos</a></li>
        </ul>
    </li>
    
@else
    <li {{ (Request::is("administracion") ? 'class=active' : '') }}>
        <a href="{{ URL::asset('administracion') }}"><i class="fa fa-file-text-o"></i>
            <span class="nav-label">Inicio </span>
        </a>
    </li>

    <li {{ (Request::is("usuarios","tipos-usuarios","solicitudes") ? 'class=active' : '') }}>
        <a href="#"><i class="fa fa-users fa-1x"></i> <span class="nav-label">Gestor de Usuarios</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li {{ (Request::is("usuarios") ? 'class=active' : '') }}><a href="{{ URL::asset('usuarios')}}" ><i class="fa fa-user"></i> Usuarios</a></li>
            <li {{ (Request::is("tipos-usuarios") ? 'class=active' : '') }}><a href="{{ URL::asset('tipos-usuarios') }}"><i class="fa fa-user-plus"></i>Tipos de Usuarios</a></li>
            <!-- <li {{ (Request::is("solicitudes") ? 'class=active' : '') }}><a href="{{ URL::asset('solicitudes') }}"><i class="fa fa-user-plus"></i>Solicitudes</a></li> -->
        </ul>
    </li>

    <li {{ (Request::is("articulos","categorias","tags","activaciones" ) ? 'class=active' : '') }}>
        <a href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span class="nav-label">Contenido</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li {{ (Request::is("articulos") ? 'class=active' : '') }}><a href="{{ URL::asset('articulos')}}" ><i class="fa fa-file-text-o"></i>Artículos</a></li>
            <li {{ (Request::is("categorias") ? 'class=active' : '') }}><a href="{{ URL::asset('categorias')}}" ><i class="fa fa-bookmark"></i>Categorias</a></li>
            <li {{ (Request::is("tags") ? 'class=active' : '') }}><a href="{{ URL::asset('tags')}}" ><i class="fa fa-tags"></i>Etiquetas</a></li>
            <li {{ (Request::is("activaciones") ? 'class=active' : '') }}><a href="{{ URL::asset('activaciones')}}" ><i class="fa fa-check"></i>Activar articulos</a></li>
        </ul>
    </li>
    <li {{ (Request::is("shop","categoriaproductos" ) ? 'class=active' : '') }}>
        <a href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span class="nav-label">Shop</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li {{ (Request::is("carrito") ? 'class=active' : '') }}><a href="{{ URL::asset('carrito')}}" ><i class="fa fa-cart-plus"></i>Carrito</a></li>
            <li {{ (Request::is("pedidos") ? 'class=active' : '') }}><a href="{{ URL::asset('pedidos')}}" ><i class="fa fa-archive"></i>Pedidos</a></li>
            <li {{ (Request::is("depositos") ? 'class=active' : '') }}><a href="{{ URL::asset('depositos')}}" ><i class="fa fa-bank"></i>Depositos</a></li>
            <li {{ (Request::is("categoriaproductos") ? 'class=active' : '') }}><a href="{{ URL::asset('categoriaproductos')}}" ><i class="fa fa-file-text-o"></i>Categorias</a></li>
            <li {{ (Request::is("shop") ? 'class=active' : '') }}><a href="{{ URL::asset('shop')}}" ><i class="fa fa-bookmark"></i>Productos</a></li>
        </ul>
    </li>
    <li {{ (Request::is("animacion","imagen","video" ) ? 'class=active' : '') }}>
        <a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="nav-label">Módulos</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li {{ (Request::is("categoriainterna") ? 'class=active' : '') }}><a href="{{ URL::asset('categoriainterna')}}" ><i class="fa fa-cart-arrow-down"></i>Categorias</a></li>
            <li {{ (Request::is("animacion") ? 'class=active' : '') }}><a href="{{ URL::asset('animacion')}}" ><i class="fa fa-cart-arrow-down"></i>Slider</a></li>
            <li {{ (Request::is("imagen") ? 'class=active' : '') }}><a href="{{ URL::asset('imagen')}}" ><i class="fa fa-check-circle-o"></i>Imagenes</a></li>
            <li {{ (Request::is("video") ? 'class=active' : '') }}><a href="{{ URL::asset('video')}}" ><i class="fa fa-check-circle-o"></i>Videos</a></li>
        </ul>
    </li>

    <li {{ (Request::is("informacion") ? 'class=active' : '') }}>
        <a href="{{URL::to('informacion')}}" ><i class="fa fa-info-circle"></i> <span class="nav-label">Información</span></a>
    </li>
@endif