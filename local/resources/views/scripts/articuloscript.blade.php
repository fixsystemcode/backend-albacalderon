<script type="text/javascript">

	var updateProgressBar = function (progress, value) {
        var $div = $(progress);//.find('div');
        //var $span = $div.find('span');
        $div.attr('aria-valuenow', value);
        $div.css('width', value + '%');
        $div.text(value + '%');
    }

    $(document).ready(function(){
    	document.getElementById('categorias').onchange = function () {
			if (this.value=="8" || this.value=="9") {
				document.getElementById('lugar').style="";
			}
			else{
				document.getElementById('lugar').style="display:none;";
			}
		}
    });

    /* CARGA DE ARCHIVOS */
	$( document ).ready(function() {

		$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } } );

		var nuevo_documento = document.getElementById('nuevo_documento');
		var div_progress = document.getElementById('upload-progress');
		var progress = document.getElementById('porcentaje');
		var cancelar_documento = document.getElementById('cancelar_documento');

		var imagenPrincipalInput = document.getElementById('imagenprincipal');
		var imagenPrincipal = document.getElementById('imgprincipal');

		var subirDocumentoAjax;

		nuevo_documento.onchange = function (e) {
			var documento = this.files[0];
			var nombre = documento.name.replace(/ /g,"-");
			nombre =  Math.floor(Math.random() * 10000001) + nombre;

			var formData = new FormData();
			formData.append('path', nombre);
			formData.append('archivo', documento);

			subirDocumentoAjax = $.ajax({
				url: "{{URL::asset('uploaddoc')}}",
				type: "post",
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(div_progress).css('display', 'block');
					progress.className = 'progress-bar progress-bar-striped progress-bar-success active';
					updateProgressBar(progress, 0);
				},
				xhr: function(){
					var xhr = new window.XMLHttpRequest();
	    			xhr.upload.addEventListener("progress", function(evt){
	    				if (evt.lengthComputable) {
	    					var percentComplete = Math.round((evt.loaded / evt.total)*100);
	    					updateProgressBar(progress, percentComplete);
	        			}
	        		}, false);
	    			return xhr;
	    		},
	    		success: function(data){
	    			progress.className = 'progress-bar progress-bar-striped progress-bar-success';	        				
					updateProgressBar(progress, 100);
					div_progress.style.display = 'none';
					var list=document.createElement('li');
					list.id=nombre;
					list.className = 'list-group-item';
					list.innerHTML = "<a onclick='eliminardoc(this)' title='Eliminar Documento'><img style='height: 20px;' src='{{URL::asset('/assets/images/elidoc.png')}}'></a> ";
					list.innerHTML+= "<a href='{{URL::asset('/local/public/doc')}}/"+nombre+"' target='_blank' title='Visualizar documento' class='btn btn-link'>"+documento.name+"</a>";
					document.getElementById("listadocs").appendChild(list);
					
					var option=document.createElement("option");
					option.value=nombre;
					option.text=nombre;
					option.selected=true;
					document.getElementById("rutadocs").appendChild(option);

					option.value=documento.name;
					option.text=documento.name;
					document.getElementById("titulodocs").appendChild(option);
	    			nuevo_documento.value = "";

	    		},
				error: function(data){
					nuevo_documento.value = "";
					progress.className = 'progress-bar progress-bar-striped progress-bar-danger';
					updateProgressBar(progress, 100);
	                progress.innerHTML="Documento no Subido";
	                window.setTimeout(function(){div_progress.style.display = 'none'}, 3000);
	            }

			});
		}

		cancelar_documento.onclick = function(e){
			if (subirDocumentoAjax != null) {
				subirDocumentoAjax.abort();
				subirDocumentoAjax = null;
			}
		};
		/* FIN CARGA DE ARCHIVOS */

		imagenPrincipalInput.onchange = function () {
			var filesToUpload = imagenPrincipalInput.files; //input de imagen
			var imagen = filesToUpload[0];
			
			var img = document.createElement("img");
			var reader = new FileReader();
			reader.onload = function(e){
				img.src = e.target.result;
				img.onload = function () {
					var canvas = document.createElement("canvas");
					var ctx = canvas.getContext("2d");
					var MAX_WIDTH = 1000;
					var MAX_HEIGHT = 1000;
					var width = img.width;
					var height = img.height;
					if (width > height) {
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
					} else {
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
					}
					canvas.width = width;
					canvas.height = height;
					ctx.drawImage(img, 0, 0, width, height);
					imagenPrincipal.src = canvas.toDataURL("image/png");
				}
			}
			reader.readAsDataURL(imagen);
		}

		//galeria dinamica
		$("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
		$("ul.reorder-documentos-list").sortable({ tolerance: 'pointer' });
		$("ul.reorder-videos-list").sortable({ tolerance: 'pointer' });
		$('#reorder-helper').slideDown('slow');
		$('.image_link').attr("href","javascript:void(0);");
		$('.image_link').css("cursor","move");

		var urlToBlobPng = function(dataurl){
			var blobBin = atob(dataurl.split(',')[1]);
			var array = [];
			for(var i = 0; i < blobBin.length; i++) {
				array.push(blobBin.charCodeAt(i));
			}
			return new Blob([new Uint8Array(array)], {type: 'image/png'});
		}

		$('.inputimg').on('change', function(e){
			var dataurl;
			var filesToUpload = this.files;
			var foto = filesToUpload[0];
			var img = document.createElement("img");
			var num;
			var n_actual = $(this).data('n');
			var readableImagen = new FileReader();

			readableImagen.onload = function(e){
				img.src = e.target.result;

				img.onload = function() {
					var canvas = document.createElement("canvas");
					var ctx = canvas.getContext("2d");
					
					var MAX_WIDTH = 1000;
					var MAX_HEIGHT = 1000;
					var width = img.width;
					var height = img.height;
					if (width > height) {
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
					} else {
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
					}
					canvas.width = width;
					canvas.height = height;

					ctx.drawImage(img, 0, 0, width, height);
					dataurl = canvas.toDataURL("image/png");

					var cadena = foto.name.replace(" ","");
					num = Math.floor(Math.random() * 10000001);
					num+=cadena;

					document.getElementById('imgfile-'+n_actual).src=dataurl;
					document.getElementById('imgfile-'+n_actual).name=num;
					document.getElementById('elim'+n_actual).style="";

					var formData = new FormData();				
					formData.append("image", urlToBlobPng(dataurl) ,num);
					formData.append('nombre', num);
					@if (isset($articulo->id))
						formData.append('idarticulo', '{{$articulo->id}}');
					@endif
					$.ajax({
						url: "{{ URL::to('/upload') }}",
						method: "POST",
						data: formData,
						processData: false,
						contentType: false,
						headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
						success: function(datos){}, 
						error: function(j){
							alert(j.responseText);
						}
					});
				}
			}
			readableImagen.readAsDataURL(foto);
		});

		$("#my-dropzone").on('submit', function(evt){
			$("#ruta").empty();
			$("ul.reorder-photos-list li a label img").each(function() {
				if ($(this).attr('name')!=undefined) {
					if ($(this).attr('name')!="nada")   {
						var option=document.createElement("option");
						option.value=$(this).attr('name');
						option.text=$(this).attr('name');
						option.selected=true;
						document.getElementById("ruta").appendChild(option)
					}
				}
			});
			$("#videos").empty();
			$("ul.reorder-videos-list li").each(function() {
				var option=document.createElement("option");
				option.value=$(this).attr('id');
				option.text=$(this).attr('id');
				option.selected=true;
				document.getElementById("videos").appendChild(option);
			});
			$("#titulovideo").empty();
			$("ul.reorder-videos-list li a").each(function() {
				if ($(this)["0"].innerText!="") {
					var option=document.createElement("option");
					option.value=$(this)["0"].innerText;
					option.text=$(this)["0"].innerText;
					option.selected=true;
					document.getElementById("titulovideo").appendChild(option);
				}
			});
			$("#rutadocs").empty();
			$("#titulodocs").empty();
			$("ul.reorder-documentos-list li").each(function() {
				var option=document.createElement("option");
				option.value=$(this).attr('id');
				option.text=$(this).attr('id');
				option.selected=true;
				document.getElementById("rutadocs").appendChild(option);
				var option=document.createElement("option");
				option.value=$(this)["0"].innerText;
				option.text=$(this)["0"].innerText;
				option.selected=true;
				document.getElementById("titulodocs").appendChild(option);
			});
		});

	});
/**
* Funcion que añade un <li> dentro del <ul>
*/
function add_li()
{
	var nuevoLi=document.getElementById("nuevo_video").value;
	var expreg = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
	if(expreg.test(nuevoLi))
	{
		if(find_li(nuevoLi))
		{
			if (nuevoLi.indexOf("youtube.com") != -1) {
				var videourl = nuevoLi;
				var titulo="";
				videourl =videourl.substr(nuevoLi.indexOf("?v=")+3,11);
				url = 'https://www.googleapis.com/youtube/v3/videos?id='+videourl+'&key=AIzaSyBttQXTDr6P1tJy90K6LMXj7KcVFfr-Gb0&fields=items(id,snippet(title))&part=snippet';
				datos = {};
					//obtener titulo dle vidoe Youtube
					$.getJSON(url, function(datos){
						$.each(datos.items,function(indice){
							titulo=datos.items[indice].snippet.title;
							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)' title='Eliminar Video de la Lista'> <img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}' ></span> &nbsp <a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")' title='Reproducir Video'> &nbsp<a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('/assets/images/play.png')}}'></span></a>";
							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
						});
						document.getElementById(nuevoLi).className = "list-group-item active";
					});
				}
				if (nuevoLi.indexOf("youtu.be/") != -1) {
					var videourl = nuevoLi;
					var titulo="";
					videourl =videourl.substr(nuevoLi.indexOf("be")+3,11);
					url = 'https://www.googleapis.com/youtube/v3/videos?id='+videourl+'&key=AIzaSyBttQXTDr6P1tJy90K6LMXj7KcVFfr-Gb0&fields=items(id,snippet(title))&part=snippet';
					datos = {};
					//obtener titulo del video Youtube
					$.getJSON(url, function(datos){
						$.each(datos.items,function(indice){
							titulo=datos.items[indice].snippet.title;
							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)'> <img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}'></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('assets/images/play.png')}}'></span></a>";
							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
						});
						document.getElementById(nuevoLi).className = "list-group-item active";
					});
				}
				if (nuevoLi.indexOf("vimeo.com") != -1) {
					var videourl = nuevoLi;
					var titulo="";
					videourl =videourl.substr(nuevoLi.indexOf("om")+3,10);
					url = 'https://vimeo.com/api/v2/video/'+videourl+'.json';
					datos = {};
					//obtener titulo del viideo de vimeo
					$.getJSON( url, function( datos ){
						$.each(datos,function(indice){
							titulo=datos[indice].title;
							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)'><img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}' ></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+"</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('assets/images/play.png')}}'></span></a>";
							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
						});
						document.getElementById(nuevoLi).className = "list-group-item active";
					});
				}
				if (nuevoLi.indexOf("facebook.com") != -1) {
					var videourl = nuevoLi;
					var titulo="";
					var inicio,fin=0;
					inicio=nuevoLi.indexOf("pcb.");
					if (inicio != -1) {
						fin=nuevoLi.indexOf("/",inicio+4);
						videourl= videourl.slice(inicio+4,fin);
					}else{
						inicio=nuevoLi.indexOf("os/");
						fin=nuevoLi.indexOf("/",inicio+3);
						videourl =videourl.slice(inicio+3,fin);
					}
					url = 'https://graph.facebook.com/v2.9/'+videourl+'?access_token=EAAJKkknnwgMBAKCeXZBr9eIZCJ0xwzQadWdCaev9YekGIZCSOLZBbYe7lXKuSgEdvChFS3whVEKliMSMxgE2pCcydDCvAdZBYZBLmvI1yfQlUy3OisqS55blaT2qtYqfr4jXJtzHyJxSscEd5e7BHPozi9nQdVJFxwJLh46sn4NgZDZD';
					datos = {};
					//obtener titulo del video
					$.getJSON(url, function(datos){
						$.each(datos,function(indice){
							titulo=datos.description;
							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)'><img  style='height: 25px;' src='{{URL::asset('assets/images/delvideo.png')}}'></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='{{URL::asset('assets/images/play.png')}}' ></span></a>";
							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
							return false;
						});
						document.getElementById(nuevoLi).className = "list-group-item active";
					});
				}
				//Ahora para crear el objeto option que le vas a añadir seria
				var option=document.createElement("option");
				option.value=nuevoLi;
				option.text=nuevoLi;
				option.selected=true;
				document.getElementById("videos").appendChild(option)
			}
		}else{
			alert("Ingrese URL válida");
			document.getElementById("nuevo_video").value="";
			return false;
		}
		document.getElementById("nuevo_video").value="";
	}

	function modificamodal(url){
		reproductor= document.getElementById("reproductor");
		if (url.indexOf("youtube.com") != -1) {
			url=url.substr(url.indexOf("?v=")+3,11);
			reproductor.innerHTML="<iframe width='500' height='400' src='https://www.youtube.com/embed/"+url+"?rel=0' frameborder='0'  encrypted-media' allowfullscreen></iframe>";
		}
		if (url.indexOf("youtu.be/") != -1) {
			url=url.substr(url.indexOf("be")+3,11);
			reproductor.innerHTML="<iframe width='500' height='400' src='https://www.youtube.com/embed/"+url+"?rel=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>";
		}
		if (url.indexOf("vimeo.com") != -1) {
			url=url.substr(url.indexOf("om")+3,10);
			reproductor.innerHTML="<iframe src='https://player.vimeo.com/video/"+url+"' width='500' height='400' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
		}
		if (url.indexOf("facebook.com") != -1 ) {
			var inicio,fin=0;
			inicio=url.indexOf("pcb.");
			if (inicio != -1) {
				fin=url.indexOf("/",inicio+4);
				url= url.slice(inicio+4,fin);
			}else{
				inicio=url.indexOf("os/");
				fin=url.indexOf("/",inicio+3);
				url =url.slice(inicio+3,fin);
			}
			reproductor.innerHTML="<iframe src='https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook%2Fvideos%2F"+url+"%2F&width=500&show_text=false&appId=571198976560471&height=280' width='500' height='280' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true' allowFullScreen='true'></iframe>";
		}
	}
/**
* Funcion que busca si existe ya el <li> dentrol del <ul>
* Devuelve true si no existe.
*/
function find_li(contenido)
{
	if (contenido.indexOf("youtube.com") != -1 ||contenido.indexOf("youtu.be") != -1 || contenido.indexOf("facebook.com") != -1 || contenido.indexOf("vimeo.com") != -1) {
		$("#listaDesordenada li").each(function(){
			if(this.id == contenido){
				return false;
			}
		});
	}else{
		alert("Solo se aceptan videos de Youtube, Facebook y Vimeo");
		return false;
	}
	return true;
}
/**
* Funcion para eliminar los elementos
* Tiene que recibir el elemento pulsado
*/
function eliminar(elemento)
{
	var id=elemento.parentNode.getAttribute("id");
	var option=document.createElement("option");
	option.value=id;
	option.text=id;
	option.selected=true;
	document.getElementById("sobrasvideos").appendChild(option);
	node=document.getElementById(id);
	node.parentNode.removeChild(node);
	$("#videos option[value='"+id+"']").remove();
}
function eliminardoc(elemento)
{
	var id=elemento.parentNode.getAttribute("id");
	node=document.getElementById(id);
	node.parentNode.removeChild(node);
	$("#rutadocs option[value='"+id+"']").remove();
	var option=document.createElement("option");
	option.value=id;
	option.text=id;
	option.selected=true;
	document.getElementById("sobrasdocs").appendChild(option);
}
function eliminarElemento(id,elim){
	imagen = document.getElementById(id);
	elimin=document.getElementById(elim);
	if (!imagen.id){
		alert("El elemento selecionado no existe");
	} else {
		var option=document.createElement("option");
		option.value=imagen.name;
		option.text=imagen.name;
		option.selected=true;
		document.getElementById("sobras").appendChild(option);
		imagen.src="{{URL::asset('assets/images/no-imagen.jpg')}}";
		elimin.style="display:none;";
		imagen.name="nada";
	}
}
</script>