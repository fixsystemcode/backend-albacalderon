<script type="text/javascript">
	$(document).ready(function(){
		$.ajaxSetup( { headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' } } );

		var imagenPrincipalInput = document.getElementById('imagenprincipal');
		var imagenPrincipal = document.getElementById('imgprincipal');

		imagenPrincipalInput.onchange = function () {
			var filesToUpload = imagenPrincipalInput.files; //input de imagen
			var imagen = filesToUpload[0];
			var img = document.createElement("img");
			var reader = new FileReader();
			reader.onload = function(e){
				img.src = e.target.result;
				img.onload = function () {
					var canvas = document.createElement("canvas");
					var ctx = canvas.getContext("2d");
					var MAX_WIDTH = 1000;
					var MAX_HEIGHT = 1000;
					var width = img.width;
					var height = img.height;
					if (width > height) {
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
					} else {
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
					}
					canvas.width = width;
					canvas.height = height;
					ctx.drawImage(img, 0, 0, width, height);
					imagenPrincipal.src = canvas.toDataURL("image/png");
				}
			}
			reader.readAsDataURL(imagen);
		}


		//Galeria
		$("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
		$("ul.reorder-documentos-list").sortable({ tolerance: 'pointer' });
		$("ul.reorder-videos-list").sortable({ tolerance: 'pointer' });
		$('#reorder-helper').slideDown('slow');
		$('.image_link').attr("href","javascript:void(0);");
		$('.image_link').css("cursor","move");

		var urlToBlobPng = function(dataurl){
			var blobBin = atob(dataurl.split(',')[1]);
			var array = [];
			for(var i = 0; i < blobBin.length; i++) {
				array.push(blobBin.charCodeAt(i));
			}
			return new Blob([new Uint8Array(array)], {type: 'image/png'});
		}

		$('.inputimg').on('change', function(e){
			var dataurl;
			var filesToUpload = this.files;
			var foto = filesToUpload[0];
			var img = document.createElement("img");
			var num;
			var n_actual = $(this).data('n');
			var readableImagen = new FileReader();
			readableImagen.onload = function(e){
				img.src = e.target.result;
				img.onload = function() {
					var canvas = document.createElement("canvas");
					var ctx = canvas.getContext("2d");
					var MAX_WIDTH = 1000;
					var MAX_HEIGHT = 1000;
					var width = img.width;
					var height = img.height;
					if (width > height) {
						if (width > MAX_WIDTH) {
							height *= MAX_WIDTH / width;
							width = MAX_WIDTH;
						}
					} else {
						if (height > MAX_HEIGHT) {
							width *= MAX_HEIGHT / height;
							height = MAX_HEIGHT;
						}
					}
					canvas.width = width;
					canvas.height = height;
					ctx.drawImage(img, 0, 0, width, height);
					dataurl = canvas.toDataURL("image/png");
					var cadena = foto.name.replace(" ","");
					num = Math.floor(Math.random() * 10000001);
					num+=cadena;
					document.getElementById('imgfile-'+n_actual).src=dataurl;
					document.getElementById('imgfile-'+n_actual).name=num;
					document.getElementById('elim'+n_actual).style="";
					var formData = new FormData();
					formData.append("image", urlToBlobPng(dataurl) ,num);
					formData.append('nombre', num);
					@if (isset($producto->id))
						formData.append('idproducto', '{{$producto->id}}');
					@endif
					$.ajax({
						url: "{{ URL::to('/upfoto') }}",
						method: "POST",
						data: formData,
						processData: false,
						contentType: false,
						headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
						success: function(datos){},
						error: function(j){
							alert(j.responseText);
						}
					});
				}
			}
			readableImagen.readAsDataURL(foto);
		});

		$("#my-dropzone").on('submit', function(evt){
			$("#ruta").empty();
			$("ul.reorder-photos-list li a label img").each(function() {
				if ($(this).attr('name')!=undefined) {
					if ($(this).attr('name')!="nada")   {
						var option=document.createElement("option");
						option.value=$(this).attr('name');
						option.text=$(this).attr('name');
						option.selected=true;
						document.getElementById("ruta").appendChild(option)
					}
				}
			});
		});
	})

function eliminarElemento(id,elim){
	imagen = document.getElementById(id);
	elimin=document.getElementById(elim);
	if (!imagen.id){
		alert("El elemento selecionado no existe");
	} else {
		var option=document.createElement("option");
		option.value=imagen.name;
		option.text=imagen.name;
		option.selected=true;
		// console.log(option);
		document.getElementById("sobras").appendChild(option);
		imagen.src="{{URL::asset('assets/images/no-imagen.jpg')}}";
		elimin.style="display:none;";
		imagen.name="nada";
	}
}
</script>