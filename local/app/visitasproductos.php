<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class visitasproductos extends Model
{
    //
  	protected $table = 'visitas_productos';

	// protected $fillable = ['titulo', 'path'];

  	public function visitaProducto()
    {
        return $this->belongsTo('App\tienda', 'tienda_id');
    }
	protected $hidden = [];
}
