<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class ClienteModel extends Authenticatable
{
	use Notifiable;

    protected $table = 'clientes';

    protected $fillable = [
        'nombre', 'apellido', 'correo', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pedidos(){
    	return $this->hasMany('App\PedidoModel', 'id_cliente', 'id');
    }

    public function direcciones(){
        return $this->hasMany('App\DireccionModel', 'id_cliente', 'id');
    }

    public function vehiculos(){
        return $this->hasMany('App\VehiculoModel', 'id_cliente', 'id');
    }

    public function rollApiKey($renew = false){
        if(!empty($this->api_token) && !$renew) return;
        do{
            $this->api_token = str_random(60);
        }while($this->where('api_token', $this->api_token)->exists());
        $this->save();
    }
}
