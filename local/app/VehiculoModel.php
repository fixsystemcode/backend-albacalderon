<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiculoModel extends Model
{
    protected $table = 'vehiculos';
}
