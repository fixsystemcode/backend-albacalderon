<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoModel extends Model
{
    protected $table = 'pedido';

    public function detalle()
    {
        return $this->hasMany('App\DetallePedidoModel', 'id_pedido');
    }
    public function pedidoCliente()
    {
        return $this->hasOne('App\ClienteModel', 'id', 'id_cliente');
    }
    public function direccion()
    {
    	return $this->hasOne('App\DireccionModel', 'id', 'id_direccion');
    }
}
