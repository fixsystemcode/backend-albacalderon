<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tienda extends Model
{
    //
    protected $table = 'tienda';

  	protected $fillable = ['nombre', 'abreviatura', 'slug', 'precio','preciodescuento', 'stock', 'peso', 'medidas', 'presentacion', 'temporada', 'campostemporada', 'destacado',  'estado',  'path', 'descripcion', 'id_usuario', 'id_categoriaproducto'];

  	protected $hidden = [];

  	public function categoriaproducto()
    {
        return $this->belongsTo('App\categoriaproducto');
    }
    public function usuario()
    {
        return $this->belongsTo('App\User');
    }
    public function visitas()
    {
      return $this->hasMany(visitasproductos::class);
    }
}
