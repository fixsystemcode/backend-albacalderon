<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
     protected $table = 'videos';

    protected $fillable = ['titulo', 'path','descripcion','orden','id_categoriainterna'];

 	protected $hidden = [];
}
