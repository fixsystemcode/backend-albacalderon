<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    //
    protected $table = 'imagenes';

    protected $fillable = ['titulo', 'path','descripcion','orden', 'id_categoriainterna'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}
