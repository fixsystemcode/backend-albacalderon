<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuloModel extends Model
{
    protected $table = 'modulos';

    public function curso(){
    	return $this->belongsTo('App\Articulo', 'id_articulo', 'id');
    }
}
