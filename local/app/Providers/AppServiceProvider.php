<?php

namespace App\Providers;
use Auth;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view){
            if (Auth::check()) {
                list($puede_leer, $puede_crear, $puede_editar, $puede_eliminar) = \Sistema::obtener_permisos_ruta();
                view()->share('puede_leer', $puede_leer);
                view()->share('puede_crear', $puede_crear);
                view()->share('puede_editar', $puede_editar);
                view()->share('puede_eliminar', $puede_eliminar);
            }else {
                view()->share('puede_leer', 0);
                view()->share('puede_crear', 0);
                view()->share('puede_editar', 0);
                view()->share('puede_eliminar', 0);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.administracion', function() {
            return base_path() . '/administracion';
        });
    }
}
