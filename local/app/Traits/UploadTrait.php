<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $filename = null)
    {
    	$dir = public_path() . $folder;
        $name = !is_null($filename) ? $filename : str_random(25);
        $ext = $uploadedFile->getClientOriginalExtension();
        $name = $name . ( empty($ext) ? '' : ('.'.$ext) );
        $uploadedFile->move($dir, $name);
        // $estatus = $uploadedFile->storeAs($folder, $name, $disk);
        return $name;
    }

}