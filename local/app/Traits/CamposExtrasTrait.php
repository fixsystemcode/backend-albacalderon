<?php

namespace App\Traits;

use App\CamposExtras;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

trait CamposExtrasTrait
{

    private function obtenerCamposExtras($camposGuardados=[]){
        $tabla = DB::table("camposextras")
            ->join("camposextrasgrupo","camposextrasgrupo.id","=","camposextras.id_campos_extras_grupo")
            ->select("camposextras.*",
            "camposextrasgrupo.id as id_grupo",
            "camposextrasgrupo.descripcion as grupo")
            ->where('camposextrasgrupo.visible', 'si')
            ->orderBy('id','ASC')
            ->get();

        $gruposCampos = [];

        foreach ($tabla as $i => $campo) {
            if(!isset($gruposCampos[$campo->id_grupo])){
                $gruposCampos[$campo->id_grupo] = array(
                    'id' => $campo->id_grupo,
                    'descripcion' => $campo->grupo,
                    'campos' => [],
                );
            }
            $gruposCampos[$campo->id_grupo]['campos'][] = array(
                'id' => $campo->id,
                'nombre' => $campo->nombre,
                'descripcion' => $campo->descripcion,
                'tipo' => $campo->tipo,
                'valor' => $campo->valor,
                'guardado' => isset($camposGuardados[$campo->id]) ? $camposGuardados[$campo->id] : NULL,
            );           
        }
        return $gruposCampos;
    }
    private function ObtenerJsonCamposExtrasRecursos($id_grupo){
        $tabla = CamposExtras::orderBy('orden','ASC')->where('estado',1)->where('id_campos_extras_grupo', $id_grupo)->get();        
        $camposextras = [];
        foreach($tabla as $key => $value){
            $valor_campo = Input::get(prefijo_campos_extra($value->nombre) . $key);
            $camposextras[$value->id] = $valor_campo;
        }            
        $camposextras = json_encode($camposextras);
        return $camposextras;
    }
}