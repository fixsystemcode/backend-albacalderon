<?php  
namespace App\Clases;
use DB;

//Modelos
use App\categoriaproductos;
use App\Informacion;
use App\PedidoModel;
use App\DetallePedidoModel;
use App\ClienteModel;
use App\tienda;
use App\CuponModel;
use App\ConfiguracionModel;
use App\CarritoModel;
use App\User;

class Sistema{
	
	public static function obtener_permisos_ruta($url=''){
		$PERFIL = auth()->user()->id_tipo_usuario;
		if($PERFIL == 4) return [1,1,1,1];
		else{
			$url = empty($url) ? request()->path() : $url;
			$url = explode("/", $url);
			$url_p = $url[0];
            if (isset($url[1]) && !is_numeric($url[1]) && $url[1] != "create") {
               $url_p.='/'.$url[1];
            }
            //dd($url_p);
            $url = $url_p;

			$permisos = DB::table('menu as m')
			->join('usuario_menu_permiso as p','m.id','=','p.menu_id')
			->select('p.crear', 'p.editar', 'p.eliminar')
			->where('m.ruta','like', $url.'%')
			->where('p.tipo_usuario_id', $PERFIL)
			->first();

			if(!is_null($permisos)) return [1, $permisos->crear, $permisos->editar, $permisos->eliminar];
			return [0,0,0,0];
		}
	}
	public static function renderiza_menu(){
     	$perfil_actual = auth()->user()->id_tipo_usuario;
     	$query = DB::table('menu as m')
			->select('m.nombre', 'm.ruta', 'm.icono', 'm.padre', 'm.id')
			->where('m.visible', 1)
			->orderBy('m.orden');
		if($perfil_actual != 4) $query->join('usuario_menu_permiso as p','m.id','=','p.menu_id')->where('p.tipo_usuario_id', $perfil_actual);
		$permisos = $query->get();

        $texto = '';
        if(empty($permisos)) return $texto;
        $tree = buildTree($permisos, 'padre', 'id');
        foreach ($tree as $k => $menu){
        	$has_child = isset($menu->children);
        	$texto.= '<li class="nav-item'.($has_child ? ' nav-dropdown' : '').'">';
            $texto.= '<a href="'.url($menu->ruta).'" class="nav-link'.($has_child ? ' nav-dropdown-toggle' : '').'">';
            $texto.= '<i class="'.$menu->icono.'"></i> <span class="nav-label">'.$menu->nombre.'<span></a>';
            if($has_child){
                $texto.= '<ul class="nav nav-second-level">';       
                foreach ($menu->children as $k1 => $menu1) {
                    $texto.= '<li class="nav-item">';
                    $texto.= '<a href="'.url($menu1->ruta).'" class="nav-link"><i class="nav-icon '.$menu1->icono.'"></i> '.$menu1->nombre.'</a>';
                    $texto.= '</li>';
                }
                $texto.= '</ul>';  
            }
            $texto.= '</li>';
        }
        return $texto;
    }

   public static function renderiza_menu_cliente(){
        $query = DB::table('menu_pagina as m')
            ->select('m.nombre', 'm.ruta', 'm.icono', 'm.padre', 'm.id')
            ->where('m.visible', 1)
            ->orderBy('m.orden')
            ->get();
        $texto = '';
        foreach ($query as $k => $menu){
            $texto.= '<a href="'.url($menu->ruta).'" class="btn btn-info btn-block mb-2">';
            $texto.= '<i class="'.$menu->icono.'"></i><span class="nav-label"> '.$menu->nombre.'</span></a>';
        }
        return $texto;
    }

    public static function getCategoria(){
        
        return categoriaproductos::all();

    }

    public static function getInfo(){
        return Informacion::orderBy('id', 'DESC')->first();
    }

    public static function misPedidos($id_cliente=null, $estado=null){
        $pedidos = PedidoModel::with('detalle');
        if(!is_null($id_cliente)) $pedidos->where('id_cliente', $id_cliente);
        if(!is_null($estado)) $pedidos->where('estado', $estado);
        $misPedidos = $pedidos->orderBy('id', 'DESC')->paginate(5);
        return $misPedidos;

    }

    public static function getTime($id=null){
        $time = ['1' => 'Minutos', '2' => 'Horas', '3' => 'Dias'];
        if( $id == null) return $time;
        
        return $time[$id];
    }

    public static function getLimiteCupon($codigo){
        // 1 = no existe o esta inactivo, 2 = ya no hay limite, 3 = ya expiro o no ha comenzado
        $cupon = CuponModel::where('codigo', $codigo)->where('estado', 1)->first();
        if(is_null($cupon)) return 1;
        $uso = PedidoModel::where('cupon', $codigo)->count();
        $fechaAct = date('Y-m-d');
        $fecha_inicio = date('Y-m-d', strtotime($cupon->fecha_inicio));
        $fecha_fin = date('Y-m-d', strtotime($cupon->fecha_fin));
        if ($cupon->limite > $uso) {
            if(($fechaAct >= $fecha_inicio) && ($fechaAct <= $fecha_fin) ){
                return 4;
            }else{
                return 3;
            }
        }else{
            return 2;
        }
    }

    public static function getDescuentoCupo($cupon){
        $cupon = CuponModel::where('codigo', $cupon)->where('estado', 1)->first();
        return $cupon->descuento;
    }

    public static function getCorreoEmpresa(){
        
        return Informacion::orderBy('id', 'DESC')->first()->email;
    
    }

    public static function getConfig($clave, $valor_default=NULL){
        $variable = ConfiguracionModel::where('clave', $clave)->first();
        return !is_null($variable) ? $variable->valor : $valor_default;
    }

    public static function getAppCode(){
        $code = CarritoModel::orderBy('id', 'DESC')->first();
        return $code->app_code;
    }

    public static function informacionCompleta($id_cliente){
        $cliente = ClienteModel::find($id_cliente);
        if(is_null($cliente->nombre) || is_null($cliente->apellido) || is_null($cliente->segundo_nombre) || is_null($cliente->correo) || is_null($cliente->ci_ruc)|| is_null($cliente->telefono)){
            return false;
        }else{
            return true;
        }
    }

    public static function getUsuarios(){
        $usuarios = User::select('id', DB::raw('concat(nombres, " " ,apellidos) as nombres'))->pluck('nombres', 'id');
        return $usuarios;
    }
}

?>