<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'login' => $this->correo,
            'nombre' => $this->nombre,
            'segundo_nombre' => $this->segundo_nombre,
            'apellido' => $this->apellido, 
            'ci_ruc' => $this->ci_ruc,
            'correo' => $this->correo,
            'telefono' => $this->telefono, 
            'direccion' => $this->direccion,
            'celular' => $this->celular,
            'estado_login' => $this->estado_login
        ];
    }
}
