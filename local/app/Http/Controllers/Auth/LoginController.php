<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use View;
use Session;
use Redirect;
use Facebook;
use TwitterAPIExchange;
use Google_Service_Oauth2;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/administracion';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate()
    {
       // return Redirect::to('administracion');
        // dd('sds');
        if(Auth::attempt(['email'=>$request['email'], 'password'=>$request['password']])){
            $idx;
            $confirmx;
            $persona2 =User::where("email","=",$request['email'])->first();
        //dd($persona2);

            if ($persona2->confirmado==1) {
                if ($persona2->aceptado == 0)
                {
                    Session::flash('message-error','Su cuenta esta en proceso de revisión y activación, este proceso puede tardar un período máximo de 24 horas. ');


                    return Redirect::to('login');
                }else
                {
                    if ($persona2->id_tipo_usuario == 5) {
                        return Redirect::to('registro');
                    }else{

                        return Redirect::to('administracion');


                    }
                }
            }else{
                Session::flash('message-error','confirme su correo electronico');
                return Redirect::to('login');
            }
        }else{
            $cedulaconsult = User::where('email',$request->email)->first();
            if(!is_null($cedulaconsult))
            {
                Session::flash('message-error','Nombre de usuario y/o contraseña incorrectos.');
                return Redirect::to('login');
            }else
            {
                Session::flash('message-error','Usuario No Existe');
                return Redirect::to('login');
            }
        }


    }

    public function logout(){

        $acep = Auth::user();
        Session::flush();
        Auth::logout();

        if (isset($acep)) {
            if ($acep->id_tipo_usuario == 5) {
                return Redirect::to('/');
            }else{
                return Redirect::to('login');
            }
        }
        else{
            return Redirect::to('login');
        }


    }
    ////FB


    function iniciafb(){

        //inicia sesion y obtiene datos personales
        $fb = new Facebook\Facebook([
            'app_id' => '644942119027203',
            'app_secret' => 'b182c4c6bae44eaab77ca5bf7f65ceb3',
            'default_graph_version' => 'v2.2',
        ]);
        //obtiene los permisos
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email','user_location','user_birthday','user_hometown','public_profile','publish_actions']; // Optional permissions


        //$loginUrl = $helper->getLoginUrl('http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/fblog', $permissions);
        $loginUrl = $helper->getLoginUrl('http://'.$_SERVER['SERVER_NAME'].'/fblog', $permissions);
        echo '<script type="text/javascript">window.location="'.$loginUrl.'";</script>';





    }
    function fblog(){
        $fb = new Facebook\Facebook([
            'app_id' => '644942119027203',
            'app_secret' => 'b182c4c6bae44eaab77ca5bf7f65ceb3',
            'default_graph_version' => 'v2.2',
        ]);
        $helper = $fb->getRedirectLoginHelper(); if (isset($_GET['state'])) { $helper->getPersistentDataHandler()->set('state', $_GET['state']); }

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error unos: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // Logged in

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        //echo '<h3>Metadata</h3>';
        //var_dump($tokenMetadata);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('644942119027203'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                exit;
            }

            //echo '<h3>Long-lived</h3>';
            //var_dump($accessToken->getValue());
        }

        $_SESSION['fb_access_token'] = (string) $accessToken;

        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        try {
        // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,email,first_name,last_name,location,birthday,picture.width(400).height(400),hometown,link,gender', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $user = $response->getGraphUser();/*
        echo '<br> ID: ' . $user->getId();
        echo '<br> Email: ' . $user->getEmail();
        echo '<br> Name: ' . $user->getName();
        echo '<br> primer nombre: ' . $user->getFirstName();
        echo '<br> Segundo nombre: ' . $user->getLastName();
        echo '<br> ciudad: '  . $user->getLocation()->getName();
        echo '<br> Link : ' . $user->getLink();
        echo '<br> foto: ' . $user->getPicture()->getUrl();
        echo '<br> Genero: ' . $user->getGender();
        echo '<br> ciudad Nacimiento: '  . $user->getHometown()->getName();
        echo '<br> Cumpleaños: ' . date_format($user->getBirthday(), 'd-m-Y');
        */
        $count = User::where('email', $user->getEmail())->count();
        if ($count>=1) {
            User::where('email', $user->getEmail())
            ->update(['nombres' => $user->getFirstName(),
                'apellidos' => $user->getLastName(),
                //'fecha_nacimiento' => date_format($user->getBirthday(), 'Y-m-d'),
                'path'=>$user->getPicture()->getUrl()
            ]);

        }else{
            $usuario = new User;
            $usuario->nombres=$user->getFirstName();
            $usuario->apellidos=$user->getLastName();
            $usuario->email=$user->getEmail();
            $usuario->path=$user->getPicture()->getUrl();
            //$usuario->fecha_nacimiento=date_format($user->getBirthday(), 'Y-m-d');
            $usuario->id_tipo_usuario=5;
            $usuario->confirmado=1;
            $usuario->save();

        }
            //login con ID
        $usuario = User::where('email', '=', $user->getEmail())->first();

        if(Auth::loginUsingId($usuario->id)){

            if ($usuario->aceptado == 0)
            {


                Session::flash('message-error','Su cuenta esta en proceso de revisión y activación, este proceso puede tardar un período máximo de 24 horas. ');


                return Redirect::to('login');
            }else
            {

                if ($usuario->id_tipo_usuario == 5) {
                    return Redirect::to('registro');
                }else{
                    return Redirect::to('administracion');

                }


            }

        }
    }


    //publica FB



    public function publicar(){
        //app la ciudad credenciales
        $fb = new Facebook\Facebook([
            'app_id' => '644942119027203',
            'app_secret' => 'b182c4c6bae44eaab77ca5bf7f65ceb3',
            'default_graph_version' => 'v2.2',
        ]);
        //datos de publicacion
        $linkData = [
            'link' => 'http://laciudad.ec/huecas/los-corviches-de-tarqui',
            'message' => 'Corviches neww',
            'value' => 'EVERYONE ',
        ];

        try {

            //$helper = $this->api->getRedirectLoginHelper();
            $helper = $fb->getRedirectLoginHelper();
            if (isset($_GET['state'])) {
                $helper->getPersistentDataHandler()->set('state', $_GET['state']);
            }
            //token generado en FB
            $accessToken = "EAAIHgKAYDVcBABFLlvMZB8X1JNmjqzCQeNy6iujCQTuI2UjRgfTSDFwt53spOwWuioqlwuGEcydWZBmf4ZCLCkpbkQy9mn9KMKmZCZB3nxPaDNSQz87BRAJPUtuBsfFGGPsptK7seIHyFbApNdsZAQ8ioiESKtQ2z97OfNBkDQ3gZDZD";
            //$accessToken = $helper->getAccessToken();


            echo 'Token: '. $accessToken;
            echo'<br>';

            // Returns a `Facebook\FacebookResponse` object
            //id de la pagina a publicar
            $response = $fb->post('/870146743130480/feed', $linkData, $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

        //echo 'Posted with id: ' . $graphNode['id'];
    }


    //publica twitter


    function sendTweets() {

        $url = "https://api.twitter.com/1.1/statuses/update.json";
        $requestMethod = 'POST';

        // configuracion de la cuenta

        $settings = array(
            'oauth_access_token' => '827526089194995712-3NdWhfdrIh8f8zA7OsZFZk670yXpXh9',
            'oauth_access_token_secret' => 'nqDfjFvU1gT7YglQ1dfw8JEIkRFKzsArGUoE9ZhhuEl3Z',
            'consumer_key' => 'tp2K5Xm0X6vdMNgfFHZA9cRYD',
            'consumer_secret' => '9wkYe7lGSL1u9LVhu0YWQj4yU918SlfY61Ko6rGZFBuRb3SKEZ',
        );

        // establecer el mensaje
        $postfields = array('status' => 'La Ciudad -> http://laciudad.ec/eventos/sol-mar-arena-y-el-chill-out-festival');
        // crea la coneccion con Twitter
        $twitter = new TwitterAPIExchange($settings);


        // envia el tweet
        $twitter->buildOauth($url, $requestMethod)
        ->setPostfields($postfields)
        ->performRequest();





    }


/*
//login Google
    function iniciagoogle(){
        $client;
        $client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
$oauthService = new Google_Service_Oauth2($client);
$userInfo = $oauthService->userinfo_v2_me->get();
echo "User info:<br>Name: ".$userInfo->name
  ."<br>givenName: ".$userInfo->givenName
  ."<br>familyName: ".$userInfo->familyName
  ."<br>email: ".$userInfo->email;
    }


     */


}
