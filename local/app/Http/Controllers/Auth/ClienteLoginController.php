<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Socialite;
use App\ClienteModel;

class ClienteLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';



       public function __construct()
       {
           $this->middleware('guest:cliente')->except('logout');
       }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('clientes.auth.login');
    }


    public function loginCLiente(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'correo'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      // Attempt to log the user in
      if (Auth::guard('cliente')->attempt(['correo' => $request->correo, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('inicio'));
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('correo', 'remember'));
    }

    public function logout()
    {
        Auth::guard('cliente')->logout();
        return redirect()->route('inicio');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            return redirect('cliente/login');
        }
        // only allow people with @company.com to login
        // if(explode("@", $user->email)[1] !== 'gmail.com'){
        //     Session::flash('message', "No esta utilizando una cuenta de gmail");
        //     return redirect()->to('/');
        // }
        // check if they're an existing user
        $existingUser = ClienteModel::where('correo', $user->email)->first();
        if($existingUser){
            // log them in
            auth('cliente')->login($existingUser, true);
        } else {
            $nombre = separarName($user->name);
            // create a new user
            $newUser              = new ClienteModel;
            $newUser->nombre      = $nombre["first"];
            $newUser->segundo_nombre = $nombre["second"];
            $newUser->apellido    = $nombre["last"];
            $newUser->correo      = $user->email;
            $newUser->provider_id = $user->id;
            $newUser->provider    = $provider;
            // $newUser->avatar          = $user->avatar;
            // $newUser->avatar_original = $user->avatar_original;
            $newUser->save();
            auth('cliente')->login($newUser, true);
        }
        return redirect()->to('/');
    }
}