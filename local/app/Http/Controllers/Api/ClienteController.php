<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\ClienteModel;
use App\PedidoModel;
use App\WishlistModel;
use App\DireccionModel;
use App\VehiculoModel;
use Auth;
use Session;
use Redirect;
use DB;

class ClienteController extends Controller
{
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function misPedidos($id_cliente){
        $misPedidos = PedidoModel::with('detalle.detalleProducto')->where('id_cliente', $id_cliente)->get();
        return response()->json(['status' => 'ok','misPedidos' => $misPedidos], 200);
    }

    public function ofertas(){
        
        return view('clientes.ofertas');

    }

    public function deseados(){
        $info = \Sistema::getInfo();
        $cliente = Auth::guard('cliente')->user();
        $wishlist = WishlistModel::with('wishlistProducto')->where('id_cliente', $cliente->id)->paginate(3);
        return view('clientes.deseados', compact ('wishlist', 'info'));

    }

    public function miPerfil($id){
        $cliente = ClienteModel::with('vehiculos')->where('id', $id)->first();
        //$vehiculo = VehiculoModel::where('id_cliente', $id)->first();
        return response()->json(['status' => 'ok','cliente' => $cliente], 200);
    }
    public function guardarPerfil(Request $request){
        $cliente = ClienteModel::find($request->get('id_cliente'));
        $cliente->ci_ruc      = $request->get('ci_ruc');
        $cliente->tipo_identificacion = $request->get('tipo_identificacion');
        $cliente->nombre      = $request->get('nombre');
        $cliente->segundo_nombre = $request->get('segundo_nombre');
        $cliente->apellido    = $request->get('apellido');
        $cliente->correo      = $request->get('correo');
        $cliente->telefono    = $request->get('telefono');
        $cliente->password    = $request->get('password');
        $cliente->celular     = $request->get('celular'); 
        $cliente->direccion   = $request->get('direccion');
        $cliente->estado_login  = 0; //Estado para registro de formulario
        if ($request->get('path_cedula') != null) {
            $cliente->path_cedula = $request->get('path_cedula');
        }
        $cliente->save();

        /*$vehiculo = VehiculoModel::where('id_cliente', $request->get('id_cliente'))->first();
        if(is_null($vehiculo)) $vehiculo =  new VehiculoModel;
        $vehiculo->marca      = $request->get('marca');
        $vehiculo->modelo     = $request->get('modelo');
        $vehiculo->placa      = $request->get('placa');
        $vehiculo->anio       = $request->get('anio');
        $vehiculo->tipo       = $request->get('tipo');
        $vehiculo->color      = $request->get('color');
        $vehiculo->id_cliente = $cliente->id; 
        $vehiculo->save();*/
        return response()->json(['status' => 'ok','cliente' => $cliente], 200);

    }

    public function guardarVehiculo(Request $request){
        // return response()->json(['status' => 'ok','vehiculo' => $request->all()], 200);
        if(!is_null($request->id_vehiculo) && !empty($request->id_vehiculo)){
            $vehiculo = VehiculoModel::find($request->id_vehiculo);
            $vehiculo->marca      = $request->get('marca');
            $vehiculo->modelo     = $request->get('modelo');
            $vehiculo->placa      = $request->get('placa');
            $vehiculo->anio       = $request->get('anio');
            $vehiculo->tipo       = $request->get('tipo');
            $vehiculo->color      = $request->get('color');
            $vehiculo->id_cliente = $request->get('id_cliente');
            if ($request->get('path_matricula') != null) {
                $vehiculo->path_matricula = $request->get('path_matricula');
            }
        }else{
            $vehiculo = new VehiculoModel;
            $vehiculo->marca      = $request->get('marca');
            $vehiculo->modelo     = $request->get('modelo');
            $vehiculo->placa      = $request->get('placa');
            $vehiculo->anio       = $request->get('anio');
            $vehiculo->tipo       = $request->get('tipo');
            $vehiculo->color      = $request->get('color');
            $vehiculo->id_cliente = $request->get('id_cliente');
            $vehiculo->path_matricula = $request->get('path_matricula');
        }
            $vehiculo->save();

            $vehiculo = VehiculoModel::where('id_cliente', $request->id_cliente)->get();
        return response()->json(['status' => 'ok','vehiculo' => $vehiculo], 200);

    }

    public function mostrarVehiculo($id){
        $vehiculo = VehiculoModel::find($id);
        return response()->json(['status' => 'ok','vehiculo' => $vehiculo], 200);
    }

    public function eliminarVehiculo(Request $request, $id){
        $vehiculo = VehiculoModel::find($id);
        $vehiculo->delete();
        $vehiculos = VehiculoModel::where('id_cliente', $request->id_cliente)->get();
        return response()->json(['status' => 'ok','vehiculos' => $vehiculos], 200);

    }

    public function cancelarPedido(request $request)
    {
        $pedido = PedidoModel::find($request->id);
        if($pedido->estado == 1){
            $pedido->delete();
            return response()->json(['status' => 'ok','respuesta' => 'eliminado correctamente'], 200); 
        }else{
            return response()->json(['status' => 'ok','respuesta' => 'No se puede eliminar'], 200);
        }
        
    }

    public function saldoPedido($id)
    {
        $saldo = PedidoModel::select(
            DB::raw('SUM(total) as total')
        )->where('id_cliente', $id)
        ->where('estado', 3)
        ->first();
        if ($saldo->total == null || $saldo->total == '') {
            $saldo->total = 0;
        }
        return response()->json(['status' => 'ok','saldo' => $saldo], 200);
    }
    
}
