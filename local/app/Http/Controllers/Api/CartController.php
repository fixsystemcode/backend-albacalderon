<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;

use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\tienda;
use App\CamposExtras;
use App\CarritoModel;
use App\Informacion;
use App\Slider;
use App\categoriaproductos;
use App\ClienteModel;
use App\PedidoModel;
use App\AdjuntoModel;
use App\DepositoModel;
use App\DireccionModel;
use Cart;
use Auth;
use App\DetallePedidoModel;


use Validator; 

class CartController extends Controller
{

    public function miCarrito()
    {
        $carrito = CarritoModel::with('politica')->first();
        if(is_null($carrito))  return response()->json(['status' => 'ok','No existe información del carrito de comprar' => 'vacio'], 200);      
        return response()->json(['status' => 'ok','carrito' => $carrito], 200);

    }
    public function saveCard(Request $request)
    {
        $productos = $request->productos;
        $cliente = $request->cliente;
        $carrito = CarritoModel::with('politica')->first();
        $porc_desc = ($carrito->descuento / 100);
        $porc_iva = ($carrito->iva / 100);
        $sub_total_12 = 0;
        $sub_total_0 = 0;
        $descuento = 0;
        $sum_desc = 0;
        foreach ($productos as $key => $value) {
            $producto = Tienda::find($value['id']);
                $descuento = ($producto->precio * $value['cantidad']) * $porc_desc;
                $sum_desc = $sum_desc + $descuento;
            if ($producto->iva == 1) {
                $sub_total_12 = $sub_total_12 + (($producto->precio * $value['cantidad']) - $descuento);
            }else{
                $sub_total_0 = $sub_total_0 + (($producto->precio * $value['cantidad']) - $descuento); 
            }
        }
        // dd("subtotal0 = " . $sub_total_0 . " subtotal12 = ". $sub_total_12);
        $iva = round(($sub_total_12) * $porc_iva , 2);
        DB::beginTransaction();

        try {
            $pedido = new PedidoModel();
            $pedido->estado = 1;
            $pedido->id_cliente = $cliente;
            $pedido->subtotal = $sub_total_0;
            $pedido->subtotal_12 = $sub_total_12;
            $pedido->descuento = $sum_desc;
            $pedido->iva = $iva;
            $pedido->total = $sub_total_0 + $sub_total_12 + $iva;
            $pedido->save();  

            foreach ($productos as $key => $value) {
                $detalle = new DetallePedidoModel();
                $detalle->id_pedido = $pedido->id;
                $detalle->id_producto = $value['id'];
                $detalle->cantidad = $value['cantidad'];
                $detalle->precio = $value['precio'];
                $detalle->precio_con_desc = $value['precio'] - ($value['precio'] * $porc_desc);
                $detalle->save();
            }   
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'ok','msj' => 'No se ha popido guardar tu pedido'], 200);
        }
        return response()->json(['status' => 'ok','msj' => $pedido], 200);
        
    }

    public function formPago($id, $id_cliente)
    {
        $cliente = ClienteModel::find($id_cliente);
        $pedido =  PedidoModel::with('direccion')->find($id);
        $tarifa_envio = 0;
        $productos =  DetallePedidoModel::with('detalleProducto')->where('id_pedido', $pedido->id)->get();
        $carrito = CarritoModel::with('politica')->first();
        if(!is_null($pedido->tarifa_envio)) $tarifa_envio = $pedido->tarifa_envio;
        $total = round($pedido->subtotal - $pedido->descuento - $pedido->descuento_cupon + $pedido->iva + $tarifa_envio, 2);
        $direcciones = DireccionModel::select(DB::raw('CONCAT(ciudad, " - ", direccion) AS full_direccion'), 'id')->where('id_cliente', $id_cliente)->orderBy('id', 'Desc')->pluck('full_direccion', 'id');
        $base_path = url('/local/public/images/');
        return response()->json(['status' => 'ok','productos' => $productos, 'carrito' => $carrito, 'cliente' => $cliente, 'total' => $total, 'pedido' => $pedido, 'direcciones' => $direcciones, 'base_path' => $base_path], 200);
    }
    public function formSave(Request $request)
    { 
        $this->validate($request, [
            'metodo_pago' => 'required',
            'metodo_entrega' => 'required'
        ]);
        $carrito = CarritoModel::first();
        $pedido = PedidoModel::find($request->id_pedido);
        if($request->metodo_entrega == 'domicilio'){
            if($carrito->tipo_entrega == 'fija'){
                $tarifa_envio = $carrito->tarifa_envio;  
            }else{
                 $tarifa_envio = $request->tarifa_envio;  
            } 

        }else{
            $tarifa_envio = $carrito->tarifa_local;
        }
        $total = round($pedido->total + $tarifa_envio , 2);

        
        $pedido->tarifa_envio  = $tarifa_envio;
        $pedido->metodo_pago  = $request->metodo_pago;
        $pedido->metodo_entrega  = $request->metodo_entrega;
        $pedido->total = $total;
        $pedido->save();  

        return response()->json(['status' => 'ok', 'pedido' => $pedido], 200);

    }
    public function prepararPago($id)
    {
        //return response()->json(['status' => 'ok', 'msj' => $id], 200);
        $carrito = CarritoModel::select('iva', 'descuento')->orderBy('id', 'DESC')->first();
        $pedido = PedidoModel::with('detalle.detalleProducto')
        ->with('pedidoCliente')
        ->with('direccion')
        ->find($id);
        $base_path = url('/local/public/images/');
        if(is_null($pedido->metodo_pago) || is_null($pedido->metodo_entrega)){
            return response()->json(['status' => 'ok', 'msj' => 'No existe metodo de entrega o metodo de pago'], 200);
        } 
        $cliente = ClienteModel::find($pedido->id_cliente);
        $productos = array();
        $subtotal = 0;
        $sum_subtotal_0 = 0;
        $sum_subtotal_12 = 0;
        $tax = 0;
        $sum_tax = 0;
        $iva = round($carrito->iva / 100, 2);
        foreach ($pedido->detalle as $key => $detalle) {
            if ($detalle->detalleProducto->iva == 1) {
                $subtotal = $detalle->precio_con_desc * $detalle->cantidad;
                $sum_subtotal_12 = $sum_subtotal_12 + $subtotal;
                $tax = $subtotal * $iva;
                $sum_tax = $tax + $sum_tax;
            }else{
                $subtotal = $detalle->precio_con_desc * $detalle->cantidad;
                $sum_subtotal_0 = $sum_subtotal_0 + $subtotal; 
                $tax = 0;
            }
            $productos[$key]['id'] = $detalle->detalleProducto->id;
            $productos[$key]['nombre'] = $detalle->detalleProducto->nombre;
            $productos[$key]['subtotal'] = floatval(numeroFormateado($subtotal));
            $productos[$key]['tax'] = floatval(numeroFormateado($tax));
            $productos[$key]['total'] = floatval(numeroFormateado(floatval($subtotal) + floatval($tax)));
            $productos[$key]['cantidad'] = $detalle->cantidad;
        }

        $cupon = "";
        $datos = array("products" => json_encode($productos),
            "coupons" => json_encode($cupon),
            "total" => floatval(numeroFormateado($sum_subtotal_0 + $sum_subtotal_12 + $sum_tax )),
            "tax" => floatval(numeroFormateado($sum_tax)),
            "subtotal12"  => floatval(numeroFormateado($sum_subtotal_12)),
            "subtotal0" => floatval(numeroFormateado($sum_subtotal_0)),
            "email" => $pedido->pedidoCliente->correo,
            "first_name" => $pedido->pedidoCliente->nombre,
            "second_name" => $pedido->pedidoCliente->segundo_nombre,
            "last_name" => $pedido->pedidoCliente->apellido,
            "document" => $pedido->pedidoCliente->ci_ruc,
            "phone" => $pedido->pedidoCliente->telefono,
            "cellphone" => $pedido->pedidoCliente->telefono,
            "address" => $pedido->pedidoCliente->direccion,
            "ip_address" => \Request::ip(),
            "order_id" => 'Gestor-' . $pedido->id,
            //"shipping" => "",
            //"shipping_tax" => "",
            "shipping" => floatval(numeroFormateado($pedido->tarifa_envio / (1 + $iva))),
            "shipping_tax" => floatval(numeroFormateado(($pedido->tarifa_envio / (1 + $iva)) * $iva )),
            "gateway" => "botonpagos",
            "status" => "pending",
            "date" => date('Y-m-d H:i:s'),
            "url_response" => (\Sistema::getConfig('redirect_pago')."/".\Sistema::getAppCode()."/".$pedido->id)
        );
        // dd($datos);
        $url = "https://portal.botonpagos.com/api/datafast/setOrder?".http_build_query($datos);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'code: '. \Sistema::getAppCode() 
        ));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)){
            return curl_error($ch);
        }
        curl_close($ch);
        $responseData = json_decode($responseData, true);
        
        return response()->json(['status' => 'ok', 'pedido' => $pedido, 'cliente' => $cliente, 'base_path' => $base_path,'responseData' => $responseData], 200);
    }

    public function estadoPago($code, $orden, $id_transaccion)
    {
        $url = "https://portal.botonpagos.com/api/datafast/tienda/checkPayment/".$code."/".$orden."/".$id_transaccion;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)){
            return curl_error($ch);
        }

        curl_close($ch);
        $responseData = json_decode($responseData, true);
         $pedido = PedidoModel::find($orden);
        if($responseData["error_code"] != 0){
            $pedido->estado = 4;
            $pedido->save();
        }else{
            $pedido->estado = 3;
            $pedido->id_trans = $responseData["id"];
            $pedido->code_auth = $responseData["resultDetails"]["AuthCode"];
            $pedido->num_referencia = $responseData["resultDetails"]["ReferenceNbr"];
            $pedido->resp_banco = $responseData["resultDetails"]["AcquirerResponse"];
            $pedido->diferido = $responseData["recurring"]["numberOfInstallments"];
            $pedido->interes = $responseData["customParameters"]["SHOPPER_interes"];
            $pedido->tipo_tarjeta = $responseData["paymentBrand"];
            $pedido->banco = $responseData["card"]["bin"];
            $pedido->propietario_tarjeta = $responseData["card"]["holder"];
            $pedido->save();

        }
        $orden_id = $orden;
        return response()->json(['status' => 'ok', 'orden_id' => $orden_id, 'responseData' => $responseData, 'pedido' => $pedido], 200);
    }
    public function addProducto($id)
    {
        $producto = Tienda::find($id);
        Cart::add(['id' => $producto->id, 'name' => $producto->nombre, 'qty' => 1, 'price' => $producto->precio, 'options' => [
            'img' => $producto->path
        ]]);
        //Session::flash('message', "Producto agregado al carrito");
        return Cart::count();
    }
    public function guardarPedido(Request $request)
    {
        $this->validate($request, [
            'ci_ruc' => 'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'ciudad' => 'required',
            'direccion' => 'required',
            'telefono' => 'required'
        ]);
        DB::beginTransaction();

        try {
            $cliente = ClienteModel::find($request->id_cliente);
            $cliente->ci_ruc = $request->ci_ruc;
            $cliente->nombre = $request->nombre;
            $cliente->apellido = $request->apellido;

            $cliente->telefono = $request->telefono;
            $cliente->correo = $request->correo;
            $cliente->mensaje = $request->mensaje;
            $cliente->ciudad = $request->ciudad;
            $cliente->direccion = $request->direccion;
            $cliente->latitud = $request->latitud;
            $cliente->longitud = $request->longitud;
            $cliente->save();

            $pedido = new PedidoModel();
            $pedido->estado = 1;
            $pedido->id_cliente = $cliente->id;
            $pedido->subtotal = $request->subtotal;
            $pedido->descuento = $request->descuento;
            $pedido->iva = $request->iva;
            $pedido->tarifa_envio  = $request->tarifa_envio;
            $pedido->total = $request->total;
            $pedido->save();  

            foreach (Cart::content() as $key => $value) {
                $detalle = new DetallePedidoModel();
                $detalle->id_pedido = $pedido->id;
                $detalle->id_producto = $value->id;
                $detalle->cantidad = $value->qty;
                $detalle->precio = $value->price;
                $detalle->save();
            }   
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return abort(500, $e);
        }
        
        $info = Informacion::find(1);
        $categorias = categoriaproductos::all();
        $productos = Cart::content();
        
        return view('cart.pago', compact('pedido', 'productos', 'cliente', 'info', 'categorias'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = Cart::update($request->id, $request->qty);
        return $cart;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pagos(Request $request)
    {
        $this->validate($request, [
            'valor' => 'required',
            'num_doc' => 'required',
            'id_adjunto' => 'required'
        ]);
        DB::beginTransaction();
        try {
            $deposito =  new DepositoModel();
            $deposito->num_doc = $request->num_doc;
            $deposito->valor = $request->valor;
            $deposito->id_adjunto = $request->id_adjunto;
            $deposito->id_pedido = $request->id_pedido;
            $deposito->save();

            $pedido = PedidoModel::find($request->id_pedido);
            $pedido->estado = 2;
            $pedido->save();
            
            Cart::destroy();
            DB::commit();
            Session::flash('message', "Información del deposito recibida con éxito");
            return redirect('/');
        } catch (Exception $e) {
            DB::rollback();
            return abort(500, $e);
        }
        
    }
    public function destroy($id)
    {
        Cart::remove($id);
        return back(); 
    }
    public function subirDocumento(Request $request){
        $adjunto = new AdjuntoModel();
        $pago = str_random(25);
        $dir = public_path().'/depositos/';
        $docs = $request->file('file');
        $extension = $docs->getClientOriginalExtension();
        $nombre_archivo = 'deposito-'. $pago . '-' . date('Ymd').'.' .$extension;
        $docs->move($dir, $nombre_archivo);
        $adjunto->nombre = $nombre_archivo;
        $adjunto->path = '/depositos/'.$nombre_archivo;
        $adjunto->tipo = 1;
        $adjunto->save();
        return $adjunto->id;          
    }

    public function agregarCupon($id, $cupon){
        $pedido =  PedidoModel::find($id);
        $detalle = DetallePedidoModel::where('id_pedido', $id)->get();
        $limite = \Sistema::getLimiteCupon($cupon);
        $carrito = CarritoModel::first();
        $sub_total_12 = 0;
        $sub_total_0 = 0;
        $descuento = 0;
        $sum_desc = 0;
        
        if ($limite == 4) {
            $porc_cupon = (\Sistema::getDescuentoCupo($cupon) / 100);
            foreach ($detalle as $key => $value) {
                $producto = Tienda::find($value->id_producto);
                $det = DetallePedidoModel::find($value->id);
                    $descuento = $value->precio_con_desc * $porc_cupon;
                    $precio_con_desc = $det->precio_con_desc - $descuento;
                    $sum_desc = $sum_desc + $descuento;
                if ($producto->iva == 1) {
                    $sub_total_12 = $sub_total_12 + $precio_con_desc;
                }else{
                    $sub_total_0 = $sub_total_0 + $precio_con_desc; 
                }
                
                $det->precio_con_desc = $precio_con_desc;
                $det->save();
            }
            $pedido->subtotal_12 = $sub_total_12;
            $pedido->subtotal = $sub_total_0;
            $pedido->cupon = $cupon;
            $pedido->descuento_cupon = $sum_desc;
            $pedido->iva = $sub_total_12 * ($carrito->iva / 100);
            $pedido->total = $sub_total_12 + $sub_total_0 + ($sub_total_12 * ($carrito->iva / 100));
            $pedido->save();

            return response()->json(['mensaje' => $limite, 'descuento' => \Sistema::getDescuentoCupo($cupon)]);   
        }
        switch ($limite) {
            case 1:
                return response()->json(['mensaje' => 'El cupón no existe o se encuentra inactivo', 'descuento' => null]);
                break;
            case 2:
                return response()->json(['mensaje' => 'El cupón ya alcanzo su límite', 'descuento' => null]);
                break;
            case 3:
                return response()->json(['mensaje' => 'El cupón ya expiró', 'descuento' => null]);
                break;
            default:
                return response()->json(['mensaje' => 'Error desconocido', 'descuento' => null]);
                break;
        }             
    }
}
