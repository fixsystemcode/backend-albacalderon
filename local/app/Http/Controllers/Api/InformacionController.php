<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Informacion;
use App\tienda;
use App\categoriaproductos;
use App\Articulo;
use App\visitasproductos;
use App\Slider;
use App\Imagen;
use DB;
use View;
use Session;
use Redirect;

class InformacionController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function informacion()
    {
        $informacion = Informacion::get();
        if(is_null($informacion))  return response()->json(['status' => 'ok','slider' => 'vacio'], 200);
        foreach ($informacion as $key => $info) {
            $info->base_path = url('/local/public/images/pagina');
        }       
        return response()->json(['status' => 'ok','informacion' => $informacion], 200);
    }

    public function slider()
    {
        $slider = Slider::all();
        if(is_null($slider))  return response()->json(['status' => 'ok','slider' => 'vacio'], 200);
        foreach ($slider as $key => $sl) {
            $sl->base_path = url('/local/public/images/');
        }
        return response()->json(['status' => 'ok','informacion' => $slider], 200);
    }

    public function productos()
    {
        $productos = tienda::where('estado', 1)->get();
        foreach ($productos as $key => $p) {
            $p->base_path = url('/local/public/images/');
        }       
        return response()->json(['status' => 'ok','productos' => $productos], 200);
    }

    public function producto($id)
    {
        $producto = tienda::find($id);
        $producto->base_path = url('/local/public/images/');
        if(is_null($producto)) return response()->json(['status' => 'ok','producto' => 'No existe un producto con el id especificado'], 200);
        return response()->json(['status' => 'ok','producto' => $producto], 200);
    }

    public function categoriaProductos()
    {
        $categoriaproductos = categoriaproductos::get();       
        return response()->json(['status' => 'ok','categoriaproductos' => $categoriaproductos], 200);
    }

    public function categoriaProducto($id)
    {
        $categoriaproductos = categoriaproductos::find($id);
        if(is_null($categoriaproductos)) return response()->json(['status' => 'ok','categoriaproductos' => 'No existe una categoria con el id especificado'], 200);       
        return response()->json(['status' => 'ok','categoriaproductos' => $categoriaproductos], 200);
    }

    public function comentarios(){
        $comentarios = DB::table("articulos")
          ->join("categorias","articulos.id_categoria","=","categorias.id")
          ->join("usuarios","articulos.id_usuario","=","usuarios.id")
          ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
          ->orderBy('created_at','DESC')
          ->where('categorias.categoria', "Comentarios")
          ->where("articulos.activado", 1)
          ->get();
        return response()->json(['status' => 'ok','comentarios' => $comentarios], 200);
    }

    public function articulo($id){
        $articulo = Articulo::with('articulosFotos')->with('articulosVideos')->with('articulosDocumentos')
        ->join("categorias as ctg","articulos.id_categoria","=","ctg.id")
        ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        ->select("articulos.*","usuarios.nombres", "usuarios.path as userpath","ctg.categoria","ctg.id as id_categoria")
        ->where('articulos.id','=', $id)
        ->first();

        return response()->json(['status' => 'ok','articulo' => $articulo], 200);
    }

    public function articuloSlug($slug){
        $articulo = Articulo::with('articulosFotos')->with('articulosVideos')->with('articulosDocumentos')
        ->join("categorias","articulos.id_categoria","=","categorias.id")
        ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.id as id_categoria","categorias.icono")
        ->where('articulos.slug','=', "$slug")
        ->first();

        return response()->json(['status' => 'ok','articulo' => $articulo], 200);
    }

    public function visitasProducto($id){
        $visitas = visitasproductos::where('tienda_id', $id)->count();
        if(is_null($visitas)) $visitas = 0;
        return response()->json(['status' => 'ok','visitas' => $visitas], 200);
    }

    public function guardarVisitaProducto(Request $request){
        if(is_null($request->get('id_producto'))) return response()->json(['status' => 'ok','mensaje' => 'debe ingresar un id de producto'], 401); 
        $visita = new visitasproductos;
        $visita->fecha = date('Y-m-d');
        $visita->ip = \Request::ip();
        $visita->tienda_id = $request->id_producto;
        $visita->save();
        $numVisita = visitasproductos::where('tienda_id', $request->id_producto)->count();
        if(is_null($numVisita)) $numVisita = 0;
        return response()->json(['status' => 'ok','numVisita' => $numVisita], 200);
        
    }

    public function topVisitas($top=5){
        $visitas = visitasproductos::select(
            DB::raw('count(*) as num_visitas') , 'tienda_id'
        )
        ->with('visitaProducto')
        ->orderBy('num','DESC')
        ->groupBy('tienda_id')
        ->take($top)->get();
        return response()->json(['status' => 'ok','visitas' => $visitas], 200);
    }

    public function noticias(){
        $noticias = DB::table("articulos")
        ->join("categorias","articulos.id_categoria","=","categorias.id")
        ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        ->select("articulos.*","usuarios.nombres", "usuarios.path as userpath", "categorias.categoria","categorias.id as id_categoria","categorias.icono")
        ->where('categorias.categoria','=', "Noticias")
        ->where('articulos.estado', '=', 1)
        ->orderBy('articulos.created_at', 'DESC')
        ->get();
        return response()->json(['status' => 'ok','noticias' => $noticias], 200);     
    }

    public function enviarmensaje(Request $request){
      $desde = $request->email;
      $nombre = $request->name;
      $mensaje = $request->message;
      $asunto = $request->subject;
      enviar_correo_contactos($desde, $nombre, $mensaje, $asunto);
    }





    /***************/
    public function noticiaslimit($limit){
        $noticias = DB::table("articulos")
            ->join("categorias","articulos.id_categoria","=","categorias.id")
            ->join("usuarios","articulos.id_usuario","=","usuarios.id")
            ->select("articulos.*","usuarios.nombres", "usuarios.path as userpath", "categorias.categoria","categorias.id as id_categoria","categorias.icono")
            ->where('categorias.categoria','=', "Noticias")
            ->where('articulos.estado', '=', 1)
            ->orderBy('articulos.created_at', 'DESC')
            ->take($limit)
            ->get();
        return response()->json(['status' => 'ok','noticias' => $noticias], 200);
    }
    public function ultimocategoria($id){
        $articulo = Articulo::with('articulosFotos')->with('articulosVideos')->with('articulosDocumentos')
        ->join("categorias as ctg","articulos.id_categoria","=","ctg.id")
        ->select("articulos.*","ctg.categoria","ctg.id as id_categoria")
        ->where('id_categoria','=', $id)
        ->first();

        return response()->json(['status' => 'ok','articulo' => $articulo], 200);
    }

    public function ultimocategoriainterna($id){
        $imgpopup = Imagen::where('id_categoriainterna', $id)->first();

        return response()->json(['status' => 'ok','articulo' => $imgpopup], 200);
    }

    public function articuloscategoria($cat){
        $articulos = DB::table("articulos")
        ->join("categorias","articulos.id_categoria","=","categorias.id")
        ->select("articulos.*","categorias.categoria","categorias.id as id_categoria","categorias.icono")
        ->where('categorias.categoria','=', "$cat")
        ->where('articulos.estado', '=', 1)
        ->get();

        return response()->json(['status' => 'ok','articulos' => $articulos], 200);
    }
}
