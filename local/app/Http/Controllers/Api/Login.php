<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use App\VehiculoModel;
use App\ClienteModel;

class Login extends Controller
{
	public function login(Request $request){
		$this->validate($request, [
			'login' => 'required',
			'password' => 'required|string|min:6',
		]);

		$login_request = $request->get('login');
		$password_request = $request->get('password');
	
		if(ClienteModel::where('correo', $login_request)->exists()){
			$user = ClienteModel::where('correo', $login_request)->first();
			$auth = Hash::check($password_request, $user->password);
			if($user && $auth){
				$user->rollApiKey();
				$array = array(
					'usuario' => new UserResource($user),
					'message' => 'Authorization Correcta!',
					'token' => $user->api_token,
				);
				return response()->json(['status' => 'ok','respuesta' => $array], 200);
		  	}
		}else{
			return response()->json(['status' => 'ok','message' => 'No Autorizado, revise sus credenciales'], 401);
		}

		return response()->json(['status' => 'ok','message' => 'No Autorizado, revise sus credenciales'], 401);
	}

	public function register(Request $request){
		$this->validate($request, [
			'login' => 'required',
			'password' => 'required|string|min:6',
			'nombre' => 'required',
			'segundo_nombre' => 'required',
			'apellidos' => 'required',
			'telefono' => 'required'
		]);
		$newUser              = new ClienteModel;
		$newUser->tipo_identificacion = $request->get('tipo_identificacion');
		$newUser->ci_ruc      = $request->get('ci_ruc');
        $newUser->nombre      = $request->get('nombre');
        $newUser->segundo_nombre = $request->get('segundo_nombre');
        $newUser->apellido    = $request->get('apellidos');
        $newUser->correo      = $request->get('login');
        $newUser->telefono	  = $request->get('telefono');
        $newUser->password    = $request->get('password');
        $newUser->celular	  = $request->get('celular'); 
        $newUser->direccion	  = $request->get('direccion');
        $newUser->estado_login	= 0; //Estado para registro de formulario
        $newUser->path_cedula = $request->get('path_cedula');
        $newUser->save();

        $newVehiculo             = new VehiculoModel;
        $newVehiculo->marca      = $request->get('marca');
        $newVehiculo->modelo 	 = $request->get('modelo');
        $newVehiculo->placa    	 = $request->get('placa');
        $newVehiculo->anio       = $request->get('anio');
        $newVehiculo->tipo	  	 = $request->get('tipo');
        $newVehiculo->color      = $request->get('color');
        $newVehiculo->path_matricula = $request->get('path_matricula');
        $newVehiculo->id_cliente = $newUser->id; 
        $newVehiculo->save();

        $usu = ClienteModel::where('correo', $newUser->correo)->first();
        $usu->rollApiKey();
		$array = array(
			'usuario' => new UserResource($usu),
			'vehiculo' => VehiculoModel::where('id_cliente', $usu->id)->first(),
			'message' => 'Usuario creado, Authorization Correcta!',
			'token' => $usu->api_token,
		);
		return response()->json(['status' => 'ok','respuesta' => $array], 200);		
	}

	public function handleProviderCallback(Request $request)
    {
    	$user = $request->get('user');
    	$provider = $request->get('provider');
        if(ClienteModel::where('correo', $user["email"])->exists()){
        	$usu = ClienteModel::where('correo', $user["email"])->first();
            $usu->rollApiKey();
			$array = array(
				'usuario' => new UserResource($usu),
				'message' => 'Authorization Correcta!',
				'token' => $usu->api_token,
			);
			return response()->json(['status' => 'ok','respuesta' => $array], 200);
        } else {
            $nombre = separarName($user["name"]);
            $newUser              = new ClienteModel;
            $newUser->nombre      = $nombre["first"];
            $newUser->segundo_nombre = $nombre["second"];
            $newUser->apellido    = $nombre["last"];
            $newUser->correo      = $user["email"];
            $newUser->provider_id = $user["id"];
            $newUser->provider    = $provider;
            $newUser->estado_login	= 1; //Estado para registro de provider
            $newUser->save();
   //       
            $usu = ClienteModel::where('correo', $newUser->correo)->first();
            $usu->rollApiKey();
			$array = array(
				'usuario' => new UserResource($usu),
				'message' => 'Usuario creado, Authorization Correcta!',
				'token' => $usu->api_token,
			);
			return response()->json(['status' => 'ok','respuesta' => $array], 200);
        }
    }
}
