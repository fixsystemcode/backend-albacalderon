<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use View;
use Session;
use Redirect;
use DB;
use Illuminate\Http\Request;
use App\WishlistModel;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:cliente', ['only' => ['edit']]);
    }

     public function index()
    {
    	$wishlist = WishlistModel::with('wishlistProducto')
    	->select(
    		'id_producto',
    		DB::raw('count(id_producto) as cantidad')
    	)
    	->groupBy('id_producto')
    	->orderBy('cantidad', 'DESC')
    	->get();
        return view('admin.wishlist.index', compact('wishlist'));
    }

    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
          'id_cliente'      => 'required',
          'id_producto'	=> 'required'
        ]);
        $wishlist = WishlistModel::where('id_cliente', $request->id_cliente)->where('id_producto',$request->id_producto)->first();
        if (is_null($wishlist)) {
        	$wishlist = new WishlistModel;
        	$wishlist->id_cliente = $request->id_cliente;
	        $wishlist->id_producto = $request->id_producto;
	        $wishlist->save();
	        Session::flash('message', "Producto agregado a favoritos");
        }else{
        	$wishlist->delete();
        	Session::flash('message', "Producto eliminado de favoritos");
        }
        return redirect()->back();

    }

    public function destroy($id)
    {
        $wishlist = WishlistModel::find($id);
        $wishlist->delete();
        Session::flash('message', "Producto eliminado de favoritos");
        return redirect()->back();

    }
}
