<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        

        $date = date('Y-m-d');
        $nombres = "DIRECCIÓN DE COMUNICACION GAD MANTA";

        $view =  \View::make('certificado', compact('nombres', 'date'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('certificado');


        $pdf = \PDF::loadView('certificado', ['nombres' => $nombres ]);
        return $pdf->download('archivo.pdf');

    }


      public function ver($id)
    {
        //
        dd($id);
        $pdf = \PDF::loadView('certificado', ['usuario' => $usuario ]);
        return $pdf->download('archivo.pdf');
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nombres)
    {
        //
       
         $date = date('Y-m-d');
       

        $view =  \View::make('certificado', compact('nombres', 'date'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('certificado');

        //$pdf = \PDF::loadView('certificado', ['nombres' => $nombres ]);
        //return $pdf->download('certificado.pdf');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
