<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
	protected $puede_leer;
    protected $puede_crear;
    protected $puede_editar;
    protected $puede_eliminar;
    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
    	// $this->middleware(function($request, $next){
	    // 	list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
	    // 	return $next($request);
	    // });
    }

    protected function verificaPermisoLeer(){
    	if(!$this->puede_leer) $this->terminateNoPermiso();
    }

    protected function verificaPermisoCrear(){
    	if(!$this->puede_crear) $this->terminateNoPermiso();
    }

    protected function verificaPermisoEditar(){
    	if(!$this->puede_editar) $this->terminateNoPermiso();
    }

    protected function verificaPermisoEliminar(){
    	if(!$this->puede_eliminar) $this->terminateNoPermiso();
    }

    protected function terminateNoPermiso(){
    	if(request()->ajax()) response('Unauthorized.', 401);
    	else print view('admin.includes.no_permiso')->render();
    	exit();
    }
}
