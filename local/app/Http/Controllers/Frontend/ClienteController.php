<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\ClienteModel;
use App\PedidoModel;
use App\WishlistModel;
use App\DireccionModel;
use Auth;
use Session;
use Redirect;

class ClienteController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:cliente', ['only' => ['index', 'edit', 'ofertas', 'misPedidos', 'deseados']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = \Sistema::getInfo();
        $pedidosPendientes = \Sistema::misPedidos(Auth::guard('cliente')->user()->id, 3);
        $pedidosError = \Sistema::misPedidos(Auth::guard('cliente')->user()->id, 4);
        return view('clientes.dashboard', compact('pedidosError', 'pedidosPendientes', 'info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // validate the data
        $this->validate($request, [
          'nombre'      => 'required',
          'apellido'	=> 'required',
          'correo'       => 'required',
          'password'    => 'required'

        ]);

        // store in the database
        $cliente = new CLienteModel;
        $cliente->nombre = $request->nombre;
        $cliente->segundo_nombre = $request->segundo_nombre;
        $cliente->ci_ruc = $request->ci;
        $cliente->telefono = $request->celular;
        $cliente->apellido = $request->apellido;
        $cliente->correo = $request->correo;
        $cliente->password=bcrypt($request->password);

        $cliente->save();


        return redirect()->route('cliente.auth.login');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function misPedidos(){

        $info = \Sistema::getInfo();
        $pedidos = \Sistema::misPedidos(Auth::guard('cliente')->user()->id);

        return view('clientes.mispedidos', compact('pedidos', 'info'));

    }

    public function ofertas(){
        
        return view('clientes.ofertas');

    }

    public function deseados(){
        $info = \Sistema::getInfo();
        $cliente = Auth::guard('cliente')->user();
        $wishlist = WishlistModel::with('wishlistProducto')->where('id_cliente', $cliente->id)->paginate(3);
        return view('clientes.deseados', compact ('wishlist', 'info'));

    }

    public function miPerfil(){
        $info = \Sistema::getInfo();
        $cliente = ClienteModel::with('direcciones')->where('id', Auth::guard('cliente')->user()->id)->first();
        return view('clientes.miperfil', compact('cliente', 'info'));
    }
    public function guardarPerfil(Request $request){
        $cliente = ClienteModel::find($request->id_cliente);
        $cliente->ci_ruc = $request->ci_ruc;
        $cliente->nombre = $request->nombre;
        $cliente->segundo_nombre = $request->segundo_nombre;
        $cliente->apellido = $request->apellido;
        $cliente->latitud = $request->latitud;
        $cliente->longitud = $request->longitud;
        $cliente->ciudad = $request->ciudad;
        $cliente->direccion = $request->direccion;
        $cliente->telefono = $request->telefono;
        $cliente->correo = $request->correo;
        $cliente->mensaje = $request->mensaje;
        $cliente->save();


        Session::flash('message','Información Actualizada');
        return redirect::back();

    }

    public function guardarDireccion(Request $request){
        if(!is_null($request->id_direccion) && !empty($request->id_direccion)){
            $direccion = DireccionModel::find($request->id_direccion);
        }else{
            $direccion = new DireccionModel;
        }
            $direccion->ciudad = $request->ciudad;
            $direccion->direccion = $request->direccion;
            $direccion->referencia = $request->referencia;
            $direccion->latitud = $request->latitud;
            $direccion->longitud = $request->longitud;
            $direccion->id_cliente = Auth::guard('cliente')->user()->id;
            $direccion->save();

            $direcciones = DireccionModel::where('id_cliente', Auth::guard('cliente')->user()->id)->get();
        return response()->json(['direcciones' => $direcciones]);

    }

    public function mostrarDireccion($id){
        $direccion = DireccionModel::find($id);
        return response()->json(['direccion' => $direccion]);
    }

    public function eliminarDireccion($id){
        $direccion = DireccionModel::find($id);
        $direccion->delete();

        $direcciones = DireccionModel::where('id_cliente', Auth::guard('cliente')->user()->id)->get();
        return response()->json(['direcciones' => $direcciones]);
    }

    public function cancelarPedido(request $request)
    {
        $pedido = PedidoModel::find($request->id);
        if($pedido->estado == 1){
            $pedido->delete();
            Session::flash('message', "Pedido eliminado correctamente");
            return back(); 
        }else{
            Session::flash('message', "Este pedido no puede ser eliminado");
            return back();
        }
        
    }
    
}
