<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Articulo;
use App\Informacion;
use App\Slider;
use App\Imagen;
use App\Foto;
use App\VisitasModel;
use App\visitasproductos;
use App\mapasModel;
use App\Categoria;
use App\CamposExtrasGrupos;
use App\CamposExtras;
use App\Tag;

use App\TiendaController;
use App\tienda;
use App\categoriaproductos;
use Carbon\Carbon;
use SendGrid;

use App\funcionesgestor;
use App\CarritoModel;

use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;



use Illuminate\Support\Facades\Input;

class PaginasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $info = Informacion::find(1);
      $categorias = categoriaproductos::all();

      $slider = Slider::all();
      $productos = tienda::all();

      $carrito = CarritoModel::first();
      // dd($carrito);

      return view('inicio.index', compact('info', 'slider', 'categorias', 'productos', 'carrito'));

    }


    public function categoria($id){

      $info = Informacion::find(1);
      $categorias = categoriaproductos::all();


      $catego = DB::table("categoriaproductos")->select('categoriaproductos.*')->where('categoriaproductos.id','=',$id)->first();
      $productos = DB::table("tienda")
                  ->join("categoriaproductos","tienda.id_categoriaproducto","=","categoriaproductos.id")
                  ->select("tienda.*")
                  ->where('categoriaproductos.id', '=', $id)
                  ->get();

      // dd($productos);


      return view('paginas.categoria', compact('info', 'categorias', 'catego', 'productos'));
    }


    public function producto($id){
      $info = Informacion::find(1);
      $categorias = categoriaproductos::all();
      $producto = DB::table("tienda")
                ->join("categoriaproductos","tienda.id_categoriaproducto","=","categoriaproductos.id")
                ->select("tienda.*", "categoriaproductos.categoria")
                ->where('tienda.id', '=', $id)
                ->first();
      // dd($producto);

      return view('paginas.producto', compact('info', 'categorias', 'producto'));
    }

    public function mostrardatosproducto(request $request){
      $id = $request->id;
      $producto = DB::table("tienda")
                ->select("tienda.*")
                ->where('tienda.id', '=', $id)
                ->first();
      return response()->json(['producto' => $producto]);
    }




    public function empresa(){
      $info = \Sistema::getInfo();

      $comentarios = DB::table("articulos")
                  ->join("categorias","articulos.id_categoria","=","categorias.id")
                  ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                  ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                  ->orderBy('created_at','DESC')
                  ->where('categorias.categoria', "Comentarios")
                  ->where("articulos.activado", 1)
                  ->get();
      // dd($comentarios);

      return view('paginas.empresa', compact('info', 'comentarios'));
    }

    public function nuestrosClientes(){
      $info = \Sistema::getInfo();
      return view('paginas.clientes', compact('info'));
    }
    public function servicios(){
      $info = Informacion::find(1);
      $servicios = DB::table("tienda")
                ->select("tienda.*")
                ->where('estado', 1)
                ->get();

      // dd($item1);
      return view('paginas.servicios', compact('info', 'servicios'));
    }
    public function traerarticulo($id){
      $artcl = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->select("articulos.*","categorias.categoria","categorias.id as id_categoria")
                ->where('articulos.id','=', $id)
                ->first();
      return $artcl;
    }
    public function productos(){
      $informacion = Informacion::find(1);
      $productos = DB::table("tienda")
                ->select("tienda.*")
                ->get();

      // dd($productos);

      return view('paginas.productos', compact('informacion', 'productos'));
    }
    public function productz($id){
      $informacion = Informacion::find(1);
      $producto = DB::table("tienda")
                ->select("tienda.*")
                ->where('tienda.id', '=', $id)
                ->first();
      $campostempo = null;
      if ($producto->campostemporada != null) {
        $campostempo = json_decode($producto->campostemporada);
      }
      // dd($campostempo);
      ////////////////////// control de visitas por fecha e ip
      $hoy = date("Y-m-d");
      Session::put('ipvistante',$_SERVER['REMOTE_ADDR']);
      $ipvi = Session::get('ipvistante');

      $visitas = visitasproductos::where("fecha",$hoy)
                ->where("ip",$ipvi)
                ->where("tienda_id",$producto->id)
                ->get();

      if (count($visitas)) {

      }
      else{
        $visita = new visitasproductos;
        $visita->fecha = $hoy;
        $visita->ip = $ipvi;
        $visita->tienda_id = $producto->id;
        $visita->num = 1;
        $visita->save();
      }

      $nvisitas = visitasproductos::where("tienda_id",$producto->id)->get();
      $numerovisitas = count($nvisitas);

      $populares = tienda::withCount(['visitas'])->orderBy('visitas_count', 'DESC')->limit(5)->get();
      $todos = tienda::orderBy('abreviatura', 'ASC')->get();
      // dd($todos);
      // dd($contaa);
      ///////////////////////////////////////////////

      return view('paginas.producto', compact('informacion', 'producto', 'populares', 'todos', 'campostempo'));
    }

    public function noticias(){

      $informacion = Informacion::find(1);
      $noticias = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.id as id_categoria","categorias.icono")
                ->where('categorias.categoria','=', "Noticias")
                ->where('articulos.estado', '=', 1)
                ->get();
      // dd($noticias);

      return view('paginas.noticias', compact('noticias', 'informacion'));
    }

    public function articulo($slug){
      $informacion = Informacion::find(1);
      $articulo = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.id as id_categoria","categorias.icono")
                ->where('articulos.slug','=', "$slug")
                ->first();
      $videos = DB::table("videosar")
                ->join("articulos","videosar.id_articulos","=","articulos.id")
                ->select("videosar.*")
                ->where("videosar.id_articulos","$articulo->id")
                ->get();
      $documentos = DB::table("documentos")
                ->join("articulos","documentos.articulo_id","=","articulos.id")
                ->select("documentos.*")
                ->orderBy('orden', 'ASC')
                ->where("documentos.articulo_id","$articulo->id")
                ->get();
      $fotos = Foto::where('articulo_id','=',$articulo->id)->get();

      // dd($documentos);
      return view('paginas.articulo', compact('articulo', 'informacion', 'videos', 'fotos', 'documentos'));
    }

    public function certificaciones(){
      $informacion = Informacion::find(1);
      $certificaciones = $this->traerarticulo("18");
      // dd($certificaciones);
      return view('paginas.certificaciones', compact('informacion', 'certificaciones'));
    }

    public function contactos(){
      $informacion = Informacion::find(1);
      $categorias = categoriaproductos::all();

      return view('paginas.contactos', compact('informacion', 'categorias'));
    }


    public function enviarmensaje(Request $request){
      $desde = $request->email;
      $nombre = $request->name;
      $mensaje = $request->message;
      $asunto = $request->subject;
      enviar_correo_contactos($desde, $nombre, $mensaje, $asunto);
      Session::flash('message', "Correo enviado correctamente");
      return redirect()->to('/');
    }


}





