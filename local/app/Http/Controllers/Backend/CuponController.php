<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Redirect;
use App\CuponModel;
use DB;

class CuponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cupones = CuponModel::select( 'cupones.*',
            DB::raw('(select count(*) from pedido as p where p.cupon = cupones.codigo) as usos')
        )
        ->get();

        return view('admin.cupones.index', compact('cupones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $estados = getEstadosCupon();
        return view('admin.cupones.form', compact('edit', 'estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required',
            'codigo' => 'required',
            'descuento' => 'required'            
        ]);
        if(!is_null($request->id)){
            $cupon = CuponModel::find($request->id);
        }else{
            $cupon  = new CuponModel;
        }
        $cupon->descripcion = $request->descripcion;
        $cupon->codigo = $request->codigo;
        $cupon->descuento = $request->descuento;
        $cupon->limite = $request->limite;
        $cupon->fecha_inicio = $request->fecha_inicio;
        $cupon->fecha_fin = $request->fecha_fin;
        $cupon->estado = $request->estado;
        $cupon->save();

        return redirect()->to('cupones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $cupon = CuponModel::find($id);
        $estados = getEstadosCupon();
        return view('admin.cupones.form', compact('edit', 'cupon', 'estados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cupon = CuponModel::find($id);
        $cupon->delete();
        return redirect()->back();
    }
}
