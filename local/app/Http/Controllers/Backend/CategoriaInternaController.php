<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CategoriaInternaCrearRequest;
use App\Http\Requests\CategoriaInternaModificarRequest;
use App\Http\Controllers\Controller;
use App\CategoriaInterna;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use App\funcionesgestor;

use Illuminate\Support\Facades\Input;
//include('app.func');
//include(app_path() . '\funcionesgestor.php');

class CategoriaInternaController extends Controller
{
    //


     function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       $categorias = CategoriaInterna::orderBy('id','DESC')->paginate(50);
        return view('admin.categoriainterna.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.categoriainterna.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaInternaCrearRequest $request)
    {
        // dd($request->all());
        $categoria = new CategoriaInterna($request->all());
        
        ////////////////////////////////// imagenprincipalcategoria
            $imagen = $request->path;
            

            $dir = public_path().'/imagescat/';

           	$docs = $request->file('path');
            
            // dd($icono);
            // dd($docs);

            if (Input::hasFile('path')){

                $imagen = $docs->getClientOriginalName();
                $imagen = $this->limpiar_caracteres_especiales($imagen);
                //dd($imagenprincipal);
                $docs->move($dir, $imagen);
            }

            

        /////////////////////////
            
        $categoria->path = $imagen;
        
        $categoria->nombre = $request->nombre;  
        $categoria->descripcion = $request->descripcion;  
        
        $categoria->save();

        Session::flash('message','Categoria fue creada correctamente');
        return redirect::to('categoriainterna');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $categoria= CategoriaInterna::find($id);
       return view('admin.categoriainterna.editar',compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaInternaModificarRequest $request, $id)
    {
         // dd($request->all());
         $categoria= CategoriaInterna::find($id);
         $categoria->fill($request->all());
         ////////////////////////////////// imagenprincipalcategoria

            $imagenprincipal = $categoria->path;
            $nombreicono = $categoria->icono;

            $dir = public_path().'/imagescat/';

            $docs = $request->file('path');
            
            // dd($docs);

            if (Input::hasFile('path')){

                $imagenprincipal = $docs->getClientOriginalName();
                $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
                //dd($imagenprincipal);
                $docs->move($dir, $imagenprincipal);
            }

            

        /////////////////////////

         $categoria->path = $imagenprincipal;
         $categoria->nombre = $request->nombre;  
         $categoria->descripcion = $request->descripcion;
         $categoria->save();

         Session::flash('message','Categoria actualizada correctamente');
        return redirect::to('categoriainterna');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoriaInterna::find($id)->delete();
        

        Session::flash('message','Categoria eliminada correctamente');
        return Redirect::route('categoriainterna.index');
      
    }

}
