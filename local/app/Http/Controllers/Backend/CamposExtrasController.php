<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests\TipoCrearRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CamposExtrasGrupos;
use App\CamposExtras;
use App\Usuarios;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

class CamposExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function __construct()
    {
        $this->middleware('auth');
    }
  

    public function index()
    {
        //

        $datos = CamposExtras::join("camposextrasgrupo","camposextras.id_campos_extras_grupo","=","camposextrasgrupo.id")
        ->select("camposextras.*","camposextrasgrupo.descripcion as tipodescripcion")
        ->orderBy('descripcion','ASC')
        ->get();


        //$datos = CamposExtras::all();
        return View::make('admin.camposextras.index', compact('datos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
       // $listatip=array("text"=>"text","select"=>"select","textarea"=>"textarea","date"=>"date","multipleselect"=>"multipleselect"); 
        //dd($listatip);

        $camposextrasgrupos = CamposExtrasGrupos::pluck('descripcion','id');

        //dd($camposextrasgrupos);
       
        return View::make('admin.camposextras.crear',compact('camposextrasgrupos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
        $dato = new CamposExtras($request->all());
        
        //$prioridad->prioridad = $request->prioridad;
        
        $dato->nombre = $string = str_replace(' ', '', $request->descripcion); 
        //$dato->id_campos_extras_grupo = $request->campos_extras_modulos_id;  
        //$dato->visible = "SI";
        //$dato->requerido = "NO";
       // $dato->valor_predefinido = 0;

        $dato->orden = 0;
        $dato->requerido = "NO";   
        $dato->visible = "SI";

        $dato->save();

        Session::flash('message','Campo extra ingresado correctamente');
        return redirect::to('camposextras');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
 


        $camposextrasgrupos = CamposExtrasGrupos::pluck('descripcion','id');
       
        $dato = CamposExtras::find($id);
        if (is_null($dato))
        {
            return Redirect::route('camposextras.index');
        }
        return View::make('admin.camposextras.editar', compact('dato','camposextrasgrupos'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       
        $dato = CamposExtras::find($id);
        $dato->fill($request->all());

        $dato->save();
        return Redirect::route('camposextras.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         CamposExtras::destroy($id);
         Session::flash('message','Campo encargada eliminado Correctamente');
         return redirect::to('camposextras') ;
    }

    
}
