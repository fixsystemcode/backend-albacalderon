<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\DepositoModel;
use App\PedidoModel;
use Session;

class DepositosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $estados = ['0' => 'Pendiente de Aprobación', '1' => 'Aprobado', '2' => 'Rechazado'];
    public function index()
    {
        $depositos = DepositoModel::with('adjunto')->get();
        $estados = $this->estados;
        return view('admin.depositos.index', compact('depositos', 'estados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $deposito = DepositoModel::find($request->id);
        $deposito->estado = $request->estado;
        $deposito->save();
        $pedido = PedidoModel::find($deposito->id_pedido);
        if($deposito->estado == 0){
            $pedido->estado = 2;
           
        }elseif ($deposito->estado == 1) {
            $pedido->estado = 3;
        }elseif ($deposito->estado == 2) {
            $pedido->estado = 4;
        }else{
            $pedido->estado = 1;
        }
        $pedido->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deposito = DepositoModel::find($id);
        $deposito->delete();
        Session::flash('message', "Deposito eliminado correctamente");
        return back();
    }
}
