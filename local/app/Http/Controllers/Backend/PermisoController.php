<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Tipos;
use App\PermisoModel;
use DB;
use Session;
use Redirect;

class PermisoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
            return $next($request);
        });
    }

    public function index(Request $request)
    {	
    	$this->verificaPermisoLeer();
    	$configuracion = ['titulo' => 'Permisos por Perfil', 'url' => 'permisos', 'todos'=>false, 'ver'=>true , 'crear'=>false, 'editar'=>true, 'eliminar'=>false];
		$tabla = Tipos::get();
		$objetos = '[
			{"tipo":"text","Descripcion":"ID","Nombre":"id","Clase":"","Valor":"Null","ValorAnterior" :"Null"},
			{"tipo":"text","Descripcion":"Perfil","Nombre":"tipo","Clase":"","Valor":"Null","ValorAnterior" :"Null"}
		]';
		$objetos = json_decode($objetos);
		return view('admin.crud.index')
			->with('campos',$objetos)
			->with('tabla',$tabla)
			->with('configuracion',$configuracion);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
            'permisos' => 'required',
            'perfil' => 'required',
        	],[
            'permisos.required' => 'Existe un problema al guardar los permisos, recarge la pagina e intentelo nuevamente',
            'perfil.required' => 'El perfil es requerido'
        ]);
		DB::beginTransaction();
        try{
            $perfil = Tipos::find($request->perfil);

            foreach ($request->permisos as $key => $permiso) { //Recorremos todos los menus
                if( !empty($permiso['permiso_id']) ){ //SI existia el permiso antes de modificar
                    if(!empty($permiso['estado'])){ //Si esta seleccionado, lo modificamos
                        $per_crud_actual = PermisoModel::find($permiso['permiso_id']);
                        $per_crud_actual->crear = $permiso['crear'];
                        $per_crud_actual->editar = $permiso['editar'];
                        $per_crud_actual->eliminar = $permiso['eliminar'];
                        $per_crud_actual->save();
                    }else{ //Si ya no esta activo, lo eliminamos
                        PermisoModel::find($permiso['permiso_id'])->delete();
                    }
                }else{ //Si no existia el permiso lo creamos
                    if(!empty($permiso['estado'])){ 
                        $per_actual = new PermisoModel;
                        $per_actual->tipo_usuario_id = $request->perfil;
                        $per_actual->menu_id = $key;
                        $per_actual->crear = $permiso['crear'];
                        $per_actual->editar = $permiso['editar'];
                        $per_actual->eliminar = $permiso['eliminar'];
                        $per_actual->save();
                    }
                }
            }
           	//addToLog('Permisos', 'Modificacion de los permisos de perfil "'.$perfil->perfil.'"', []);
            DB::commit();

           Session::flash('message', 'Permisos modificado correctamente');
           return Redirect::to('permisos');

        }catch(Exception $e){
            DB::rollback();
            return 'Error al guardar los datos';
        }
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->verificaPermisoLeer();
    	$perfil_id = $id;
    	$perfil = Tipos::find($perfil_id);
    	$permisos_actuales = $this->getQueryPerfil($perfil_id)->whereNotNull('p.id')->get();
    	$permisos = buildTree($permisos_actuales,'padre','id');
    	$solo_ver = true;
    	return view('admin.permisos.index', compact('permisos', 'solo_ver', 'perfil'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->verificaPermisoEditar();
    	$perfil_id = $id;
    	$perfil = Tipos::find($perfil_id);    	
    	$permisos_actuales = $this->getQueryPerfil($perfil_id)->get();
    	$permisos = buildTree($permisos_actuales,'padre','id');
    	return view('admin.permisos.index', compact('permisos', 'perfil'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

    }

    private function getQueryPerfil($perfil_id){
    	return DB::table('menu AS m')
    	->leftJoin('usuario_menu_permiso AS p', function($join) use ($perfil_id){
    		$join->on('m.id','=','p.menu_id')->where('p.tipo_usuario_id', $perfil_id);
    	})
    	#->whereNotNull('p.id') //SOLO PARA OBTENER LOS ACTUALES
    	->select('m.nivel', 'm.id', 'm.orden', 'm.nombre', 'm.ruta', 'p.id as permiso_id','m.padre',
    		//DB::raw('CASE WHEN m.idmain = m.id THEN 0 ELSE m.idmain END AS parent'),
    		DB::raw('IFNULL(p.crear, 0) AS crear'),
    		DB::raw('IFNULL(p.editar, 0) AS editar'),
    		DB::raw('IFNULL(p.eliminar, 0) AS eliminar')
    	)->orderBy('m.orden');
    }
}
