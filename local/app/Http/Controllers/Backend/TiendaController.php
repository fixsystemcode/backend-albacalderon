<?php

namespace App\Http\Controllers\Backend;

use App\tienda;
use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

use App\categoriaproductos;
use App\galeriaproducto;
use App\VisitasModel;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use File;
use Illuminate\Support\Facades\Input;
use Facebook;
use TwitterAPIExchange;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        return $s;
    }

    public function index(){

        $productos = DB::table("tienda")
                ->join("categoriaproductos","tienda.id_categoriaproducto","=","categoriaproductos.id")
                ->join("usuarios","tienda.id_usuario","=","usuarios.id")
                ->select("tienda.*","usuarios.nombres","categoriaproductos.categoria")
                ->orderBy('created_at','ASC')
                ->get();
        // dd($productos);

        return view('admin.productos.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = categoriaproductos::pluck('categoria','id');
        $meses =  MesesCastellanoCorto();
        return view('admin.productos.crear', compact('categorias', 'meses') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $producto = new tienda();

        $producto->fill($request->all());
        $producto->slug = str_slug($request->nombre.' '.rand(0, 9999), '-');
        $producto->destacado = ($request->destacado) ? 1 : 0;
        $producto->estado = ($request->estado) ? 1 : 0;
        $producto->temporada = ($request->temporada) ? 1 : 0;
        $producto->iva = ($request->iva) ? 1 : 0;
        if ($producto->temporada == 1) {
            $campostemporada = [];
            for ($i=0; $i < 12; $i++) {
                $valor = ('mes'.$i);
                $control = ($request->$valor) ? 1 : 0;
                $campostemporada[$i] = $control;
            }
            $campostem=json_encode($campostemporada);
            $producto->campostemporada = $campostem;
        }else{
            $producto->campostemporada = null;
        }
        // dd($producto);

        $images = $request->ruta;

        //Imagen principal
        $dir = public_path().'/images/';
        $ImgFile = $request->file('imagenprincipal');
        if ( Input::hasFile('imagenprincipal') ){
            $producto->path =  $this->guardarArchivo( $ImgFile , $dir);
        }

        //Guardar producto
        $producto->save();

        //Guardar imagenes
        $id_producto = $producto->id;
        $dir = public_path().'/galeria/';

        if ($images != null) {
            $numero = 0;
            foreach ($images as $imagen) {
                $foto = new galeriaproducto;
                $foto->titulo = $this->limpiar_caracteres_especiales($imagen);
                $foto->path = $this->limpiar_caracteres_especiales($imagen);
                $foto->id_producto = $id_producto;
                $foto->orden = $numero;
                $foto->save();
                $numero++;
            }
        }

        Session::flash('message','Producto fue creado correctamente');
        return redirect::to('shop');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function show(tienda $tienda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $producto = tienda::findOrFail($id);
        $galeria = galeriaproducto::where('id_producto','=', $producto->id)->orderBy('orden','asc')->get();

        $categorias =categoriaproductos::pluck('categoria', 'id');
        $campostempo = json_decode($producto->campostemporada);
        // dd($campostempo);


        // dd($galeria);

        return view('admin.productos.editar', compact('producto', 'categorias', 'galeria', 'campostempo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = tienda::find($id);
        $imagenGuardada = $producto->path;
        $producto->fill($request->all());
        $producto->destacado = ($request->destacado) ? 1 : 0;
        $producto->estado = ($request->estado) ? 1 : 0;
        $producto->iva = ($request->iva) ? 1 : 0;
        $imagenes = $request->ruta;
        $sobras = $request->sobras;


        $producto->temporada = ($request->temporada) ? 1 : 0;
        if ($producto->temporada == 1) {
            $campostemporada = [];
            for ($i=0; $i < 12; $i++) {
                $valor = ('mes'.$i);
                $control = ($request->$valor) ? 1 : 0;
                $campostemporada[$i] = $control;
            }
            $campostem=json_encode($campostemporada);
            $producto->campostemporada = $campostem;
        }else{
            $producto->campostemporada = null;
        }

        // dd($producto->campostemporada);






        /*** Imagen Central (Principal) ****/
        $dir = public_path().'/images/';
        $ImgFile = $request->file('imagenprincipal');
        if ( Input::hasFile('imagenprincipal') ){
            $producto->path =  $this->guardarArchivo( $ImgFile , $dir);
        }else{
            $producto->path = $imagenGuardada;
        }
        /*** FIN Imagen Central (Principal) ****/
        $producto->save();
        /**/
        $id_producto = $producto->id;
        $dir = public_path().'/galeria/';
        if ($sobras != null) {
            foreach ($sobras as $sobra) {
                galeriaproducto::where('path', 'LIKE', $sobra)->delete();
                File::delete($dir.$sobra);
            }
        }
        $galeria=array();
        $fotos = galeriaproducto::where('id_producto','=',$id_producto)->get();
        foreach ($fotos as $foto) {
            array_push($galeria,$foto->path);
        }
        if ($imagenes != null) {
            $numero =0;
            foreach ($imagenes as $imagen) {
                if (in_array($imagen,$galeria)) {
                    $foto = galeriaproducto::where('path','like',$imagen)->firstOrFail();
                    $foto->orden=$numero;
                    $foto->save();
                }else{
                    $foto = new galeriaproducto;
                    $foto->titulo = $this->limpiar_caracteres_especiales($imagen);
                    $foto->path = $this->limpiar_caracteres_especiales($imagen);
                    $foto->orden = $numero;
                    $foto->id_producto = $id_producto;
                    $foto->save();
                }
                $numero++;
            }
        }
        if ($request->inicio!='on') {
            Session::flash('message','Producto editado Correctamente');
            return redirect::to('shop') ;
        }else{
            Session::flash('message','Producto editado Correctamente');
            return redirect()->route('shop.edit', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tienda  $tienda
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = tienda::find($id);
        $fotos = galeriaproducto::where('id_producto','=',$id)->get();

        $dir = public_path().'/galeria/';
        foreach ($fotos as $foto) {
            File::delete($dir.$foto->path);
        }
        $dir = public_path().'/images/';
        if ($producto->path != null) {
            File::delete($dir.$producto->path);
        }

        galeriaproducto::where('id_producto', '=', $id)->delete();
        tienda::destroy($id);

        Session::flash('message','Producto eliminado Correctamente');
        return redirect::to('shop') ;
    }


    public function subirfotogaleria()
    {
        if (isset($_FILES["image"])){

            $file = $_FILES["image"];
            //$nombre = $file["name"];
            $nombre=$_POST['nombre'];;
            $nombre=$this->limpiar_caracteres_especiales($nombre);
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = public_path().'/galeria';
            if (!file_exists($carpeta)) {
            mkdir($carpeta, 0775, true);
            }
            $carpeta.='/';
            //$file= Image::make($file)->fit(500, 500);
            if ($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png' && $tipo != 'image/gif'){
                echo "Error, el archivo no es una imagen";
            }else{
                $src = $carpeta.$nombre;
                move_uploaded_file($ruta_provisional, $src);
            }
        }
    }
    private function guardarArchivo($file, $dir=''){
        if( empty($dir) ) $dir = public_path();
        
        if ( $file->isValid() ){
            $fileDetails = $file->getClientOriginalName();
            $fileExtension = $file->getClientOriginalExtension();
            $fileDetails = $this->limpiar_caracteres_especiales($fileDetails);
            $fileDetails = md5( $fileDetails . microtime() ) . '.' . $fileExtension;
            $file->move($dir, $fileDetails);
            return $fileDetails;
        }else{
            return null;
        }
    }
}
