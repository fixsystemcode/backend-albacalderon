<?php
namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\tienda;
use App\CamposExtras;
use App\CarritoModel;
use App\AdjuntoModel;


use Validator; 

class CarritoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $tipo_cuenta = ['1' => 'Debito', '2' => 'Credito', '3' => 'Corriente'];

    public function index()
    {
        $carrito = CarritoModel::with('politica')->first();
        $tipo_cuenta = $this->tipo_cuenta;
        $time = \Sistema::getTime();
        if (is_null($carrito)) {
            $carrito =  new CarritoModel();
            $carrito->estado = 0;
            $carrito->estado_domicilio = 1;
            $carrito->tipo_domicilio = "fija";
            $carrito->tarifa_envio = 0;
            $carrito->save();
        }
        //dd($carrito);
        return view('admin.carrito.index', compact('carrito', 'tipo_cuenta', 'time'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        //time = a min=>1, horas=>2, dias=>3
        try {
            $carrito =  CarritoModel::first();
            if(is_null($carrito)){
                $carrito = new CarritoModel();
            }
            
            //$carrito->estado = $request->carrito;
            $carrito->descuento = $request->descuento;
            $carrito->paypal = $request->paypal;
            $carrito->estado_paypal = $request->estado_paypal;
            $carrito->estado_tarjeta = $request->estado_tarjeta;
            $carrito->estado_deposito = $request->estado_deposito;
            $carrito->estado_local = $request->estado_local;
            $carrito->estado_domicilio = $request->estado_domicilio;
            $carrito->app_code = $request->app_code;
            //$carrito->app_key = $request->app_key;
            $carrito->num_cuenta = $request->num_cuenta;
            $carrito->nombre_cuenta = $request->nombre_cuenta;
            $carrito->banco = $request->banco;
            $carrito->tipo_cuenta = $request->tipo_cuenta;
            $carrito->correo = $request->correo;
            $carrito->iva = $request->iva;
            $carrito->tiempo_estimado = $request->tiempo;
            $carrito->tipo_domicilio = $request->tipo_domicilio;
            $carrito->tarifa_envio = $request->tarifa_envio;
            $carrito->tarifa_local = $request->tarifa_local;
            $carrito->tarifa_kilometro = $request->tarifa_kilometro;
            $carrito->time = $request->time;
            $carrito->politicas = $request->texto;
            $carrito->id_adjunto = $request->id_adjunto;
            $carrito->save();
            DB::commit();

            Session::flash('message','Configuración guardada correctamente');
            return redirect::to('carrito') ;
        } catch (Exception $e) {
            DB::rollback();
            Session::flash('message','Ocurrio un error');
            return redirect::to('carrito') ;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function obtenerCamposExtras($camposGuardados=[], $campoGrupo){
        $tabla = DB::table("camposextras")
            ->join("camposextrasgrupo","camposextrasgrupo.id","=","camposextras.id_campos_extras_grupo")
            ->select("camposextras.*",
            "camposextrasgrupo.id as id_grupo",
            "camposextrasgrupo.descripcion as grupo")
            ->where('camposextrasgrupo.descripcion', $campoGrupo)
            ->orderBy('id','ASC')
            ->get();

        $gruposCampos = [];

        foreach ($tabla as $i => $campo) {
            if(!isset($gruposCampos[$campo->id_grupo])){
                $gruposCampos[$campo->id_grupo] = array(
                    'id' => $campo->id_grupo,
                    'descripcion' => $campo->grupo,
                    'campos' => [],
                );
            }
            $gruposCampos[$campo->id_grupo]['campos'][] = array(
                'icono' => $campo->icono,
                'id' => $campo->id,
                'nombre' => $campo->nombre,
                'descripcion' => $campo->descripcion,
                'tipo' => $campo->tipo,
                'valor' => $campo->valor,
                'guardado' => isset($camposGuardados[$campo->id]) ? $camposGuardados[$campo->id] : NULL,
            );           
        }
        return $gruposCampos;
    }
    public function subirDocumento(Request $request){
        $adjunto = new AdjuntoModel();
        $name = str_random(25);
        $dir = public_path().'/politica/';
        $docs = $request->file('file');
        $extension = $docs->getClientOriginalExtension();
        $nombre_archivo = 'politica-'. $name . '-' . date('Ymd').'.' .$extension;
        $docs->move($dir, $nombre_archivo);
        $adjunto->nombre = $nombre_archivo;
        $adjunto->path = '/politica/'.$nombre_archivo;
        $adjunto->tipo = 2;
        $adjunto->save();
        return $adjunto->id;
                
    }
}
