<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;

use App\Articulo;
use App\ModuloModel;

class CursosController extends Controller
{
    public function index()
    {
        return view('admin.cursos.index');
    }

    public function filtrar(Request $request)
    {
        $cursos = Articulo::join("categorias as cat","articulos.id_categoria","=","cat.id")
        ->join("usuarios as u","articulos.id_usuario","=","u.id")
        ->select("articulos.*","u.nombres", "u.apellidos", "cat.categoria")
        ->where('cat.id', 15)
        ->orderBy('created_at','ASC')
        ->get();
		return response()->json(['tabla' => $cursos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $estados = getEstadosCursos();
        $usuarios = \Sistema::getUsuarios();
        return view('admin.cursos.form', compact('edit', 'estados', 'usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'titulo' => 'required',
        //     'texto' => 'required',
        //     'estado' => 'required',
        //     'portada' => 'required',
        //     'fecha_inicio' => 'required',
        //     'fecha_fin' => 'required',
        //     'usuario' => 'required'            
        // ]);
        if(!is_null($request->id)){
            $cursos = Articulo::find($request->id);
        }else{
            $cursos  = new Articulo;
        }
        $cursos->titulo = $request->titulo;
        $cursos->texto = $request->texto;
        $cursos->estado = $request->estado;
        $cursos->fechainicio = $request->fecha_inicio;
        $cursos->fechafin = $request->fecha_fin;
        $cursos->estado = $request->estado;
        $cursos->id_usuario = $request->usuario;
        $dir = public_path().'/images/';
        $ImgFile = $request->file('path');
        if ( Input::hasFile('path') ){
            $cursos->path =  guardarArchivo( $ImgFile , $dir);
        }
        $cursos->save();

        return redirect()->to('cursos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $cursos = Articulo::find($id);
        $estados = getEstadosCursos();
        $usuarios = \Sistema::getUsuarios();
        return view('admin.cursos.form', compact('edit', 'cursos', 'estados', 'usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }

    public function ModulosArticulo($id)
    {
        $modulos = ModuloModel::where('id_articulo', $id)->get();
        return response()->json($modulos);
    }
}
