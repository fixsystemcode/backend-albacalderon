<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Traits\UploadTrait;

class UploadController extends Controller
{

	use UploadTrait;

    public function upload_image(Request $request){
    	$request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:50000'
        ]);

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = 'img_'.time();
            $folder = 'upload/campania/';
            $name = $this->uploadOne($image, $folder, 'public', $name);
            $filePath = $folder . $name;
            return $filePath;
        }
        return false;
    }

    public function upload_file(Request $request){
    	$request->validate([
            'file' => 'required|max:50000'
        ]);

        if ($request->has('file')) {
            $file = $request->file('file');
            $name = md5('file_'.microtime());
            $folder = 'upload/other/';
            $filePath = $this->uploadOne($file, $folder, 'public', $name);
            return response()->json(['success' => 1, 'file'=> $folder . $filePath, 'name' => $file->getClientOriginalName() , 'url'=>asset($folder . $filePath)]);
        }
        return response()->json(['success' => 0]);
    }
}
