<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\MenuModel;

class MenuController extends Controller
{

	private $campos = '[
		{"nombre":"nombre", "tipo":"texto", "texto":"Nombre"},
		{"nombre":"ruta", "tipo":"texto", "texto":"ruta"},
		{"nombre":"icono", "tipo":"texto", "texto":"Icono"},
		{"nombre":"nivel", "tipo":"texto", "texto":"nivel"},
		{"nombre":"padre", "tipo":"texto", "texto":"padre"},
		{"nombre":"orden", "tipo":"texto", "texto":"orden"},
		{"nombre":"visible", "tipo":"texto", "texto":"visible"}
	]';
	private $configuracion = [
		'titulo' => 'Menú Usuarios', 'url' => 'menu', 'todos'=>false, 'crear'=>true, 'eliminar'=>true
	];

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$this->verificaPermisoLeer();
        $tabla = MenuModel::orderby('orden')->get();
        return view('admin.crud.grilla')
            ->with('tabla',$tabla)
            ->with('configuracion',$this->configuracion)
            ->with('campos',json_decode($this->campos));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entradas = $request->all();
		$ruta = $this->configuracion['url'];
		$id = $request->id;
		if($id == 0){
			$msg = "Registro Creado Exitosamente...!";
			$guardar = new MenuModel;
			$guardar->id = $id;
		}else{
			$msg = "Registro Actualizado Exitosamente...!";
			$guardar = MenuModel::find($id);
		}
		$this->validate($request, MenuModel::rules());

		foreach($entradas as $key => $value){
			if($key != "_method" && $key != "_token" && $key != "id") $guardar->$key = $value;
		}
		$guardar->save();

		return $guardar->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	try{
	        $menu = MenuModel::find($id);
	        $menu->delete();
	        return $menu->id;
	    }catch(\Expection $e){
	    	return abort(500,$e);
	    }
    }
}
