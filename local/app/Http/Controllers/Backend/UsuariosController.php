<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UsuarioCrearRequest;
use App\Http\Requests\UsuarioActualizarRequest;
use App\Http\Requests;

use App\User;
use App\Tipos;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;


class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuarios = User::all();
        $usuarios = DB::table("usuarios")
                ->join("tipo_usuarios","usuarios.id_tipo_usuario","=","tipo_usuarios.id")
                ->select("usuarios.*","tipo_usuarios.tipo")
                ->get();
      
        return View::make('admin.usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = Tipos::pluck('tipo', 'id');
        return View::make('admin.usuarios.crear', compact('tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioCrearRequest $request)
    {
        //  \App\Usuarios::create([
        //   'nombres' => $request['nombres'],
        //   'email' => $request['email'],
        //   'telefono' => $request['telefono'],
        //   'login' => $request['login'],
        //   'password' => $request['password'],
        //   'fecha_nacimiento' => $request['fecha_nacimiento'],
        //   'id_tipo_usuario' => $request['id_tipo_usuario'],
        //   
        //   ]);

        $usuario = new User($request->all());
        $dir = public_path().'/perfil/';
        $docs = $request->file('path');
        if (Input::hasFile('path')){
            $fileName = $docs->getClientOriginalName();
            $docs->move($dir, $fileName);
        }
        
        if ($usuario->path!=null) {
            $usuario->path = $fileName;
        }else{
            $usuario->path="avatar.jpg";
        }            

        $usuario->password =  bcrypt($request->password);
        $usuario->save();

        Session::flash('message','Usuario creado correctamente');
        return Redirect::route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        if (is_null($usuario)){
            return Redirect::route('usuarios.index');
        }
        $tipos = Tipos::pluck('tipo', 'id');
        return View::make('admin.usuarios.editar', compact('usuario','tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioActualizarRequest $request, $id)
    {
        $usuario = User::find($id);
        $password = $usuario->password;
        $usuario->fill($request->all());

        if ( !empty($request->password) ) {
            $usuario->password = bcrypt($request->password);
        }else{
            $usuario->password = $password;
        }

        if (Input::hasFile('path')){

            $dir = public_path().'/perfil/';
            $images = $request->file('path');
            $fileName = $images->getClientOriginalName();
            $images->move($dir, $fileName);

            $usuario->path = $fileName;
        }

        $usuario->save();

        if(isset($request->perfil) ) return Redirect::route('perfil');
        else return Redirect::to('usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        try {
            User::find($id)->delete();
            Session::flash('message', 'Se elimino el usuario correctamente correctamente');

        }catch (\Illuminate\Database\QueryException $e){            
            Session::flash('message-error', 'No se puede eliminar el usuario, debido a que le pertenece información, elimine la informacion que le pertenece para poder eliminarlo');
        }
        return Redirect::route('usuarios.index');
    }

    public function perfil(){
        $usuario = User::find(Auth::user()->id);
        //$tipos = Tipos::pluck('tipo', 'id');
        return View('admin.usuarios.perfil', compact('usuario'));
    }
}


    