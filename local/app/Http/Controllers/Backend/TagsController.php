<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tag;
use App\Categoria;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;




class TagsController extends Controller
{
   

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $categorias = Categoria::all();
        $tags = Tag::all();
        return View::make('admin.tags.index', compact('tags', 'categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Redirect::route('tags.index');
        //return view('admin.tags.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tag::create([
           'tag' => $request->tag,
           'descripcion' => $request->descripcion,
           'categoria_id' => $request->categoria_id
        ]);

        Session::flash('message','Tags fue creado correctamente');
        return redirect::to('tags');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Categoria::all();
        $tag = Tag::find($id);
        return view('admin.tags.editar',compact('tag', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tags = Tag::find($id);
        $tags->fill( $request->all() );
        $tags->save();

        Session::flash('message','Tags editado Correctamente');
        return redirect::to('tags') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tag::destroy($id);
        Session::flash('message','Tags eliminado Correctamente');
        return redirect::to('tags') ;
    }

    public function getTagsByCategoria($id){
        $tags = Tag::where('categoria_id', '=', $id)->get();
        return response()->json($tags);
    }
}
