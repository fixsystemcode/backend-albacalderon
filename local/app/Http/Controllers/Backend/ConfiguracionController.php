<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\ConfiguracionModel;
use Illuminate\Support\Facades\Input;
use Validator;
//use Session;
use Redirect;

class ConfiguracionController extends Controller
{
	private $campos = '[
		{"nombre":"clave", "tipo":"texto", "texto":"Clave"},
		{"nombre":"valor", "tipo":"texto", "texto":"Valor"},
		{"nombre":"descripcion", "tipo":"html", "texto":"Descripcion"}
	]';
	private $configuracion = [
		'titulo' => 'Configuraciones', 'url' => 'configuracion', 'todos'=>false, 'crear'=>true, 'eliminar'=>true
	];

	public function __construct(){
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
            return $next($request);
        });
    }

    	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$this->verificaPermisoLeer();
    	$tabla = ConfiguracionModel::all();
        return view('admin.crud.grilla')
            ->with('tabla',$tabla)
            ->with('configuracion',$this->configuracion)
            ->with('campos',json_decode($this->campos));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entradas = Input::all();
		$ruta = $this->configuracion['url'];
		$id = $request->id;
		if($id == 0){
			$msg = "Registro Creado Exitosamente...!";
			$guardar = new ConfiguracionModel;
			$guardar->id = $id;
		}else{
			$msg = "Registro Actualizado Exitosamente...!";
			$guardar = ConfiguracionModel::find($id);
		}
		$this->validate($request, ConfiguracionModel::rules());

		foreach($entradas as $key => $value){
			if($key != "_method" && $key != "_token" && $key != "id") $guardar->$key = $value;
		}
		$guardar->save();

		return $guardar->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function set_config_valor(Request $request, $clave){
		$valor = $request->has('valor') ? $request->valor : '';
		$descripcion = $request->has('descripcion') ? $request->descripcion : '-';

		$registro = ConfiguracionModel::where('clave', $clave)->first();
		if(empty($registro)){
			$registro = new ConfiguracionModel;
			$registro->clave = $clave;
			$registro->descripcion = $descripcion;
		}
		$registro->valor = $valor;
        $registro->save(); 
        return $registro->id;           
	}
}
