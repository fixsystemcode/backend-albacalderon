<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use App\categoriaproductos;
use App\Http\Requests;
use Illuminate\Http\Request;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;


use Illuminate\Support\Facades\Input;


class CategoriaproductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        return $s;
    }

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $categoriaspro = categoriaproductos::orderBy('id','DESC')->paginate(50);
        return view('admin.categoriaproductos.index', compact('categoriaspro'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categoriaproductos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoriapro = new categoriaproductos($request->all());
        // dd($categoriapro);
        ////////////////////////////////// imagenprincipalcategoria
            $imagenprincipal = $request->path;
            $nombreicono = $request->icono;
            $dir = public_path().'/images/';
            $docs = $request->file('path');
            $icono = $request->file('icono');
            if (Input::hasFile('path')){
                $imagenprincipal = $docs->getClientOriginalName();
                $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
                $docs->move($dir, $imagenprincipal);
            }
            if (Input::hasFile('icono')){
                $nombreicono = $icono->getClientOriginalName();
                $nombreicono = $this->limpiar_caracteres_especiales($nombreicono);
                $icono->move($dir, $nombreicono);
            }
        /////////////////////////

        $categoriapro->path = $imagenprincipal;
        $categoriapro->icono = $nombreicono;
        $categoriapro->destacado = $request->destacado;
        // $categoriapro->descripcion = $request->descripcion;
        $categoriapro->save();

        Session::flash('message','Categoria fue creada correctamente');
        return redirect::to('categoriaproductos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\categoriaproductos  $categoriaproductos
     * @return \Illuminate\Http\Response
     */
    public function show(categoriaproductos $categoriaproductos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\categoriaproductos  $categoriaproductos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoriapro = categoriaproductos::find($id);
        return view('admin.categoriaproductos.editar',compact('categoriapro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categoriaproductos  $categoriaproductos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria= categoriaproductos::find($id);
        $categoria->fill($request->all());
        ////////////////////////////////// imagenprincipalcategoria
            $imagenprincipal = $categoria->path;
            $nombreicono = $categoria->icono;
            $dir = public_path().'/images/';
            $docs = $request->file('path');
            $icono = $request->file('icono');

            if (Input::hasFile('path')){
                $imagenprincipal = $docs->getClientOriginalName();
                $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
                $docs->move($dir, $imagenprincipal);
            }
            if (Input::hasFile('icono')){
                $nombreicono = $icono->getClientOriginalName();
                $nombreicono = $this->limpiar_caracteres_especiales($nombreicono);
                $icono->move($dir, $nombreicono);
            }
        /////////////////////////

         $categoria->path = $imagenprincipal;
         $categoria->icono = $nombreicono;
         $categoria->destacado = $request->destacado;
         $categoria->save();

         Session::flash('message','Categoria actualizada correctamente');
        return redirect::to('categoriaproductos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\categoriaproductos  $categoriaproductos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        categoriaproductos::find($id)->delete();

        Session::flash('message','Categoria eliminada correctamente');
        return Redirect::route('categoriaproductos.index');
    }


    public function getCproductos(){
        $categorias = categoriaproductos::pluck('categoria','id');
        return response()->json($categorias);
    }
}
