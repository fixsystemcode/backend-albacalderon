<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ModuloModel;
use App\Videosar;
use App\Documentos;
use Session;
use Redirect;
use File;

class ModuloController extends Controller
{
    public function show($id)
    {
        $modulo = ModuloModel::find($id);
        return response()->json($modulo);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'descripcion' => 'required',
            'estado' => 'required',            
        ]);
        if(!is_null($request->id_modulo)){
            $modulo = ModuloModel::find($request->id_modulo);
        }else{
            $modulo  = new ModuloModel;
        }
        $modulo->nombre = $request->nombre;
        $modulo->descripcion = $request->descripcion;
        $modulo->estado = $request->estado;
        $modulo->id_articulo = $request->id_articulo;
        $modulo->save();

        $modulos = ModuloModel::where('id_articulo', $request->id_articulo)->get();
        return response()->json($modulos);

    }

    public function edit($id)
    {
    	$modulo = ModuloModel::with('curso')->find($id);
    	$mis_videos = Videosar::where('id_articulos', $modulo->id_articulo)->where("id_modulo",$id)->orderBy('orden','asc')->get();
        $docs = Documentos::where('articulo_id','=', $modulo->id_articulo)->where("id_modulo",$id)->orderBy('orden','asc')->get();
        return view('admin.cursos.modulos', compact('mis_videos', 'modulo', 'docs'));
    }

    public function update(Request $request, $id)
    {
        $modulo = ModuloModel::find($id);
        $dir = public_path().'/doc/';

        $videos = $request->videos;
        $titulos=$request->titulovideo;
        $rutadocs=$request->rutadocs;
        $titulodocs=$request->titulodocs;

        $videos_g = Videosar::where('id_modulo',$id)->where('id_articulos', $modulo->id_articulo)->get();


        if(!is_null($videos_g)){
            foreach ($videos_g as $vid) {
                $vid->delete();
            }
        }

        if(!is_null($videos)) {
            $orden=1;
            foreach ($videos as $v =>  $video) {
                $videosar = new Videosar;
                $videosar->path     = $video;
                $videosar->titulo   = $titulos[$v];
                $videosar->orden    = $orden;
                $videosar->id_articulos = $modulo->id_articulo;
                $videosar->id_modulo  = $id;
                $videosar->save();
                $orden++;
            }
        }

        $documentos = Documentos::where('id_modulo',$id)->where('articulo_id', $modulo->id_articulo)->get();
        
        if(!is_null($documentos)){
            foreach ($documentos as $doc) {
                File::delete($dir.$doc->path);
                $doc->delete();
            }
        }

        if(!is_null($rutadocs)){
            $orden = 1;
            foreach ($rutadocs as $v => $rutadoc) {
                $rutadoc = limpiar_caracteres_especiales($rutadoc);
                $newdoc = new Documentos;
                $newdoc->path   = $rutadoc;
                $newdoc->orden  = $orden;
                $newdoc->titulo = $titulodocs[$v];
                $newdoc->orden  = $orden;
                $newdoc->articulo_id = $modulo->id_articulo;
                $newdoc->id_modulo = $id;
                $newdoc->save();
                $orden++;

            }
        }
            
        Session::flash('message','Modulo editado Correctamente');
        return redirect::back();
    }
}
