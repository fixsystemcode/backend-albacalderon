<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Slider;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use App\CategoriaInterna;
use App\Http\Requests\SliderActualizarRequest;
use App\Http\Requests\SliderCrearRequest;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\ImageManipulator;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');
    }
    public function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        return $s;
    }

    public function index()
    {
        //
        //$imgs = Slider::all();
        $imgs = DB::table("slider")
                ->join("categoriainterna","slider.id_categoriainterna","=","categoriainterna.id")
                ->select("slider.*","categoriainterna.nombre")
                ->get();

        
        return View::make('admin.slider.index', compact('imgs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorias = CategoriaInterna::pluck('nombre','id');

        return View::make('admin.slider.crear' , compact('categorias'));


        //return view('admin.slider.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderCrearRequest $request)
    {
        //
        $img = new Slider($request->all());

        ///////// subir imagenes

                //require_once('ImageManipulator.php'); 

                $dir = public_path().'/images/';

                // dd(public_path());

                $docs = $request->file('path');

                $validExtensions = array('.jpg', '.jpeg', '.gif', '.png','.JPG', '.JPEG', '.GIF', '.PNG'); // get extension of the uploaded file
                $fileExtension = strrchr($_FILES['path']['name'], "."); 
                //dd($_FILES['path']['name']);
                //dd($fileExtension);
                // check if file Extension is on the list of allowed ones

                $newNamePrefix = null;
                if (in_array($fileExtension, $validExtensions)) {
                    $newNamePrefix = time() . '_';

                    // $manipulator = new ImageManipulator($_FILES['path']['tmp_name']);

                    // $newImage = $manipulator->resample(1890, 900);



                    // $width  = $manipulator->getWidth();
                    // $height = $manipulator->getHeight();
                    // $centreX = round($width / 2);
                    // $centreY = round($height / 2);

                    // $x1 = $centreX - 945; 
                    // $y1 = $centreY - 450; 
             
                    // $x2 = $centreX + 945; 
                    // $y2 = $centreY + 450; 


                    

                    // $newImage = $manipulator->crop($x1, $y1, $x2, $y2);
                    // saving file to uploads folder
                    // $manipulator->save($dir . $newNamePrefix . $_FILES['path']['name']);

                    //echo 'Done ...';
                    
                } else {
                    echo 'You must upload an image...';
                }

            ////////////////// fin subir imagenes
        $img->path =  $this->guardarArchivo( $docs , $dir);
        // $img->path = $newNamePrefix . $_FILES['path']['name'];

        if ($request->mostrar){
            $img->mostrar=1;
        }else{
            $img->mostrar=0;
        }
        if ($request->tipo){
            $img->tipo=1;
        }else{
            $img->tipo=0;
        }

        $img->save();
        Session::flash('message','Imagen guardada correctamente');
        return redirect::to('animacion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $img= Slider::find($id);
        $categorias = CategoriaInterna::pluck('nombre','id');
        // dd($img);
        

       return view('admin.slider.editar',compact('img','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderActualizarRequest $request, $id)
    {
        $img= Slider::find($id);
        $img->fill($request->all());
        if ( Input::hasFile('path') ){
            // require_once('ImageManipulator.php');
            $dir = public_path().'/images/';
            $docs = $request->file('path');
            $validExtensions = array('.jpg', '.jpeg', '.gif', '.png','.JPG', '.JPEG', '.GIF', '.PNG'); // get extension of the uploaded file
            $fileExtension = strrchr($_FILES['path']['name'], "."); 
            // check if file Extension is on the list of allowed ones

            $newNamePrefix = null;
            if (in_array($fileExtension, $validExtensions)) {
                $newNamePrefix = time() . '_';
                // $manipulator = new ImageManipulator($_FILES['path']['tmp_name']);
                // $newImage = $manipulator->resample(1890, 900);
                // $width  = $manipulator->getWidth();
                // $height = $manipulator->getHeight();
                // $centreX = round($width / 2);
                // $centreY = round($height / 2);
                // $x1 = $centreX - 945;
                // $y1 = $centreY - 450;
                // $x2 = $centreX + 945;
                // $y2 = $centreY + 450; 
                // $newImage = $manipulator->crop($x1, $y1, $x2, $y2);
                // $manipulator->save($dir . $newNamePrefix . $_FILES['path']['name']);
            } else {
                echo 'You must upload an image...';
            }
            $img->path =  $this->guardarArchivo( $docs , $dir);
            // $img->path = $newNamePrefix . $_FILES['path']['name'];
        }else{
            $img->path = $img->path;
        }

        if ($request->mostrar){
            $img->mostrar=1;
        }else{
            $img->mostrar=0;
        }
        if ($request->tipo){
            $img->tipo=1;
        }else{
            $img->tipo=0;
        }

         $img->save();

         Session::flash('message','Imagen actualizada correctamente');
        return redirect::to('animacion');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::find($id)->delete();
        
        Session::flash('message','Imagen eliminada correctamente');
        return Redirect::route('animacion.index');
    }



    private function guardarArchivo($file, $dir=''){
        if( empty($dir) ) $dir = public_path();
        
        if ( $file->isValid() ){
            $fileDetails = $file->getClientOriginalName();
            $fileExtension = $file->getClientOriginalExtension();
            $fileDetails = $this->limpiar_caracteres_especiales($fileDetails);
            $fileDetails = md5( $fileDetails . microtime() ) . '.' . $fileExtension;
            $file->move($dir, $fileDetails);
            return $fileDetails;
        }else{
            return null;
        }
    }
}

