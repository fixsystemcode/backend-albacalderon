<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\ClienteModel;
use DB;

class ClientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
            return $next($request);
        });
    }
    
    public function index()
    {
    	$clientes = ClienteModel::leftjoin('pedido as p', 'id_cliente', '=', 'clientes.id')
    	->select('clientes.nombre', 'clientes.apellido', 'clientes.direccion', 'clientes.correo', 'clientes.telefono',
    		DB::raw('count(p.id) as pedidos'),
    		DB::raw('SUM(IF(p.estado = 1, 1, 0)) as pedidos_no_pagado'),
    		DB::raw('SUM(IF(p.estado = 2, 1, 0)) as pedidos_pedientes'),
    		DB::raw('SUM(IF(p.estado = 3, 1, 0)) as pedidos_por_enviar'),
    		DB::raw('SUM(IF(p.estado = 4, 1, 0)) as pedidos_error'),
    		DB::raw('SUM(IF(p.estado = 5, 1, 0)) as pedidos_enviados')
    	)
    	->groupBy('clientes.nombre', 'clientes.apellido', 'clientes.direccion', 'clientes.correo', 'clientes.telefono')
    	->orderBy('pedidos', 'DESC')
    	->get();
        return view('admin.clientes.index', compact('clientes'));
    }

    public function saldoCliente()
    {
        $clientes = ClienteModel::leftjoin('pedido as p', 'id_cliente', '=', 'clientes.id')
        ->select('clientes.id','clientes.nombre', 'clientes.segundo_nombre','clientes.apellido', 'clientes.direccion', 'clientes.correo', 'clientes.telefono',
            DB::raw('SUM(IF(p.estado = 3, p.total, 0)) as saldo'),
            DB::raw('(select count(v.id) from vehiculos as v where v.id_cliente = clientes.id) as num_vehiculos')
        )
        ->groupBy('clientes.id', 'clientes.segundo_nombre','clientes.nombre', 'clientes.apellido', 'clientes.direccion', 'clientes.correo', 'clientes.telefono')
        //->orderBy('pedidos.id', 'DESC')
        ->get();
        //dd($clientes);
        return view('admin.clientes.saldo', compact('clientes'));
    }
}
