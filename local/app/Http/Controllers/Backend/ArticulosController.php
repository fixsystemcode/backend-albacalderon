<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;

use App\Tag;
use App\Foto;
use App\Articulo;
use App\User;
use App\Imagen;
use App\Documentos;
use App\Videosar;
use App\prioridad;

use App\Categoria;
use App\VisitasModel;
use App\ArticuloTagModel;
use App\CamposExtrasGrupos;
use App\CamposExtras;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use File;
use Illuminate\Support\Facades\Input;
use Facebook;
use TwitterAPIExchange;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class ArticulosController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        return $s;
    }

    public function index()
    {
        
        if ( !Auth::user()->isAdmin ) {
            $articulos = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria")
                ->where('articulos.id_usuario', Auth::user()->id )
                ->orderBy('created_at','ASC')
                ->get();
        }else{
            $articulos = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria")
                ->orderBy('created_at','ASC')
                ->get();
        }
        return view('admin.articulos.index', compact('articulos'));
    }

    public function activaciones()
    {
        $articulos = DB::table("articulos")
            ->join("categorias","articulos.id_categoria","=","categorias.id")
            ->join("usuarios","articulos.id_usuario","=","usuarios.id")
            ->select("articulos.*","usuarios.nombres","categorias.categoria")
            ->where("articulos.activado", false)
            ->orderBy('created_at','ASC')
            ->get();

        return view('admin.articulos.activacion', compact('articulos'));
    }

    public function habilitarArticulo($id)
    {
        $articulo = Articulo::find($id);
        $articulo->activado=true;
        $articulo->save();

        Session::flash('message','Artículo activado correctamente');
        return Redirect::to('activaciones');
    }

    public function create()
    {
        $prioridad = prioridad::pluck('nombre','id')->all();        
        $categorias = Categoria::pluck('categoria','id');
        $tags = Categoria::first()->tags()->pluck('tag', 'id');

        $gruposCampos = $this->obtenerCamposExtras();

        return view('admin.articulos.crearnuevo', compact('gruposCampos', 'tags', 'categorias', 'prioridad') );
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $articulo = new Articulo();

        $articulo->fill($request->all());
        $articulo->slug = str_slug($request->titulo.' '.rand(0, 9999), '-');
        $articulo->destacado = ($request->destacado) ? 1 : 0;
        $articulo->estado = ($request->estado) ? 1 : 0;
        $articulo->latitud = $request->latitud;
        $articulo->longitud = $request->longitud;
        $articulo->activado = (Auth::user()->isAdmin) ? true : false;

        $tags = $request->tags;
        $videos = $request->videos;
        $titulos = $request->titulovideo;
        $imagenes = $request->ruta;
        $sobras = $request->sobras;
        $rutadocs = $request->rutadocs;
        $titulodocs = $request->titulodocs;
        $sobrasdocs = $request->sobrasdocs;

        $dir = public_path().'/images/';
        $ImgFile = $request->file('imagenprincipal');
        if ( Input::hasFile('imagenprincipal') ){
            $articulo->path =  $this->guardarArchivo( $ImgFile , $dir);
        }

        /*** Guardar Campos Extras ****/
        $tabla= CamposExtras::orderBy('orden','ASC')->get();        
        
        $camposextras = [];
        foreach($tabla as $key => $value){ 
            $camposextras[$value->id]=Input::get($value->nombre);
        }            
        $camposextras=json_encode($camposextras);
        $articulo->camposextras = $camposextras;
        
        /*** Fin de campo de extras ***/
    
        $articulo->save(); //Guardar el articulo

        if ($tags != null) {
            foreach ($tags as $tag) {
                $pivote = new ArticuloTagModel;
                $pivote->tag_id  = (int) $tag;
                $pivote->articulo_id = $articulo->id;
                $pivote->save();
            }
        }

        if ($videos != null) {
            $orden = 1;
            foreach ($videos as $v => $video ) {
                $videosar = new Videosar;
                $videosar->path = $video;
                $videosar->titulo = $titulos[$v];
                $videosar->orden = $orden;
                $videosar->id_articulos = $articulo->id;
                $videosar->save();
                $orden++;
            }
        }

        $id_articulo = $articulo->id;
        $dir = public_path().'/galeria/';

        if ($imagenes != null) {
            $numero = 0;
            foreach ($imagenes as $imagen) {
                $foto = new Foto;
                $foto->titulo = $this->limpiar_caracteres_especiales($imagen);
                $foto->path = $this->limpiar_caracteres_especiales($imagen);
                $foto->orden = $numero;
                $foto->articulo_id = $id_articulo;
                $foto->save();
                $numero++;
            }
        }

        if ($sobras != null) {
            foreach ($sobras as $sobra) {
                File::delete($dir.$sobra);
            }
        }

        $dir = public_path().'/doc/';

        if ($rutadocs != null) {
            $numero =0;
            foreach ($rutadocs as $v =>  $rutadoc) {
                $doc = new Documentos;
                $doc->path = $this->limpiar_caracteres_especiales($rutadoc);
                $doc->orden = $numero;
                $doc->titulo=$titulodocs[$v];
                $doc->articulo_id = $id_articulo;
                $doc->save();
                $numero++;
            }
        }

        if ($sobrasdocs != null) {
            foreach ($sobrasdocs as $sobra) {
                File::delete($dir.$sobra);
            }
        }

        Session::flash('message','Artículo fue creado correctamente');
        return redirect::to('articulos');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $articulo = Articulo::findOrFail($id);

        $imgs2 = Foto::where('articulo_id','=', $articulo->id)->orderBy('orden','asc')->get();
        $docs = Documentos::where('articulo_id','=', $articulo->id)->orderBy('orden','asc')->get();
        $mis_videos = Videosar::where("id_articulos",$articulo->id)->orderBy('orden','asc')->get();

        $categorias =Categoria::pluck('categoria', 'id');
        $prioridad = prioridad::pluck('nombre','id')->all();
        
        $tags = Categoria::find($articulo->id_categoria)->tags()->pluck('tag', 'id');
        $mis_Tags = DB::table('articulo_tag')->where('articulo_id', '=', $articulo->id)->pluck('tag_id')->ToArray();

        $gruposCampos = $this->obtenerCamposExtras( json_decode($articulo->camposextras, true) );
        
        return view('admin.articulos.editarnuevo', compact('articulo', 'categorias', 'tags', 'mis_Tags', 'mis_videos', 'imgs2', 'docs', 'prioridad', 'gruposCampos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articulo = Articulo::find($id);

        $imagenGuardada = $articulo->path;
        $articulo->fill($request->all());

        $articulo->destacado = ($request->destacado) ? 1 : 0;
        $articulo->estado = ($request->estado) ? 1 : 0;
        $articulo->latitud = $request->latitud;
        $articulo->longitud = $request->longitud;
        $articulo->activado = (Auth::user()->isAdmin) ? 1 : 0;

        $videos = $request->videos;
        $titulos=$request->titulovideo;
        $imagenes = $request->ruta;
        $sobras = $request->sobras;
        $rutadocs=$request->rutadocs;
        $titulodocs=$request->titulodocs;
        $sobrasdocs=$request->sobrasdocs;
        
        /*** Imagen Central (Principal) ****/
        $dir = public_path().'/images/';
        $ImgFile = $request->file('imagenprincipal');
        if ( Input::hasFile('imagenprincipal') ){
            $articulo->path =  $this->guardarArchivo( $ImgFile , $dir);
        }else{
            $articulo->path = $imagenGuardada;
        }
        /*** FIN Imagen Central (Principal) ****/

        /*** Guardar Campos Extras ****/
        $tabla= CamposExtras::orderBy('orden','ASC')->get();        
        
        $camposextras = [];
        foreach($tabla as $key => $value){ 
            $camposextras[$value->id]=Input::get($value->nombre);
        }            
        $camposextras=json_encode($camposextras);
        $articulo->camposextras = $camposextras;
        /*** Fin de campo de extras ***/

        //$fileName = $articulo->rutadoc;
        $articulo->save();

        //guardar tag de mucho a mucho
        if($request->tags <> NULL) $articulo->tags()->sync($request->tags);
        else $articulo->tags()->detach();

        $sobrasvideos=$request->sobrasvideos;
        if ($sobrasvideos != null) {
            foreach ($sobrasvideos as $sobra) {
                Videosar::where('id_articulos', 'LIKE', $id)->where('path','like',$sobra)->delete();
            }
        }

        if($request->videos <> NULL) {
            $videos = $request->videos;
            $orden=1;
            foreach ($videos as $v =>  $video) {
                /*$videoArt = Videosar::firstOrNew(['id_articulos' => $articulo->id, 'path' => $video]);
                $videoArt->orden = $orden;
                $videoArt->titulo=$titulos[$v];
                $videoArt->save();*/

                try {
                    $model = Videosar::where('id_articulos', 'like', $id)
                    ->where('path','like',$video)
                    ->firstOrFail();
                    $model->orden=$orden;
                    $model->save();
                } catch (ModelNotFoundException $ex) {
                    $videosar = new Videosar;
                    $videosar->path=$video;
                    $videosar->titulo=$titulos[$v];
                    $videosar->orden=$orden;
                    $videosar->id_articulos=$id;
                    $videosar->save();
                }

                $orden++;
            }
        }

        $id_articulo = $articulo->id;
        $dir = public_path().'/galeria/';
        if ($sobras != null) {
            foreach ($sobras as $sobra) {
                Foto::where('path', 'LIKE', $sobra)->delete();
                File::delete($dir.$sobra);
            }
        }

        $galeria=array();
        $fotos = Foto::where('articulo_id','=',$id_articulo)->get();
        foreach ($fotos as $foto) {
            array_push($galeria,$foto->path);
        }

        if ($imagenes != null) {
            $numero =0;
            foreach ($imagenes as $imagen) {
                if (in_array($imagen,$galeria)) {
                    $foto = Foto::where('path','like',$imagen)->firstOrFail();
                    $foto->orden=$numero;
                    $foto->save();
                }else{
                    $foto = new Foto;
                    $foto->titulo = $this->limpiar_caracteres_especiales($imagen);
                    $foto->path = $this->limpiar_caracteres_especiales($imagen);
                    $foto->orden = $numero;
                    $foto->articulo_id = $id_articulo;
                    $foto->save();
                }
                $numero++;
            }
        }

        $dir = public_path().'/doc/';
        if ($sobrasdocs != null) {
            foreach ($sobrasdocs as $sobra) {
                File::delete($dir.$sobra);
                Documentos::where('articulo_id', 'LIKE', $id)->where('path','like',$sobra)->delete();
            }
        }

        if ($rutadocs != null) {
            $orden=1;
            foreach ($rutadocs as $v =>  $rutadoc) {
                $rutadoc=$this->limpiar_caracteres_especiales($rutadoc);
                try {
                    $model = Documentos::where('articulo_id', 'like', $id)
                        ->where('path','like',$rutadoc)
                        ->firstOrFail();
                    $model->orden=$orden;
                    $model->save();
                } catch (ModelNotFoundException $ex) {
                    $doc = new Documentos;
                    $doc->path = $rutadoc;
                    $doc->orden = $orden;
                    $doc->titulo=$titulodocs[$v];
                    $doc->orden=$orden;
                    $doc->articulo_id = $id_articulo;
                    $doc->save();
                }
                $orden++;
            }
        }

        if ($request->inicio!='on') {
            Session::flash('message','Articulo editado Correctamente');
            return redirect::to('articulos') ;
        }else{
            Session::flash('message','Articulo editado Correctamente');
            return redirect()->route('articulos.edit', $id);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articulo = Articulo::find($id);

        $fotos = Foto::where('articulo_id','=',$id)->get();

        $dir = public_path().'/galeria/';
        foreach ($fotos as $foto) {
            File::delete($dir.$foto->path);
        }

        $dir = public_path().'/images/';
        
        if ($articulo->path != null) {
            File::delete($dir.$articulo->path);
        }

        $documentos = Documentos::where('articulo_id','=',$id)->get();
        
        $dir = public_path().'/doc/';
        foreach ($documentos as $doc) {
            File::delete($dir.$doc->path);
        }

        Videosar::where('id_articulos', '=', $id)->delete();
        Documentos::where('articulo_id', '=', $id)->delete();
        Foto::where('articulo_id', '=', $id)->delete();
        Articulo::destroy($id);

        Session::flash('message','Articulo eliminado Correctamente');
        return redirect::to('articulos') ;
    }


    function publicarfb($nomcategoria, $titulo,$mensaje){
        //app la ciudad credenciales
        $fb = new Facebook\Facebook([
            'app_id' => '644942119027203',
            'app_secret' => 'b182c4c6bae44eaab77ca5bf7f65ceb3',
            'default_graph_version' => 'v2.2',
        ]);

        //datos de publicacion
        $nomcategoria=$this->limpiar_caracteres_especiales($nomcategoria);
        $nomcategoria = strtolower($nomcategoria);
        $enlace='http://laciudad.ec/'.$nomcategoria.'/'.$titulo;
        $linkData = [
            'link' => $enlace,
            'message' => $mensaje,
            'value' => 'EVERYONE ',
        ];

        try {

            $helper = $fb->getRedirectLoginHelper();
            if (isset($_GET['state'])) {
                $helper->getPersistentDataHandler()->set('state', $_GET['state']);
            }
            //token generado en FB
            $accessToken = "EAAJKkknnwgMBAKCeXZBr9eIZCJ0xwzQadWdCaev9YekGIZCSOLZBbYe7lXKuSgEdvChFS3whVEKliMSMxgE2pCcydDCvAdZBYZBLmvI1yfQlUy3OisqS55blaT2qtYqfr4jXJtzHyJxSscEd5e7BHPozi9nQdVJFxwJLh46sn4NgZDZD";

            //token prueba
            //EAAIHgKAYDVcBABFLlvMZB8X1JNmjqzCQeNy6iujCQTuI2UjRgfTSDFwt53spOwWuioqlwuGEcydWZBmf4ZCLCkpbkQy9mn9KMKmZCZB3nxPaDNSQz87BRAJPUtuBsfFGGPsptK7seIHyFbApNdsZAQ8ioiESKtQ2z97OfNBkDQ3gZDZD
            //$accessToken = $helper->getAccessToken();
            // Returns a `Facebook\FacebookResponse` object
            //id de la pagina a publicar
            $response = $fb->post('/870146743130480/feed', $linkData, $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

    }


    function creatweets($nomcategoria, $titulo,$mensaje) {

        $url = "https://api.twitter.com/1.1/statuses/update.json";
        $requestMethod = 'POST';

        // configuracion de la cuenta
        $settings = array(
            'oauth_access_token' => '827526089194995712-3NdWhfdrIh8f8zA7OsZFZk670yXpXh9',
            'oauth_access_token_secret' => 'nqDfjFvU1gT7YglQ1dfw8JEIkRFKzsArGUoE9ZhhuEl3Z',
            'consumer_key' => 'tp2K5Xm0X6vdMNgfFHZA9cRYD',
            'consumer_secret' => '9wkYe7lGSL1u9LVhu0YWQj4yU918SlfY61Ko6rGZFBuRb3SKEZ',
        );

        $nomcategoria=$this->limpiar_caracteres_especiales($nomcategoria);
        $nomcategoria = strtolower($nomcategoria);
        $enlace='http://laciudad.ec/'.$nomcategoria.'/'.$titulo;
        $status= $mensaje.'->'.$enlace;

        // establecer el mensaje
        $postfields = array('status' => $status);

        // crea la coneccion con Twitter
        $twitter = new TwitterAPIExchange($settings);
        // envia el tweet
        $twitter->buildOauth($url, $requestMethod)
            ->setPostfields($postfields)
            ->performRequest();
    }

    public function subirfotogaleria()
    {
        if (isset($_FILES["image"])){

            $file = $_FILES["image"];
            //$nombre = $file["name"];
            $nombre=$_POST['nombre'];;
            $nombre=$this->limpiar_caracteres_especiales($nombre);
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = public_path().'/galeria';
            if (!file_exists($carpeta)) {
            mkdir($carpeta, 0775, true);
            }
            $carpeta.='/';
            //$file= Image::make($file)->fit(500, 500);
            if ($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png' && $tipo != 'image/gif'){
                echo "Error, el archivo no es una imagen";
            }else{
                $src = $carpeta.$nombre;
                move_uploaded_file($ruta_provisional, $src);
            }
        }

    }

    public function subirdoc()
    {
        $file = $_FILES["archivo"];
        //$nombre = $file["name"];
        $path=$_POST['path'];
        $path= $this->limpiar_caracteres_especiales($path);
        $ruta_provisional = $file["tmp_name"];
        $carpeta = public_path().'/doc';
        if (!file_exists($carpeta)) {
        mkdir($carpeta, 0775, true);
        }
        $carpeta = public_path().'/doc/';
        $src = $carpeta.$path;

        while (is_file($src)) {
            $path=rand(0, 9999).$path;
            $src =$carpeta.$path;
        }
        move_uploaded_file($ruta_provisional, $src);
    }

    private function obtenerCamposExtras($camposGuardados=[]){
        $tabla = DB::table("camposextras")
            ->join("camposextrasgrupo","camposextrasgrupo.id","=","camposextras.id_campos_extras_grupo")
            ->select("camposextras.*",
            "camposextrasgrupo.id as id_grupo",
            "camposextrasgrupo.descripcion as grupo")
            ->where('camposextrasgrupo.visible', 'si')
            ->orderBy('id','ASC')
            ->get();

        $gruposCampos = [];

        foreach ($tabla as $i => $campo) {
            if(!isset($gruposCampos[$campo->id_grupo])){
                $gruposCampos[$campo->id_grupo] = array(
                    'id' => $campo->id_grupo,
                    'descripcion' => $campo->grupo,
                    'campos' => [],
                );
            }
            $gruposCampos[$campo->id_grupo]['campos'][] = array(
                'id' => $campo->id,
                'nombre' => $campo->nombre,
                'descripcion' => $campo->descripcion,
                'tipo' => $campo->tipo,
                'valor' => $campo->valor,
                'guardado' => isset($camposGuardados[$campo->id]) ? $camposGuardados[$campo->id] : NULL,
            );           
        }
        return $gruposCampos;
    }

    private function guardarArchivo($file, $dir=''){
        if( empty($dir) ) $dir = public_path();
        
        if ( $file->isValid() ){
            $fileDetails = $file->getClientOriginalName();
            $fileExtension = $file->getClientOriginalExtension();
            $fileDetails = $this->limpiar_caracteres_especiales($fileDetails);
            $fileDetails = md5( $fileDetails . microtime() ) . '.' . $fileExtension;
            $file->move($dir, $fileDetails);
            return $fileDetails;
        }else{
            return null;
        }
    }
}