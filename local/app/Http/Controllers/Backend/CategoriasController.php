<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Categoria;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;


use Illuminate\Support\Facades\Input;

class CategoriasController extends Controller
{
    /**  public function __construct(){
       $this->middleware('auth');
       $this->middleware('admin', ['only' =>['edit','update','destroy','create']]);
    }

     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }


          public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
       $categorias = Categoria::orderBy('id','DESC')->paginate(50);
        return view('admin.categoria.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.categoria.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $categoria = new Categoria($request->all());

        ////////////////////////////////// imagenprincipalcategoria
            $imagenprincipal = $request->path;
            $nombreicono = $request->icono;

            $dir = public_path().'/images/';

            $docs = $request->file('path');
            $icono = $request->file('icono');
            // dd($icono);
            // dd($docs);

            if (Input::hasFile('path')){

                $imagenprincipal = $docs->getClientOriginalName();
                $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
                //dd($imagenprincipal);
                $docs->move($dir, $imagenprincipal);
            }

            if (Input::hasFile('icono')){

                $nombreicono = $icono->getClientOriginalName();
                $nombreicono = $this->limpiar_caracteres_especiales($nombreicono);
                //dd($imagenprincipal);
                $icono->move($dir, $nombreicono);
            }

        /////////////////////////

        $categoria->path = $imagenprincipal;
        $categoria->icono = $nombreicono;
        $categoria->cat_destacado = $request->cat_destacado;
        $categoria->save();

        Session::flash('message','Categoria fue creada correctamente');
        return redirect::to('categorias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $categoria= Categoria::find($id);
       return view('admin.categoria.editar',compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // dd($request->all());
         $categoria= Categoria::find($id);
         $categoria->fill($request->all());
         ////////////////////////////////// imagenprincipalcategoria
            $imagenprincipal = $categoria->path;
            $nombreicono = $categoria->icono;

            $dir = public_path().'/images/';

            $docs = $request->file('path');
            $icono = $request->file('icono');
            // dd($icono);
            // dd($docs);

            if (Input::hasFile('path')){

                $imagenprincipal = $docs->getClientOriginalName();
                $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
                //dd($imagenprincipal);
                $docs->move($dir, $imagenprincipal);
            }

            if (Input::hasFile('icono')){

                $nombreicono = $icono->getClientOriginalName();
                $nombreicono = $this->limpiar_caracteres_especiales($nombreicono);
                //dd($imagenprincipal);
                $icono->move($dir, $nombreicono);
            }

        /////////////////////////

         $categoria->path = $imagenprincipal;
         $categoria->icono = $nombreicono;
         $categoria->cat_destacado = $request->cat_destacado;
         $categoria->save();

         Session::flash('message','Categoria actualizada correctamente');
        return redirect::to('categorias');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Categoria::find($id)->delete();


        Session::flash('message','Categoria eliminada correctamente');
        return Redirect::route('categorias.index');

    }
}
