<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use App\CategoriaInterna;
use App\Http\Requests\VideoActualizarRequest;
use App\Http\Requests\VideoCrearRequest;

class VideoController extends Controller
{
    

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
      

         $videos = DB::table("videos")
                ->join("categoriainterna","videos.id_categoriainterna","=","categoriainterna.id")
                ->select("videos.*","categoriainterna.nombre")
                ->get();

        
        return View::make('admin.video.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorias = CategoriaInterna::pluck('nombre','id');
      
         return View::make('admin.video.crear' , compact('categorias'));
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoCrearRequest $request)
    {
        //
        $video = new Video($request->all());
        
        $video->save();
        Session::flash('message','Video guardada correctamente');
        return redirect::to('video');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $video= Video::find($id);
         $categorias = CategoriaInterna::pluck('nombre','id');
       return view('admin.video.editar',compact('video','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VideoActualizarRequest $request, $id)
    {
        //
          $video= Video::find($id);
         $video->fill($request->all());
         $video->save();

         Session::flash('message','Video actualizada correctamente');
         return redirect::to('video');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Video::find($id)->delete();
        
         Session::flash('message','Video eliminada correctamente');
         return Redirect::route('video.index');
    }
}
