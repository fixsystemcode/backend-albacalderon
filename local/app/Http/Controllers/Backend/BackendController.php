<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;

use Auth;
use App\Foto;
use App\Articulo;
use App\User;
use App\Slider;
use App\Video;
use App\Categoria;
use App\VisitasModel;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;



use Illuminate\Support\Facades\Input;

class BackendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
            return $next($request);
        });
    }

    public function index()
    {
        $carticulo = DB::table("articulos")->count();
                /*->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria")->orderBy('created_at','ASC')->get();*/
        if(Auth::user()->isAdmin){
            $articulos = DB::table("articulos")
                    ->join("categorias","articulos.id_categoria","=","categorias.id")
                    ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                    ->select("articulos.*","usuarios.nombres","categorias.categoria")->orderBy('articulos.created_at','DESC')->paginate(5);
        }else{
            $articulos = DB::table("articulos")
                    ->join("categorias","articulos.id_categoria","=","categorias.id")
                    ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                    ->select("articulos.*","usuarios.nombres","categorias.categoria")
                    ->where('articulos.id_usuario', '=', Auth::user()->id )
                    ->orderBy('articulos.created_at','DESC')->paginate(5);
        }

        $categorias = DB::select( DB::raw('SELECT categorias.id, categorias.categoria, (SELECT COUNT(articulos.id) FROM articulos WHERE articulos.id_categoria = categorias.id) AS total FROM categorias '));
        $cslider = Slider::count();
        $cusuarios = User::count();
        $cvideos = Video::count();

        return view('admin.inicio',compact('carticulo','cslider','cusuarios','cvideos','articulos', 'categorias'));
    }
}
