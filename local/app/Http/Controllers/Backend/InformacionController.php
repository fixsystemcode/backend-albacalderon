<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Informacion;
use View;
use Session;
use Redirect;
use App\Traits\UploadTrait;

class InformacionController extends Controller
{
    use UploadTrait;
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $informacion = \Sistema::getInfo();       
        return view('admin.informacion.index', compact('informacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        try {
            $informacion = Informacion::find($id);
            $informacion->nombre = $request->nombre;
            $informacion->direccion = $request->direccion;
            $informacion->telefono = $request->telefono;
            $informacion->email = $request->email;
            $informacion->biografia = $request->biografia;
            $informacion->quienessomos = $request->quienessomos;
            $informacion->mision = $request->mision;        
            $informacion->vision = $request->vision;
            $informacion->cliente = 0;
            $informacion->carrito = 0;
            if (isset($request->carrito)) {
                $informacion->carrito = 1;
                $informacion->cliente = 1;
            }
            if (isset($request->cliente)) {
                $informacion->cliente = 1;
            }       
            if (request()->hasFile('logo')) {
                $informacion->path_logo = $this->saveImage('logo', '/images/pagina/', 'logo');
            }
            if (request()->hasFile('icono')) {
                $informacion->path_ico = $this->saveImage('icono', '/images/pagina/', 'icono');
            }
            $informacion->save();
            Session::flash('message','Información actualizada correctamente');        
            return Redirect::route('informacion.index');
        } catch (\Exception $e) {
            Session::flash('message','La información no ha sido actualizada');        
            return Redirect::route('informacion.index');
        }
        
    }
    private function saveImage($file, $dir, $nombre){
        if (request()->hasFile($file)) {
            $image = request()->file($file);
            $name = str_slug($nombre).time();
            $folder = $dir;
            $this->uploadOne($image, $folder,$name);
            return $name. '.' . $image->getClientOriginalExtension();
        }
        return false;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        return View::make('admin.informacion.crear');
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
        //
         $informacion = new Informacion($request->all());
        $informacion->save();
        Session::flash('message','Artículo fue creado correctamente');
        return redirect::to('informacion');
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        //
        $informacion = Informacion::find($id);
        if (is_null($informacion))
        {
            return Redirect::route('informacion.index');
        }
        return View::make('admin.informacion.editar', compact('informacion'));
    }*/

}
