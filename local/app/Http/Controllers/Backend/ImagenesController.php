<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Imagen;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;
use App\CategoriaInterna;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\ImagenesCrearRequest;




use Illuminate\Support\Facades\Input;


class ImagenesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        //
        
         $imgs = DB::table("imagenes")
                ->join("categoriainterna","imagenes.id_categoriainterna","=","categoriainterna.id")
                ->select("imagenes.*","categoriainterna.nombre")
                ->get();
 
        return View::make('admin.imagen.index', compact('imgs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        $categorias = CategoriaInterna::pluck('nombre','id');
        //return view('admin.imagen.crear');
         return View::make('admin.imagen.crear' , compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImagenesCrearRequest $request)
    {
        //
        $img = new Imagen($request->all());
        
        ////////////////////////////////// imagenprincipal
        $imagenprincipal = $img->path;
       
        $dir = public_path().'/images/';
        $docs = $request->file('path');
        //dd($docs);
        if (Input::hasFile('path')){

            $imagenprincipal = $docs->getClientOriginalName();
            $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
            //dd($imagenprincipal);
            $docs->move($dir, $imagenprincipal);
        }
        ///////////////////////////////////
        $img->path = $imagenprincipal;
        $img->save();
        Session::flash('message','Imagen guardada correctamente');
        return redirect::to('imagen');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $img= Imagen::find($id);
         $categorias = CategoriaInterna::pluck('nombre','id');
       return view('admin.imagen.editar',compact('img','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImagenesActualizarRequest $request, $id)
    {
        //
         $img= Imagen::find($id);
         $img->fill($request->all());

         ////////////////////////////////// imagenprincipal
        $imagenprincipal = $img->path;
        

       

        $dir = public_path().'/images/';

        $docs = $request->file('path');

        //dd($docs);

        if (Input::hasFile('path')){

            $imagenprincipal = $docs->getClientOriginalName();
            $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
            //dd($imagenprincipal);
            $docs->move($dir, $imagenprincipal);
        }
        
        ///////////////////////////////////
        $img->path = $imagenprincipal;
         $img->save();

         Session::flash('message','Imagen actualizada correctamente');
        return redirect::to('imagen');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Imagen::find($id)->delete();
        
        Session::flash('message','Imagen eliminada correctamente');
        return Redirect::route('imagen.index');
    }
}
