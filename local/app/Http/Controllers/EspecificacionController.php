<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EspecificacionModel;

class EspecificacionController extends Controller
{
    private $campos = '[
		{"nombre":"nombre", "tipo":"texto", "texto":"Nombre"},
		{"nombre":"descripcion", "tipo":"texto", "texto":"Descripción"},
		{"nombre":"orden", "tipo":"texto", "texto":"orden"},
		{"nombre":"estado", "tipo":"texto", "texto":"Estado"}
	]';
	private $configuracion = [
		'titulo' => 'Especificaciones', 'url' => 'especificaciones', 'todos'=>false, 'crear'=>true, 'eliminar'=>true
	];

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function($request, $next){
            list($this->puede_leer, $this->puede_crear, $this->puede_editar, $this->puede_eliminar) = \Sistema::obtener_permisos_ruta();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$this->verificaPermisoLeer();
        $tabla = EspecificacionModel::orderby('orden')->get();
        return view('admin.crud.grilla')
            ->with('tabla',$tabla)
            ->with('configuracion',$this->configuracion)
            ->with('campos',json_decode($this->campos));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entradas = $request->all();
		$ruta = $this->configuracion['url'];
		$id = $request->id;
		if($id == 0){
			$msg = "Registro Creado Exitosamente...!";
			$guardar = new EspecificacionModel;
			$guardar->id = $id;
		}else{
			$msg = "Registro Actualizado Exitosamente...!";
			$guardar = EspecificacionModel::find($id);
		}
		$this->validate($request, EspecificacionModel::rules());

		foreach($entradas as $key => $value){
			if($key != "_method" && $key != "_token" && $key != "id") $guardar->$key = $value;
		}
		$guardar->save();

		return $guardar->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	try{
	        $menu = EspecificacionModel::find($id);
	        $menu->delete();
	        return $menu->id;
	    }catch(\Expection $e){
	    	return abort(500,$e);
	    }
    }
}
