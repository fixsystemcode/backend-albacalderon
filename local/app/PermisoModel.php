<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermisoModel extends Model
{
    protected $table = 'usuario_menu_permiso';
}
