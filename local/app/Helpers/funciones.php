<?php 
	
	function appName(){
		return config('app.name', '');
	}
	function appEmpresa(){
		return appName();//Temporal
	}
	function iconPagina(){
		return asset('local/public/images/pagina/' . \Sistema::getInfo()->path_ico);
	}
	function logoPagina(){
		return asset('local/public/images/pagina/'. \Sistema::getInfo()->path_logo);
	}
	function logoPagina2(){
		return asset('admin/images/logo2.png');
	}
	function buildTree($flat, $parent_key, $id_key = null)
	{
		$grouped = array();
		foreach ($flat as $sub){
			$grouped[$sub->$parent_key][] = $sub;
		}
		$fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $id_key) {
			foreach ($siblings as $k => $sibling) {
				$id = $sibling->$id_key;
				if(isset($grouped[$id])) {
					$sibling->children = $fnBuilder($grouped[$id]);
				}
				$siblings[$k] = $sibling;
			}
			return $siblings;
		};
		if(!array_key_exists(0, $grouped)) return $grouped;
		$tree = $fnBuilder($grouped[0]);
		return $tree;
	}

/**** FOTO DE PERFIL DE USUARIO ****/
	function obtenerFotoUsuario($foto, $miniatura=true){
		$ruta_fotos = public_path('perfil/');
		if(empty(trim($foto))) return getDefaultFotoPerfil();
		if($miniatura && file_exists($ruta_fotos.getNameMiniFoto($foto)) ) return getUrlFotoPerfil( getNameMiniFoto($foto) ); 		
		if(file_exists($ruta_fotos.$foto)){
			if($miniatura) return crearMiniFotoPerfil($foto, $ruta_fotos);
			return getUrlFotoPerfil($foto);
		}
		return getDefaultFotoPerfil($foto);
	}

	function getUrlFotoPerfil($foto){
		return asset('local/public/perfil/'.$foto);
	}

	function getNameMiniFoto($foto){
		return 'mini_'.$foto;
	}

	function getDefaultFotoPerfil(){
		return asset('local/public/perfil/avatar.jpg');
	}

	function crearMiniFotoPerfil($foto, $ruta_foto){
		$resultado = resizeImagen($ruta_foto, $foto, 100, 100, getNameMiniFoto($foto));
		return ($resultado) ? getUrlFotoPerfil( getNameMiniFoto($foto) ) : getDefaultFotoPerfil();
	}

	function getImagenCursos(){
		return asset('local/public/images');
	}

	function getDefaultImage(){
		return asset('local/public/images/default.png');
	}

	function resizeImagen($ruta, $nombre, $alto, $ancho, $nombreN, $auto = true){
		try{
			$rutaImagenOriginal = $ruta.$nombre;
		    $calidad = 100;

		    $max_ancho = $ancho;
		    $max_alto = $alto;

		    $extension = str_replace('.', '', strtolower(strrchr($nombre,'.')) );
		    if($extension == 'gif') $_imagen_original = imagecreatefromgif($rutaImagenOriginal);
		    if($extension == 'jpg' || $extension == 'jpeg') $_imagen_original = imagecreatefromjpeg($rutaImagenOriginal);
		    if($extension == 'png') $_imagen_original = imagecreatefrompng($rutaImagenOriginal);
		    
		    list($ancho,$alto) = getimagesize($rutaImagenOriginal);
		    $x_ratio = $max_ancho / $ancho;
		    $y_ratio = $max_alto / $alto;
		    if( ($ancho <= $max_ancho) && ($alto <= $max_alto) && $auto ){//Si ancho 
		  		$ancho_final = $ancho;
				$alto_final = $alto;
			} elseif (($x_ratio * $alto) < $max_alto){
				$alto_final = ceil($x_ratio * $alto);
				$ancho_final = $max_ancho;
			} else{
				$ancho_final = ceil($y_ratio * $ancho);
				$alto_final = $max_alto;
			}

		    $_tmp_imagen = imagecreatetruecolor($ancho_final,$alto_final);
		    if($extension == 'png'){
		    	$transparent = imagecolorallocatealpha($_tmp_imagen, 255, 255, 255, 127);
		    	imagefill( $_tmp_imagen, 0, 0, $transparent ); 
			    imageAlphaBlending($_tmp_imagen, false);
				imageSaveAlpha($_tmp_imagen, true);
		    }		    
		    imagecopyresampled($_tmp_imagen,$_imagen_original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
		    if($extension == 'gif') imagegif($_tmp_imagen, $ruta.$nombreN);  
		    if($extension == 'jpg' || $extension == 'jpeg') imagejpeg($_tmp_imagen, $ruta.$nombreN ,$calidad);
		    if($extension == 'png') imagepng($_tmp_imagen, $ruta.$nombreN);

		    return true;
		}catch(\Exception $e){
			return false;
		}	    
	}
/**** FOTO DE PERFIL DE USUARIO ****/

	function fechaHumana($fecha, $con_hora=false, $corto=false) {
	if ($fecha == null || $fecha == '') {
			return '-';
		}
		$fecha = strtotime($fecha);
		$dia = date('d', $fecha);
		$mes = date('n', $fecha);
		$anio = date('Y', $fecha);
		$hora = date('H:i', $fecha);
		$meses_ES = array('',"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		$meses_ES_corto = array('',"Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic");
		$mes = (!$corto) ? $meses_ES[$mes] : $meses_ES_corto[$mes];
		$texto_add = (!$corto) ? ' a las ' : ' - ';
		return $dia . ' ' . $mes . ', '.$anio . ($con_hora ? ($texto_add.$hora) : '');
	}
	
	function enviar_correo_contactos($desde, $nombre, $mensaje='Mensaje sin cuerpo',$asunto='Sin asunto')
	{
		$para = \Sistema::getCorreoEmpresa();
		$apiKey = \Sistema::getConfig('email_key');
		if(is_null($desde)) return;
			$contenido = "<div>
			<blockquote class='blockquote'>
				<p>" . $mensaje . "</p>
				<footer class='blockquote-footer'>Enviado por: <cite title='Source Title'>" . $nombre . "</cite></footer>
			</blockquote>
			</div>";
			$email = new \SendGrid\Mail\Mail(); 
			$email->setFrom($desde);
			$email->setSubject($asunto);
			$email->addTo($para);
			$email->addContent("text/html", $contenido );
			$sendgrid = new \SendGrid($apiKey);
			try {
			    $response = $sendgrid->send($email);
			   	print $response->statusCode();
			   	return $response->statusCode(). '- Correo Enviado';
			} catch (Exception $e) {
			    echo 'Caught exception: '. $e->getMessage() ."\n";
			    return $e->getMessage();
			}
	}
	// function enviar_correo($para, $nombre, $mensaje='Mensaje sin cuerpo',$asunto='Sin asunto', $desde='logicsoftmanta@gmail.com')
	// {
	// 	if(is_null($para)) return;
	// 		$apiKey = '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';
	// 		$from = new SendGrid\Email(null, $desde);
	// 		$to = new SendGrid\Email($nombre, $para);
	// 		$content = new SendGrid\Content("text/html", $mensaje);
	// 		$mail = new SendGrid\Mail($from, $asunto, $to, $content);
	// 		$sg = new \SendGrid($apiKey);
	// 		$response = $sg->client->mail()->send()->post($mail);
	// 		return $response;
	// }
	function estadoPedido($estado){
		switch($estado){
            case 1: 
                return '<span class="badge badge-warning">Pendiente de pago</span>';
                break;
            case 2:
                return '<span class="badge badge-info">Pagado</span>';
                break;
            case 3:
                return '<span class="badge badge-success">Pago Confirmado</span>';
                break;
            case 4:
                return '<span class="badge badge-danger">Error en el pago</span>';
                break;
            case 5:
                return '<span class="badge badge-primary">Enviado</span>';
                break;
            default:
                return '<span class="badge badge-secundary">Indefinido</span>';
        }
	}
	function separarName($user){
		$palabras = explode(" ", $user);
		$name = array();
		switch (count($palabras)) {
			case 1:
				$name = ["first" => $palabras[0], "second" => "NA", "last"=>"NA"];
				return $name;
				break;
			case 2:
				$name = ["first" => $palabras[0], "second" => "NA", "last"=>$palabras[1]];
				return $name;
				break;
			case 3:
				$name = ["first" => $palabras[0], "second" => $palabras[1], "last"=>$palabras[2]];
				return $name;
				break;
			case 4:
				$name = ["first" => $palabras[0], "second" => $palabras[1], "last"=> $palabras[2]. " ". $palabras[3]];
				return $name;
				break;
			default:
				$name = ["first" => $palabras[0], "second" => $palabras[1], "last"=> $palabras[2]. " ". $palabras[3]];
				return $name;
				break;
		}
	}
	function nombreUsuario($user){
		$palabras = explode(" ", $user);

		if(count($palabras) == 4) return $palabras[0] . " ".  $palabras[1];
		return $palabras[0];
	}

	function apellidoUsuario($user){
		$palabras = explode(" ", $user);

		if(count($palabras) == 4) return $palabras[2] . " " . $palabras[3];
		if(count($palabras) == 3) return $palabras[1] . " " . $palabras[2];
		return $palabras[1];
	}

	function getEstadosCupon($id=null){
        $estado = array(
		'1' => 'Activo', 
		'0' => 'Inactivo');
		if(!is_null($id)){
			return $estado[$id];
		}
		return $estado;
    }

    function MesesCastellano($mes=null){
		$meses = array(
			'1' => 'Enero', 
			'2' => 'Febrero',
			'3' => 'Marzo',
			'4' => 'Abril',
			'5' => 'Mayo',
			'6' => 'Junio', 
			'7' => 'Julio', 
			'8' => 'Agosto',
			'9' => 'Septiembre',
			'10' => 'Octubre',
			'11' => 'Noviembre',
			'12' => 'Diciembre');
		if(!is_null($mes)) return $meses[$mes];
		return $meses;
	}

	function MesesCastellanoCorto($mes=null){
		$meses = array(
			'1' => 'Ene', 
			'2' => 'Feb',
			'3' => 'Mar',
			'4' => 'Abr',
			'5' => 'May',
			'6' => 'Jun', 
			'7' => 'Jul', 
			'8' => 'Ago',
			'9' => 'Sep',
			'10' => 'Oct',
			'11' => 'Nov',
			'12' => 'Dic');
		if(!is_null($mes)) return $meses[$mes];
		return $meses;
	}

	function numeroFormateado($numero, $decimales=2){
		return number_format($numero, $decimales, '.', '');
	}

	function getEstadosCursos($curso=null){
		$estado = array(
			'1' => 'Activo', 
			'0' => 'Inactivo');
		if(!is_null($curso)) return $estado[$curso];
		return $estado;
	}

	function getEstadosModulos($modulo=null){
		$estado = array(
			'1' => 'Activo', 
			'0' => 'Inactivo');
		if(!is_null($modulo)) return $estado[$modulo];
		return $estado;
	}

	function guardarArchivo($file, $dir=''){
        if( empty($dir) ) $dir = public_path();
        
        if ( $file->isValid() ){
            $fileDetails = $file->getClientOriginalName();
            $fileExtension = $file->getClientOriginalExtension();
            $fileDetails = limpiar_caracteres_especiales($fileDetails);
            $fileDetails = md5( $fileDetails . microtime() ) . '.' . $fileExtension;
            $file->move($dir, $fileDetails);
            return $fileDetails;
        }else{
            return null;
        }
    }

    function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        return $s;
    }
?>