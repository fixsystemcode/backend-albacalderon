<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Cviebrock\EloquentSluggable\SluggableInterface;
// use Cviebrock\EloquentSluggable\SluggableTrait;

// class Articulo extends Model implements SluggableInterface
class Articulo extends Model
{

    // use SluggableTrait;

    // protected $sluggable = [
    //     'build_from' => 'titulo',
    //     'save_to'    => 'slug',
    // ];


    //
  protected $table = 'articulos';

  protected $fillable = ['longitud','latitud','titulo', 'slug', 'texto','id_categoria', 'activado', 'destacado',  'path',  'rutadoc','id_usuario', 'seo_keyword', 'seo_descripcion','fechainicio','fechafin','lugar','id_prioridad','descripcionredes'];

  protected $hidden = [];



     public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }
    public function usuario()
    {
        return $this->belongsTo('App\User');
    }

     public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function articulosFotos()
    {
        return $this->hasMany('App\Foto', 'articulo_id');
    }

    public function articulosVideos()
    {
        return $this->hasMany('App\Videosar', 'id_articulos');
    }
    public function articulosDocumentos()
    {
        return $this->hasMany('App\Documentos', 'articulo_id');
    }


}
