<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'usuarios';

    protected $fillable = ['path','nombres','apellidos', 'email', 'telefono', 'login' , 'password', 'fecha_nacimiento' , 'id_tipo_usuario'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles(){
        return $this->hasMany('App\Articulo');
    }

         public function tipos()
    {
        return $this->belongsTo('App\Tipos');
    }

    public function getIsAdminAttribute() {
        return ($this->id_tipo_usuario == 4) ;
    }
    
}
