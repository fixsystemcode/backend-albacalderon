<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videosar extends Model
{
    //
	Protected $table='videosar';
    protected $primarykey='ID';


    protected $fillable = ['path', 'orden','titulo','id_articulos'];


}
