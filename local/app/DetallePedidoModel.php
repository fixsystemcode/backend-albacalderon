<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePedidoModel extends Model
{
    protected $table = 'detalle_pedido';

    public function pedido()
    {
        return $this->belongsTo('App\PedidoModel');
    }
    public function detalleProducto()
    {
        return $this->hasOne('App\tienda', 'id', 'id_producto');
    }
}
