<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
     //
    protected $table = 'tags';

    protected $fillable = ['tag','path', 'categoria_id'];

 	   public function articulos()
    {
        return $this->belongsToMany('App\Articulo');
    }

    public function categoria(){
      return $this->belongsTo('App\Categoria');
    }

    public function scopeSeachTag($query,$tag){

    	return $query->where('tag','=', $tag);
    }
}
