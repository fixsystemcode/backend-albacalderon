<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    //
    protected $table = 'slider';

    protected $fillable = ['titulo', 'path','descripcion', 'enlace', 'orden','id_categoriainterna', 'mostrar', 'tipo'];

 	// protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }

}
