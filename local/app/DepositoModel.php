<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepositoModel extends Model
{
    protected $table = 'depositos';
    public function adjunto()
    {
        return $this->hasOne('App\AdjuntoModel', 'id', 'id_adjunto');
    }
}
