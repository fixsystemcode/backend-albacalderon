<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishlistModel extends Model
{
    protected $table = "wishlist";

    public function wishlistCliente(){
    	return $this->belongsTo('App\ClienteModel', 'id_cliente', 'id');
    }

    public function wishlistProducto(){
    	return $this->belongsTo('App\tienda', 'id_producto', 'id');
    }
}
