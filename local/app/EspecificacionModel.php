<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspecificacionModel extends Model
{
    protected $table = 'especificaciones';

    public static function rules(){
    	return [
    		'descripcion' => 'required',
    		'nombre' => 'required',
    		'estado' => 'required'
    	];
    }
}
