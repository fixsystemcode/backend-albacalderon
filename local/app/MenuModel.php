<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
     protected $table = "menu";

    public static function rules(){
    	return [
    		'ruta' => 'required|max:150',
    		'nombre' => 'required',
    		'visible' => 'required'
    	];
    }
}
