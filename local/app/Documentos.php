<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
    //
    Protected $table='documentos';
    protected $primarykey='ID';


    protected $fillable = ['path', 'orden','articulo_id','titulo'];
}
