<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CamposExtrasGrupos extends Model
{
    //
    protected $table = 'camposextrasgrupo';

    protected $fillable = ['descripcion','orden','visible','icono','vistacampos_id'];

 	protected $hidden = ['remember_token'];

 	public function categorias()
    {
        return $this->belongsToMany('App\Categoria','categoriacamposextrasgrupo','id_campos_extras_grupo');
    }
}
