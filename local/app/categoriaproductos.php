<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoriaproductos extends Model
{
    //
    protected $table = "categoriaproductos";
    protected $fillable = ['categoria', 'descripcion', 'destacado', 'path'];

    // public function articulos(){
    // 	return $this->hasMany('App\Articulo');
    // }
}
