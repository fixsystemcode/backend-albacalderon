<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuPaginaModel extends Model
{
    protected $table = "menu_pagina";

    public static function rules(){
    	return [
    		'ruta' => 'required|max:150',
    		'nombre' => 'required',
    		'visible' => 'required'
    	];
    }

}
