<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacion extends Model
{
    //
     protected $table = 'informacion';

    protected $fillable = ['nombre','direccion','telefono','quienessomos','biografia', 'email'];

}
