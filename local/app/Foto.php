<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    //

    protected $table = 'fotos';

    protected $fillable = ['titulo', 'path','articulo_id','orden'];

  	protected $hidden = [];



      public function articulo()
    {
        return $this->belongsTo('App\Articulo');
    }
}
