<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class galeriaproducto extends Model
{
    //
    protected $table = 'galeriaproducto';

    protected $fillable = ['titulo', 'path','orden','id_producto'];

  	protected $hidden = [];



      public function producto()
    {
        return $this->belongsTo('App\tienda');
    }
}
