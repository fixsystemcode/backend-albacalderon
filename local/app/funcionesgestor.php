<?php
use App\Articulo;
use App\Informacion;
use App\Slider;
use App\Imagen;
use App\Foto;
use App\VisitasModel;
use App\Categoria;
use App\CamposExtrasGrupos;
use App\CamposExtras;



  function articulostotal($paginate)
    {
       $articulos = DB::table("articulos")
	                ->join("categorias","articulos.id_categoria","=","categorias.id")
	                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
	                ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
	                ->orderBy('created_at','DESC')
	                ->where("articulos.activado", true)
	                ->paginate($paginate); 

	    return $articulos;
    }

    function traeretiquetasarticulo($articulo){

      $tags = Articulo::join("articulo_tag","articulos.id","=","articulo_tag.articulo_id")
        ->join("tags","tags.id","=","articulo_tag.tag_id")
        ->select("tags.tag")
        ->where("articulo_tag.articulo_id","=",$articulo)
        ->where("articulos.activado", true)
        //->lists("tag");
        ->get();
      
      return $tags;
    }

    function traerarticulo($slug){

      $articulo = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.id as id_categoria","categorias.icono")
                ->where("articulos.activado", true)
                ->where('slug','=', $slug)
                ->first();
      return $articulo;
    }

    function fotosdearticulos($articulo){
      
      $fotos = Foto::where('articulo_id','=',$articulo)->get();

      return $fotos ;
    }

    function camposextras(){

       


    }



    function numerovisitas($articulo){
        $hoy = date("Y-m-d");
                    Session::put('ipvistante',$_SERVER['REMOTE_ADDR']);
                    $ipvi = Session::get('ipvistante');

                    //dd($ipvi);

                   
                    

                    $visitas = VisitasModel::where("fecha",$hoy)
                    ->where("ip",$ipvi)
                    ->where("articulo_id",$articulo)
                    ->get();

                     
                     //dd($visitas);

                    if (count($visitas)) {
                        
                    }
                    else{
                        $visita = new VisitasModel;
                        $visita->fecha = $hoy;
                        $visita->ip = $ipvi;
                        $visita->articulo_id = $articulo;
                        $visita->num = 1;
                        $visita->save();
                    }
                    
                    //dd($visitas);  

                    
                    $nvisitas = VisitasModel::where("articulo_id",$articulo)
                    ->get();

                    $numerovisitas = count($nvisitas);

        return $numerovisitas;
    }

    function categoriasdestacadas()
    {
       $articulos = DB::table("categorias")
                  ->select("categorias.*")
                  ->where("categorias.cat_destacado", 1)
                  ->get(); 

      return $articulos;
    }

    function articuloscategoriasdestacadas()
    {
       $articulos = DB::table("articulos")
                  ->join("categorias","articulos.id_categoria","=","categorias.id")
                  ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                  ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.cat_destacado","categorias.icono")
                  ->orderBy('id','DESC')
                  ->where("categorias.cat_destacado", 1) 
                  ->get(); 
                  
      return $articulos;
    }

    function ultimosarticulos($id_categoria,$numerodearticulos){

      $artiuclos = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                 ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                 ->orderBy('like','DESC')
                 ->where('categorias.id', $id_categoria)->where("articulos.activado", true)->paginate($numerodearticulos);

      return $artiuclos;
    }


    function articulosporcategoria($categoria,$paginacion)
    {
       $articulos = null;
       if ($paginacion == 0) {

       	    $articulos = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                 ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                 ->orderBy('like','DESC')
                 ->where('categorias.categoria', $categoria)
                 ->where("articulos.activado", true)
                 ->get();

       }else{

       		$articulos = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                 ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                 ->orderBy('like','DESC')
                 ->where('categorias.categoria', $categoria)
                 ->where("articulos.activado", true)
                 ->paginate($paginacion);
       }

       
       	// dd($articulos);
        return $articulos; 
    }

    function articulosporcategoriaid($categoria,$paginacion)
    {
       $articulos = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                 ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                 ->orderBy('like','DESC')
                 ->where('categorias.id', $categoria)
                 ->where("articulos.activado", true)
                 ->paginate(7);

        return $articulos; 
    }


    function articuloporslug($slug)
    {
       $articulo = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.id as id_categoria","categorias.icono")
                ->where("articulos.activado", true)
                ->where('slug','=', $slug)
                ->first();

        return $articulo; 
    }

    
///////////////////////// helper admin


?>

