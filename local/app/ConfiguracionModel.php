<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionModel extends Model
{
    protected $table = 'configuracion';

    public static function rules(){
    	return [
            'clave' => 'required|max:100'
        ];
    }
}
