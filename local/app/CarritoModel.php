<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarritoModel extends Model
{
    protected $table = 'carrito';

    public function politica()
    {
        return $this->hasOne('App\AdjuntoModel', 'id', 'id_adjunto');
    }
}
