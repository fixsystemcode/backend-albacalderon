<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'google' => [ 
        'client_id' => '837353362442-4njlqbuus044i355mh8ul9hq2ublm6kc.apps.googleusercontent.com', 
        'client_secret' => 'xfzErIIeDBsBp8q_NgBTZP4c', 
        'redirect' => 'http://localhost/fs/fixsystem/cliente/callback/google', 
    ],

    'facebook' => [ 
        'client_id' => '538144190207344', 
        'client_secret' => 'd99a75f38c8dfc139b6e2b98af404bee', 
        'redirect' => 'http://localhost/fs/fixsystem/cliente/callback/facebook', 
    ],
    // 'google' => [ 
    //     'client_id' => '837353362442-dqace84igfcjsa9jhqt21kgddnaqgvmq.apps.googleusercontent.com', 
    //     'client_secret' => 'yCHTJfpmsXk6riSlpFGEEnw3', 
    //     'redirect' => 'http://floristeria-ilusion.com/fixsystem/cliente/callback/google', 
    // ],

    // 'facebook' => [ 
    //     'client_id' => '538144190207344', 
    //     'client_secret' => 'd99a75f38c8dfc139b6e2b98af404bee', 
    //     'redirect' => 'http://floristeria-ilusion.com/cliente/callback/facebook', 
    // ],

];
