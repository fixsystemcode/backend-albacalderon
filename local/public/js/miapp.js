function open_ajax(){ $('#_modal_wait').modal(); }
function close_ajax(){ $('#_modal_wait').modal('hide'); }

function ajax(ruta, metodo, datos, done, fail, always) {
	if(typeof done !== 'function') done = function() {};
	if(typeof fail !== 'function') fail = function() {};
	if(typeof always !== 'function') always = function() {};
	$.ajax({ url: ruta,type: metodo, data: datos }).done(done).fail(fail).always(always);
}

function validacionCampos(jqXHR, textStatus, errorThrown) {
	if (jqXHR.status == 422) {
		var errores = '<ul>';
		var ObjetoDeErrores = jqXHR.responseJSON.errors;
		for (var clave in ObjetoDeErrores){
			if (ObjetoDeErrores.hasOwnProperty(clave)) {
				for (var i = 0; i < ObjetoDeErrores[clave].length; i++) {
					errores+='<li>'+ObjetoDeErrores[clave][i]+'</li>';
				}
			}
		}
		errores+='</ul>';
		showToast('Error de Validación',errores,'warning',10000);
	}else if (jqXHR.status == 401) {
		showToast('Su sesion expiro','Su sesion expiro, por favor inicie sesion nuevamente','error',5000);
		setTimeout(goToLogin, 3000);
	}else if (jqXHR.status == 404) {
		showToast('URL no encontrada','No se encontró la URL solicitada','error',5000);
	}else if (jqXHR.status == 500) {
		showToast('Error de Servidor','Error interno del servidor, recargue la página e inténtelo de nuevo, si el problema persiste comuniquelo a sistema','error',10000);
	}else{
		error_coneccion_ajax();
	}
}

function error_coneccion_ajax() {
	showToast('Error de Conexión','No se pudo establecer conexión con el servidor.<br/> Inténtelo nuevamente','error',5000);
}

function bloquearPantalla(estado, bloqueo_id){
	if(estado){
		$(bloqueo_id).block({
			message: '<strong><i class="fa fa-spinner"></i><br>Generando. Por favor espere :)</strong>',
			centerY: 0,
			css: {
				border: 'none',
				padding: '15px',
				backgroundColor: '#fff',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: 1,//.5,
				color: '#000',
				top: '30px'
			}
		});
	}else{
		setTimeout(function(){ $(bloqueo_id).unblock(); }, 200);
	}
}
function validacionCampos(jqXHR, textStatus, errorThrown) {
	if (jqXHR.status == 422) {
		var errores = '<ul>';
		var ObjetoDeErrores = jqXHR.responseJSON.errors;
		for (var clave in ObjetoDeErrores){
			if (ObjetoDeErrores.hasOwnProperty(clave)) {
				for (var i = 0; i < ObjetoDeErrores[clave].length; i++) {
					errores+='<li>'+ObjetoDeErrores[clave][i]+'</li>';
				}
			}
		}
		errores+='</ul>';
		showToast('Error de Validación',errores,'warning',10000);
	}else if (jqXHR.status == 401) {
		showToast('Su sesion expiro','Su sesion expiro, por favor inicie sesion nuevamente','error',5000);
		setTimeout(goToLogin, 3000);
	}else if (jqXHR.status == 404) {
		showToast('URL no encontrada','No se encontró la URL solicitada','error',5000);
	}else if (jqXHR.status == 500) {
		showToast('Error de Servidor','Error interno del servidor, recargue la página e inténtelo de nuevo, si el problema persiste comuniquelo a sistema','error',10000);
	}else{
		error_coneccion_ajax();
	}
}
function showToast(head, content, icon, time) {
	$.toast({
		heading: head,
		text: content,
		position: 'top-right',
		stack: 3,
		icon: icon,
		hideAfter: time,
		showHideTransition: 'plain',
		allowToastClose: true,
		loader: false,
	});
}

function fechaHumana(fecha, sin_hora){
	if (fecha == null || fecha == '' || fecha == 'null') return '';
	if(typeof sin_hora === 'undefined') sin_hora = true;
	if(sin_hora) return moment(fecha,"YYYY-MM-DD H:mm:ss").format('D MMMM, YYYY');
 	return moment(fecha,"YYYY-MM-DD H:mm:ss").format('D MMMM, YYYY [a las] H:mm');
}
function fechaHumana2(fecha, con_hora=true){
	if (fecha == null || fecha == '' || fecha == 'null') return '';
	if(typeof con_hora === 'undefined') con_hora = true;
	if(con_hora) return moment(fecha,"YYYY-MM-DD H:mm:ss").format('D MMM YYYY [-] H:mm');
	return moment(fecha,"YYYY-MM-DD H:mm:ss").format('D MMM YYYY');
 	
}

function  getEstadosModulos(key){
	var estado = {'0': 'Inactivo', '1': 'Activo'};
	return estado[key];
}
