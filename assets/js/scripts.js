/*PopUp on load*/
$(window).on('load', function(){
        $('.popup').fadeIn();
        $('.popup_in').addClass('normal');
})
function closeDialog(id) {
    $('.popup_in').removeClass('normal');
    $('.popup').fadeOut();
}
$('#popup').on('click', function(){
    $('.popup_in').removeClass('normal');
    $('.popup').fadeOut();
})

/*** Gallery carousel***/
/***********************/
$(document).ready(function(){
	/*Slider*/
	$(".bx-slidah").bxSlider({
        auto: true,
        speed: 800,
        pause: 9000,
        mode: 'fade',
        controls: false,
        touchEnabled: false
    });

	/*Parallax effect slider */
    $(window).scroll(function(){
        var barra = $(window).scrollTop();
        var opa = 1 - (barra * 0.0015);
        var trans = barra * 0.08;
        console.log(trans);
        if (opa <= 0) {
            return;
        }else{
            $('.sls_context').css({'opacity': opa, 'transform': 'translateY('+trans+'px)'})
        }
    });

});
