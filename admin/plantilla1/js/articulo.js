$( document ).ready(function() {


	var subirdoc;

	document.getElementById('categorias').onchange = function () {
		if (this.value=="19") {
			document.getElementById('calendario').style="";
		}else{
			document.getElementById('calendario').style="display:none;";
		}

	}



	document.getElementById('doc').onchange = function () {
		console.log('doc');
		var dataurl;
		var filesToUpload = this.files;
		var documento = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var cadena = documento.name.replace(" ","");
		num = Math.floor(Math.random() * 10000001);
		num+=cadena;
		var btncanc = document.getElementById('btncancelardoc');
		var bar = document.getElementById('barra');
		var percent = document.getElementById('porcentaje');
		var mensaje = document.getElementById('subiendodoc');
		var btn = document.getElementById('botonsubirdoc');
		//Ahora para crear el objeto option que le vas a añadir seria
		var formData = new FormData();
		formData.append('path',num);
		formData.append('archivo',documento);

		subirdoc =$.ajax({
			url: "/uploaddoc",
			type: "post",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},

			beforeSend: function() {
				var percentVal = '0%';
				bar.style='width:90%';
				percent.style="width:"+percentVal;
				percent.innerHTML=percentVal;
				mensaje.style="";
				btn.style="display:none;";
				btncanc.style="";
				bar.className="progress progress-striped active";
				percent.className="progress-bar progress-bar-success";


			},
			xhr: function()
			{
				var xhr = new window.XMLHttpRequest();
    			//Upload progress
    			xhr.upload.addEventListener("progress", function(evt){
    				if (evt.lengthComputable) {
    					var percentComplete = Math.round((evt.loaded / evt.total)*100)+"%";
        				//Do something with upload progress
        				percent.style.width=percentComplete;
        				percent.innerHTML=percentComplete;
        		}
        		}, false);


    			return xhr;
    		},

    		success: function(datos)
    		{
				//crear li
				var lis=document.createElement('li');
				lis.id=num;
				lis.innerHTML="<span onclick='eliminardoc(this)' title='Eliminar Documento'><img  style='height: 20px;' src='/assets/images/elidoc.png'></span> &nbsp<a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+documento.name+"</a> <span title='Visualizar Documento'> &nbsp<a href='/local/public/doc/"+num+"' target='_blank' ><img  style='height: 20px;' src='/assets/images/verdoc.png'></a></span>";
				document.getElementById("listadocs").appendChild(lis);
				document.getElementById(num).className = "list-group-item active";
				var option=document.createElement("option");
				option.value=num;
				option.text=num;
				option.selected=true;
				document.getElementById("rutadocs").appendChild(option);
				var option=document.createElement("option");
				option.value=documento.name;
				option.text=documento.name;
				option.selected=true;
				document.getElementById("titulodocs").appendChild(option);
				var percentVal = '100%';
				bar.style='width:40%';
	        	percent.style.width=percentVal;
	        	mensaje.style="display:none;";
				percent.innerHTML="Documento Subido";

				bar.className="progress";
				percent.className="progress-bar";
				btn.style="";
				btncanc.style="display:none;";
				window.setTimeout(function(){bar.style='width:90%;display:none;';}, 3000);
			},
			 error: function(data){
			 	document.getElementById("doc").value = "";
                bar.className="progress progress-bar-danger";
                btncanc.style="display:none;";
                percent.style.width='100%';
                bar.style='width:60%';
                //bar.className="progress progress-bar-danger";
                percent.className="progress-bar-danger";
                percent.innerHTML="Documento no Subido";
                btn.style="";
                mensaje.style="display:none;";

                window.setTimeout(function(){bar.style='width:90%;display:none;';}, 3000);

            }

        });
	}

	document.getElementById('btncancelardoc').onclick =function() {
		if (subirdoc != null) {
			subirdoc.abort();
			subirdoc = null;
		}
	};


	//Imagen Principal
	document.getElementById('imagenprincipal').onchange = function () {
		var dataurl = null;
		var filesToUpload = document.getElementById('imagenprincipal').files;//input de imagen
		var file = filesToUpload[0];
		// Create an image
		var img = document.createElement("img");
		// Create a file reader
		var reader = new FileReader();
		// Set the image once loaded into file reader
		reader.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				document.getElementById('imgprincipal').src = dataurl;
			}
		}

		reader.readAsDataURL(file);

	}

	//galeria dinamica
	$("ul.reorder-photos-list").sortable({ tolerance: 'pointer' });
	$("ul.reorder-documentos-list").sortable({ tolerance: 'pointer' });
	$("ul.reorder-videos-list").sortable({ tolerance: 'pointer' });
	$('#reorder-helper').slideDown('slow');
	$('.image_link').attr("href","javascript:void(0);");
	$('.image_link').css("cursor","move");
	document.getElementById('file-1').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-1').src=dataurl;
				document.getElementById('imgfile-1').name=num;
				document.getElementById('elim1').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-2').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-2').src=dataurl;
				document.getElementById('imgfile-2').name=num;
				document.getElementById('elim2').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-3').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-3').src=dataurl;
				document.getElementById('imgfile-3').name=num;
				document.getElementById('elim3').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-4').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-4').src=dataurl;
				document.getElementById('imgfile-4').name=num;
				document.getElementById('elim4').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-5').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-5').src=dataurl;
				document.getElementById('imgfile-5').name=num;
				document.getElementById('elim5').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-6').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-6').src=dataurl;
				document.getElementById('imgfile-6').name=num;
				document.getElementById('elim6').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-7').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-7').src=dataurl;
				document.getElementById('imgfile-7').name=num;
				document.getElementById('elim7').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-8').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-8').src=dataurl;
				document.getElementById('imgfile-8').name=num;
				document.getElementById('elim8').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-9').onchange = function () {
		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-9').src=dataurl;
				document.getElementById('imgfile-9').name=num;
				document.getElementById('elim9').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	document.getElementById('file-10').onchange = function () {

		var dataurl;
		var filesToUpload = this.files;
		var foto = filesToUpload[0];
		var img = document.createElement("img");
		var num;
		var readergale = new FileReader();
		readergale.onload = function(e)
		{
			img.src = e.target.result;
			img.onload = function () {
				var canvas = document.createElement("canvas");
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var MAX_WIDTH = 1000;
				var MAX_HEIGHT = 1000;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				dataurl = canvas.toDataURL("image/png");
				var cadena = foto.name.replace(" ","");
				num = Math.floor(Math.random() * 10000001);
				num+=cadena;
				document.getElementById('imgfile-10').src=dataurl;
				document.getElementById('imgfile-10').name=num;
				document.getElementById('elim10').style="";

				var formData = new FormData();

				var blobBin = atob(dataurl.split(',')[1]);
				var array = [];
				for(var i = 0; i < blobBin.length; i++) {
					array.push(blobBin.charCodeAt(i));
				}
				var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
				formData.append("image", file,num);
				formData.append('nombre', num);
				$.ajax({
					url: "/upload",
					type: "post",
					data: formData,
					processData: false,
					contentType: false,
					headers: {"X-CSRF-Token": $('meta[name=_token]').attr('content')},
					success: function(datos)
					{

					}
				});
			}
		}
		readergale.readAsDataURL(foto);
	}
	$("#my-dropzone").on('submit', function(evt){

		$("#ruta").empty();
		$("ul.reorder-photos-list li a label img").each(function() {
			if ($(this).attr('name')!=undefined) {
				if ($(this).attr('name')!="nada")   {
					var option=document.createElement("option");
					option.value=$(this).attr('name');
					option.text=$(this).attr('name');
					option.selected=true;
					document.getElementById("ruta").appendChild(option)
				}
			}
		});
		$("#videos").empty();
		$("ul.reorder-videos-list li").each(function() {

			var option=document.createElement("option");
			option.value=$(this).attr('id');
			option.text=$(this).attr('id');
			option.selected=true;
			document.getElementById("videos").appendChild(option);


		});
		$("#titulovideo").empty();
		$("ul.reorder-videos-list li a").each(function() {
			if ($(this)["0"].innerText!="") {
				var option=document.createElement("option");
				option.value=$(this)["0"].innerText;
				option.text=$(this)["0"].innerText;
				option.selected=true;
				document.getElementById("titulovideo").appendChild(option);

			}
		});
		$("#rutadocs").empty();
		$("#titulodocs").empty();
		$("ul.reorder-documentos-list li").each(function() {

			var option=document.createElement("option");
			option.value=$(this).attr('id');
			option.text=$(this).attr('id');
			option.selected=true;
			document.getElementById("rutadocs").appendChild(option);
			var option=document.createElement("option");
			option.value=$(this)["0"].innerText;
			option.text=$(this)["0"].innerText;
			option.selected=true;
			document.getElementById("titulodocs").appendChild(option);



		});

	});
});

/**
* Funcion que añade un <li> dentro del <ul>
*/
function add_li()
{
	var nuevoLi=document.getElementById("nuevo_video").value;
	var expreg = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
	if(expreg.test(nuevoLi))
	{
		if(find_li(nuevoLi))
		{
			if (nuevoLi.indexOf("youtube.com") != -1) {
				var videourl = nuevoLi;
				var titulo="";
				videourl =videourl.substr(nuevoLi.indexOf("?v=")+3,11);
				url = 'https://www.googleapis.com/youtube/v3/videos?id='+videourl+'&key=AIzaSyB7coy2hS2NyeJucEztL4ytwUcYjdGHrrg&fields=items(id,snippet(title))&part=snippet';
				datos = {};
					//obtener titulo dle vidoe Youtube
					$.getJSON(url, function(datos){
						$.each(datos.items,function(indice){
							titulo=datos.items[indice].snippet.title;

							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)' title='Eliminar Video de la Lista'> <img  style='height: 25px;' src='/assets/images/delvideo.png' ></span> &nbsp <a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")' title='Reproducir Video'> &nbsp<a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='/assets/images/play.png'></span></a>";


							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
						});
						document.getElementById(nuevoLi).className = "list-group-item active";


					});


				}
				if (nuevoLi.indexOf("youtu.be/") != -1) {
					var videourl = nuevoLi;
					var titulo="";
					videourl =videourl.substr(nuevoLi.indexOf("be")+3,11);
					url = 'https://www.googleapis.com/youtube/v3/videos?id='+videourl+'&key=AIzaSyB7coy2hS2NyeJucEztL4ytwUcYjdGHrrg&fields=items(id,snippet(title))&part=snippet';
					datos = {};
					//obtener titulo del video Youtube
					$.getJSON(url, function(datos){
						$.each(datos.items,function(indice){
							titulo=datos.items[indice].snippet.title;

							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)'> <img  style='height: 25px;' src='/assets/images/delvideo.png'></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='/assets/images/play.png'></span></a>";


							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
						});
						document.getElementById(nuevoLi).className = "list-group-item active";

					});


				}
				if (nuevoLi.indexOf("vimeo.com") != -1) {
					var videourl = nuevoLi;
					var titulo="";
					videourl =videourl.substr(nuevoLi.indexOf("om")+3,10);
					url = 'https://vimeo.com/api/v2/video/'+videourl+'.json';
					datos = {};
					//obtener titulo del viideo de vimeo
					$.getJSON( url, function( datos ){
						$.each(datos,function(indice){
							titulo=datos[indice].title;
							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)'><img  style='height: 25px;' src='/assets/images/delvideo.png' ></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+"</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='/assets/images/play.png'></span></a>";


							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)

						});
						document.getElementById(nuevoLi).className = "list-group-item active";

					});


				}
				if (nuevoLi.indexOf("facebook.com") != -1) {
					var videourl = nuevoLi;
					var titulo="";
					var inicio,fin=0;
					inicio=nuevoLi.indexOf("pcb.");

					if (inicio != -1) {
						fin=nuevoLi.indexOf("/",inicio+4);

						videourl= videourl.slice(inicio+4,fin);


					}else{
						inicio=nuevoLi.indexOf("os/");
						fin=nuevoLi.indexOf("/",inicio+3);
						videourl =videourl.slice(inicio+3,fin);

					}
					url = 'https://graph.facebook.com/v2.9/'+videourl+'?access_token=EAAJKkknnwgMBAKCeXZBr9eIZCJ0xwzQadWdCaev9YekGIZCSOLZBbYe7lXKuSgEdvChFS3whVEKliMSMxgE2pCcydDCvAdZBYZBLmvI1yfQlUy3OisqS55blaT2qtYqfr4jXJtzHyJxSscEd5e7BHPozi9nQdVJFxwJLh46sn4NgZDZD';
					datos = {};
					//obtener titulo del video
					$.getJSON(url, function(datos){
						$.each(datos,function(indice){
							titulo=datos.description;
							var lis=document.createElement('li');
							lis.id=nuevoLi;
							lis.innerHTML="<span onclick='eliminar(this)'><img  style='height: 25px;' src='/assets/images/delvideo.png'></span><a href='javascript:void(0);' style='float:none; color: white;' class='image_link'>"+titulo+ "</a><span onclick='modificamodal(\""+nuevoLi+"\")'><a href='' data-target='#modal-vervideo' data-toggle='modal'><img  style='height: 25px;' src='/assets/images/play.png' ></span></a>";


							document.getElementById("listaDesordenada").appendChild(lis);
							var options=document.createElement("option");
							options.value=titulo;
							options.text=titulo;
							options.selected=true;
							document.getElementById("titulovideo").appendChild(options)
							return false;

						});
						document.getElementById(nuevoLi).className = "list-group-item active";

					});


				}
				//Ahora para crear el objeto option que le vas a añadir seria
				var option=document.createElement("option");
				option.value=nuevoLi;
				option.text=nuevoLi;
				option.selected=true;
				document.getElementById("videos").appendChild(option)
			}
		}else{
			alert("Ingrese URL válida");
			document.getElementById("nuevo_video").value="";
			return false;
		}

		document.getElementById("nuevo_video").value="";
	}
	function modificamodal(url){
		reprod= document.getElementById("reproductor");

		if (url.indexOf("youtube.com") != -1) {
			url=url.substr(url.indexOf("?v=")+3,11);
			reprod.innerHTML="<iframe width='500' height='400' src='https://www.youtube.com/embed/"+url+"?rel=0' frameborder='0'  encrypted-media' allowfullscreen></iframe>";
		}
		if (url.indexOf("youtu.be/") != -1) {
			url=url.substr(url.indexOf("be")+3,11);
			reprod.innerHTML="<iframe width='500' height='400' src='https://www.youtube.com/embed/"+url+"?rel=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>";
		}
		if (url.indexOf("vimeo.com") != -1) {
			url=url.substr(url.indexOf("om")+3,10);
			reprod.innerHTML="<iframe src='https://player.vimeo.com/video/"+url+"' width='500' height='400' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
		}
		if (url.indexOf("facebook.com") != -1 ) {
			var inicio,fin=0;
			inicio=url.indexOf("pcb.");


			if (inicio != -1) {
				fin=url.indexOf("/",inicio+4);

				url= url.slice(inicio+4,fin);


			}else{
				inicio=url.indexOf("os/");
				fin=url.indexOf("/",inicio+3);
				url =url.slice(inicio+3,fin);

			}

			reprod.innerHTML="<iframe src='https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook%2Fvideos%2F"+url+"%2F&width=500&show_text=false&appId=571198976560471&height=280' width='500' height='280' style='border:none;overflow:hidden' scrolling='no' frameborder='0' allowTransparency='true' allowFullScreen='true'></iframe>";



		}
	}
/**
* Funcion que busca si existe ya el <li> dentrol del <ul>
* Devuelve true si no existe.
*/
function find_li(contenido)
{
	var bandera=true;
	if (contenido.indexOf("youtube.com") != -1 ||contenido.indexOf("youtu.be") != -1 || contenido.indexOf("facebook.com") != -1 || contenido.indexOf("vimeo.com") != -1) {

		$("#listaDesordenada li").each(function(){

			if(this.id == contenido){
				bandera = false;
				return bandera;
			}
		});



	}else{
		alert("Solo se aceptan videos de Youtube, Facebook y Vimeo");
		bandera = false;
		return bandera;

	}

	return bandera;
}
/**
* Funcion para eliminar los elementos
* Tiene que recibir el elemento pulsado
*/
function eliminar(elemento)
{
	var id=elemento.parentNode.getAttribute("id");
	var option=document.createElement("option");
	option.value=id;
	option.text=id;
	option.selected=true;
	document.getElementById("sobrasvideos").appendChild(option);
	node=document.getElementById(id);
	node.parentNode.removeChild(node);
	$("#videos option[value='"+id+"']").remove();


}
function eliminardoc(elemento)
{
	var id=elemento.parentNode.getAttribute("id");
	node=document.getElementById(id);
	node.parentNode.removeChild(node);
	$("#rutadocs option[value='"+id+"']").remove();
	var option=document.createElement("option");
	option.value=id;
	option.text=id;
	option.selected=true;
	document.getElementById("sobrasdocs").appendChild(option);

}
function eliminarElemento(id,elim){

	imagen = document.getElementById(id);
	elimin=document.getElementById(elim);
	if (!imagen.id){
		alert("El elemento selecionado no existe");
	} else {
		var option=document.createElement("option");
		option.value=imagen.name;
		option.text=imagen.name;
		option.selected=true;
		document.getElementById("sobras").appendChild(option);
		imagen.src="/assets/images/no-imagen.jpg";
		elimin.style="display:none;";
		imagen.name="nada";
	}
}