-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-05-2021 a las 23:21:55
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `backend_alba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adjuntos`
--

CREATE TABLE `adjuntos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL COMMENT '1 = depositos',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `adjuntos`
--

INSERT INTO `adjuntos` (`id`, `nombre`, `path`, `tipo`, `created_at`, `updated_at`) VALUES
(12, 'deposito-I9MEU3Oh5IQhakeDuGg6ay8Rh-20200409.png', '/depositos/deposito-I9MEU3Oh5IQhakeDuGg6ay8Rh-20200409.png', 1, '2020-04-10 00:12:04', '2020-04-10 00:12:04'),
(13, 'deposito-xJ6FHkStGzglfu8AyhIWGvLl1-20200410.pdf', '/depositos/deposito-xJ6FHkStGzglfu8AyhIWGvLl1-20200410.pdf', 1, '2020-04-10 16:38:18', '2020-04-10 16:38:18'),
(14, 'deposito-uLxaiarjEN3Kgnfiklk7BpQYM-20200410.pdf', '/depositos/deposito-uLxaiarjEN3Kgnfiklk7BpQYM-20200410.pdf', 1, '2020-04-10 17:02:28', '2020-04-10 17:02:28'),
(15, 'deposito-KepZTRNbl2iIPBN20bMKD0WP8-20200410.pdf', '/depositos/deposito-KepZTRNbl2iIPBN20bMKD0WP8-20200410.pdf', 1, '2020-04-10 21:25:22', '2020-04-10 21:25:22'),
(16, 'deposito-doG2JhZBns5h349xIAZLAmjTj-20200410.jpeg', '/depositos/deposito-doG2JhZBns5h349xIAZLAmjTj-20200410.jpeg', 1, '2020-04-10 22:35:04', '2020-04-10 22:35:04'),
(17, 'deposito-0t9KPTiQUfqZHPDHk4zOQBfz9-20200415.txt', '/depositos/deposito-0t9KPTiQUfqZHPDHk4zOQBfz9-20200415.txt', 1, '2020-04-15 19:55:00', '2020-04-15 19:55:00'),
(18, 'politica-BsGlV5uVwUVNQnvapv28ucO2q-20200415.txt', '/politica/politica-BsGlV5uVwUVNQnvapv28ucO2q-20200415.txt', 2, '2020-04-15 19:58:11', '2020-04-15 19:58:11'),
(19, 'politica-9hqTRdc6oE11FZo462FtuSKKG-20200415.jpg', '/politica/politica-9hqTRdc6oE11FZo462FtuSKKG-20200415.jpg', 2, '2020-04-15 20:04:54', '2020-04-15 20:04:54'),
(20, 'deposito-RN5JApdhdij93cyUOY9RxDnGw-20200415.txt', '/depositos/deposito-RN5JApdhdij93cyUOY9RxDnGw-20200415.txt', 1, '2020-04-15 20:06:47', '2020-04-15 20:06:47'),
(21, 'politica-ZQt0pwu6UiTE4xaccYzkiNVuo-20200415.txt', '/politica/politica-ZQt0pwu6UiTE4xaccYzkiNVuo-20200415.txt', 2, '2020-04-15 20:07:18', '2020-04-15 20:07:18'),
(22, 'politica-bNOp5F129kryVu1joYjvTY05w-20200415.txt', '/politica/politica-bNOp5F129kryVu1joYjvTY05w-20200415.txt', 2, '2020-04-15 20:14:09', '2020-04-15 20:14:09'),
(23, 'politica-hw9kJVZq5T4rE9eF25yfehw8R-20200415.xlsx', '/politica/politica-hw9kJVZq5T4rE9eF25yfehw8R-20200415.xlsx', 2, '2020-04-15 20:19:58', '2020-04-15 20:19:58'),
(24, 'deposito-CluyQKMyvb57t2FbBNaaAdvnK-20200429.jpg', '/depositos/deposito-CluyQKMyvb57t2FbBNaaAdvnK-20200429.jpg', 1, '2020-04-29 16:52:41', '2020-04-29 16:52:41'),
(25, 'deposito-KUQV9kfYU7RTrNmcQuGghIYfc-20200505.jpeg', '/depositos/deposito-KUQV9kfYU7RTrNmcQuGghIYfc-20200505.jpeg', 1, '2020-05-05 22:26:51', '2020-05-05 22:26:51'),
(26, 'deposito-ccP9okMInmfI4kOgVx180eXmj-20200505.jpeg', '/depositos/deposito-ccP9okMInmfI4kOgVx180eXmj-20200505.jpeg', 1, '2020-05-05 23:00:09', '2020-05-05 23:00:09'),
(27, 'deposito-OUUQjrXp6W1vFDqznXtIj5t6i-20200508.xlsx', '/depositos/deposito-OUUQjrXp6W1vFDqznXtIj5t6i-20200508.xlsx', 1, '2020-05-08 16:07:04', '2020-05-08 16:07:04'),
(28, 'deposito-urVCXScrRlNJ7pVZ8y2xFGei2-20200511.jpg', '/depositos/deposito-urVCXScrRlNJ7pVZ8y2xFGei2-20200511.jpg', 1, '2020-05-11 14:42:29', '2020-05-11 14:42:29'),
(29, 'politica-hTLr9EokutYFxQEnDNykOXs1a-20200525.pdf', '/politica/politica-hTLr9EokutYFxQEnDNykOXs1a-20200525.pdf', 2, '2020-05-25 16:02:41', '2020-05-25 16:02:41'),
(30, 'deposito-LZIuKwdUCchzYtX7dQ5GTPOiT-20200623.jpeg', '/depositos/deposito-LZIuKwdUCchzYtX7dQ5GTPOiT-20200623.jpeg', 1, '2020-06-23 15:04:46', '2020-06-23 15:04:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `texto` text DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL,
  `destacado` bit(1) DEFAULT NULL,
  `path` mediumtext DEFAULT NULL,
  `rutadoc` mediumtext DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `seo_keyword` longtext DEFAULT NULL,
  `seo_descripcion` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `longitud` longtext DEFAULT NULL,
  `latitud` longtext DEFAULT NULL,
  `camposextras` longtext DEFAULT NULL,
  `activado` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `like` int(11) DEFAULT NULL,
  `descripcionredes` varchar(190) DEFAULT NULL,
  `fechainicio` datetime DEFAULT NULL,
  `fechafin` datetime DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL,
  `id_prioridad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `titulo`, `slug`, `texto`, `tags`, `id_categoria`, `activo`, `destacado`, `path`, `rutadoc`, `id_usuario`, `seo_keyword`, `seo_descripcion`, `created_at`, `updated_at`, `video`, `longitud`, `latitud`, `camposextras`, `activado`, `estado`, `like`, `descripcionredes`, `fechainicio`, `fechafin`, `lugar`, `id_prioridad`) VALUES
(24, '¡Con ustedes todo, sin ustedes nada!', 'lorem-ipsum-dolor-sit-amet-consectetur-adipiscing-elit-9632', '<p>Nos sentimos ogullosos de la fortaleza, dedicacion y sobre todo amor hacia nuestros alumnos de todos los niveles. Sus sonrisas alimentan nuestro espiritu,&nbsp;nos motiva, nos llena de orgullo; porque estamos haciendo las cosas bien.&nbsp;Gracias queridos estudiantes por todo lo realizado.</p>', NULL, 17, NULL, b'0', '16080f8d35d0ccef9d6c06453b279b74.png', NULL, 56, NULL, NULL, '2020-11-19 12:31:56', '2021-05-03 14:57:34', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(25, 'Entrega de textos y mochilas para nuestros estudiantes - DASE', 'mauris-eu-malesuada-ligula-etiam-non-nunc-in-sem-dignissim-dignissim-1653', '<p>Hoy empezamos con la entrega de textos y mochilas para nuestros queridos estudiantes gracias al Municipio de Guayaquil - Dase</p>', NULL, 16, NULL, b'0', '7b8f719755b073269c9ec839ad0fba1c.jpeg', NULL, 56, NULL, NULL, '2021-04-26 16:11:14', '2021-05-03 16:08:02', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(26, 'Pronto iniciaremos con la entrega de textos del programa Más Libros.', 'integer-commodo-ex-nisl-vel-varius-libero-ullamcorper-non-integer-suscipit-tortor-erat-8369', '<p>Desde la pr&oacute;xima semana empezamos con la entrega de los textos, mochila y acceso a la Plataforma.</p>', NULL, 16, NULL, b'0', '5bdc39a7c95cacc07f7a6e290b229b7d.jpeg', NULL, 56, NULL, NULL, '2021-04-21 15:06:21', '2021-05-03 16:16:56', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(27, 'Te estamos esperando!!', 'donec-dapibus-lacus-lacus-sit-amet-egestas-est-molestie-eu-808', NULL, NULL, 16, NULL, b'0', '81895b1e1bca2b7cb102a59152aabcd0.jpeg', NULL, 56, NULL, NULL, '2021-02-18 15:06:44', '2021-05-03 16:17:54', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(30, 'Desarrollo Runachay', 'desarrollo-runachay-1873', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lacinia gravida quam, ac consequat magna congue eu. Praesent dictum efficitur blandit. Ut condimentum aliquam quam non egestas.&nbsp;</p>', NULL, 19, NULL, b'0', '7bea69749b01cd7a74a9c6eaaa0fe2af.png', NULL, 56, NULL, NULL, '2020-11-24 15:16:10', '2020-11-24 15:16:10', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(31, 'Desarrollo Runachay', 'desarrollo-runachay-9763', '<p>Integer tortor ligula, pulvinar vitae fermentum sit amet, tempor ac enim. Fusce id elit vitae velit congue porta ac ac arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae.</p>', NULL, 19, NULL, b'0', '025b5d7de53986a893cfaebe5fbddb31.png', NULL, 56, NULL, NULL, '2020-11-24 15:16:40', '2020-11-24 15:16:40', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(33, 'Misión', 'mision-736', '<p>Mision de la instituci&oacute;n</p>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2020-12-10 10:28:19', '2021-04-15 18:15:40', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(34, 'Visión', 'vision-7314', '<p>Vision de la institucion</p>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2020-12-10 10:28:48', '2021-04-15 18:13:34', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(35, 'Historia', 'historia-5670', '<h2>Historia de la instituci&oacute;n</h2>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2020-12-10 10:31:28', '2021-04-15 18:13:08', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(36, 'Conoce todo sobre nuestra oferta educativa.', 'conoce-todo-sobre-nuestra-oferta-educativa-6733', '<p>Integer commodo ex nisl, vel varius libero ullamcorper non. Integer suscipit tortor erat. Vestibulum luctus sollicitudin vulputate. Ut mi est, consectetur nec arcu at, feugiat imperdiet felis. Etiam dignissim, ipsum id commodo feugiat, elit magna bibendum lorem, ac finibus leo nisl eu urna.</p>', NULL, 17, NULL, b'0', '55f003b4eae869b091ba4a5f420fd074.jpeg', NULL, 56, NULL, NULL, '2020-12-10 10:33:30', '2021-05-03 15:42:04', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(37, 'EDUCACIÓN INICIAL', 'educacion-inicial-2621', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>', NULL, 20, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2020-12-10 10:34:11', '2020-12-10 12:09:57', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(38, 'EDUCACIÓN GENERAL BÁSICA', 'educacion-general-basica-561', '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri\"><strong><span style=\"font-size:12.0pt\">Plan de Estudios para el Nivel de Bachillerato General Unificado (BGU)</span></strong></span></span></p>', NULL, 20, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2020-12-10 10:34:36', '2021-04-21 14:51:29', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(42, 'Nombre de la autoridad.', 'nombre-de-la-autoridad-4512', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>', NULL, 21, NULL, b'0', 'b2263679541e497363c9d68158c3be00.jpg', NULL, 56, NULL, NULL, '2020-12-10 12:09:12', '2020-12-10 12:09:12', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(43, 'Nombre del docente.', 'nombre-del-docente-8189', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend dolor eget purus fermentum fermentum. Morbi mattis erat molestie urna interdum, et mattis mi pulvinar.</p>', NULL, 22, NULL, b'0', 'bb8fa03eeb34664746c25419dd42dd90.jpg', NULL, 56, NULL, NULL, '2020-12-10 12:09:30', '2020-12-10 12:09:30', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(45, 'Conócenos', 'nuestros-ninos-en-la-graduacion-de-inicial-ii-4464', NULL, NULL, 18, NULL, b'0', '9b4c3cb48fb7b132d3ab8dc359823dde.jpeg', NULL, 56, NULL, NULL, '2021-03-16 17:22:59', '2021-05-03 16:20:29', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 3),
(47, 'Años de experiencia', 'experiencia-3086', '<p>15</p>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2021-03-22 16:12:00', '2021-05-03 15:01:04', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(48, 'Estudiantes graduados', 'graduados-5657', '<p>4500</p>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2021-03-22 16:16:58', '2021-05-03 15:01:31', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(49, 'Profesores y personal administrativo calificado', 'personal-8809', '<p>34</p>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2021-03-22 16:19:17', '2021-05-03 15:01:46', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(50, 'Padres satisfechos', 'satisfaccion-4490', '<p>100</p>', NULL, 17, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2021-03-22 16:22:16', '2021-05-03 15:07:13', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 1, NULL, NULL, NULL, NULL, NULL, 1),
(51, 'Cifras que hablan de nuestra labor como institución educativa.', NULL, NULL, NULL, 17, NULL, b'0', '7e3771c0ddf3dd136ae25edf5ccd68ad.jpeg', NULL, 56, NULL, NULL, '2021-03-22 16:22:16', '2021-05-03 15:30:07', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 0, NULL, NULL, NULL, NULL, NULL, 1),
(52, 'Ideario', 'ideario-5309', '<p>ideario de la institucion</p>', NULL, 16, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2021-04-20 18:49:36', '2021-05-03 16:02:04', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 0, NULL, NULL, NULL, NULL, NULL, 1),
(53, 'autoridades', 'autoridades-1984', '<p>autoridades de la institucion</p>', NULL, 16, NULL, b'0', NULL, NULL, 56, NULL, NULL, '2021-04-20 18:49:55', '2021-04-20 18:49:55', NULL, NULL, NULL, '{\"31\":null,\"32\":null,\"33\":null,\"34\":null}', 1, 0, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_tag`
--

CREATE TABLE `articulo_tag` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `articulo_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `articulo_tag`
--

INSERT INTO `articulo_tag` (`id`, `tag_id`, `articulo_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-04-25 09:37:21', '2019-04-25 09:37:21'),
(2, 1, 21, '2020-06-12 11:45:27', '2020-06-12 11:45:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camposextras`
--

CREATE TABLE `camposextras` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `valor` longtext DEFAULT NULL,
  `requerido` varchar(2) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `id_campos_extras_grupo` int(11) DEFAULT NULL,
  `orden` varchar(45) DEFAULT NULL,
  `visible` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `camposextras`
--

INSERT INTO `camposextras` (`id`, `nombre`, `descripcion`, `valor`, `requerido`, `tipo`, `id_campos_extras_grupo`, `orden`, `visible`, `created_at`, `updated_at`, `icono`) VALUES
(31, 'Dirección', 'Dirección ', '', 'NO', 'text', 12, '0', 'SI', '2017-07-19 01:41:36', '2017-07-19 02:51:19', 'fa fa-street-view'),
(32, 'horariosdeatencion', 'Horarios de atención', '', 'NO', 'text', 12, '0', 'SI', '2017-07-19 02:46:30', '2017-07-19 03:00:24', 'fa fa-clock-o'),
(33, 'Teléfonos', 'Teléfonos', '', 'NO', 'text', 12, '0', 'SI', '2017-07-19 02:59:17', '2017-07-19 02:59:17', 'fa fa-phone'),
(34, 'email', 'email', '', 'NO', 'text', 12, '0', 'SI', '2017-07-19 04:39:02', '2017-07-19 04:39:02', 'fa fa-television');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camposextrasgrupo`
--

CREATE TABLE `camposextrasgrupo` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `visible` varchar(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icono` varchar(45) DEFAULT NULL,
  `vistacampos_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `camposextrasgrupo`
--

INSERT INTO `camposextrasgrupo` (`id`, `descripcion`, `orden`, `visible`, `created_at`, `updated_at`, `icono`, `vistacampos_id`) VALUES
(12, 'Información', 2, 'si', '2017-07-18 18:59:39', '2017-07-19 01:43:23', 'fa fa-bed', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `paypal` varchar(255) DEFAULT NULL,
  `estado_paypal` tinyint(1) NOT NULL DEFAULT 0,
  `estado_tarjeta` tinyint(1) NOT NULL DEFAULT 0,
  `estado_deposito` tinyint(1) NOT NULL DEFAULT 0,
  `app_code` varchar(255) DEFAULT NULL,
  `app_key` varchar(255) DEFAULT NULL,
  `num_cuenta` varchar(255) DEFAULT NULL,
  `nombre_cuenta` varchar(255) DEFAULT NULL,
  `banco` varchar(255) DEFAULT NULL,
  `tipo_cuenta` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `descuento` float DEFAULT NULL,
  `iva` float DEFAULT NULL,
  `tiempo_estimado` int(11) DEFAULT NULL,
  `estado_local` tinyint(1) NOT NULL DEFAULT 0,
  `estado_domicilio` tinyint(1) NOT NULL DEFAULT 0,
  `tipo_domicilio` varchar(100) DEFAULT NULL,
  `tarifa_local` float DEFAULT NULL,
  `tarifa_kilometro` float DEFAULT NULL,
  `tarifa_envio` float DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `politicas` longtext DEFAULT NULL,
  `id_adjunto` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`id`, `estado`, `paypal`, `estado_paypal`, `estado_tarjeta`, `estado_deposito`, `app_code`, `app_key`, `num_cuenta`, `nombre_cuenta`, `banco`, `tipo_cuenta`, `correo`, `descuento`, `iva`, `tiempo_estimado`, `estado_local`, `estado_domicilio`, `tipo_domicilio`, `tarifa_local`, `tarifa_kilometro`, `tarifa_envio`, `time`, `politicas`, `id_adjunto`, `created_at`, `updated_at`) VALUES
(4, 0, '23sada', 1, 1, 1, '5efe1bfeeca7c', NULL, '123456', 'Luis', 'Pacifico', '3', 'luismera1794@gmail.com', 3, 12, 6, 1, 1, 'fija', 2, 0.5, 3, 2, '<p>El pedido se procesara para la entrega luego de la confirmaci&oacute;n del pago total de la deuda.</p>', 29, '2020-04-09 18:22:46', '2020-07-06 14:20:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriacamposextrasgrupo`
--

CREATE TABLE `categoriacamposextrasgrupo` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `id_campos_extras_grupo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoriacamposextrasgrupo`
--

INSERT INTO `categoriacamposextrasgrupo` (`id`, `id_categoria`, `id_campos_extras_grupo`, `created_at`, `updated_at`) VALUES
(31, 33, 14, '2017-07-18 19:00:41', '2017-07-18 19:00:41'),
(32, 19, 13, '2017-07-18 19:01:36', '2017-07-18 19:01:36'),
(39, 33, 11, '2017-07-25 01:53:20', '2017-07-25 01:53:20'),
(40, 19, 15, '2017-08-03 20:58:35', '2017-08-03 20:58:35'),
(41, 20, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(42, 22, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(43, 23, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(44, 24, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(45, 25, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(46, 26, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(47, 32, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(48, 33, 15, '2017-08-03 20:58:36', '2017-08-03 20:58:36'),
(49, 19, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(50, 20, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(51, 22, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(52, 23, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(53, 24, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(54, 25, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(55, 26, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(56, 32, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(57, 33, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(58, 34, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(59, 35, 12, '2017-08-18 05:54:44', '2017-08-18 05:54:44'),
(60, 14, 13, '2020-06-10 19:23:40', '2020-06-10 19:23:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriainterna`
--

CREATE TABLE `categoriainterna` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` longtext DEFAULT NULL,
  `path` longtext DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoriainterna`
--

INSERT INTO `categoriainterna` (`id`, `nombre`, `descripcion`, `path`, `created_at`, `updated_at`) VALUES
(1, 'slider principal', 'slider de la página principal.', NULL, '2018-08-20 16:04:50', '2018-08-20 16:04:50'),
(2, 'Slider', 'slider principal', NULL, '2019-04-25 14:48:15', '2019-04-25 14:49:21'),
(5, 'Videos', 'Videos', NULL, '2019-04-25 14:50:26', '2019-04-25 14:50:26'),
(6, 'Imagenes servicios', 'Imagenes mostradas en página de servicios', NULL, '2019-05-06 16:51:27', '2019-05-06 16:51:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriaproductos`
--

CREATE TABLE `categoriaproductos` (
  `id` int(11) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `path` mediumtext DEFAULT NULL,
  `icono` mediumtext DEFAULT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `destacado` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoriaproductos`
--

INSERT INTO `categoriaproductos` (`id`, `categoria`, `path`, `icono`, `descripcion`, `destacado`, `created_at`, `updated_at`) VALUES
(11, 'Globos y dulces', '86389204_2570447013077831_1692044070391644160_o.jpg', NULL, NULL, 0, '2020-03-30 12:32:28', '2020-03-30 12:41:00'),
(12, 'Arreglo frutales', '86352144_2570447956411070_4768138560190545920_o.jpg', NULL, NULL, 0, '2020-03-30 12:32:48', '2020-03-30 12:40:53'),
(13, 'Arreglos florales', '86371077_2570446393077893_3179560135995424768_o.jpg', NULL, NULL, 0, '2020-03-30 12:33:01', '2020-03-30 12:40:44'),
(14, 'Servicios', '1_vxjAHkrXbGG6gOiPZgjeZA.jpeg', NULL, 'Servicios', 1, '2020-06-15 12:04:13', '2020-07-02 17:50:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `path` mediumtext DEFAULT NULL,
  `icono` varchar(100) DEFAULT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `cat_destacado` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `path`, `icono`, `descripcion`, `cat_destacado`, `created_at`, `updated_at`) VALUES
(16, 'Noticias', NULL, NULL, NULL, 0, '2020-11-19 12:21:28', '2020-11-19 12:21:28'),
(17, 'Institución', NULL, NULL, NULL, 0, '2020-11-24 10:38:27', '2020-11-24 10:38:27'),
(18, 'Galería', NULL, NULL, NULL, 0, '2020-11-24 14:48:27', '2020-11-24 14:48:27'),
(19, 'Comentarios', NULL, NULL, NULL, 0, '2020-11-24 14:48:48', '2020-11-24 14:48:48'),
(20, 'Oferta Académica', NULL, NULL, NULL, 0, '2020-11-24 14:49:16', '2020-11-24 14:49:16'),
(21, 'Autoridades', NULL, NULL, NULL, 0, '2020-12-10 10:27:06', '2020-12-10 10:27:06'),
(22, 'Docentes', NULL, NULL, NULL, 0, '2020-12-10 10:27:15', '2020-12-10 10:27:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `ci_ruc` varchar(15) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `segundo_nombre` varchar(200) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `correo` varchar(100) NOT NULL,
  `mensaje` longtext DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `api_token` varchar(100) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `provider_id` varchar(255) DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `ci_ruc`, `nombre`, `segundo_nombre`, `apellido`, `latitud`, `longitud`, `ciudad`, `direccion`, `telefono`, `correo`, `mensaje`, `password`, `api_token`, `remember_token`, `provider_id`, `provider`, `created_at`, `updated_at`) VALUES
(72, '', 'Luis', '', 'Mera', 'Ecuador', 'Manabí', 'Manta', 'Parroquia Eloy Alfaro', '0989516389', 'lm@g.c', NULL, '$2y$10$NzOM2vMPrUqNtBmMJz6l0.I.Spl.nax9o7SC5Fp2XuHnT6cHcOVB2', NULL, '23PH4ZE7MNvR9CDNPbU1qDUsyHKIaTct1djHmk9ZUhnRLptswZXTatRBxEv3', NULL, NULL, '2020-04-10 22:34:43', '2020-05-08 22:21:00'),
(79, '1315358232', 'Luis', 'Alfredo', 'Mera Rivas', '-0.9640792758734131', '-80.73391016342605', 'Manta', 'Parroquia Eloy Alfaro', '0989715223', 'l@g.c', 'Preguntar por Leovo', '$2y$10$NzOM2vMPrUqNtBmMJz6l0.I.Spl.nax9o7SC5Fp2XuHnT6cHcOVB2', 'axFl0njhtOetTAkcg6aRPmFLY9OEqhi6SItMJYmIMTaFAOynKOuFcNM7yeVu', 'LwUZybG1Xhe7l98kS8wfISSgb4bNFiWBdDynlcFISU6d0oeqA98eXv4IVK7R', NULL, NULL, '2020-05-02 19:57:27', '2020-07-27 16:04:55'),
(80, NULL, 'Alfredo', '', 'Mera', '', '', NULL, NULL, NULL, 'A@g.c', NULL, '$2y$10$NzOM2vMPrUqNtBmMJz6l0.I.Spl.nax9o7SC5Fp2XuHnT6cHcOVB2', NULL, NULL, NULL, NULL, '2020-05-04 16:15:46', '2020-05-04 16:15:46'),
(82, NULL, 'ds', '', 'sd', NULL, NULL, NULL, NULL, NULL, 'jonathanpalma25@hotmail.com', NULL, '$2y$10$Ax5XadKoGeRX.JUVQ1ztp.5ypbsz/rBt4YE72euqbYzDBVBHVs.9S', NULL, NULL, NULL, NULL, '2020-06-05 04:04:01', '2020-06-05 04:04:01'),
(83, '1315358232001', 'Luis', '', 'Mera', NULL, NULL, NULL, NULL, '0987654321', 'luisalfremera@hotmail.com', NULL, NULL, NULL, 'dzcpGF84PEa5IhaKONAeDs0sMjOLUVcGwrrPapj5sMNtBysWVtF0w42aziWd', '3858933237510017', 'facebook', '2020-06-09 22:32:51', '2020-06-09 22:33:18'),
(89, '12345', 'Luis', 'Alfredo', 'Viva Santander', NULL, NULL, NULL, NULL, '0989516389', 'czam@gmail.com', NULL, '$2y$10$bKKYjkmQP1fHQGYqDP4JXOfPNq9.PzsxSs3z2LZ.x33LKhWz8TxpC', NULL, 'uOE07EUwBp6eNvHsHMfFhkf0Jt9zpUAc8FCN0gqfaBaqwGibJyVWjniJ8K3K', NULL, NULL, '2020-07-05 19:54:52', '2020-07-05 19:54:52'),
(90, '12313223', 'Logic', 'Soft', 'Manta', NULL, NULL, NULL, NULL, '99444', 'logicsoftmanta@gmail.com', NULL, NULL, NULL, 'PTCRahBxt4utHSQWFZ2zlVoRUXpuReKq3V0X576PzIzAE1gJGkCo1hmvK4cp', '100887357781653877174', 'google', '2020-07-05 19:55:58', '2020-07-05 22:38:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(11) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `valor` varchar(200) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `clave`, `valor`, `descripcion`, `created_at`, `updated_at`) VALUES
(2, 'email_key', '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', 'Esta es la clave del api de sendgrid', '2019-12-17 14:36:45', '2021-01-11 14:35:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupones`
--

CREATE TABLE `cupones` (
  `id` int(11) NOT NULL,
  `descripcion` longtext NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `descuento` float NOT NULL,
  `limite` int(11) NOT NULL,
  `fecha_inicio` timestamp NULL DEFAULT NULL,
  `fecha_fin` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cupones`
--

INSERT INTO `cupones` (`id`, `descripcion`, `codigo`, `descuento`, `limite`, `fecha_inicio`, `fecha_fin`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Cupón de descuento rebajas Junio', 'RebajasJunio', 5, 20, '2020-06-01 05:00:00', '2020-07-31 05:00:00', 1, '2020-06-09 22:36:42', '2020-07-06 19:26:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depositos`
--

CREATE TABLE `depositos` (
  `id` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `num_doc` varchar(100) NOT NULL,
  `valor` float NOT NULL,
  `id_adjunto` int(11) NOT NULL,
  `estado` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` float NOT NULL,
  `precio_con_desc` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id`, `id_pedido`, `id_producto`, `cantidad`, `precio`, `precio_con_desc`, `created_at`, `updated_at`) VALUES
(77, 111, 52, 1, 200, NULL, '2020-07-03 14:17:55', '2020-07-03 14:17:55'),
(78, 112, 52, 1, 200, NULL, '2020-07-04 13:36:33', '2020-07-04 13:36:33'),
(79, 113, 53, 1, 200, NULL, '2020-07-04 20:36:48', '2020-07-04 20:36:48'),
(80, 114, 52, 1, 200, NULL, '2020-07-04 23:11:45', '2020-07-04 23:11:45'),
(81, 115, 53, 1, 200, NULL, '2020-07-05 20:07:17', '2020-07-05 20:07:17'),
(82, 116, 56, 1, 500, NULL, '2020-07-05 21:53:35', '2020-07-05 21:53:35'),
(83, 116, 55, 1, 500, NULL, '2020-07-05 21:53:35', '2020-07-05 21:53:35'),
(84, 117, 56, 1, 500, NULL, '2020-07-06 14:21:57', '2020-07-06 14:21:57'),
(85, 117, 55, 1, 500, NULL, '2020-07-06 14:21:57', '2020-07-06 14:21:57'),
(90, 122, 53, 1, 200, NULL, '2020-07-06 22:10:05', '2020-07-06 22:10:05'),
(91, 122, 56, 1, 500, NULL, '2020-07-06 22:10:05', '2020-07-06 22:10:05'),
(92, 122, 55, 1, 500, NULL, '2020-07-06 22:10:05', '2020-07-06 22:10:05'),
(93, 123, 53, 1, 200, NULL, '2020-07-07 13:55:25', '2020-07-07 13:55:25'),
(94, 124, 53, 1, 200, NULL, '2020-07-07 14:51:42', '2020-07-07 14:51:42'),
(95, 124, 56, 1, 500, NULL, '2020-07-07 14:51:42', '2020-07-07 14:51:42'),
(96, 125, 52, 1, 200, NULL, '2020-07-07 14:55:32', '2020-07-07 14:55:32'),
(97, 126, 51, 1, 300, NULL, '2020-07-07 15:24:29', '2020-07-07 15:24:29'),
(98, 127, 51, 1, 50, NULL, '2020-07-07 15:44:42', '2020-07-07 15:44:42'),
(99, 127, 56, 1, 80, NULL, '2020-07-07 15:44:42', '2020-07-07 15:44:42'),
(100, 127, 54, 1, 75, NULL, '2020-07-07 15:44:42', '2020-07-07 15:44:42'),
(101, 128, 51, 1, 50, 0, '2020-07-07 15:55:22', '2020-07-07 18:20:32'),
(102, 128, 56, 1, 80, 0, '2020-07-07 15:55:22', '2020-07-07 18:20:32'),
(103, 128, 54, 1, 75, 0, '2020-07-07 15:55:22', '2020-07-07 18:20:32'),
(104, 129, 53, 1, 194, 0, '2020-07-07 18:25:45', '2020-07-07 18:25:54'),
(105, 130, 53, 1, 200, -9.7, '2020-07-07 18:27:36', '2020-07-07 18:27:47'),
(106, 131, 53, 1, 200, 184.3, '2020-07-07 18:34:28', '2020-07-07 18:34:45'),
(107, 132, 53, 1, 200, 184.3, '2020-07-07 18:40:44', '2020-07-07 18:40:51'),
(108, 133, 53, 1, 200, 184.3, '2020-07-07 19:12:45', '2020-07-07 19:12:52'),
(109, 134, 56, 1, 80, 73.72, '2020-07-07 19:45:19', '2020-07-07 19:53:12'),
(110, 134, 53, 1, 200, 184.3, '2020-07-07 19:45:19', '2020-07-07 19:53:12'),
(111, 136, 53, 1, 200, 184.3, '2020-07-07 20:07:51', '2020-07-07 20:08:00'),
(112, 136, 56, 1, 80, 73.72, '2020-07-07 20:07:51', '2020-07-07 20:08:00'),
(113, 137, 53, 1, 200, 184.3, '2020-07-07 20:19:32', '2020-07-07 20:19:40'),
(114, 137, 56, 1, 80, 73.72, '2020-07-07 20:19:32', '2020-07-07 20:19:40'),
(115, 138, 56, 1, 80, 73.72, '2020-07-07 20:21:55', '2020-07-07 20:22:10'),
(116, 138, 53, 1, 200, 184.3, '2020-07-07 20:21:55', '2020-07-07 20:22:10'),
(117, 139, 53, 1, 20, 18.43, '2020-07-07 22:38:58', '2020-07-07 22:39:05'),
(118, 139, 56, 1, 80, 73.72, '2020-07-07 22:38:58', '2020-07-07 22:39:05'),
(119, 140, 56, 2, 80, 77.6, '2020-07-07 22:42:32', '2020-07-07 22:42:32'),
(120, 141, 56, 1, 80, 77.6, '2020-07-07 22:49:27', '2020-07-07 22:49:27'),
(121, 142, 56, 2, 80, 77.6, '2020-07-07 22:50:26', '2020-07-07 22:50:26'),
(122, 143, 54, 1, 75, 72.75, '2020-07-07 23:01:36', '2020-07-07 23:01:36'),
(123, 144, 56, 1, 80, 77.6, '2020-07-07 23:17:57', '2020-07-07 23:17:57'),
(124, 145, 56, 1, 80, 77.6, '2020-07-15 16:56:02', '2020-07-15 16:56:02'),
(125, 146, 52, 1, 20, 19.4, '2020-08-06 00:48:40', '2020-08-06 00:48:40'),
(126, 147, 51, 1, 50, 48.5, '2020-08-06 00:49:30', '2020-08-06 00:49:30'),
(127, 148, 53, 1, 20, 19.4, '2020-08-06 00:53:50', '2020-08-06 00:53:50'),
(128, 149, 52, 1, 20, 19.4, '2020-08-07 15:44:47', '2020-08-07 15:44:47'),
(129, 150, 53, 1, 20, 19.4, '2020-09-08 19:31:08', '2020-09-08 19:31:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `ciudad` varchar(200) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `referencia` longtext NOT NULL,
  `latitud` varchar(100) NOT NULL,
  `longitud` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id`, `id_cliente`, `ciudad`, `direccion`, `referencia`, `latitud`, `longitud`, `created_at`, `updated_at`) VALUES
(21, 83, 'Manta', 'Parroquia Eloy Alfaro', 'ffff', '-0.9644553210303033', '-80.70281980629886', '2020-06-09 22:33:30', '2020-06-09 22:33:30'),
(22, 83, 'Portoviejo', 'Calle 317 av. 215', 'ppp', '-1.0179082257545224', '-80.67648066103366', '2020-06-09 22:34:30', '2020-06-09 22:34:30'),
(24, 86, 'Manta-MANABI', 'Calle 317 av. 215', 'mkkk', '-0.9650560506790179', '-80.70359228249515', '2020-07-04 20:37:05', '2020-07-04 20:37:05'),
(26, 87, 'Manta', 'Calle 317 av. 215', 'casa de 2 pisos', '-0.9825314589786177', '-80.70606404134982', '2020-07-04 23:54:24', '2020-07-04 23:54:24'),
(33, 79, 'Manta', 'Calle 317 av. 215', 'Cuba', '-0.9640262283591643', '-80.70273397561039', '2020-07-23 23:26:08', '2020-07-27 16:26:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id` int(11) NOT NULL,
  `path` mediumtext NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `titulo` varchar(100) NOT NULL,
  `articulo_id` int(11) NOT NULL,
  `id_modulo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especificaciones`
--

CREATE TABLE `especificaciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `requerido` tinyint(1) NOT NULL DEFAULT 1,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `orden` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `especificaciones`
--

INSERT INTO `especificaciones` (`id`, `nombre`, `descripcion`, `requerido`, `estado`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'talla', 'Talla', 1, 1, 0, '2020-06-10 20:58:08', '2020-06-10 20:58:08'),
(3, 'color', 'Color', 1, 1, 0, '2020-06-12 19:36:18', '2020-06-12 19:36:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotos`
--

CREATE TABLE `fotos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) CHARACTER SET utf8 NOT NULL,
  `path` varchar(200) CHARACTER SET utf8 NOT NULL,
  `articulo_id` int(11) NOT NULL,
  `id_modulo` int(11) DEFAULT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fotos`
--

INSERT INTO `fotos` (`id`, `titulo`, `path`, `articulo_id`, `id_modulo`, `descripcion`, `orden`, `created_at`, `updated_at`) VALUES
(695, '5379344malla-egb1.png', '5379344malla-egb1.png', 38, NULL, NULL, 0, '2021-04-21 19:51:29', '2021-04-21 19:51:29'),
(696, '4585780malla-egb2.png', '4585780malla-egb2.png', 38, NULL, NULL, 1, '2021-04-21 19:51:29', '2021-04-21 19:51:29'),
(697, '608324178396542_474346137106736_2581161670409231347_n.jpeg', '608324178396542_474346137106736_2581161670409231347_n.jpeg', 25, NULL, NULL, 0, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(698, '6033433178451033_474346073773409_3835178821020761269_n.jpeg', '6033433178451033_474346073773409_3835178821020761269_n.jpeg', 25, NULL, NULL, 1, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(699, '6749543178573235_474346110440072_568189112334249015_n.jpeg', '6749543178573235_474346110440072_568189112334249015_n.jpeg', 25, NULL, NULL, 2, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(700, '6779662178720319_474346230440060_8146895481945004030_n.jpeg', '6779662178720319_474346230440060_8146895481945004030_n.jpeg', 25, NULL, NULL, 3, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(701, '494916178731326_474346163773400_6819016715479568041_n.jpeg', '494916178731326_474346163773400_6819016715479568041_n.jpeg', 25, NULL, NULL, 4, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(702, '4528665178936578_474346197106730_4400929579860999274_n.jpeg', '4528665178936578_474346197106730_4400929579860999274_n.jpeg', 25, NULL, NULL, 5, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(703, '4382627179235437_474346030440080_7051696914853351933_n.jpeg', '4382627179235437_474346030440080_7051696914853351933_n.jpeg', 25, NULL, NULL, 6, '2021-05-03 21:08:02', '2021-05-03 21:08:02'),
(704, '7673277176103539_471271354080881_6265442211277493598_n.jpeg', '7673277176103539_471271354080881_6265442211277493598_n.jpeg', 26, NULL, NULL, 0, '2021-05-03 21:14:13', '2021-05-03 21:14:13'),
(705, '807562762073959_123074925567194_125999969504067584_n.jpeg', '807562762073959_123074925567194_125999969504067584_n.jpeg', 45, NULL, NULL, 0, '2021-05-03 21:20:29', '2021-05-03 21:20:29'),
(706, '766162662077056_123074928900527_2332291452182200320_n.jpeg', '766162662077056_123074928900527_2332291452182200320_n.jpeg', 45, NULL, NULL, 1, '2021-05-03 21:20:29', '2021-05-03 21:20:29'),
(707, '668801387066986_190197255521627_1907354801029513216_n.jpeg', '668801387066986_190197255521627_1907354801029513216_n.jpeg', 45, NULL, NULL, 0, '2021-05-03 21:23:23', '2021-05-03 21:23:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeriaarticulo`
--

CREATE TABLE `galeriaarticulo` (
  `id` int(11) NOT NULL,
  `id_articulo` int(11) DEFAULT NULL,
  `id_modulo` int(11) DEFAULT NULL,
  `id_galeria_cabecera` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeriacabecera`
--

CREATE TABLE `galeriacabecera` (
  `id` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeriadetalle`
--

CREATE TABLE `galeriadetalle` (
  `id` int(11) NOT NULL,
  `nombre` text DEFAULT NULL,
  `ruta` mediumtext DEFAULT NULL,
  `id_galeria_cabecera` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeriaproducto`
--

CREATE TABLE `galeriaproducto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `id_producto` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeriaproducto`
--

INSERT INTO `galeriaproducto` (`id`, `titulo`, `path`, `descripcion`, `orden`, `id_producto`, `created_at`, `updated_at`) VALUES
(3, '1439852AC.jpg', '1439852AC.jpg', NULL, 0, 43, '2020-04-29 10:37:51', '2020-04-29 10:37:51'),
(4, '273523AC.jpg', '273523AC.jpg', NULL, 1, 43, '2020-04-29 10:37:51', '2020-04-29 10:37:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `id` int(11) NOT NULL,
  `titulo` varchar(150) DEFAULT NULL,
  `descripcion` longtext DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `id_categoriainterna` int(11) DEFAULT NULL,
  `path` mediumtext DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`id`, `titulo`, `descripcion`, `orden`, `id_categoriainterna`, `path`, `updated_at`, `created_at`) VALUES
(2, 'Misión', 'dsdsd', NULL, 4, 'sl2.jpg', '2019-04-25 14:53:56', '2019-04-25 14:53:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informacion`
--

CREATE TABLE `informacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `direccion` varchar(200) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `biografia` mediumtext DEFAULT NULL,
  `quienessomos` longtext DEFAULT NULL,
  `mision` longtext DEFAULT NULL,
  `vision` longtext DEFAULT NULL,
  `carrito` tinyint(1) DEFAULT 0,
  `cliente` tinyint(1) DEFAULT 0,
  `identificador` varchar(100) DEFAULT NULL,
  `url_api` varchar(250) DEFAULT NULL,
  `path_logo` varchar(255) DEFAULT NULL,
  `path_ico` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informacion`
--

INSERT INTO `informacion` (`id`, `nombre`, `direccion`, `telefono`, `email`, `biografia`, `quienessomos`, `mision`, `vision`, `carrito`, `cliente`, `identificador`, `url_api`, `path_logo`, `path_ico`, `updated_at`, `created_at`) VALUES
(1, 'Escuela de Educación Básica Alba Calderón de Gil', 'Guayaquil', '042318187', 'albacalderondegil@gmail.com', '¡Amamos lo que hacemos!', '-', 'Misión de la institución', 'Visión de la institución', 0, 0, NULL, NULL, 'logo1619731247.png', 'icono1619729122.png', '2021-05-03 16:53:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `ruta` varchar(255) DEFAULT NULL,
  `icono` varchar(45) DEFAULT NULL,
  `nivel` tinyint(4) DEFAULT NULL,
  `padre` int(11) DEFAULT NULL,
  `orden` decimal(10,2) DEFAULT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id`, `nombre`, `ruta`, `icono`, `nivel`, `padre`, `orden`, `visible`, `created_at`, `updated_at`) VALUES
(1, 'Menú', 'menu', NULL, 1, 1, '1.00', 0, NULL, '2020-05-01 15:35:48'),
(4, 'Menú Cliente', 'menu-pagina', 'fa fa-sitemap', 1, 0, '9.00', 1, '2020-05-01 14:52:27', '2020-06-10 14:43:36'),
(5, 'Inicio', 'administracion', 'fa fa-file-text-o', 1, 0, '1.00', 1, '2020-05-01 14:53:20', '2020-05-01 14:53:20'),
(6, 'Gestor de Usuarios', '#', 'fa fa-users', 1, 0, '2.00', 1, '2020-05-01 14:54:35', '2020-05-01 14:54:35'),
(7, 'Usuarios', 'usuarios', 'fa fa-user', 2, 6, '2.10', 1, '2020-05-01 14:56:08', '2020-05-01 14:57:37'),
(8, 'Tipos Usuarios', 'tipos-usuarios', 'fa fa-user-plus', 2, 6, '2.20', 1, '2020-05-01 14:56:51', '2020-05-01 14:57:43'),
(9, 'Contenido', '#', 'fa fa-newspaper-o', 1, 0, '3.00', 1, '2020-05-01 14:58:41', '2020-05-01 15:54:18'),
(10, 'Articulos', 'articulos', 'fa fa-file-text-o', 2, 9, '3.10', 1, '2020-05-01 14:59:24', '2020-05-01 18:05:36'),
(11, 'Categorias', 'categorias', 'fa fa-bookmark', 2, 9, '3.20', 1, '2020-05-01 15:00:03', '2020-05-01 15:02:03'),
(12, 'Tags', 'tags', 'fa fa-tags', 2, 9, '3.30', 1, '2020-05-01 15:01:58', '2020-05-01 15:01:58'),
(13, 'Activaciones', 'activaciones', 'fa fa-check', 2, 9, '3.40', 1, '2020-05-01 15:03:05', '2020-05-01 15:03:05'),
(14, 'Tienda', '#', 'fa fa-archive', 1, 0, '4.00', 1, '2020-05-01 15:24:44', '2020-06-10 15:14:41'),
(15, 'Carrito', 'carrito', 'fa fa-cart-plus', 2, 14, '4.10', 1, '2020-05-01 15:26:08', '2020-05-01 15:26:08'),
(16, 'Pedidos', 'pedidos', 'fa fa-archive', 2, 14, '4.20', 1, '2020-05-01 15:27:30', '2020-05-01 15:27:30'),
(17, 'Depositos', 'depositos', 'fa fa-bank', 2, 14, '4.30', 1, '2020-05-01 15:28:30', '2020-05-01 15:28:30'),
(18, 'Categorias', 'categoriaproductos', 'fa fa-file-text-o', 2, 29, '5.20', 1, '2020-05-01 15:29:18', '2020-06-10 15:17:46'),
(19, 'Producto', 'shop', 'fa fa-bookmark', 2, 29, '5.10', 1, '2020-05-01 15:30:05', '2020-06-10 15:17:37'),
(20, 'Módulos', '#', 'fa fa-shopping-cart', 1, 0, '7.00', 1, '2020-05-01 15:30:33', '2020-06-10 14:43:48'),
(21, 'Categorias', 'categoriainterna', 'fa fa-cart-arrow-down', 2, 20, '7.10', 1, '2020-05-01 15:31:21', '2020-06-10 14:43:53'),
(22, 'Slider', 'animacion', 'fa fa-cart-arrow-down', 2, 20, '7.20', 1, '2020-05-01 15:32:18', '2020-06-10 14:43:58'),
(23, 'Imagenes', 'imagen', 'fa fa-check-circle-o', 2, 20, '7.30', 1, '2020-05-01 15:32:59', '2020-06-10 14:44:03'),
(24, 'Video', 'video', 'fa fa-check-circle-o', 2, 20, '7.40', 1, '2020-05-01 15:33:28', '2020-06-10 14:44:08'),
(25, 'Información', 'informacion', 'fa fa-info-circle', 1, 0, '8.00', 1, '2020-05-01 15:34:08', '2020-06-10 14:43:34'),
(26, 'Permisos', 'permisos', 'fa fa-exclamation-triangle', 2, 6, '2.30', 1, '2020-05-01 16:16:37', '2020-05-01 16:16:37'),
(27, 'Gestor Clientes', 'clientes', 'fa fa-users', 1, 0, '2.90', 1, '2020-05-12 15:41:55', '2020-05-12 15:44:09'),
(28, 'Cupón', 'cupones', 'fa fa-ticket', 2, 14, '4.15', 1, '2020-06-10 12:10:07', '2020-06-10 12:10:07'),
(29, 'Productos', '#', 'fa fa-cubes', 1, 0, '5.00', 1, '2020-06-10 15:16:08', '2020-06-10 15:16:08'),
(30, 'Atributos', 'especificaciones', 'fa fa-bars', 2, 29, '5.30', 0, '2020-06-10 16:07:08', '2020-06-15 12:42:24'),
(31, 'Configuración', 'configuracion', 'fa fa-cog', 1, 0, '10.00', 1, '2020-06-11 16:30:19', '2020-06-11 16:30:19'),
(32, 'Cursos', 'cursos', 'fa fa-university', 1, 0, '6.00', 1, '2020-08-04 10:51:40', '2020-08-04 10:59:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_pagina`
--

CREATE TABLE `menu_pagina` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `ruta` varchar(255) DEFAULT NULL,
  `icono` varchar(45) DEFAULT NULL,
  `nivel` tinyint(4) DEFAULT NULL,
  `padre` int(11) DEFAULT NULL,
  `orden` decimal(10,2) DEFAULT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_pagina`
--

INSERT INTO `menu_pagina` (`id`, `nombre`, `ruta`, `icono`, `nivel`, `padre`, `orden`, `visible`, `created_at`, `updated_at`) VALUES
(1, 'Ir a la Página', '/', 'fa fa-file-text-o', NULL, 0, '1.00', 0, '2020-05-01 18:33:42', '2020-07-16 16:23:53'),
(2, 'Pedidos', 'cliente/mis-pedidos', 'fa fa-cart-plus', NULL, 0, '2.00', 1, '2020-05-01 18:34:51', '2020-05-04 18:31:41'),
(3, 'Ofertas', 'cliente/ofertas', 'fa fa-star', NULL, 0, '2.00', 0, '2020-05-01 18:37:12', '2020-05-05 18:03:55'),
(4, 'Deseados', 'cliente/deseados', 'fa fa-heart-o', NULL, 0, '4.00', 1, '2020-05-01 18:37:44', '2020-05-12 15:04:48'),
(5, 'Mi Perfil', 'cliente/miperfil', 'fa fa-user', NULL, 0, '5.00', 1, '2020-05-01 18:45:19', '2020-05-04 18:35:49'),
(7, 'Dashboard', 'cliente/dashboard', 'fa fa-bars', NULL, 0, '0.00', 1, '2020-05-04 14:28:36', '2020-05-04 18:29:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` longtext DEFAULT NULL,
  `estado` tinyint(1) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_identities`
--

CREATE TABLE `oauth_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider_user_id` varchar(255) NOT NULL,
  `provider` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `oauth_identities`
--

INSERT INTO `oauth_identities` (`id`, `user_id`, `provider_user_id`, `provider`, `access_token`, `created_at`, `updated_at`) VALUES
(2, 31, '10210824735669274', 'facebook', 'EAAJKkknnwgMBANBVjCaMOBfIr9ylOjgboPvL3FYC4qOGVy4wZCQZBaN6zd3vcjiWbqwIslgPd5AXT7wnVA1bwWxoBAPPaNdlmjyZBsmcQ8lkblZBpVq78ZADTMfnZBQZB4EwgejnNCGoa8icVylHmjyVimFSEwtYtkZD', '2017-08-01 23:09:24', '2017-08-02 04:09:24'),
(3, 32, '10214141026579210', 'facebook', 'EAAJKkknnwgMBALUMIABWKEvrbIKU9HOGZABl0eqs9tlCSOazX84gxP4vZAxKKWnMMzMSEl5S7zNuCcLtdpLsaMyV2Qa8xqfqpMcWjtXtS5TFKArWcdk6NKy6aw6in8yTN1CZAhqXMLPZCZCDyJ1He6SfRtgjNAnatxmqelzOk82IkqakloXll', '2017-07-31 18:25:41', '2017-07-31 18:25:41'),
(4, 33, '1575981309130709', 'facebook', 'EAAJKkknnwgMBAL6XvoSc3RrGwOQ8bDrNEiYZB24KScnqtcg0H5WVgt4kHTvcFqhMYaFA80lRhVL0cIkMPIY7n0tYZAPMtQo4qQHpifaZBxIldoCLwMiyL54JT88Cu6MLA8InQColLkzZBdGbNPOZAhZA6de3lqcH1eWxLlmckMXAZDZD', '2017-08-01 01:47:12', '2017-08-01 01:47:12'),
(5, 34, '998230430280632', 'facebook', 'EAAJKkknnwgMBAP1ElZCYbzrNZAUR8XRMbZAYWiXGgAvQVLUkjjZAn6zA8S5KviTS7UX6muk3roRKQQ6060X7ZChZCOyDIu4zmjSqKiY25WQFynMbxrBSWfQ5XuwE8jqXUopP9bgOi8Ii3eEZCbsfbYqwc25kqn0Gdd44GMvZCXiJhgZDZD', '2017-08-04 22:40:28', '2017-08-04 22:40:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id` int(11) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `subtotal` float DEFAULT NULL,
  `subtotal_12` float NOT NULL DEFAULT 0,
  `descuento` float DEFAULT NULL,
  `iva` float DEFAULT NULL,
  `metodo_pago` varchar(100) DEFAULT NULL,
  `metodo_entrega` varchar(100) DEFAULT NULL,
  `tarifa_envio` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `cupon` varchar(100) DEFAULT NULL,
  `descuento_cupon` float DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_direccion` int(11) DEFAULT NULL,
  `id_trans` varchar(200) DEFAULT NULL,
  `code_auth` varchar(200) DEFAULT NULL,
  `num_referencia` varchar(200) DEFAULT NULL,
  `resp_banco` varchar(200) DEFAULT NULL,
  `diferido` varchar(200) DEFAULT NULL,
  `interes` varchar(200) DEFAULT NULL,
  `tipo_tarjeta` varchar(200) DEFAULT NULL,
  `banco` varchar(200) DEFAULT NULL,
  `propietario_tarjeta` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id`, `estado`, `subtotal`, `subtotal_12`, `descuento`, `iva`, `metodo_pago`, `metodo_entrega`, `tarifa_envio`, `total`, `cupon`, `descuento_cupon`, `id_cliente`, `id_direccion`, `id_trans`, `code_auth`, `num_referencia`, `resp_banco`, `diferido`, `interes`, `tipo_tarjeta`, `banco`, `propietario_tarjeta`, `created_at`, `updated_at`) VALUES
(111, 1, 200, 0, 8, 9.6, 'tarjeta', 'local', 2, 203.6, NULL, NULL, 79, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 14:17:55', '2020-07-03 14:20:07'),
(112, 1, 200, 0, 8, 9.6, 'tarjeta', 'local', 2, 203.6, NULL, NULL, 79, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-04 13:36:33', '2020-07-04 13:38:31'),
(113, 1, 200, 0, 8, 9.6, NULL, NULL, NULL, 201.6, NULL, NULL, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-04 20:36:48', '2020-07-04 20:36:48'),
(114, 1, 200, 0, 8, 9.6, NULL, NULL, NULL, 201.6, NULL, NULL, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-04 23:11:45', '2020-07-04 23:11:45'),
(122, 4, 700, 500, 36, 60, 'tarjeta', 'local', 2, 1191, 'RebajasJunio', 35, 90, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 22:10:05', '2020-07-07 14:41:07'),
(123, 3, 200, 0, 6, 0, 'tarjeta', 'local', 2, 186, 'RebajasJunio', 10, 90, 27, '8ac7a4a173293e2e01732990d62e1b9e', '380110', '200707_000121', '00_02DC', '0', '0', 'DINERS', '360218', 'Luis Mera', '2020-07-07 13:55:25', '2020-07-07 14:36:21'),
(133, 4, 184.3, 0, 6, 0, 'tarjeta', 'domicilio', NULL, 184.3, 'RebajasJunio', 9.7, 90, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 19:12:45', '2020-07-07 19:32:18'),
(136, 4, 184.3, 73.72, 8.4, 8.8464, 'tarjeta', 'domicilio', 3, 187.3, 'RebajasJunio', 13.58, 90, 27, '8ac7a49f7329f6d601732ae7d858212e', '695198', '200707_000243', '00_02DC', '0', '0', 'DINERS', '360218', 'Luis Mera', '2020-07-07 20:07:51', '2020-07-07 20:12:10'),
(138, 1, 184.3, 73.72, 8.4, 8.8464, 'tarjeta', 'domicilio', NULL, 269.87, 'RebajasJunio', 13.58, 90, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 20:21:55', '2020-07-07 22:32:51'),
(139, 3, 18.43, 73.72, 3, 8.8464, 'tarjeta', 'domicilio', 3, 104, 'RebajasJunio', 4.85, 90, 27, '8ac7a4a07329ff2701732b6fb3c663de', '648591', '200707_000271', '00_02DC', '3', '0', 'DINERS', '360218', 'Luis Mera', '2020-07-07 22:38:58', '2020-07-07 22:40:31'),
(142, 4, 0, 155.2, 4.8, 18.62, 'tarjeta', 'local', 2, 175.82, NULL, NULL, 90, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 22:50:26', '2020-07-07 22:59:49'),
(143, 3, 72.75, 0, 2.25, 0, 'tarjeta', 'local', 2, 76.75, NULL, NULL, 90, 27, '8ac7a4a17329f6d901732b83f4d03ebd', '112700', '200707_000273', '00_02DC', '0', '0', 'DINERS', '360218', 'Luis Mera', '2020-07-07 23:01:36', '2020-07-07 23:02:18'),
(144, 4, 0, 77.6, 2.4, 9.31, 'tarjeta', 'local', 2, 88.91, NULL, NULL, 90, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 23:17:57', '2020-07-07 23:18:33'),
(145, 1, 0, 77.6, 2.4, 9.31, 'tarjeta', 'local', 2, 88.91, NULL, NULL, 79, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-15 16:56:02', '2020-07-15 16:56:09'),
(146, 1, 19.4, 0, 0.6, 0, 'tarjeta', 'local', 2, 21.4, NULL, NULL, 79, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-06 00:48:40', '2020-08-06 00:48:48'),
(147, 1, 0, 48.5, 1.5, 5.82, 'tarjeta', 'local', 2, 56.32, NULL, NULL, 79, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-06 00:49:30', '2020-08-06 00:49:38'),
(148, 3, 19.4, 0, 0.6, 0, 'tarjeta', 'local', 2, 23.4, NULL, NULL, 79, 33, '8ac7a49f73bdef620173c17ccfdb76f1', '285450', '200805_000131', '00_04BG', '0', '0', 'AMEX', '376651', 'Prueba', '2020-08-06 00:53:50', '2020-08-06 01:57:34'),
(149, 1, 19.4, 0, 0.6, 0, 'tarjeta', 'local', 2, 21.4, NULL, NULL, 79, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-07 15:44:47', '2020-08-07 15:45:16'),
(150, 3, 19.4, 0, 0.6, 0, 'tarjeta', 'local', 2, 21.4, NULL, NULL, 79, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-08 19:31:08', '2020-09-08 19:32:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prioridad`
--

CREATE TABLE `prioridad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `orden` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prioridad`
--

INSERT INTO `prioridad` (`id`, `nombre`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'Pagada', 1, NULL, NULL),
(2, 'Alta', 2, NULL, NULL),
(3, 'Media', 3, NULL, NULL),
(4, 'Baja', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_ruta`
--

CREATE TABLE `registro_ruta` (
  `id` int(11) NOT NULL,
  `documento` varchar(60) DEFAULT NULL,
  `identificacion` varchar(60) DEFAULT NULL,
  `nombres` varchar(120) DEFAULT NULL,
  `apellidos` varchar(120) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `nacionalidad` varchar(60) DEFAULT NULL,
  `tallacamiseta` varchar(30) DEFAULT NULL,
  `modalidad` varchar(60) DEFAULT NULL,
  `categoria` varchar(30) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `numero_real` varchar(6) DEFAULT NULL,
  `contacto` varchar(120) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `celular` varchar(11) DEFAULT NULL,
  `sintomas` varchar(10) DEFAULT NULL,
  `detalle` longtext DEFAULT NULL,
  `temperatura` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registro_ruta`
--

INSERT INTO `registro_ruta` (`id`, `documento`, `identificacion`, `nombres`, `apellidos`, `fechanacimiento`, `nacionalidad`, `tallacamiseta`, `modalidad`, `categoria`, `numero`, `numero_real`, `contacto`, `email`, `direccion`, `celular`, `sintomas`, `detalle`, `temperatura`, `created_at`, `updated_at`) VALUES
(26, 'Cédula', '1315526929', 'Erick', 'Delgado Vargas', '1996-05-06', 'Ecuatoriana', 'L', '5K', '18 a 29 años', 1, '000001', '0998071544', 'erick96dv@gmail.com', NULL, NULL, NULL, NULL, NULL, '2019-02-15 20:22:09', '2019-02-15 20:22:09'),
(27, 'Cédula', '1315526925', 'Erick', 'Delgado Vargas', '1982-05-06', 'Ecuatoriana', 'S', '5K', '30 a 39 años', 2, '00002', '0998071544', 'erick96dv@gmail.com', NULL, NULL, NULL, NULL, NULL, '2019-02-15 20:26:11', '2019-02-15 20:26:11'),
(28, 'Cédula', '1315584774', 'Carlos', 'Delgado Vargas', '2001-05-06', 'Ecuatoriana', 'M', '5K', '11 a 17 años', 3, '00003', '0998071544', 'erick96dv@gmail.com', NULL, NULL, NULL, NULL, NULL, '2019-02-15 20:28:50', '2019-02-15 20:28:50'),
(29, 'Cédula', '1315526555', 'Carlos E.', 'Manosalvas Garcia', '2002-01-21', 'Ecuatoriana', 'XL', '5K', '11 a 17 años', 4, '00004', '0998071544', 'erick96dv@gmail.com', NULL, NULL, NULL, NULL, NULL, '2019-02-15 20:29:34', '2019-02-15 20:29:34'),
(31, 'Cédula', '1315526933', 'Carlos', 'Delgado Vargas', '1995-05-06', 'Ecuatoriana', 'S', '2K', '18 a 29 años', NULL, NULL, '0998071544', 'edelgado@fixsystem.com.ec', NULL, NULL, NULL, NULL, NULL, '2019-02-15 20:34:31', '2019-02-15 20:34:31'),
(32, 'Cédula', '1315358232', 'Luis', 'Mera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0989516389', 'luismera1794@gmail.com', 'Parroquia Eloy Alfaro', '0989516389', 'no', NULL, NULL, '2020-05-17 18:41:49', '2020-05-17 18:41:49'),
(33, 'Cédula', '1315358235', 'Luis', 'Mera', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0989516389', 'luismera1794@gmail.com', 'Parroquia Eloy Alfaro', '0989516389', 'si', 'tos, fiebre', '39', '2020-05-17 18:42:26', '2020-05-17 18:42:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `enlace` varchar(380) DEFAULT NULL,
  `id_categoriainterna` int(11) NOT NULL,
  `path` mediumtext NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `mostrar` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `titulo`, `orden`, `enlace`, `id_categoriainterna`, `path`, `descripcion`, `updated_at`, `created_at`, `mostrar`, `tipo`) VALUES
(12, 'Amamos lo que Hacemos!', NULL, 'http://runachay.com.ec/', 1, '5b4ca7bd163af78bc771313c6ed7a576.jpeg', 'Enseñar es nuestro pasión, amamos lo que hacemos. Verlos crecer y ser parte de este proceso es gratificante.', '2021-05-03 20:35:46', '2020-11-19 16:26:04', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `path` mediumtext DEFAULT NULL,
  `descripcion` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `categoria_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas`
--

CREATE TABLE `tareas` (
  `id` int(11) NOT NULL,
  `respuesta` longtext DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_tarea` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tareas_cab`
--

CREATE TABLE `tareas_cab` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` longtext NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `respuesta` int(11) NOT NULL DEFAULT 0,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `abreviatura` varchar(90) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `preciodescuento` float DEFAULT NULL,
  `iva` tinyint(1) NOT NULL DEFAULT 0,
  `stock` int(11) DEFAULT NULL,
  `peso` varchar(45) DEFAULT NULL,
  `medidas` varchar(45) DEFAULT NULL,
  `presentacion` varchar(120) DEFAULT NULL,
  `temporada` int(11) DEFAULT NULL,
  `campostemporada` longtext DEFAULT NULL,
  `destacado` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `path` mediumtext DEFAULT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `id_categoriaproducto` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuarios`
--

CREATE TABLE `tipo_usuarios` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_usuarios`
--

INSERT INTO `tipo_usuarios` (`id`, `tipo`, `created_at`, `updated_at`) VALUES
(4, 'administrador', NULL, '2019-05-01 17:42:47'),
(5, 'Editor', NULL, '2019-05-01 17:42:55'),
(6, 'Profesor', '2020-08-04 10:40:27', '2020-08-04 10:40:27'),
(7, 'Estudiante', '2020-08-04 10:40:38', '2020-08-04 10:40:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `path` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `login` varchar(45) DEFAULT NULL,
  `ci` varchar(45) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` datetime DEFAULT NULL,
  `id_tipo_usuario` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `asunto` varchar(100) DEFAULT NULL,
  `actividad` varchar(100) DEFAULT NULL,
  `aceptado` int(11) DEFAULT NULL,
  `codigo` text DEFAULT NULL,
  `confirmado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `path`, `nombres`, `apellidos`, `email`, `telefono`, `login`, `ci`, `password`, `remember_token`, `fecha_nacimiento`, `id_tipo_usuario`, `created_at`, `updated_at`, `asunto`, `actividad`, `aceptado`, `codigo`, `confirmado`) VALUES
(15, 'Cb31tsVWwAAwn2h.jpg', 'Danna Palma', NULL, 'jonathanpalma25@gmail.com', '0998135412', 'danna', 'null', '$2y$10$b/nGxpIYYr6pqSBKx..7a.Vq1ghqriluIlQDkwhPdwC5bi5keef0S', 'VEH4G6vBSDAg7eJdt7vWV0miBEW4Dg8zQG1bqQjNERx4GKZWYojxQHZv0Rvb', '2016-04-11 00:00:00', 4, '2016-04-11 03:21:17', '2017-09-14 17:38:24', 'null', 'null', 1, NULL, 1),
(56, 'user.jpg', 'Desarrollo', 'Web', 'edelgado@fixsystem.com.ec', '0998071544', 'edelgado', NULL, '$2y$10$2czxTuYNIfcMNGGDhSaRXuGvOK4ziH7JMM5Ns7oMYXUr72F31duHC', 'ZudAga8vflPLnuHU9NPXeEiS6U9jdzMF680kOz2ZV4rEWlV291Kt48ZI8TgH', '1996-06-05 00:00:00', 4, '2018-07-23 16:19:36', '2021-03-18 17:28:01', NULL, NULL, NULL, NULL, NULL),
(58, 'pp.jpg', 'Luis', 'Mera', 'luismera1794@gmail.com', '989516389', 'admin', NULL, '$2y$10$G..UeSITyjaZp60/af7oY.4Db9Kx41epMTMTeJDp/Q.y.YUpf7WgW', NULL, '2020-05-01 00:00:00', 4, '2020-05-01 11:23:21', '2020-07-16 16:06:07', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_menu_permiso`
--

CREATE TABLE `usuario_menu_permiso` (
  `id` int(11) NOT NULL,
  `tipo_usuario_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `crear` int(11) NOT NULL,
  `editar` int(11) NOT NULL,
  `eliminar` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_menu_permiso`
--

INSERT INTO `usuario_menu_permiso` (`id`, `tipo_usuario_id`, `menu_id`, `crear`, `editar`, `eliminar`, `created_at`, `updated_at`) VALUES
(1, 5, 5, 0, 0, 0, '2020-05-01 16:43:54', '2020-05-01 16:43:54'),
(4, 5, 25, 0, 0, 0, '2020-05-01 16:44:07', '2020-05-01 16:44:07'),
(7, 5, 9, 0, 0, 0, '2020-05-01 16:54:19', '2020-05-01 16:54:19'),
(8, 5, 10, 1, 1, 1, '2020-05-01 16:54:19', '2020-05-01 16:54:56'),
(9, 5, 11, 1, 1, 1, '2020-05-01 16:54:19', '2020-05-01 16:54:56'),
(10, 5, 12, 1, 1, 1, '2020-05-01 16:54:19', '2020-05-01 16:54:56'),
(11, 5, 13, 1, 1, 1, '2020-05-01 16:54:19', '2020-05-01 16:54:56'),
(12, 5, 6, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(13, 5, 7, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(14, 5, 8, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(16, 5, 14, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(17, 5, 15, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(18, 5, 16, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(19, 5, 17, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(20, 5, 18, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(21, 5, 19, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(22, 5, 20, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(23, 5, 21, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(24, 5, 22, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(25, 5, 23, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(26, 5, 24, 0, 0, 0, '2020-05-01 18:04:57', '2020-05-01 18:04:57'),
(27, 5, 4, 1, 1, 1, '2020-05-01 18:04:57', '2020-05-01 18:32:35'),
(28, 6, 32, 0, 1, 0, '2020-08-04 11:20:06', '2020-08-04 11:49:06'),
(29, 7, 32, 0, 0, 0, '2020-08-04 12:03:57', '2020-08-04 12:03:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `id_categoriainterna` int(11) NOT NULL,
  `titulo` varchar(200) CHARACTER SET utf8 NOT NULL,
  `path` mediumtext CHARACTER SET utf8 NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `descripcion`, `orden`, `id_categoriainterna`, `titulo`, `path`, `updated_at`, `created_at`) VALUES
(27, 'wayne', NULL, 5, 'Wayne', 'https://www.youtube.com/watch?v=p84O3JAp_IM', '2019-04-25 14:54:44', '2019-04-25 14:54:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videosar`
--

CREATE TABLE `videosar` (
  `id` int(11) NOT NULL,
  `path` longtext NOT NULL,
  `orden` int(11) DEFAULT NULL,
  `titulo` longtext NOT NULL,
  `id_articulos` int(11) NOT NULL,
  `id_modulo` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas`
--

CREATE TABLE `visitas` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `ip` varchar(45) NOT NULL,
  `num` int(11) NOT NULL,
  `articulo_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitas_productos`
--

CREATE TABLE `visitas_productos` (
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `tienda_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `visitas_productos`
--

INSERT INTO `visitas_productos` (`id`, `fecha`, `ip`, `num`, `tienda_id`, `created_at`, `updated_at`) VALUES
(1, '2019-06-20', '::1', 1, 14, '2019-06-20 10:59:19', '2019-06-20 10:59:19'),
(2, '2019-06-20', '::1', 1, 27, '2019-06-20 11:00:09', '2019-06-20 11:00:09'),
(3, '2019-06-24', '::1', 1, 16, '2019-06-24 10:23:57', '2019-06-24 10:23:57'),
(4, '2019-06-24', '::1', 1, 21, '2019-06-24 10:58:15', '2019-06-24 10:58:15'),
(5, '2019-06-24', '::1', 1, 14, '2019-06-24 11:03:37', '2019-06-24 11:03:37'),
(6, '2019-06-24', '::1', 1, 27, '2019-06-24 12:52:17', '2019-06-24 12:52:17'),
(7, '2019-06-24', '::1', 1, 15, '2019-06-24 15:40:47', '2019-06-24 15:40:47'),
(8, '2019-06-25', '::1', 3, 54, '2019-06-25 12:47:26', '2019-06-25 12:47:26'),
(9, '2019-06-25', '::1', 1, 22, '2019-06-25 13:58:32', '2019-06-25 13:58:32'),
(10, '2019-06-25', '::1', 1, 28, '2019-06-25 13:58:37', '2019-06-25 13:58:37'),
(11, '2019-06-25', '::1', 1, 23, '2019-06-25 13:59:14', '2019-06-25 13:59:14'),
(12, '2019-06-25', '::1', 1, 14, '2019-06-25 13:59:19', '2019-06-25 13:59:19'),
(13, '2019-06-25', '::1', 1, 16, '2019-06-25 13:59:41', '2019-06-25 13:59:41'),
(14, '2019-06-25', '::1', 11, 53, '2019-06-25 14:00:34', '2019-06-25 14:00:34'),
(15, '2019-06-25', '::1', 1, 27, '2019-06-25 14:01:03', '2019-06-25 14:01:03'),
(16, '2019-06-25', '::1', 1, 17, '2019-06-25 14:10:57', '2019-06-25 14:10:57'),
(17, '2019-06-25', '::1', 1, 30, '2019-06-25 14:11:38', '2019-06-25 14:11:38'),
(18, '2019-06-25', '::1', 1, 20, '2019-06-25 14:11:42', '2019-06-25 14:11:42'),
(19, '2019-06-25', '::1', 1, 32, '2019-06-25 14:13:13', '2019-06-25 14:13:13'),
(20, '2019-06-25', '::1', 1, 26, '2019-06-25 14:13:17', '2019-06-25 14:13:17'),
(21, '2019-06-25', '::1', 9, 52, '2019-06-25 14:17:08', '2019-06-25 14:17:08'),
(22, '2019-06-25', '::1', 1, 25, '2019-06-25 14:17:42', '2019-06-25 14:17:42'),
(23, '2019-06-27', '::1', 7, 51, '2019-06-27 17:17:48', '2019-06-27 17:17:48'),
(24, '2019-07-03', '181.198.196.24', 1, 14, '2019-07-03 16:04:36', '2019-07-03 16:04:36'),
(25, '2019-07-03', '181.198.196.24', 1, 15, '2019-07-03 16:04:46', '2019-07-03 16:04:46'),
(26, '2019-08-05', '190.63.222.137', 1, 15, '2019-08-05 15:34:43', '2019-08-05 15:34:43'),
(27, '2019-08-05', '190.63.222.137', 1, 14, '2019-08-05 15:35:03', '2019-08-05 15:35:03'),
(28, '2019-08-05', '181.198.196.24', 4, 50, '2019-08-05 15:49:46', '2019-08-05 15:49:46'),
(29, '2019-08-05', '181.188.201.194', 1, 14, '2019-08-05 16:19:04', '2019-08-05 16:19:04'),
(30, '2019-08-05', '181.188.201.194', 1, 26, '2019-08-05 16:19:27', '2019-08-05 16:19:27'),
(31, '2019-11-06', '::1', 1, 16, '2019-11-06 12:07:52', '2019-11-06 12:07:52'),
(32, '2019-11-29', '::1', 1, 19, '2019-11-29 08:58:11', '2019-11-29 08:58:11'),
(53, '2020-07-20', '::1', NULL, NULL, '2020-07-20 14:24:22', '2020-07-20 14:24:22'),
(54, '2020-07-20', '::1', NULL, NULL, '2020-07-20 14:25:06', '2020-07-20 14:25:06'),
(55, '2020-07-20', '::1', NULL, NULL, '2020-07-20 14:25:29', '2020-07-20 14:25:29'),
(56, '2020-07-20', '::1', NULL, 51, '2020-07-20 14:28:23', '2020-07-20 14:28:23'),
(57, '2020-07-20', '::1', NULL, 51, '2020-07-20 14:28:26', '2020-07-20 14:28:26'),
(58, '2020-07-20', '::1', NULL, 51, '2020-07-20 14:28:27', '2020-07-20 14:28:27'),
(59, '2020-07-20', '::1', NULL, 51, '2020-07-20 14:28:28', '2020-07-20 14:28:28'),
(60, '2020-07-22', '::1', NULL, 51, '2020-07-22 18:07:11', '2020-07-22 18:07:11'),
(61, '2020-07-24', '::1', NULL, 51, '2020-07-24 15:11:55', '2020-07-24 15:11:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vistacampos`
--

CREATE TABLE `vistacampos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vistacampos`
--

INSERT INTO `vistacampos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'iconos grandes', NULL, NULL),
(2, 'iconos pequeños', NULL, NULL),
(3, 'iconos pequeños cantidad', NULL, NULL),
(4, 'iconos pequeños detalles', NULL, NULL),
(5, 'mapa', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `id_cliente` int(10) NOT NULL,
  `id_producto` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adjuntos`
--
ALTER TABLE `adjuntos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_articulos_usuario1_idx` (`id_usuario`),
  ADD KEY `fk_articulos_categoria1_idx` (`id_categoria`);

--
-- Indices de la tabla `articulo_tag`
--
ALTER TABLE `articulo_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tagarticulo_articulos1_idx` (`articulo_id`),
  ADD KEY `fk_articulo_tag_tags1_idx` (`tag_id`);

--
-- Indices de la tabla `camposextras`
--
ALTER TABLE `camposextras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_camposextras_camposextrasgrupo1_idx` (`id_campos_extras_grupo`);

--
-- Indices de la tabla `camposextrasgrupo`
--
ALTER TABLE `camposextrasgrupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_camposextrasgrupo_vistacampos1_idx` (`vistacampos_id`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoriacamposextrasgrupo`
--
ALTER TABLE `categoriacamposextrasgrupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categoriacamposextrasgrupo_categorias1_idx` (`id_categoria`),
  ADD KEY `fk_categoriacamposextrasgrupo_camposextrasgrupo1_idx` (`id_campos_extras_grupo`);

--
-- Indices de la tabla `categoriainterna`
--
ALTER TABLE `categoriainterna`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoriaproductos`
--
ALTER TABLE `categoriaproductos`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clave` (`clave`);

--
-- Indices de la tabla `cupones`
--
ALTER TABLE `cupones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `depositos`
--
ALTER TABLE `depositos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `especificaciones`
--
ALTER TABLE `especificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articulo_id` (`articulo_id`);

--
-- Indices de la tabla `galeriaarticulo`
--
ALTER TABLE `galeriaarticulo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_galeriaarticulo_articulos1_idx` (`id_articulo`),
  ADD KEY `fk_galeriaarticulo_galeriacabecera1_idx` (`id_galeria_cabecera`);

--
-- Indices de la tabla `galeriacabecera`
--
ALTER TABLE `galeriacabecera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeriadetalle`
--
ALTER TABLE `galeriadetalle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_galeriadetalle_galeriacabecera1_idx` (`id_galeria_cabecera`);

--
-- Indices de la tabla `galeriaproducto`
--
ALTER TABLE `galeriaproducto`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_producto` (`id_producto`) USING BTREE;

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informacion`
--
ALTER TABLE `informacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu_pagina`
--
ALTER TABLE `menu_pagina`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_articulo` (`id_articulo`);

--
-- Indices de la tabla `oauth_identities`
--
ALTER TABLE `oauth_identities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prioridad`
--
ALTER TABLE `prioridad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tareas`
--
ALTER TABLE `tareas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tareas_cab`
--
ALTER TABLE `tareas_cab`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tarea_art` (`id_articulo`),
  ADD KEY `fk_tarea_mod` (`id_modulo`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_productos_usuario1_idx` (`id_usuario`) USING BTREE,
  ADD KEY `fk_productos_categoriaproductos1_idx` (`id_categoriaproducto`);

--
-- Indices de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuario_tipo_usuario_idx` (`id_tipo_usuario`);

--
-- Indices de la tabla `usuario_menu_permiso`
--
ALTER TABLE `usuario_menu_permiso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_menu_permiso` (`menu_id`),
  ADD KEY `fk_tipo_permiso` (`tipo_usuario_id`) USING BTREE;

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videosar`
--
ALTER TABLE `videosar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitas`
--
ALTER TABLE `visitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_visitas_articulos1_idx` (`articulo_id`);

--
-- Indices de la tabla `visitas_productos`
--
ALTER TABLE `visitas_productos`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_visitas_productos_productos1_idx` (`tienda_id`) USING BTREE;

--
-- Indices de la tabla `vistacampos`
--
ALTER TABLE `vistacampos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adjuntos`
--
ALTER TABLE `adjuntos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `articulo_tag`
--
ALTER TABLE `articulo_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `camposextras`
--
ALTER TABLE `camposextras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `camposextrasgrupo`
--
ALTER TABLE `camposextrasgrupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `categoriacamposextrasgrupo`
--
ALTER TABLE `categoriacamposextrasgrupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `categoriainterna`
--
ALTER TABLE `categoriainterna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categoriaproductos`
--
ALTER TABLE `categoriaproductos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cupones`
--
ALTER TABLE `cupones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `depositos`
--
ALTER TABLE `depositos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `especificaciones`
--
ALTER TABLE `especificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `fotos`
--
ALTER TABLE `fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=708;

--
-- AUTO_INCREMENT de la tabla `galeriaproducto`
--
ALTER TABLE `galeriaproducto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `informacion`
--
ALTER TABLE `informacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `menu_pagina`
--
ALTER TABLE `menu_pagina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `oauth_identities`
--
ALTER TABLE `oauth_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT de la tabla `prioridad`
--
ALTER TABLE `prioridad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tareas`
--
ALTER TABLE `tareas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tareas_cab`
--
ALTER TABLE `tareas_cab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `usuario_menu_permiso`
--
ALTER TABLE `usuario_menu_permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `videosar`
--
ALTER TABLE `videosar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `visitas`
--
ALTER TABLE `visitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visitas_productos`
--
ALTER TABLE `visitas_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `vistacampos`
--
ALTER TABLE `vistacampos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD CONSTRAINT `fk_articulos_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_articulos_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `camposextras`
--
ALTER TABLE `camposextras`
  ADD CONSTRAINT `fk_camposextras_camposextrasgrupo1` FOREIGN KEY (`id_campos_extras_grupo`) REFERENCES `camposextrasgrupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD CONSTRAINT `fk_modulo_curso` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`);

--
-- Filtros para la tabla `tareas_cab`
--
ALTER TABLE `tareas_cab`
  ADD CONSTRAINT `fk_tarea_art` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`),
  ADD CONSTRAINT `fk_tarea_mod` FOREIGN KEY (`id_modulo`) REFERENCES `modulos` (`id`);

--
-- Filtros para la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD CONSTRAINT `fk_productos_categoriaproductos1` FOREIGN KEY (`id_categoriaproducto`) REFERENCES `categoriaproductos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_productos_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
